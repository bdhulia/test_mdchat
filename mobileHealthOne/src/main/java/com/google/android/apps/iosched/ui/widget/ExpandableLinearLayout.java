package com.google.android.apps.iosched.ui.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mobile.health.one.R;

public class ExpandableLinearLayout extends LinearLayout {

	private int ID_CHILD = 101;
	private ImageView expandeLayoutArrow;
	private boolean isExpanded = false;
	private final LayoutInflater inflater;
	private TextView expandeLayoutTitle;
	private RelativeLayout mainLayout;

	public ExpandableLinearLayout(final Context context) {
		super(context);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public ExpandableLinearLayout(final Context context, final AttributeSet attrs) {
		super(context, attrs);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public ExpandableLinearLayout(final Context context, final AttributeSet attrs,
			final int defStyle) {
		super(context, attrs, defStyle);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void setTitle(final CharSequence title) {
		if (expandeLayoutTitle != null) {
			expandeLayoutTitle.setText(title);
		}
	}

	public void setTitle(final int titleRes) {
		if (expandeLayoutTitle != null) {
			expandeLayoutTitle.setText(titleRes);
		}
	}

	public void init() {
		setBackgroundResource(R.drawable.round_corners_black);
		setPadding(10, 10, 10, 10);
		mainLayout = (RelativeLayout) inflater.inflate(R.layout.expandable_layout, this, false);
		expandeLayoutArrow = (ImageView) mainLayout.findViewById(R.id.expandLayoutArrow);
		expandeLayoutTitle = (TextView) mainLayout.findViewById(R.id.expandLayoutTitle);
		this.setOnClickListener(new OnExpandViewClickListener());
		this.addView(mainLayout, 0);
	}

	@Override
	public void addView(final View child) {
		super.addView(child);
		if (!child.equals(mainLayout)) {
			child.setTag(ID_CHILD++);
		}
	}

	private void setExpanded(final View v, final boolean isExpanded) {
		this.isExpanded = isExpanded;
		final ViewGroup group = (ViewGroup) v;
		for (int i = 1; i < ID_CHILD; i++) {
			final View collapseView = group.findViewWithTag(i);
			if (collapseView != null)
				collapseView.setVisibility(isExpanded ? View.VISIBLE: View.GONE);
		}

		if (this.isExpanded) {
			expandeLayoutArrow
					.setImageResource(R.drawable.profile_section_arrow_up);
		} else {
			expandeLayoutArrow
					.setImageResource(R.drawable.profile_section_arrow_down);
		}
	}

	private class OnExpandViewClickListener implements OnClickListener {

		@Override
		public void onClick(final View v) {
			setExpanded(v, !isExpanded);
		}
	}
}
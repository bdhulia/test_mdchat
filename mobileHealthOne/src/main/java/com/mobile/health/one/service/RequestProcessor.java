package com.mobile.health.one.service;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.message.BasicHeader;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.util.Log;

import com.google.gson.Gson;
import com.google.resting.Resting;
import com.google.resting.component.EncodingTypes;
import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.google.resting.component.impl.ServiceResponse;
import com.google.resting.method.post.PostHelper;
import com.mobile.health.one.app.LoginActivity;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.SettingsManager;

/**
 * @author Denis Migol
 */
public abstract class RequestProcessor {

	public static final String EXTRA_ERROR_STRING = "error_str";
	public static final String EXTRA_RESPONSE	 	= "responce";
	public static final int STATUS_SUCCESS 		= 0; // Success
	public static final int ERROR_GENERAL 		= 1; // General response error
	public static final int ERROR_VALIDATION 		= 4; // Input validation error
	public static final int ERROR_AUTH 			= 5; // Authentication error
	public static final int ERROR_NEED_PIN_VERIF 	= 6; // Needs pin verification
	protected static final String TAG = RequestProcessor.class.getCanonicalName();


	protected String serviceName = null;
	private ProcessorStatusListener listener;
	private final Context context;
	private final SettingsManager settings;
	private boolean tryRequestCalled = false;
	private static volatile Header mCookieHeader;

	/**
	 * @return the context
	 */
	public Context getContext() {
		return context;
	}

	protected RequestProcessor(final Context context) {
		this(context, null);
	}

	protected RequestProcessor(final Context context, final String serviceName) {
		this.serviceName = serviceName;
		this.context = context;
		settings = new SettingsManager(context);
	}

	protected SettingsManager getSettings() {
		return settings;
	}

	/**
	 * Process the request to remote URI
	 *
	 * @param receiver
	 * @param resolver
	 *            target ContentResolver for storing data
	 * @throws IOException
	 * @throws HttpException
	 * @throws Exception
	 */
	public void process(final ResultReceiver receiver, final ContentResolver resolver, final Bundle extras) throws IOException, HttpException, Exception {
		try {
			processInternal(receiver, resolver, extras);
			if (!tryRequestCalled) {
				throw new InvalidAlgorithmParameterException("tryRequest() should be called");
			}
		} catch (final Exception e) {
			throw e;
		}
	}

	protected abstract void processInternal(ResultReceiver receiver, ContentResolver resolver, final Bundle extras) throws IOException, HttpException,
	Exception;

	/**
	 * Gets Resting JSON response from the specific URI
	 *
	 * @param uri
	 *            URI (path) or full URL
	 * @return
	 * @throws IOException
	 * @throws HttpException
	 */
	protected <E extends Object> List<E> getResponce(final String uri, final RequestParams params, final String jsonAlias) {
		final Class<E> clz = null;
		//Get the list of Product objects by passing "product" as alias
		int port = 80;
		if (uri.startsWith("https")) {
			port = 443;
		}
		return Resting.getByJSON(uri, port, params, clz, jsonAlias);
	}

	/**
	 * Gets Resting general response from the specific URI
	 *
	 * @param uri
	 *            URI (path) or full URL
	 * @return
	 * @throws IOException
	 * @throws HttpException
	 */
	protected ServiceResponse getResponce(final String uri, final RequestParams params) {
		int port = 80;
		if (uri.startsWith("https")) {
			port = 443;
		}
		return Resting.get(uri, port, params);
	}

	/**
	 * Gets Resting general response from the specific URI using POST
	 *
	 * @param uri
	 *            URI (path) or full URL
	 * @return
	 * @throws IOException
	 * @throws HttpException
	 */
	protected ServiceResponse postResponce(final String uri, final RequestParams params) {
		int port = 80;
		if (uri.startsWith("https")) {
			port = 443;
		}
		final ArrayList<Header> headers = new ArrayList<Header>(1);
		headers.add(getCookieHeader());
		return PostHelper.post(uri, port, EncodingTypes.UTF8, params, headers);
	}

	protected void saveCookies(final ServiceResponse responce) {
		Header foundHeader = null;
		for (final Header header : responce.getResponseHeaders()) {
			if ("Set-Cookie".equals(header.getName())) {
				foundHeader = header;
			}
		}
		if (foundHeader != null)
			RequestProcessor.mCookieHeader = foundHeader;
	}

	public static Header getCookieHeader() {
		if (mCookieHeader != null)
			return new BasicHeader("Cookie", mCookieHeader.getValue());
		else
			return null;
	}

	protected <E extends IResponce & Parcelable> E tryRequest(final ResultReceiver receiver, final boolean methodGet, RequestParams params, final Class<E> responce, Bundle extras) {
		if (params == null) {
			params = new BasicRequestParams();
		}
		if (extras == null) {
			extras = new Bundle();
		}
		tryRequestCalled = true;

		ServiceResponse in;
		receiver.send(SyncService.STATUS_RUNNING, null);
		Log.i(TAG, "Status running sended");

		try {
			if (methodGet) {
				in = getResponce(getUri().toString(), params);
			} else {
				in = postResponce(getUri().toString(), params);
			}
			if (in != null && in.getStatusCode() == 200) {
				Log.d(this.getClass().getSimpleName(), in.getResponseString());
				saveCookies(in);
				final Gson gson = new Gson();
				final E resp = gson.fromJson(in.getContentData().getContentInString(), responce);

				extras.putParcelable(EXTRA_RESPONSE, resp);
				if (STATUS_SUCCESS == resp.getCode()) {
					Log.d(TAG, "STATUS_FINISHED");
					receiver.send(SyncService.STATUS_FINISHED, prepareExtrasFromResult(resp, extras));
					Log.i(TAG, "Status finish sended");
					return resp;
				} else {
					Log.d(TAG, "STATUS_ERROR");
					//this.settings.removeSessionId();
					//this.settings.removePin();
					extras.putString(EXTRA_ERROR_STRING,
							prepareErrorFromResult(resp));
					receiver.send(SyncService.STATUS_ERROR, extras);
					//launchLogin();
					// TODO : auto log out
					Log.i(TAG, "Status error sended");
				}

			} else {
				extras.putString(EXTRA_ERROR_STRING, "Connection error");
				receiver.send(SyncService.STATUS_ERROR, extras);
				Log.i(TAG, "Status error sended");
			}
		} catch (final Exception e) {
			extras.putString(EXTRA_ERROR_STRING, e.getLocalizedMessage());
			receiver.send(SyncService.STATUS_ERROR, extras);
			Log.i(TAG, "Status error sended");
			e.printStackTrace();
		}

		return null;

	}

	@SuppressWarnings("unused")
	private void launchLogin() {
		final Intent login = new Intent(getContext(), LoginActivity.class);
		login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
		//login.setAction(Intent.ACTION_MAIN);
		//login.addCategory(Intent.CATEGORY_LAUNCHER);
		context.startActivity(login);
	}

	protected abstract <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(final E result, final Bundle extras);
	protected abstract <E extends IResponce & Parcelable> String prepareErrorFromResult(final E result);

	/**
	 * @return the listener
	 */
	public ProcessorStatusListener getListener() {
		return listener;
	}

	/**
	 * @param listener
	 *            the listener to set
	 */
	public void setListener(final ProcessorStatusListener listener) {
		this.listener = listener;
	}

	/**
	 * Every request processor should know its URL for getting data
	 * or process by itself
	 *
	 * @return
	 */
	public abstract Uri getUri();

	/**
	 * Interface to monitor status changes of processing
	 *
	 * @author Igor Yanishevskiy
	 */
	public interface ProcessorStatusListener {
		public void onStartProcessing(String uri);

		public void onEndProcessing(String uri);

		public void onErrorProcessing(String uri, Exception e);
	}
}

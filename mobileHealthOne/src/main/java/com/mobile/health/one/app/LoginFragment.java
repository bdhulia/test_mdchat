package com.mobile.health.one.app;

import android.app.Activity;
import android.os.Bundle;
import android.util.Patterns;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobile.health.one.R;
import com.mobile.health.one.app.ViewHolder.Validator;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.service.worker.LoginProcessor;
import com.mobile.health.one.util.DateFormatUtil;
import com.mobile.health.one.util.SettingsManager;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class LoginFragment extends SherlockFragment implements OnClickListener {

	public interface LoginCallback {
		public void onLogin(final boolean needPin);
	}

	protected class LoginReceiver implements Receiver {
		public LoginReceiver() {
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				setProgress(true);
				break;
			case SyncService.STATUS_FINISHED:
				if (resultData != null) {
					final String sessionId = resultData
							.getString(LoginProcessor.EXTRA_TOKEN);
                    final String email = resultData.getString(LoginProcessor.EXTRA_LOGIN);
					settingsManager.setSessionId(sessionId);
					callback.onLogin(resultData.getInt(
							LoginProcessor.EXTRA_NEED_PIN, 0) == 1 ? true
									: false);
                    settingsManager.setLastLoginEmail(email);
                    settingsManager.setLastLoginTime(System.currentTimeMillis());
				}
				setProgress(false);
				break;
			case SyncService.STATUS_ERROR:
				if (resultData != null) {
					final Responce resp = resultData
							.getParcelable(RequestProcessor.EXTRA_RESPONSE);
					if (resp != null) {
						for (final String tag : resp.getInvalidParams()) {
							final TextView view = (TextView) getView()
									.findViewWithTag(tag);
							view.setError(resp.getMessages());
							view.requestFocus();
						}
					} else {
						ToastUtil.showError(getActivity(), resultData.getString(RequestProcessor.EXTRA_ERROR_STRING));
					}
				}
				setProgress(false);
				break;
			}
		}

	}

	private final SparseArray<ViewHolder> views = new SparseArray<ViewHolder>();

	private CharSequence oldTitle;

	private LoginCallback callback;

	private SettingsManager settingsManager;

	private Button loginBtn;

	private ProgressBar progress;

	private boolean syncStated;

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		final ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.fragment_login, container, false);
		settingsManager = new SettingsManager(getActivity());
		final int PASS_COUNT = getResources().getInteger(R.integer.max_pass_chars);

		final ViewHolder.Validator validator = new Validator() {

			@Override
			public ValidationResult onValidate(final ViewHolder vh,
					final CharSequence s) {
				return new ValidationResult();
			}

			@Override
			public ValidationResult onLoseFocus(final ViewHolder vh,
					final CharSequence s) {
				final int length = s.length();
				final int viewId = vh.view.getId();
				boolean match = false;
				CharSequence error = null;

				switch (viewId) {
				case R.id.login: {
					if (Patterns.EMAIL_ADDRESS.matcher(s).matches()) {
						match = true;
					} else {
						error = getText(R.string.error_email_invalid);
					}
					break;
				}
				case R.id.pass:
					if (length > 0 && length < PASS_COUNT) {
						error = getText(R.string.error_pass_not_match);
					} else if (length == 0) {
						error = getText(R.string.error_pass_empty);
					} else {
						match = true;
					}
				}

				return new ValidationResult(match, error);
			}

		};

		final EditText loginView = (EditText) view.findViewById(R.id.login);
		loginView.setTag(LoginProcessor.EXTRA_LOGIN);
		views.append(R.id.login, new ViewHolder(loginView, validator, false));

		final EditText passView = (EditText) view.findViewById(R.id.pass);
		passView.setTag(LoginProcessor.EXTRA_PASS);
		views.append(R.id.pass, new ViewHolder(passView, validator, false));

		loginBtn = (Button) view.findViewById(R.id.btn_login);
		loginBtn.setOnClickListener(this);

		progress = (ProgressBar) view.findViewById(android.R.id.progress);

		return view;
	}

	public void setProgress(final boolean b) {
		loginBtn.setEnabled(!b);
		progress.setVisibility(b ? View.VISIBLE : View.INVISIBLE);
		syncStated = b;
	}

	@Override
	public void onClick(final View v) {
		if (v.getId() == R.id.btn_login) {
			final ViewHolder login = views.get(R.id.login);
			final ViewHolder pass = views.get(R.id.pass);
			if (!syncStated && login.isValid() && pass.isValid()) {

				final Bundle extras = new Bundle();
				extras.putString(LoginProcessor.EXTRA_LOGIN, login.view
						.getText().toString());
				extras.putString(LoginProcessor.EXTRA_PASS, pass.view.getText()
						.toString());
				SyncService.startSyncService(getActivity(),
						AppSyncService.LOGIN_REQUEST, new LoginReceiver(),
						extras);

			} else {
				// if they are not valid, set focus to them
				if (!login.isValid()) {
					login.view.requestFocus();
				} else if (!pass.isValid()) {
					pass.view.requestFocus();
				}
			}
		}

	}

	@Override
	public void onAttach(final Activity activity) {
		activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		super.onAttach(activity);
		oldTitle = activity.getTitle();
		activity.setTitle(R.string.title_activity_main);
		if (activity instanceof LoginCallback) {
			callback = (LoginCallback) activity;
		} else {
			throw new ClassCastException(
					"Parent activity should implement callbacks");
		}

	}

	@Override
	public void onDetach() {
		super.onDetach();
		getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

		getActivity().setTitle(oldTitle);
		SyncService.stopSyncService(getActivity());
	}

}

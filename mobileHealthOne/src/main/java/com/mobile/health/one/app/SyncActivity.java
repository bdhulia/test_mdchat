package com.mobile.health.one.app;

import android.os.Bundle;
import android.view.View;

import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.util.NotifyingAsyncQueryHandler;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.mobile.health.one.util.NotifyingAsyncQueryHandler.AsyncQueryListener;

/**
 * Base class for activities, which communicate with IntentService
 *
 * @author Denis Migol
 */
public abstract class SyncActivity extends AppFragmentActivity implements AsyncQueryListener, Receiver {

	private View progressView;
	protected NotifyingAsyncQueryHandler handler;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		handler = new NotifyingAsyncQueryHandler(getContentResolver(), this);
	}

	@Override
	public void setContentView(final int layoutResID) {
		super.setContentView(layoutResID);
		progressView = findViewById(android.R.id.progress);
	}

	/**
	 * call startSyncService(uri, serviceRequest, this, null, false)
	 *
	 * @param uri
	 * @param serviceRequest
	 */
	protected void startSyncService(final String serviceRequest) {
		startSyncService(serviceRequest, this, null, false);
	}

	/**
	 * call startSyncService(uri, serviceRequest, this, extras, false)
	 *
	 * @param uri
	 * @param serviceRequest
	 * @param extras
	 */
	protected void startSyncService(final String serviceRequest, final Bundle extras) {
		startSyncService(serviceRequest, this, extras, false);
	}

	/**
	 * call startSyncService(uri, serviceRequest, this, null, startQuery)
	 *
	 * @param uri
	 * @param serviceRequest
	 * @param startQuery
	 */
	protected void startSyncService(final String serviceRequest, final boolean startQuery) {
		startSyncService(serviceRequest, this, null, startQuery);
	}

	/**
	 * call startSyncService(uri, serviceRequest, receiver, null, startQuery)
	 *
	 * @param uri
	 * @param serviceRequest
	 * @param receiver
	 * @param startQuery
	 */
	protected void startSyncService(final String serviceRequest, final Receiver receiver,
			final boolean startQuery) {
		startSyncService(serviceRequest, receiver, null, startQuery);
	}

	/**
	 * Starts SyncService
	 *
	 * @param uri
	 * @param serviceRequest
	 * @param receiver
	 * @param extras
	 * @param startQuery
	 */
	protected void startSyncService(final String serviceRequest, final Receiver receiver,
			final Bundle extras, final boolean startQuery) {
		SyncService.startSyncService(this, serviceRequest, receiver, extras);

		if (startQuery) {
			startQuery();
		}
	}

	/**
	 * An example receiver class
	 */
	protected static class SimpleReceiver implements Receiver {
		public SimpleReceiver() {
		}

		@Override
		public void onReceiveResult(final int resultCode, final Bundle resultData) {
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				break;
			case SyncService.STATUS_FINISHED:
				break;
			case SyncService.STATUS_ERROR:
				break;
			}
		}

	}

	@Override
	public void onReceiveResult(final int resultCode, final Bundle resultData) {
		switch (resultCode) {
		case SyncService.STATUS_RUNNING:
			showProgress();
			break;
		case SyncService.STATUS_FINISHED:
			hideProgress();
			startQuery();
			break;
		case SyncService.STATUS_ERROR:
			hideProgress();
			// AlertDialogUtil.errorDialog(this,
			// resultData.getString(SyncService.EXTRA_ERROR_MESSAGE)).show();
			break;
		}
	}

	protected void showProgress() {
		if (progressView != null) {
			progressView.setVisibility(View.VISIBLE);
		}
	}

	protected void hideProgress() {
		if (progressView != null) {
			progressView.setVisibility(View.GONE);
		}
	}

	/**
	 * Starts asynchronous query to DB
	 */
	protected abstract void startQuery();
}

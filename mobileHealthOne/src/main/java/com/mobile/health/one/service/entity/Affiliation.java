package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Affiliation {
	public static class Entity implements Parcelable {
		@SerializedName("hospital_name")
		public String hospitalName;
		@SerializedName("department")
		public String department;
		@SerializedName("street_1")
		public String street1;
		@SerializedName("street_2")
		public String street2;
		@SerializedName("state")
		public String state;
		@SerializedName("city")
		public String city;
		@SerializedName("zip_code")
		public String zipCode;

		public Entity(final Parcel in) {
			hospitalName = in.readString();
			department = in.readString();
			street1 = in.readString();
			street2 = in.readString();
			state = in.readString();
			city = in.readString();
			zipCode = in.readString();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(hospitalName);
			dest.writeString(department);
			dest.writeString(street1);
			dest.writeString(street2);
			dest.writeString(state);
			dest.writeString(city);
			dest.writeString(zipCode);
		}

		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};
	}
}
package com.mobile.health.one.service.worker;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;
import android.util.Log;

import com.google.gson.Gson;
import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.google.resting.component.impl.ServiceResponse;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.SuggestResponce;
import com.mobile.health.one.util.SettingsManager;

/**
 *
 * This class is not comparable with SyncService
 *
 * @author Igor Yanishevskiy
 */
public abstract class AutocompleteSuggestProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_QUERY = "query";
	public static final String EXTRA_TYPE = "type";

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	private final SettingsManager settings;

	public AutocompleteSuggestProcessor(final Context context) {
		super(context);
		settings = new SettingsManager(context);
	}

	public SuggestResponce doRequest(final Bundle extras) {

		if (!settings.hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}
		final String token = settings.getSessionId();

		final RequestParams params = new BasicRequestParams();

		params.add(EXTRA_TOKEN, token);
		Log.i(TAG, "::processInternal:" + " token " + token);
		params.add(EXTRA_QUERY, extras.getCharSequence(EXTRA_QUERY).toString());
		params.add(EXTRA_DEVICE_ID, androidId);

		final ServiceResponse in = postResponce(getUri().toString(), params);
		if (in != null && in.getStatusCode() == 200) {
			saveCookies(in);
			final Gson gson = new Gson();
			final SuggestResponce resp = gson.fromJson(in.getContentData().getContentInString(), SuggestResponce.class);
			return resp;
		} else {
			return null;
		}
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(final E result,
			final Bundle extras) {
		// nothing to do here, we don't receive any data, just status
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(final E result) {
		return result.getMessages();
	}

	@Override
	protected void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) throws IOException,
			HttpException, Exception {
		// stub

	}

}

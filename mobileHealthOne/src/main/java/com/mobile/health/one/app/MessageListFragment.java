package com.mobile.health.one.app;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MergeCursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.webkit.URLUtil;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.db.entity.Message;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.NodeWorkerService.IMessageReceiver;
import com.mobile.health.one.service.NodeWorkerService.IThreadsReceiver;
import com.mobile.health.one.service.entity.MessageThread;
import com.mobile.health.one.util.DateFormatUtil;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.SortCursor;
import com.mobile.health.one.widget.TopBarLabelsView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MessageListFragment extends SherlockListFragment implements
		IMessageReceiver, IThreadsReceiver, OnScrollListener, NodeServiceConnector {

	private static final String EXTRA_CUR_CHOICE = "curChoice";

	private static final int ITEMS_PER_PAGE = 10;

	private boolean dualPane;
	private int curCheckPosition = 0;

	// This is the Adapter being used to display the list's data.
	private SimpleCursorAdapter mAdapter;

	public boolean syncStarted;

	private NodeWorkerService mService;

	private Handler mHandler;

	private String mLabelId;

	private String mLabelText;

	private Cursor mOldCursor;

	final SimpleDateFormat mDateFormat = new SimpleDateFormat(
			"yyyy-MM-dd kk:mm:ss z", Locale.getDefault());

	private int mPrevTotalItemCount;

	private boolean mIsSearch = false;

	private TopBarLabelsView mTopBarLabelsView;

	private boolean fromDashboard;

	public static MessageListFragment newInstance(final boolean fromDashboard) {
		final Bundle b = new Bundle();
		b.putBoolean(SearchParams.AliasVariant.IntentBuilder.EXTRA_FROM_DASHBOARD, fromDashboard);
		final MessageListFragment f = new MessageListFragment();
		f.setArguments(b);
		return f;
	}

	@Override
	public void onScrollStateChanged(final AbsListView view,
			final int scrollState) {
	}

	@Override
	public void onScroll(final AbsListView view, final int firstVisibleItem,
			final int visibleItemCount, final int totalItemCount) {
		final SimpleCursorAdapter adapter = (SimpleCursorAdapter) view.getAdapter();
		if (adapter != null) {
			// maybe will mark threads read here. then...
			if (firstVisibleItem + visibleItemCount >= totalItemCount
					&& totalItemCount != mPrevTotalItemCount && !isSearch()) {
				mPrevTotalItemCount = totalItemCount;
				loadThreads(totalItemCount, ITEMS_PER_PAGE);
			}
		}
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		setEmptyText(getString(R.string.error_no_messages));

		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);

		mHandler = new Handler();

		// Check to see if we have a frame in which to embed the details
		// fragment directly in the containing UI.
		final View detailsFrame = getActivity().findViewById(R.id.details);
		dualPane = detailsFrame != null
				&& detailsFrame.getVisibility() == View.VISIBLE;

		if (savedInstanceState != null) {
			// Restore last state for checked position.
			curCheckPosition = savedInstanceState.getInt(EXTRA_CUR_CHOICE, 0);
		}

		if (getArguments() != null)
			fromDashboard = getArguments().getBoolean(SearchParams.AliasVariant.IntentBuilder.EXTRA_FROM_DASHBOARD, false);
		Log.i(TAG, "::onActivityCreated:" + "fromDashboard " + fromDashboard);

		if (dualPane) {
			// In dual-pane mode, the list view highlights the selected item.
			getListView().setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			// Make sure our UI is in the correct state.
			showDetails(curCheckPosition);
		}

	}

	public void loadThreads(final String labelId, final String labelText) {
		resetPage();
		loadThreads(labelId, labelText, 0, ITEMS_PER_PAGE);
	}

	public void loadThreads(final int offset, final int count) {
		loadThreads(mLabelId, mLabelText, offset, count);
	}

	public void loadThreads(final String labelId, final String labelText,
			final int offset, final int count) {
		mLabelId = labelId;
		mLabelText = labelText;
		if (offset >= 0 && count > 0)
			mService.getThreadsInLabel(labelId, offset, count);
		else if (labelId != null)
			mService.getAllThreadsInLabel(labelId);
		else
			mService.getAllThreads();

		setTitleText(labelText);
		if (getListAdapter() != null && getListAdapter().isEmpty())
			setListShown(false);
		else if (getListAdapter() == null) // TODO shitcode
			setListShown(false);
		// if no responce, show list after 3 secs
		final Timer timer = new Timer();
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				if (getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							setListShown(true);
						}
					});
				}
			}
		}, 10000);
	}

	public void setTitleText(final String labelText) {
		mTopBarLabelsView.setTitleText(labelText);
		mLabelText = labelText;
	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		final ViewGroup v = (ViewGroup) super.onCreateView(inflater, container,
				savedInstanceState);
		final ListView list = (ListView) v.findViewById(android.R.id.list);
		final Resources res = getActivity().getResources();
		v.findViewById(android.R.id.list).setPadding(list.getPaddingLeft(),
				res.getDimensionPixelSize(R.dimen.top_bar_height),
				list.getPaddingRight(), list.getPaddingBottom());
		final LayoutAnimationController controller = new LayoutAnimationController(
				AnimationUtils.loadAnimation(getActivity(),
						android.R.anim.fade_in));
		list.setLayoutAnimation(controller);
		list.setOnScrollListener(this);
		mTopBarLabelsView = new TopBarLabelsView(getActivity(), (OnClickListener) getActivity());
		v.addView(mTopBarLabelsView);
		return v;
	}

	private MatrixCursor convertToData(final List<MessageThread> conversations) {
		final String[] PROJECTION = new String[] { BaseColumns._ID,
				MessageThread.Entity.SUBJECT, MessageThread.Entity.MESSAGE,
				MessageThread.Entity.DATE, MessageThread.Entity.FULL_USER_NAME,
				MessageThread.Entity.THREAD_ID,
				MessageThread.Entity.AVATAR_SMALL,
				MessageThread.Entity.UNREADMESSAGES};
		final MatrixCursor c = new MatrixCursor(PROJECTION,
				conversations.size());
        for (final MessageThread conversation : conversations) {
            c.newRow().add(conversation.id).add(conversation.subject)
                    .add(conversation.message).add(conversation.date)
                    .add(conversation.full_user_name)
                    .add(conversation.thread_id).add(conversation.avatar_small)
                    .add(conversation.unreadMessages);

        }
		return c;
	}

	private void createAdapterAndSetData(final List<MessageThread> conversations) {
		final MatrixCursor data = convertToData(conversations);

		if (mAdapter == null) {
			final SortCursor cursor = new SortCursor(new Cursor[] { data },
					MessageThread.Entity.DATE);

			mAdapter = new SimpleCursorAdapter(getActivity(),
					R.layout.item_message_list, cursor, new String[] {
							MessageThread.Entity.FULL_USER_NAME,
							MessageThread.Entity.SUBJECT,
							MessageThread.Entity.MESSAGE,
							MessageThread.Entity.DATE,
							MessageThread.Entity.AVATAR_SMALL,
							MessageThread.Entity.UNREADMESSAGES }, new int[] {
							R.id.name, R.id.title, R.id.message, R.id.date,
							R.id.avatar, R.id.count_badge }, 0);

			final ImageLoader loader = ImageLoader.getInstance();
			mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {

				@Override
				public boolean setViewValue(final View view,
						final Cursor cursor, final int columnIndex) {
					switch (view.getId()) {
					case R.id.date:
						final String dateStr = cursor.getString(columnIndex);
						try {
							final CharSequence dateText = DateFormatUtil.getFormattedDate(dateStr);
							((TextView) view).setText(dateText);
						} catch (final ParseException e) {
							((TextView) view).setText(dateStr);
						}

						return true;
					case R.id.avatar:
						String uri = cursor.getString(columnIndex);
						if (uri == null || !URLUtil.isValidUrl(uri)) {
							uri = null;
						}
						loader.displayImage(uri, (ImageView) view);

						return true;
					case R.id.count_badge:
						final int count = cursor.getInt(columnIndex);
						final TextView textView = (TextView) view;
						if (count > 0) {
							textView.setText(Integer.toString(count));
							textView.setVisibility(View.VISIBLE);
						} else {
							textView.setText(null);
							textView.setVisibility(View.GONE);
						}
						return true;
					default:
						return false;
					}
				}
			});
			setListAdapter(mAdapter);
			Log.i(TAG, "::createAdapterAndSetData:" + "fromDashboard " + fromDashboard);
			if (fromDashboard)
				setListShown(false);
			else
				setListShown(true);
		} else {
			setData(data);
		}

	}

	public void setCursor(final Cursor c) {
		if (!mAdapter.isEmpty()) {
			final MergeCursor merge = new MergeCursor(new Cursor[] {
					mAdapter.getCursor(), c });
			mAdapter.changeCursor(merge);
		} else {
			mAdapter.changeCursor(c);
		}
	}

	public void setCursorSavingOld(final Cursor c) {
		final CursorAdapter adapter = mAdapter;
		if (!isSearch()) {
			final Cursor old = adapter.swapCursor(c);
			if (old != null && !old.isClosed())
				mOldCursor = old;
		} else
			adapter.changeCursor(c);
	}

	public void setOldCursor() {
		if (mOldCursor != null && !mOldCursor.isClosed()) {
			final CursorAdapter adapter = mAdapter;
			adapter.changeCursor(mOldCursor);
		}
	}

	private void setData(final Cursor data) {

		setCursor(data);

		if (!fromDashboard) {
			// The list should now be shown.
			if (isResumed()) {
				setListShown(true);
			} else {
				setListShownNoAnimation(true);
			}
		}
	}

	/**
	 * if change status, reset page to 1
	 */
	private void resetPage() {
		mPrevTotalItemCount = 0;
		if (mAdapter != null)
			mAdapter.changeCursor(null);
	}

	@Override
	public void onStop() {
		if (mService != null) {
			mService.unregisterMessageCallback(this);
			mService.unregisterThreadsCallback(this);
		}
		super.onStop();
	}

	@Override
	public void onStart() {
		((MHOApplication) getActivity().getApplication())
		.getNodeService(this);
		super.onStart();
	}

	@Override
	public void onNodeConnected(final NodeWorkerService service) {
		mService = service;
		mService.registerMessageCallback(this);
		mService.registerThreadsCallback(this);

		resetPage();
		loadThreads(0, ITEMS_PER_PAGE);
	}

	@Override
	public void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(EXTRA_CUR_CHOICE, curCheckPosition);
	}

	@Override
	public void onListItemClick(final ListView l, final View v,
			final int position, final long id) {
		showDetails(position);
		Log.i("FragmentComplexList", "Item clicked: " + id);
	}

	/**
	 * Helper function to show the details of a selected item, either by
	 * displaying a fragment in-place in the current UI, or starting a whole new
	 * activity in which it is displayed.
	 */
	void showDetails(final int index) {
		curCheckPosition = index;
		final Cursor c = (Cursor) getListView().getAdapter().getItem(index);
		final String remoteId = c.getString(c
				.getColumnIndex(MessageThread.Entity.THREAD_ID));

		final Bundle args = new Bundle();
		args.putString(Message.CONVERSATION_ID, remoteId);

		if (dualPane) {

			// We can display everything in-place with fragments, so update
			// the list to highlight the selected item and show the data.
			getListView().setItemChecked(index, true);
			// Check what fragment is currently shown, replace if needed.
			MessageDetailFragment details = (MessageDetailFragment) getFragmentManager()
					.findFragmentById(R.id.details);
			if (details == null) {
				// Make new fragment to show this selection.
				details = new MessageDetailFragment();
				details.setArguments(args);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				final FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.replace(R.id.details, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			final Intent intent = new Intent();
			intent.setClass(getActivity(), MessageDetailActivity.class);
			intent.putExtras(args);
			startActivity(intent);
		}
	}

	public static final String TAG = MessageListFragment.class.getSimpleName();

	@Override
	public void onMessageReceived(final HashMap<String, Object> message) {

		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				if (getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							loadThreads(mLabelId, mLabelText);
						}
					});
				}
			}
		}, 1000);

	}

	@Override
	public void onMessageCount(final int count) {
	}

	@Override
	public void onReceiveThreads(final List<MessageThread> conversations) {
		createAdapterAndSetData(conversations);
	}

	@Override
	public Handler getHandler() {
		return mHandler;
	}

	public boolean isSearch() {
		return mIsSearch;
	}

	public void setSearch(final boolean mIsSearch) {
		this.mIsSearch = mIsSearch;
	}


	public TopBarLabelsView getTopBarLabelsView() {
		return mTopBarLabelsView;
	}
}
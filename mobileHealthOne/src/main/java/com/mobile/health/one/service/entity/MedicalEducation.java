package com.mobile.health.one.service.entity;

import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

public class MedicalEducation {
	public static class Entity implements Parcelable {
		@SerializedName("medical_school")
		public String medicalSchool;
		@SerializedName("degree")
		public String degree;
		@SerializedName("gradutaion_date")
		public String graduationDate;

		public Entity(final Parcel in) {
			medicalSchool = in.readString();
			degree = in.readString();
			graduationDate = in.readString();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(medicalSchool);
			dest.writeString(degree);
			dest.writeString(graduationDate);
		}
		
		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};
	}
}
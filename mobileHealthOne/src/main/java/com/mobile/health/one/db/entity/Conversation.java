package com.mobile.health.one.db.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

// The database table
public class Conversation implements BaseColumns {
	public static class Entity implements Parcelable {
		public String id;
		public String subject;
		public long deletedAt;
		public String[] labels;
		public User.Entity[] users;
		public Message.Entity[] messages;
		public Entity(final Parcel in) {
			id = in.readString();
			subject = in.readString();
			deletedAt = in.readLong();
			labels = in.createStringArray();
			users = in.createTypedArray(User.Entity.CREATOR);
			messages = in.createTypedArray(Message.Entity.CREATOR);
		}
		public Entity() {	}
		@Override
		public int describeContents() {
			return hashCode();
		}
		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(id);
			dest.writeString(subject);
			dest.writeLong(deletedAt);
			dest.writeStringArray(labels);
			dest.writeTypedArray(users, 0);
			dest.writeTypedArray(messages, 0);
		}
		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};
	}

	// This defines the path component of the content URI.
	// For most instances, it's best to just use the classname here:
	public static final String PATH = "conversation";
	public static final String FILTER_PATH = "filter";
	public static final String CONVERSATIONS_PATH = "conversations";

	// Column definitions ///////////////////////////////////

	/**
	 * Conversation title
	 */
	public static final String TITLE = "title";

	public static final String REMOTE_ID = "remoteId";


	/* leave it as an example of JoinKeyDBHelper */
	/*
	public static final Uri USERS_MESSAGES_URI = CONTENT_URI.buildUpon()
			.appendQueryParameter(JoinKeyDBHelper.GROUP_BY_PARAM, ContentItemUtils.f(Conversation.PATH, ContentItem._ID))
			.appendQueryParameter(JoinKeyDBHelper.GROUP_CONCAT_PARAM, User.FULL_NAME)
			.appendQueryParameter(JoinKeyDBHelper.JOIN_PARAM, joinTable)
			.appendQueryParameter(JoinKeyDBHelper.JOIN_ON_PARAM, ContentItemUtils.f(joinTable,M2MColumns.FROM_ID) + "=" + ContentItemUtils.f(Conversation.PATH, ContentItem._ID))
			.appendQueryParameter(JoinKeyDBHelper.JOIN_PARAM, User.PATH)
			.appendQueryParameter(JoinKeyDBHelper.JOIN_ON_PARAM, ContentItemUtils.f(joinTable,M2MColumns.TO_ID) + "=" + ContentItemUtils.f(User.PATH, ContentItem._ID))
			.appendQueryParameter(JoinKeyDBHelper.JOIN_PARAM, Message.PATH)
			.appendQueryParameter(JoinKeyDBHelper.JOIN_ON_PARAM, ContentItemUtils.f(Message.PATH,Message.CONVERSATION_ID) + "=" + ContentItemUtils.f(Conversation.PATH, ContentItem._ID))
			.build();
	*/
}

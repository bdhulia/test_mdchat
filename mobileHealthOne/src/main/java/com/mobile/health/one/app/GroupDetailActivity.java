package com.mobile.health.one.app;

import org.apache.commons.lang.StringUtils;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.service.entity.Group;

public class GroupDetailActivity extends AppFragmentActivity {
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		final String title = getIntent().getStringExtra(Group.Entity.SHORT_NAME);
		setTitle(StringUtils.isEmpty(title) ? getString(R.string.group_profile) : title);
		final FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		final GroupDetailFragment fragment = new GroupDetailFragment();
		fragment.setArguments(getIntent().getExtras());
		transaction.add(android.R.id.content, fragment);
		transaction.commit();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}

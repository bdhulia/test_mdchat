/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.mobile.health.one.db.entity.Message;
import com.mobile.health.one.db.entity.User;
import com.mobile.health.one.service.entity.MessageThread.Entity;
import com.mobile.health.one.util.DataSaver;
import com.mobile.health.one.util.DateFormatUtil;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class MessagesResponce implements Parcelable, IResponce, DataSaver {

	public final static String[] PROJECTION = new String[] {
		BaseColumns._ID,
		User.AVATAR_LINK,
		Message.BODY,
		User.FULL_NAME,
		Entity.UNREADMESSAGES,
		Entity.SUBJECT,
		Entity.USER_ID,
		Message.CREATED_DATE,
		Message.REMOTE_ID,
		Entity.AVATAR_SMALL
	};

	public static class Data implements Parcelable, DataSaver {
		Pagination pagination;
		MessageThread[] messages;

		public Data(final Parcel in) {
			pagination = in.readParcelable(Pagination.class.getClassLoader());
			messages = in.createTypedArray(MessageThread.CREATOR);
		}
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeParcelable(pagination, flags);
			dest.writeTypedArray(messages, flags);
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};

		@Override
		public ArrayList<HashMap<String, Object>> saveData() {
			final ArrayList<HashMap<String, Object>> result = new ArrayList<HashMap<String, Object>>(messages.length);
			final String[] pj = PROJECTION;
			for (final MessageThread message : messages) {
				String user_name = message.full_user_name;
				if (StringUtils.isEmpty(message.full_user_name))
					user_name = message.sender_name;

				String avatar = message.avatar_small;
				if (StringUtils.isEmpty(message.avatar_small))
					avatar = message.sender_avatar;
				final HashMap<String, Object> map = new HashMap<String, Object>(pj.length);
				int count = 0;
				map.put(pj[count++], message.id);
				map.put(pj[count++], message.avatar_normal);
				map.put(pj[count++], message.message);
				map.put(pj[count++], user_name);
				map.put(pj[count++], message.unreadMessages);
				map.put(pj[count++], message.subject);
				map.put(pj[count++], message.user_id);
				map.put(pj[count++], new Long(DateFormatUtil.getFormattedDateEpoch(message.date, "yyyy-MM-dd kk:mm a z").getTime()));
				map.put(pj[count++], message.message_id);
				map.put(pj[count++], avatar);

				result.add(map);
			}

			return result;
		}
	}

	protected int code;

	private Data data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<MessagesResponce> CREATOR = new Parcelable.Creator<MessagesResponce>() {
		@Override
		public MessagesResponce createFromParcel(final Parcel in) {
			return new MessagesResponce(in);
		}

		@Override
		public MessagesResponce[] newArray(final int size) {
			return new MessagesResponce[size];
		}
	};

	public MessagesResponce() {
		code = 0;
	}

	public MessagesResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Data getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return data.pagination.total_users_count;
	}

	/**
	 * Stub for saving to matrix cursor
	 */
	@Override
	public ArrayList<HashMap<String, Object>> saveData() {

		return data.saveData();
	}
}

/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.google.gson.annotations.Expose;
import com.mobile.health.one.util.CursorSaver;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class UserLabelsResponce implements Parcelable, IResponce, CursorSaver {

	public static class Label {

		public static class Entity implements BaseColumns {
			public static final String LABEL_ID = "label_id";
			public static final String NAME = "name";
			public static final String IS_ROOT = "is_root";
			public static final String IS_VISIBLE = "is_root";
			public static final String COUNT = "count";

			public static final String[] PROJECTION_ALL = new String[] { _ID, LABEL_ID, NAME, IS_ROOT, IS_VISIBLE, COUNT };

			public static final int INDEX_ID = 0;
			public static final int INDEX_LABEL_ID = 1;
			public static final int INDEX_NAME = 2;
			public static final int INDEX_IS_ROOT = 3;
			public static final int INDEX_IS_VISIBLE = 4;
			public static final int INDEX_COUNT = 5;


		}

		public boolean is_root;
		public String label_name;
		public int count;
		public String users_ids;
		public boolean is_visible;
	}

	protected int code;

	private Map<String, Label> data;

	protected List<String> invalidParams;
	protected String messages;

	@Expose
	private Label rootLabel;

	public static final Parcelable.Creator<UserLabelsResponce> CREATOR = new Parcelable.Creator<UserLabelsResponce>() {
		@Override
		public UserLabelsResponce createFromParcel(final Parcel in) {
			return new UserLabelsResponce(in);
		}

		@Override
		public UserLabelsResponce[] newArray(final int size) {
			return new UserLabelsResponce[size];
		}
	};

	public UserLabelsResponce() {
		code = 0;
		data = new HashMap<String, Label>();
	}

	public UserLabelsResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = new HashMap<String, Label>();
		final int count = in.readInt();
		for (int i = 0; i < count; i++) {
			final String key = in.readString();
			final Label l = new Label();
			l.is_root = (Boolean) in.readValue(Boolean.class.getClassLoader());
			l.label_name = in.readString();
			l.count = in.readInt();
			l.users_ids = in.readString();
			l.is_visible = (Boolean) in.readValue(Boolean.class.getClassLoader());

			data.put(key, l);
		}
		in.readStringList(invalidParams);
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Map<String, Label> getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeInt(data.size());
		for (final String key : data.keySet()) {
			dest.writeString(key);
			final Label l = data.get(key);
			dest.writeValue(l.is_root);
			dest.writeString(l.label_name);
			dest.writeInt(l.count);
			dest.writeString(l.users_ids);
			dest.writeValue(l.is_visible);
		}
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public MatrixCursor saveToCursor() {
		final int count = getCount();
		final Set<String> keys = data.keySet();

		final MatrixCursor cursor = new MatrixCursor(Label.Entity.PROJECTION_ALL, count);

		for (final String key : keys) {
			final Label l = data.get(key);
			cursor.newRow()
				.add(key.hashCode())
				.add(key)
				.add(l.label_name)
				.add(l.is_root)
				.add(l.is_visible)
				.add(l.count);
			if (l.is_root)
				setRootLabel(l);
		}

		return cursor;
	}

	public ArrayList<HashMap<String, Object>> saveToArray(final boolean excludeRoot) {
		final ArrayList<HashMap<String, Object>> converted = new ArrayList<HashMap<String,Object>>(data.size());

		final Set<String> keys = data.keySet();

		for (final String key : keys) {
			final Label l = data.get(key);
			if (l.is_root) {
				setRootLabel(l);
				l.count = -1;
			}

			if (excludeRoot && l.is_root)
				continue;

			final HashMap<String, Object> hash = new HashMap<String, Object>();
			hash.put(BaseColumns._ID, key.hashCode());
			hash.put(Label.Entity.LABEL_ID, key);
			hash.put(Label.Entity.NAME, l.label_name);
			hash.put(Label.Entity.IS_ROOT, l.is_root);
			hash.put(Label.Entity.IS_VISIBLE, l.is_visible);
			hash.put(Label.Entity.COUNT, l.count);
			converted.add(hash);
		}
		return converted;
	}

	public Label getRootLabel() {
		return rootLabel;
	}

	private void setRootLabel(final Label rootLabel) {
		this.rootLabel = rootLabel;
	}
}

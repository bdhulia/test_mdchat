package com.mobile.health.one.search;

import android.os.Bundle;
import android.view.View;

import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.worker.AutocompleteSuggestProcessor;

public class AutocompleteSuggestTask extends TextTask {

	public static final int TYPE_INVITES = 1;
	public static final int TYPE_NETWORK = 2;

	public static class Creator extends TextTask.Creator {

		public Creator setType(final int type) {
			this.type = type;
			return this;
		}

		private int type;

		@Override
		public TextTask create() {
			return new AutocompleteSuggestTask(getListener(), getView(), getSearch(), type);
		}

	}

	private final int type;

	public AutocompleteSuggestTask(final Listener listener, final View view,
			final CharSequence s, final int type) {
		super(listener, view, s);
		this.type = type;
	}

	@Override
	protected Bundle getExtras() {
		final Bundle extras = new Bundle();

		extras.putString(AutocompleteSuggestProcessor.EXTRA_QUERY, getText().toString());
		return extras;
	}

	@Override
	protected String getSearchWorkerName() {
		switch (type) {
		case TYPE_INVITES:
			return AppSyncService.AUTOCOMPLETE_INVITE;
		case TYPE_NETWORK:
			return AppSyncService.AUTOCOMPLETE_NETWORK;
		default:
			throw new IllegalArgumentException("Autocomplete type must be specified");
		}
	}

}
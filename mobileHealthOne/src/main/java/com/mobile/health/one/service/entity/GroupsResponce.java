/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.List;

import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.mobile.health.one.util.CursorSaver;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class GroupsResponce implements Parcelable, IResponce, CursorSaver {

	public static class Data implements Parcelable, CursorSaver {
		Pagination pagination;
		Group[] groups;

		public Data(final Parcel in) {
			pagination = in.readParcelable(Pagination.class.getClassLoader());
			groups = in.createTypedArray(Group.CREATOR);
		}
		@Override
		public int describeContents() {
			return 0;
		}

		public Pagination getPagination() {
			return pagination;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeParcelable(pagination, flags);
			dest.writeTypedArray(groups, flags);
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};

		public final static String[] PROJECTION = new String[] {
				BaseColumns._ID,
				Group.Entity.SHORT_NAME,
				Group.Entity.LONG_NAME,
				Group.Entity.DESCRIPTION,
				Group.Entity.ADMIN_ID,
				Group.Entity.ADMIN_NAME,
				Group.Entity.AVATAR_LINK
		};

		@Override
		public MatrixCursor saveToCursor() {

			final MatrixCursor cursor = new MatrixCursor(PROJECTION, groups.length);
			for (final Group group : groups) {
				cursor.newRow()
					.add(group.id)
					.add(group.short_name)
					.add(group.long_name)
					.add(group.description)
					.add(group.admin.id)
					.add(group.admin.fullName)
					.add(group.avatar_link);
			}
			return cursor;
		}
	}

	protected int code;

	private Data data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<GroupsResponce> CREATOR = new Parcelable.Creator<GroupsResponce>() {
		@Override
		public GroupsResponce createFromParcel(final Parcel in) {
			return new GroupsResponce(in);
		}

		@Override
		public GroupsResponce[] newArray(final int size) {
			return new GroupsResponce[size];
		}
	};

	public GroupsResponce() {
		code = 0;
	}

	public GroupsResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Data getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	/**
	 * Stub for saving to matrix cursor
	 */
	@Override
	public MatrixCursor saveToCursor() {
		return data.saveToCursor();
	}

	@Override
	public int getCount() {
		return getData().pagination.total_groups_count;
	}
}

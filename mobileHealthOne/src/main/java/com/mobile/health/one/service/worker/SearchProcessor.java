package com.mobile.health.one.service.worker;

import java.security.InvalidParameterException;
import java.util.Arrays;
import java.util.Set;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;
import android.util.Log;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SearchParams.Param;
import com.mobile.health.one.util.SearchParams.SearchParam;
import com.mobile.health.one.util.SearchParams.SearchType;
import com.mobile.health.one.util.SearchParams.SearchVariant;

/**
 * @author Igor Yanishevskiy
 */
public class SearchProcessor extends RequestProcessor {

	public static final String EXTRA_PARAM_TOKEN = "token";
	public static final String EXTRA_PARAM_DEVICE_ID = "device_id";

	public static final String EXTRA_PARAM_FORM_NAME = "%s[%s]";

	public static final String EXTRA_FORM_FIELDS = "form_fields";
	public static final String EXTRA_SEARCH = "searches";

	public static final String EXTRA_TYPE = "type";

	public static final String EXTRA_ADDITIONAL = "additional";

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public static final String URI_CONTACTS = Settings.URI_BASE
			+ "/api/api_search/contacts";

	public static final String URI_MEMBERS = Settings.URI_BASE
			+ "/api/api_search/members";

	public static final String URI_GROUPS = Settings.URI_BASE
			+ "/api/api_search/groups";

	public static final String URI_MESSAGES = Settings.URI_BASE
			+ "/api/api_search/threadMessages";

	public static final String URI_THREADS = Settings.URI_BASE
			+ "/api/api_search/threads";

	private Uri mUri;

	public SearchProcessor(final Context context) {
		super(context);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		final SearchType type = SearchType.values()[extras.getInt(EXTRA_TYPE,
				SearchType.MESSAGES.ordinal())];
		final Param p = SearchParams.getParams(type);
		if (p instanceof SearchParam) {
			final SearchParams.SearchParam sp = (SearchParam) p;

			// find exclude endpoint
			final SearchVariant[] variants = sp.getVariants();
			SearchVariant foundVariant = null;
			if (variants != null) {
				for (final SearchVariant searchVariant : variants) {
					if (searchVariant.getFormFields().equals(sp.getDefaultFormFields())) {
						foundVariant = searchVariant;
					}
				}
			}

			String endpoint;
			if (foundVariant != null && StringUtils.isNotEmpty(foundVariant.getEndpoint())) {
				endpoint = foundVariant.getEndpoint();
			} else {
				endpoint = sp.getEndoint();
			}
			mUri = Uri.parse(endpoint);

			String[] fields;
			if (extras.containsKey(EXTRA_FORM_FIELDS)) {
				fields = extras.getStringArray(EXTRA_FORM_FIELDS);
			} else {
				fields = sp.getDefaultFormFields();
			}

			String[] searchVals = extras.getStringArray(EXTRA_SEARCH);

			if (searchVals.length == 1 && searchVals.length < fields.length) {
				final String searchValue = searchVals[0];
				searchVals = new String[fields.length];
				Arrays.fill(searchVals, searchValue);
			}

			final String[] searches = (String[]) ArrayUtils.addAll(searchVals,
					sp.getSearches());

			final String[] formFields = (String[]) ArrayUtils.addAll(fields,
					sp.getFormFields());

			try {
				if (searches.length != formFields.length)
					throw new InvalidParameterException(
							"EXTRA_USER_SEARCH and EXTRA_FORM_FIELDS arrays should be the same length");
			} catch (final NullPointerException e) {
				throw new InvalidParameterException(
						"EXTRA_USER_SEARCH and EXTRA_FORM_FIELDS arrays should be passed");
			}

			final String token = getSettings().getSessionId();

			final RequestParams params = new BasicRequestParams();
			params.add(EXTRA_PARAM_TOKEN, token);
			params.add(EXTRA_PARAM_DEVICE_ID, androidId);
			params.add("current_page", "1");
			params.add("per_page", "1000");
			for (int i = 0; i < searches.length; i++) {
				final String searchText = searches[i];
				final String formField = formFields[i];
//				final String formattedParam = String.format(
//						EXTRA_PARAM_FORM_NAME, sp.getFormName(), formField);
				params.add(formField, searchText);
			}

			final Bundle add = extras.getBundle(EXTRA_ADDITIONAL);
			if (add != null) {
				final Set<String> keys = add.keySet();
				for (final String key : keys) {
					params.add(key, add.getString(key));
				}
			}

			if (!getSettings().hasSessionId()) {
				throw new InvalidParameterException("Please login first");
			}
			Log.d(TAG, params.getRequestParams().toString());
			@SuppressWarnings("rawtypes")
			final Class c = sp.getResponce();
			tryRequest(receiver, false, params, c, extras);
		}
	}

	@Override
	public Uri getUri() {
		return mUri;
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return "Error performing search";
	}

}

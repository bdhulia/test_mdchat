package com.mobile.health.one.util;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

/**
 * @author Denis Migol
 */
public final class ToastUtil {
	private ToastUtil() {
	}

	public static void showText(final Context context, final CharSequence text) {
		if (context != null)
			Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
	}

	public static void showError(final Context context, final CharSequence text) {
		if (context != null) {
			final Toast t = Toast.makeText(context, text, Toast.LENGTH_LONG);
			t.setGravity(Gravity.CENTER, 0, -100);
			t.show();
		}
	}

	public static void showText(final Context context, final int resid) {
		if (context != null)
			Toast.makeText(context, resid, Toast.LENGTH_SHORT).show();
	}

	public static void showNotImplemented(final Context context) {
		showText(context, "Not yet implemented");
	}

	public static void showNoNetwork(final Context context) {
		showError(context, "There is no network connection");
	}
}

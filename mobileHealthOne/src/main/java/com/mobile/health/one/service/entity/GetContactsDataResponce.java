package com.mobile.health.one.service.entity;

import org.apache.commons.lang.StringUtils;

import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.mobile.health.one.db.entity.User;
import com.mobile.health.one.util.CursorSaver;


public class GetContactsDataResponce implements Parcelable, IResponce, CursorSaver {

	public static class Data implements Parcelable {
		public Pagination pagination;

		public User.Entity[] users;

		public Data(final Parcel in) {
			pagination = in.readParcelable(Pagination.class.getClassLoader());
			users = in.createTypedArray(User.Entity.CREATOR);
		}

		@Override
		public int describeContents() {
			return hashCode();
		}
		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeValue(pagination);
			dest.writeTypedArray(users, 0);
		}

		public Pagination getPagination() {
			return pagination;
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};

	}

	private int code;

	private Data data;

	public static final Parcelable.Creator<GetContactsDataResponce> CREATOR = new Parcelable.Creator<GetContactsDataResponce>() {
		@Override
		public GetContactsDataResponce createFromParcel(final Parcel in) {
			return new GetContactsDataResponce(in);
		}

		@Override
		public GetContactsDataResponce[] newArray(final int size) {
			return new GetContactsDataResponce[size];
		}
	};

	@Override
	public int getCode() {
		return code;
	}

	public GetContactsDataResponce() {	}

	public GetContactsDataResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
	}

	private String result;

	public String getResult() {
		return result;
	}

	public Data getData() {
		return data;
	}

	public User.Entity[] getUsers() {
		return data.users;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, 0);
	}

	@Override
	public String getMessages() {
		// TODO Auto-generated method stub
		return null;
	}

	private static final String[] PROJECTION = new String[] { User._ID,
		User.FIRST_NAME,
		User.MIDDLE_NAME,
		User.LAST_NAME,
		User.FULL_NAME,
		User.STATUS,
		User.STATUS_TEXT,
		User.SPECIALITY,
		User.AVATAR_LINK};

	@Override
	public MatrixCursor saveToCursor() {
		final MatrixCursor cursor = new MatrixCursor(PROJECTION, data.users.length);
		for (final User.Entity user : data.users) {
			final MatrixCursor.RowBuilder b = cursor.newRow();
			if (user.id != null && user.id > 0)
				b.add(user.id);
			else
				b.add(user.id2);

			b.add(user.firstName);
			b.add(user.lastName);
			b.add(user.middleName);
			if (StringUtils.isEmpty(user.fullName)) {
				b.add(user.lastName);
			} else {
				b.add(user.fullName);
			}
			b.add(user.status);
			b.add(user.statusText);
			b.add(user.speciality);
			b.add(user.avatarLink);
		}
		return cursor;
	}

	@Override
	public int getCount() {
		return getData().pagination.total_users_count;
	}
}
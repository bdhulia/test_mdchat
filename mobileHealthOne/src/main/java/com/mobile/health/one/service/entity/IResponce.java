package com.mobile.health.one.service.entity;

public interface IResponce {
	public int getCode();
	public int getCount();
	public String getMessages();
}
package com.mobile.health.one.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;

import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.db.entity.User;

public class RecentListActivity extends AppFragmentActivity implements OnNavigationListener {

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.recently_contacted_activity_title);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// getSupportActionBar().setListNavigationCallbacks(this.adapter, this);
		//setContentView(R.layout.activity_people);

		final Bundle b = new Bundle(1);
		b.putInt(User.TYPE, User.TYPE_RECENT);
		final PeopleListFragment peopleList = new ContactsListFragment();
		peopleList.setArguments(b);
		// add top context menu
		getSupportFragmentManager()
			.beginTransaction()
			.add(new MenuMessageCreateFragment(),
					MenuMessageCreateFragment.TAG)
			.add(android.R.id.content, peopleList)
			.commit();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menu_labels:
			final Intent i = new Intent(this, LabelsActivity.class);
			startActivityForResult(i, LabelsListFragment.REQUEST_GET_LABEL);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(final int itemPosition,
			final long itemId) {
		return false;
	}

}

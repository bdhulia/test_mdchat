package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.google.gson.annotations.SerializedName;

// The database table
public class RecentUser implements BaseColumns {

	public static class Entity implements Parcelable {
		public Long id;
		public String name;
		public String speciality;
		@SerializedName("availability_status")
		public Boolean status;
		@SerializedName("availability_status_text")
		public String statusText;
		@SerializedName("avatar_normal")
		public String avatarLink;

		public int type;
		public Boolean isFriend;

		public Entity(final Parcel in) {
			id = in.readLong();
			name = in.readString();
			status = (Boolean) in.readValue(Boolean.class.getClassLoader());
			statusText = in.readString();
			avatarLink = in.readString();
		}
		public Entity() {	}
		//String privacy;
		@Override
		public int describeContents() {
			return hashCode();
		}
		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeLong(id);
			dest.writeString(name);
			dest.writeValue(status);
			dest.writeString(statusText);
			dest.writeString(avatarLink);
		}
		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};

		@Override
		public String toString() {
			return id.toString();
		}
	}

	// This defines the path component of the content URI.
	// For most instances, it's best to just use the classname here:
	public static final String PATH = "user";
	public static final String FILTER_PATH = "user/filter";

	// Column definitions ///////////////////////////////////

	/**
	 * Message date
	 */
	public static final String CREATED_DATE = "created";


	/**
	 * User name
	 */
	public static final String FIRST_NAME = "first_name";

	/**
	 * User name
	 */
	public static final String MIDDLE_NAME = "middle_name";

	/**
	 * User family name
	 */
	public static final String LAST_NAME = "last_name";

	/**
	 * User full name
	 */
	public static final String FULL_NAME = "full_name";

	public static final String STATUS = "status";

	public static final String STATUS_TEXT = "status_text";

	public static final String SPECIALITY = "speciality";

	public static final String AVATAR_LINK = "avatar_link";


	public static final int TYPE_UNDEFINED = 0;
	public static final int TYPE_HCP = 1;
	public static final int TYPE_RECENT = 2;

	public static final String TYPE = "type";

	public static final String IS_FRIEND = "is_friend";

}

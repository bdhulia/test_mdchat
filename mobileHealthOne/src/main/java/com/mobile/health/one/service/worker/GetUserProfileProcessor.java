package com.mobile.health.one.service.worker;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.mobile.health.one.R;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.GetUserProfileResponse;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.Settings;
import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;

public class GetUserProfileProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_USER_ID = "user_id";

	private final Context context;

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	private static final Uri URI_USER_PROFILE = Uri.parse(Settings.URI_BASE
			+ "/api/api_browse_profile/getUserProfile");

	public GetUserProfileProcessor(final Context context) {
		super(context);
		this.context = context;
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) throws IOException,
			HttpException, Exception {
		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		if (extras != null) {
			final long userId = extras.getLong(EXTRA_USER_ID, 0);
			if (userId > 0)
				params.add(EXTRA_USER_ID, String.valueOf(userId));
		}

		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);

		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		tryRequest(receiver, false, params, GetUserProfileResponse.class,
				extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return context.getResources().getString(R.string.error_get_user_profile);
	}

	@Override
	public Uri getUri() {
		return URI_USER_PROFILE;
	}
}
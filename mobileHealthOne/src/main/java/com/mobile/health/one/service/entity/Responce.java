/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class Responce implements Parcelable, IResponce {

	protected int code;

	private Map<String, String> data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<Responce> CREATOR = new Parcelable.Creator<Responce>() {
		@Override
		public Responce createFromParcel(final Parcel in) {
			return new Responce(in);
		}

		@Override
		public Responce[] newArray(final int size) {
			return new Responce[size];
		}
	};

	public Responce() {
		code = 0;
		data = new HashMap<String, String>();
	}

	public Responce(final Parcel in) {
		this();
		code = in.readInt();
		data = new HashMap<String, String>();
		final int count = in.readInt();
		for (int i = 0; i < count; i++) {
			data.put(in.readString(), in.readString());
		}
		in.readStringList(invalidParams);
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Map<String, String> getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeInt(data.size());
		for (final String s : data.keySet()) {
			dest.writeString(s);
			dest.writeString(data.get(s));
		}
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		return data.size();
	}
}

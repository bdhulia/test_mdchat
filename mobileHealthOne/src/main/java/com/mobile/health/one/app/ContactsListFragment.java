package com.mobile.health.one.app;

import java.lang.ref.WeakReference;

import org.apache.commons.lang.StringUtils;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.database.AbstractCursor;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.MatrixCursor.RowBuilder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.LongSparseArray;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.webkit.URLUtil;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.db.entity.User;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.NodeWorkerService.ILabelsReceiver;
import com.mobile.health.one.service.NodeWorkerService.IStatusChangeReceiver;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.worker.GetContactsDataProcessor;
import com.mobile.health.one.service.worker.GetUserProfileProcessor;
import com.mobile.health.one.util.CursorSaver;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.mobile.health.one.widget.TopBarLabelsView;
import com.nostra13.universalimageloader.core.ImageLoader;

public class ContactsListFragment extends PeopleListFragment implements
		OnScrollListener, IStatusChangeReceiver {

	private ILabelsReceiver mLabelsReceiver;

	private boolean dualPane;
	private int curCheckPosition = 0;

	// This is the Adapter being used to display the list's data.
	private SimpleCursorAdapter mAdapter;

	public boolean syncStarted;

	private NodeWorkerService mService;

	private TopBarLabelsView mTopBarLabelsView;

	private boolean isTopBarVisible;

	private Long curId;

	private boolean isGetContact;

	private LongSparseArray<Integer> mIdToPositionMap;

	private static final String TAG = PeopleListFragment.class.getSimpleName();

	private static final String EXTRA_FROM_DASHBOARD = SearchParams.AliasVariant.IntentBuilder.EXTRA_FROM_DASHBOARD;
	private ProgressBar mProgress;

	private String mLabelId;

	private boolean fromDashboard;

	private String mStatusText;

	private boolean mStatus;

	protected static class PeopleListReceiver implements Receiver {
		private final String TAG = PeopleListReceiver.class.getCanonicalName();
		private final WeakReference<ContactsListFragment> fragment;
		private final boolean fromDashboard;

		public PeopleListReceiver(final ContactsListFragment fragment) {
			this(fragment, false);
		}

		public PeopleListReceiver(final ContactsListFragment fragment, final boolean fromDashboard) {
			this.fragment = new WeakReference<ContactsListFragment>(fragment);
			this.fromDashboard = fromDashboard;
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final ContactsListFragment f = fragment.get();
			if (f == null || !f.isAdded())
				return; // no action if fragment destroyed
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start pin registering");
				// Start out with a progress indicator.
				if (f.getPrevTotalItemCount() == 0)
					f.setListShown(false);
				else {
					f.mProgress.setVisibility(View.VISIBLE);
				}
				break;
			case SyncService.STATUS_FINISHED:
				final IResponce responce = (IResponce) resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				if (responce != null) {
					Log.i(TAG, "Saving to db start. fromDashboard " + fromDashboard);
					f.setTotalItemCount(responce.getCount());
					if (f != null && f.isVisible()) {
						final CursorSaver resp = (CursorSaver) responce;
						f.setResponce(resp);
						if (!fromDashboard) {
							if (f.isResumed()) {
								f.setListShown(true);
							} else {
								f.setListShownNoAnimation(true);
							}
						} else {
							f.setListShown(false);
						}
					}
				}

				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error getting people list");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(f.getActivity(), error);
				}
				if (f.getActivity() instanceof AppFragmentActivity) {
					//((AppFragmentActivity) f.getActivity()).updateSession(true);
				}

				//f.getActivity().finish();
				break;
			}
		}
	}


	public static ContactsListFragment newInstance(final boolean fromDashboard) {
		final Bundle b = new Bundle();
		b.putBoolean(EXTRA_FROM_DASHBOARD, fromDashboard);
		final ContactsListFragment f = new ContactsListFragment();
		f.setArguments(b);
		return f;
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		// Check to see if we have a frame in which to embed the details
		// fragment directly in the containing UI.
		final View detailsFrame = getActivity().findViewById(R.id.details);
		dualPane = detailsFrame != null
				&& detailsFrame.getVisibility() == View.VISIBLE;

		if (savedInstanceState != null) {
			// Restore last state for checked position.
			curCheckPosition = savedInstanceState.getInt(EXTRA_CUR_CHOICE, 0);
			curId = savedInstanceState.getLong(EXTRA_CUR_ID, 0);
		}

		Log.i(TAG, "::onActivityCreated:" + "fromDashboard " + fromDashboard);

		if (dualPane) {
			// In dual-pane mode, the list view highlights the selected item.
			getListView().setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			// Make sure our UI is in the correct state.
			showDetails(curCheckPosition, curId);
		}

		isGetContact = getArguments().getBoolean(
				PeopleListActivity.ACTION_GET_CONTACT, false);
	}

	public void setResponce(final CursorSaver resp) {

		final MatrixCursor c = (MatrixCursor) resp.saveToCursor();
		final int idIndex = c.getColumnIndex(BaseColumns._ID);
		int position = 0;
		if (mIdToPositionMap == null) {
			mIdToPositionMap = new LongSparseArray<Integer>(c.getCount());
		} else {
			position = mIdToPositionMap.size();
		}
		while (c.moveToNext()) {
			final Long id = c.getLong(idIndex);
			mIdToPositionMap.append(id, position++);
			// just ignore if null, this could happen rarely
			if (mService != null)
				mService.registerStatusChangesCallback(this, id.toString());
		};

		c.moveToFirst();
		if (fromDashboard) {
			setOldCursor(c);
		} else {
			setCursor(c);
		}
	}

	private void init() {
		setEmptyText(getString(R.string.error_no_people));

		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);

		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new SimpleCursorAdapter(getActivity(),
				R.layout.item_people_list, null, new String[] { User.FULL_NAME,
						User.SPECIALITY, User.STATUS, User.STATUS_TEXT,
						User.AVATAR_LINK }, new int[] { R.id.name,
						R.id.speciality, R.id.status, R.id.status_text,
						R.id.avatar }, 0);
		final ImageLoader loader = ImageLoader.getInstance();
		mAdapter.setViewBinder(new ViewBinder() {

			@Override
			public boolean setViewValue(final View view, final Cursor cursor,
					final int columnIndex) {
				switch (view.getId()) {
				case R.id.status:
					final ImageView image = (ImageView) view;
					final String statusStr = cursor.getString(cursor
							.getColumnIndex(User.STATUS));
					Log.i(TAG, "::setViewValue:" + "statusStr " + statusStr);
					final int drawableRes = statusStr.equals("true") ? R.drawable.status_circle_on
							: R.drawable.status_circle_off;
					image.setImageResource(drawableRes);

					return true;
				case R.id.avatar:
					String uri = cursor.getString(columnIndex);
					if (uri == null || !URLUtil.isValidUrl(uri)) {
						uri = null;
					}
					loader.displayImage(uri, (ImageView) view);

					return true;
				default:
					return false;
				}
			}
		});
		mProgress = new ProgressBar(getActivity());
		mProgress.setIndeterminate(true);
		mProgress.setVisibility(View.GONE);
		setListAdapter(mAdapter);
		getListView().setOnScrollListener(this);
		getListView().addFooterView(mProgress);

		// Check to see if we have a frame in which to embed the details
		// fragment directly in the containing UI.
		final View detailsFrame = getActivity().findViewById(R.id.details);
		dualPane = detailsFrame != null
				&& detailsFrame.getVisibility() == View.VISIBLE;

		isTopBarVisible = getArguments().getBoolean(PARAM_IS_LABEL_BAR_VISIBLE);
	}

	@Override
	public void onAttach(final Activity activity) {
		mLabelsReceiver = new LabelsReceiver(activity);
		 ((MHOApplication) activity.getApplication())
				.getNodeService(new NodeServiceConnector() {

					@Override
					public void onNodeConnected(final NodeWorkerService service) {
						service.registerLabelsCallback(mLabelsReceiver);
						mService = service;
					}
				});
		reset();
		super.onAttach(activity);
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		init();
		loadData();
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onDetach() {
		if (mService != null) {
			mService.unregisterLabelsCallback(mLabelsReceiver);
			mService.unregisterStatusChangesCallback(this);
		}
		mLabelsReceiver = null;
		super.onDetach();
	}

	@Override
	public void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(EXTRA_CUR_CHOICE, curCheckPosition);
	}

	@Override
	public void onListItemClick(final ListView l, final View v,
			final int position, final long id) {
		if (isGetContact) {
			final Activity a = getActivity();
			if (a instanceof PeopleListListener) {
				final Cursor c = (Cursor) getListAdapter().getItem(position);
				final String userName = c.getString(c
						.getColumnIndex(User.FULL_NAME));
				final PeopleListListener listener = (PeopleListListener) a;
				listener.onListItemClick(userName, id);
			}
		} else {
			showDetails(position, id);
		}

		Log.i("FragmentComplexList", "Item clicked: " + id);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		isTopBarVisible = getArguments().getBoolean(PARAM_IS_LABEL_BAR_VISIBLE);
		if (isTopBarVisible) {
			final ViewGroup v = (ViewGroup) super.onCreateView(inflater,
					container, savedInstanceState);
			final ListView list = (ListView) v.findViewById(android.R.id.list);
			final Resources res = getActivity().getResources();
			v.findViewById(android.R.id.list).setPadding(list.getPaddingLeft(),
					res.getDimensionPixelSize(R.dimen.top_bar_height),
					list.getPaddingRight(), list.getPaddingBottom());
			final LayoutAnimationController controller = new LayoutAnimationController(
					AnimationUtils.loadAnimation(getActivity(),
							android.R.anim.fade_in));
			list.setLayoutAnimation(controller);
			// list.setOnScrollListener(this);
			mTopBarLabelsView = new TopBarLabelsView(getActivity(),
					(OnClickListener) getActivity());
			v.addView(mTopBarLabelsView);
			return v;
		} else {
			return super.onCreateView(inflater, container, savedInstanceState);
		}
	}

	/**
	 * Helper function to show the details of a selected item, either by
	 * displaying a fragment in-place in the current UI, or starting a whole new
	 * activity in which it is displayed.
	 */
	void showDetails(final int index, final long id) {
		curCheckPosition = index;
		curId = id;

		final Bundle args = new Bundle();
		args.putLong(GetUserProfileProcessor.EXTRA_USER_ID, id);

		if (dualPane) {
			// We can display everything in-place with fragments, so update
			// the list to highlight the selected item and show the data.
			getListView().setItemChecked(index, true);
			// Check what fragment is currently shown, replace if needed.
			UserProfileFragment details = (UserProfileFragment) getFragmentManager()
					.findFragmentById(R.id.details);
			if (details == null) {
				// Make new fragment to show this selection.
				details = new UserProfileFragment();
				details.setArguments(args);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				final FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.replace(R.id.details, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			final Intent intent = new Intent();
			intent.setClass(getActivity(), UserProfileActivity.class);
			intent.putExtras(args);
			getActivity().startActivityForResult(intent, 1);
		}
	}

	@Override
	protected void loadData(final int page, final boolean showProgress) {
		Log.v(TAG, "page: " + page);

		if (getArguments() != null)
			fromDashboard = getArguments().getBoolean(EXTRA_FROM_DASHBOARD, false);

		if (page > 0) {
			if (StringUtils.isNotEmpty(mLabelId)) {
				getArguments().putString(
						GetContactsDataProcessor.EXTRA_LABEL_ID, mLabelId);
			}
			getArguments().putString(
					GetContactsDataProcessor.EXTRA_ITEMS_PER_PAGE,
					Integer.toString(ITEMS_PER_PAGE));
			getArguments().putString(
					GetContactsDataProcessor.EXTRA_CURRENT_PAGE,
					Integer.toString(page));


			SyncService.startSyncService(getActivity(),
					AppSyncService.GET_CONTACTS_DATA_REQUEST,
					new PeopleListReceiver(this, fromDashboard), getArguments());

			if (getListAdapter().isEmpty())
				setListShown(false);
		}
	}

	@Override
	protected void loadData(final String labelId) {
		setLabelId(labelId);
		loadData(getCurrentPage(), true);
	}

	private void setLabelId(final String labelId) {
		mLabelId = labelId;
	}

	@Override
	protected ProgressBar getLazyLoadingProgressView() {
		return mProgress;
	}

	@Override
	protected CursorAdapter getCursorAdapter() {
		return mAdapter;
	}

	@Override
	public TopBarLabelsView getTopBarLabelsView() {
		return mTopBarLabelsView;
	}

	@Override
	protected void reset() {
		super.reset();
		mLabelId = null;
		mIdToPositionMap = null;
	}

	@Override
	public Handler getHandler() {
		return new Handler(getActivity().getMainLooper());
	}

	@Override
	public void onStatusTextChanged(final long userId, final String statusText) {
		// FIXME: shitcode! rewrite it. Refactor code to use non-cursor data
		final Integer position = mIdToPositionMap.get(userId);
		final AbstractCursor oldCursor = (AbstractCursor) mAdapter.getCursor();
		mStatusText = statusText; // now it can be changed on the fly without reinit async task
		new AsyncTask<Object, Void, Cursor>() {

			private Parcelable state;

			@Override
			protected void onPreExecute() {
				state = getListView().onSaveInstanceState();
			};

			@Override
			protected Cursor doInBackground(final Object... params) {
				final Cursor oldCursor = (Cursor) params[0];
				final Integer position = (Integer) params[1];
				final MatrixCursor newCursor = new MatrixCursor(
						oldCursor.getColumnNames(), oldCursor.getCount());
				oldCursor.moveToFirst();
				final int statusTextIndex = oldCursor
						.getColumnIndex(User.STATUS_TEXT);
				do {
					final RowBuilder builder = newCursor.newRow();
					for (int i = 0; i < oldCursor.getColumnCount(); i++) {
						if (oldCursor.getPosition() == position
								&& i == statusTextIndex) {
							builder.add(mStatusText);
						} else {
							final String val = oldCursor.getString(i);
							builder.add(val);
						}
					}
				} while (oldCursor.moveToNext());
				return newCursor;
			}

			@Override
			protected void onPostExecute(final Cursor result) {
				mAdapter.changeCursor(result);
				getListView().onRestoreInstanceState(state);
				super.onPostExecute(result);
			}

		}.execute(oldCursor, position);
	}

	@Override
	public void onStatusChanged(final long userId, final boolean isAvailable) {
		// FIXME: shitcode! rewrite it. Refactor code to use non-cursor data
		final Integer position = mIdToPositionMap.get(userId);
		final AbstractCursor oldCursor = (AbstractCursor) mAdapter.getCursor();
		mStatus = isAvailable; // now it can be changed on the fly without reinit async task
		new AsyncTask<Object, Void, Cursor>() {

			private Parcelable state;

			@Override
			protected void onPreExecute() {
				state = getListView().onSaveInstanceState();
			};

			@Override
			protected Cursor doInBackground(final Object... params) {
				final Cursor oldCursor = (Cursor) params[0];
				final Integer position = (Integer) params[1];
				final MatrixCursor newCursor = new MatrixCursor(
						oldCursor.getColumnNames(), oldCursor.getCount());
				oldCursor.moveToFirst();
				final int statusIndex = oldCursor.getColumnIndex(User.STATUS);
				do {
					final RowBuilder builder = newCursor.newRow();
					for (int i = 0; i < oldCursor.getColumnCount(); i++) {
						if (oldCursor.getPosition() == position
								&& i == statusIndex) {
							builder.add(mStatus);
						} else {
							final String val = oldCursor.getString(i);
							builder.add(val);
						}
					}
				} while (oldCursor.moveToNext());
				return newCursor;
			}

			@Override
			protected void onPostExecute(final Cursor result) {
				mAdapter.changeCursor(result);
				getListView().onRestoreInstanceState(state);
				super.onPostExecute(result);
			}

		}.execute(oldCursor, position);
	}
}
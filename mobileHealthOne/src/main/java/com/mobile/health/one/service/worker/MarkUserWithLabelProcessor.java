package com.mobile.health.one.service.worker;

import java.security.InvalidParameterException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;
import android.util.Log;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.NewResponce;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SettingsManager;

/**
 * @author Igor Yanishevskiy
 */
public class MarkUserWithLabelProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_FRIEND_ID = "friend_id";
	public static final String EXTRA_LABEL_ID = "label_id";

	private static final Uri URI = Uri
			.parse(Settings.URI_BASE + "/api/user.labels_api/markUserWithLabel");

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	private final SettingsManager settings;

	public MarkUserWithLabelProcessor(final Context context) {
		super(context);
		settings = new SettingsManager(context);
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		if (!settings.hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}
		final String token = settings.getSessionId();

		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		Log.i(TAG, "::processInternal:" + " token " + token);
		params.add(EXTRA_FRIEND_ID, extras.getString(EXTRA_FRIEND_ID));
		params.add(EXTRA_LABEL_ID, extras.getString(EXTRA_LABEL_ID));
		params.add(EXTRA_DEVICE_ID, androidId);

		tryRequest(receiver, false, params, NewResponce.class, extras);
	}

	@Override
	public Uri getUri() {
		return URI;
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(final E result,
			final Bundle extras) {
		// nothing to do here, we don't receive any data, just status
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(final E result) {
		final NewResponce r = (NewResponce) result;
		if (r.getInvalidParams() != null) {
			for (final String invalidParam : r.getInvalidParams()) {
				if ("label_id".equals(invalidParam))
					return "Label already set for this user";
			}
		}

		return result.getMessages();
	}

}

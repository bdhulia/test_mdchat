package com.mobile.health.one.service.entity;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class GetInvitesTemplatesResponse implements Parcelable, IResponce {

	public static class Data implements Parcelable {

		public Message[] messages;

		public Data(final Parcel in) {
			messages = in.createTypedArray(Message.CREATOR);
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeTypedArray(messages, flags);
		}
	}

	public static class Message implements Parcelable {

		public String title;
		public String description;

		public Message(final Parcel in) {
			title = in.readString();
			description = in.readString();
		}

		public static final Parcelable.Creator<Message> CREATOR = new Parcelable.Creator<Message>() {
			@Override
			public Message createFromParcel(final Parcel in) {
				return new Message(in);
			}

			@Override
			public Message[] newArray(final int size) {
				return new Message[size];
			}
		};

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(title);
			dest.writeString(description);
		}
	}

	protected int code;

	private Data data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<GetInvitesTemplatesResponse> CREATOR = new Parcelable.Creator<GetInvitesTemplatesResponse>() {
		@Override
		public GetInvitesTemplatesResponse createFromParcel(final Parcel in) {
			return new GetInvitesTemplatesResponse(in);
		}

		@Override
		public GetInvitesTemplatesResponse[] newArray(final int size) {
			return new GetInvitesTemplatesResponse[size];
		}
	};

	public GetInvitesTemplatesResponse() {
		code = 0;
	}

	public GetInvitesTemplatesResponse(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Data getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		return getData().messages.length;
	}

}

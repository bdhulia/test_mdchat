package com.mobile.health.one.app;

import android.content.Intent;
import android.os.Bundle;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.mobile.health.one.R;

public class MenuMessageCreateFragment extends SherlockFragment {

	public static final String TAG = MenuMessageCreateFragment.class.getSimpleName();

	public static final String EXTRA_EXCLUDE_MENU = "exclude_menu";
	public static final String EXTRA_SENDER_ID = "sender_id";
	public static final String EXTRA_SENDER_NAME = "sender_name";

	public static MenuMessageCreateFragment newInstance() {
		return new MenuMessageCreateFragment();
	}

	public static MenuMessageCreateFragment newInstance(final int excludeMenuItem) {
		return newInstance(excludeMenuItem, null, null);
	}
	public static MenuMessageCreateFragment newInstance(final int excludeMenuItem, final String id, final String name) {
		final MenuMessageCreateFragment f = new MenuMessageCreateFragment();
		final Bundle extras = new Bundle();
		extras.putInt(EXTRA_EXCLUDE_MENU, excludeMenuItem);
		extras.putString(EXTRA_SENDER_ID, id);
		extras.putString(EXTRA_SENDER_NAME, name);
		f.setArguments(extras);

		return f;
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
		inflater.inflate(R.menu.message_create, menu);
		final Bundle args = getArguments();

		final Intent composeIntent = new Intent(getActivity(), MessageCreateActivity.class);
		if (args != null)
			composeIntent.putExtras(args);
		menu.findItem(R.id.compose).setIntent(composeIntent);

		final Intent sendInviteIntent = new Intent(getActivity(), SendInvitationActivity.class);
		if (args != null)
			sendInviteIntent.putExtras(args);
		menu.findItem(R.id.send_invite).setIntent(sendInviteIntent);

		if (args != null && args.containsKey(EXTRA_EXCLUDE_MENU)) {
			final com.actionbarsherlock.view.MenuItem item = menu.findItem(args.getInt(EXTRA_EXCLUDE_MENU));
			if (item != null) {
				item.setVisible(false);
			}
		}
	};

}

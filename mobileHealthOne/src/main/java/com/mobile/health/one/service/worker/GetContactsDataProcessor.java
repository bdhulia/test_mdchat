package com.mobile.health.one.service.worker;

import java.security.InvalidParameterException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.db.entity.User;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.GetContactsDataResponce;
import com.mobile.health.one.service.entity.GetDirectoryResponse;
import com.mobile.health.one.service.entity.GetRecentContactsResponce;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.Settings;

/**
 * @author Igor Yanishevskiy
 */
public class GetContactsDataProcessor extends RequestProcessor {

	public static final String EXTRA_METHOD = "method";
	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_ONLY_HCP = "only_hcp";
	public static final String EXTRA_CURRENT_PAGE = "current_page";
	public static final String EXTRA_ITEMS_PER_PAGE = "per_page";
	public static final String EXTRA_LABEL_ID = "label_id";

	private static final Uri URI_FRIENS = Uri.parse(Settings.URI_BASE
			+ "/api/archer_network_api/getContactsData");
	private static final Uri URI_ALL = Uri.parse(Settings.URI_BASE
			+ "/api/archer_network_api/getMDChatMembersData");
	private static final Uri URI_RECENT = Uri.parse(Settings.URI_BASE
			+ "/api/archer_message.manager/getRecentlyContactedApi");

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	private int contactsType = User.TYPE_UNDEFINED;
	private boolean isFriend = true;

	public GetContactsDataProcessor(final Context context) {
		super(context);
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();

		if (extras != null) {
			contactsType = extras.getInt(User.TYPE, User.TYPE_UNDEFINED);
			isFriend = extras.getBoolean(User.IS_FRIEND, true);

			params.add(EXTRA_CURRENT_PAGE, extras.getString(EXTRA_CURRENT_PAGE));
			params.add(EXTRA_ITEMS_PER_PAGE,
					extras.getString(EXTRA_ITEMS_PER_PAGE));

			params.add(EXTRA_LABEL_ID, extras.getString(EXTRA_LABEL_ID));
		}

		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);

		if (contactsType == User.TYPE_HCP) {
			params.add(EXTRA_ONLY_HCP, "true");
		}

		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}
		if (contactsType == User.TYPE_RECENT)
			tryRequest(receiver, false, params,
					GetRecentContactsResponce.class, extras);
		else if (contactsType == User.TYPE_HCP)
			tryRequest(receiver, false, params, GetDirectoryResponse.class,
					extras);
		else
			tryRequest(receiver, false, params, GetContactsDataResponce.class,
					extras);

	}

	@Override
	public Uri getUri() {
		if (contactsType == User.TYPE_RECENT)
			return URI_RECENT;
		else
			return isFriend ? URI_FRIENS : URI_ALL;
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		if (contactsType != User.TYPE_RECENT && contactsType != User.TYPE_HCP) {
			final GetContactsDataResponce responce = (GetContactsDataResponce) result;

			for (final User.Entity user : responce.getData().users) {
				user.isFriend = isFriend;
				user.type = contactsType;
			}
		}
		
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return "Error receiving people list";
	}

}

package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

public class Invite implements Parcelable {

	public static interface Entity extends BaseColumns {
		public static final int STATUS_ALL = 3;
		public static final int STATUS_PENDING = 0;
		public static final int STATUS_ACCEPTED = 1;
		public static final int STATUS_DECLINED = 2;

		public static final int TYPE_INCOMING = 1;
		public static final int TYPE_OUTGOING = 2;
		
		public static final int CONTACT_INVITES = 1;
		public static final int GROUP_INVITES = 2;

		public static final String EXTRA_TYPE = "type";

		public static final String USER_ID = "user_id";
		public static final String USER_NAME = "user_name";
		public static final String USER_AVATAR = "user_avatar";
		public static final String INVITATION_TEXT = "invitation_text";
		public static final String INVITATION_ID = "invitation_id";
		public static final String STATUS = "status";

		public final String[] PROJECTION = new String[] {
				BaseColumns._ID,
				USER_ID,
				USER_NAME,
				USER_AVATAR,
				INVITATION_TEXT,
				INVITATION_ID,
				STATUS
		};
	}

	public long user_id;
	public String user_name;
	public String user_avatar;
	public String invitation_text;
	public long invitation_id;
	public int status;

	public Invite(final Parcel in) {
		user_id = in.readLong();
		user_name = in.readString();
		user_avatar = in.readString();
		invitation_text = in.readString();
		invitation_id = in.readLong();
		status = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeLong(user_id);
		dest.writeString(user_name);
		dest.writeString(user_avatar);
		dest.writeString(invitation_text);
		dest.writeLong(invitation_id);
		dest.writeInt(status);
	}

	public static final Parcelable.Creator<Invite> CREATOR = new Parcelable.Creator<Invite>() {
		@Override
		public Invite createFromParcel(final Parcel in) {
			return new Invite(in);
		}

		@Override
		public Invite[] newArray(final int size) {
			return new Invite[size];
		}
	};

}

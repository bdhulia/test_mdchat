package com.mobile.health.one.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.app.EnterPinFragment.EnterPinListener;
import com.mobile.health.one.app.LoginFragment.LoginCallback;
import com.mobile.health.one.app.RegisterPinFragment.RegisterPinCallback;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SettingsManager;
import com.mobile.health.one.util.ToastUtil;

public class LoginActivity extends SherlockFragmentActivity implements RegisterPinCallback,
LoginCallback, EnterPinListener {

	private static final String TAG = LoginActivity.class.getSimpleName();

	public static final String EXTRA_LOGIN_ONLY = "loginOnly";
	private SettingsManager settingsManager;
	private boolean mFirstLogin;
	private boolean mLoginOnly;
	private final NodeServiceConnector mNodeRegister = new NodeServiceConnector() {

		@Override
		public void onNodeConnected(final NodeWorkerService service) {
			if (settingsManager != null)
				service.serverAuth(settingsManager.getSessionId());
		}
	};

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        if (!isTaskRoot()) {
            final Intent intent = getIntent();
            final String intentAction = intent.getAction();
            if (intent.hasCategory(Intent.CATEGORY_LAUNCHER) &&
                    intentAction != null && intentAction.equals(Intent.ACTION_MAIN)) {
                finish();
            }
        }
		requestWindowFeature(com.actionbarsherlock.view.Window.FEATURE_INDETERMINATE_PROGRESS);
		final ActionBar ab = getSupportActionBar();
		// set defaults for logo
		ab.setDisplayUseLogoEnabled(true);
		setSupportProgressBarIndeterminateVisibility(false);
		mLoginOnly = getIntent().getBooleanExtra(EXTRA_LOGIN_ONLY, false);
		Log.i(TAG, "::onCreate:" + "mLoginOnly " + mLoginOnly);
		settingsManager = new SettingsManager(getApplicationContext());
	}

	@Override
	protected void onNewIntent(final Intent intent) {
		mLoginOnly = intent.getBooleanExtra(EXTRA_LOGIN_ONLY, false);
		Log.i(TAG, "::onNewIntent:" + "mLoginOnly " + mLoginOnly);
		super.onNewIntent(intent);
	}

	@Override
	protected void onResume() {
		super.onResume();
		settingsManager.setAppVisible(true);
		// Create the list fragment and add it as our sole content.
		final boolean hasSessionId = settingsManager.hasSessionId();
		final boolean isPinRegistered = settingsManager.isPinRegistered();
		final FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();

		if (hasSessionId && isPinRegistered) {
			final EnterPinFragment pin = EnterPinFragment.newInstance(false);
			transaction.replace(android.R.id.content, pin);
		} else if (hasSessionId && !isPinRegistered) {
			final FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
			final LoginFragment loginFragment = new LoginFragment();
			transaction2.replace(android.R.id.content, loginFragment);
			transaction2.commit();

			final RegisterPinFragment registerPin = new RegisterPinFragment();
			transaction.replace(android.R.id.content, registerPin);
		} else if (!hasSessionId) {
			final FragmentTransaction transaction2 = getSupportFragmentManager().beginTransaction();
			final LoginFragment loginFragment = new LoginFragment();
			transaction2.replace(android.R.id.content, loginFragment);
			transaction2.commit();
			//transaction.addToBackStack(null);
			mFirstLogin = true;
		}
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		transaction.commit();

	}

	@Override
	protected void onPause() {
		settingsManager.setAppVisible(false);
		super.onPause();
	}

	@Override
	public void onPinRegistered() {
		nodeRegister();
		if (mLoginOnly) {
			finish();
		} else {
			final Intent i = new Intent(this, HomeActivity.class);
			i.putExtra(HomeActivity.EXTRA_SHOW_WELCOME, true);
			startActivity(i);
		}
	}

	private void nodeRegister() {
		((MHOApplication) getApplication()).getNodeService(mNodeRegister);
	}

	@Override
	public void onLogin(final boolean needPin) {
		final FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		settingsManager.setPinRegistered(!needPin);
		nodeRegister();
		if (needPin) {
			Log.v("LoginActivity", "::onLogin:" + "needPin");
			transaction.replace(android.R.id.content, new RegisterPinFragment());
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			transaction.addToBackStack(null);
		} else {
			Log.v("LoginActivity", "::onLogin:" + "");
			// added EnterPinFragment so we can use it them for updateSession and register in GCM
			transaction.replace(android.R.id.content, EnterPinFragment.newInstance(false));
			transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
			if (mFirstLogin) {
				transaction.addToBackStack(null);
			}
			//final Intent i = new Intent(this, HomeActivity.class);
			//startActivity(i);
		}
		transaction.commit();
	}

	@Override
	public void onPinRegisterError(final String error) {
		ToastUtil.showText(this, error);
		final FragmentTransaction transaction = getSupportFragmentManager()
				.beginTransaction();
		transaction.replace(android.R.id.content, new LoginFragment());
		transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
		transaction.commit();
	}

	@Override
	public void onPinEnter() {
		nodeRegister();
		if (mLoginOnly) {
			finish();
		} else {
			final Intent messages = new Intent(this,
					HomeActivity.class);
			startActivity(messages);
		}
	}

	@Override
	public void onBackPressed() {
		if (!mLoginOnly)
			super.onBackPressed();
	}

}

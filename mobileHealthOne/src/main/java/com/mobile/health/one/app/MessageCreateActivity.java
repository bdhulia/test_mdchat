package com.mobile.health.one.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewParent;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MultiAutoCompleteTextView;
import android.widget.MultiAutoCompleteTextView.CommaTokenizer;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.app.ViewHolder.Validator;
import com.mobile.health.one.db.entity.Message;
import com.mobile.health.one.search.MultiSuggestTextWatcher;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.NodeWorkerService.IMessageReceiver;
import com.mobile.health.one.service.worker.AutocompleteNetworkProcessor;
import com.mobile.health.one.util.FileUtils;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.TextCounterListener;
import com.mobile.health.one.util.ToastUtil;

public class MessageCreateActivity extends AppFragmentActivity {

	private final IMessageReceiver mMessageReceiver = new IMessageReceiver() {

		@Override
		public void onMessageCount(final int count) {
		}

		@Override
		public Handler getHandler() {
			if (mHandler == null) {
				mHandler = new Handler(
						MessageCreateActivity.this.getMainLooper());
			}
			return mHandler;
		}

		@Override
		public void onMessageReceived(final HashMap<String, Object> message) {
		}
	};

	@SuppressLint("ValidFragment")
	public class ConfirmDialog extends DialogFragment implements
			DialogInterface.OnClickListener {

		public static final String EXTRA_BACK_PRESSED = "back_pressed";
		private boolean isBackPressed;

		@Override
		public void onCreate(final Bundle savedInstanceState) {
			isBackPressed = getArguments().getBoolean(EXTRA_BACK_PRESSED);
			super.onCreate(savedInstanceState);
		}

		@Override
		public Dialog onCreateDialog(final Bundle savedInstanceState) {
			final AlertDialog.Builder dialog = new AlertDialog.Builder(
					getActivity());
			dialog.setMessage(R.string.message_discard_message);
			dialog.setTitle(R.string.message_discard_title);
			dialog.setPositiveButton(R.string.message_discard_btn, this);
			dialog.setNegativeButton(R.string.btn_no, this);
			return dialog.create();
		}

		@Override
		public void onClick(final DialogInterface dialog, final int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				dialog.dismiss();
				MessageCreateActivity.this.setIsSended(true);
				if (isBackPressed)
					MessageCreateActivity.this.onBackPressed();
				else
					NavUtils.navigateUpFromSameTask(MessageCreateActivity.this);
				break;
			case DialogInterface.BUTTON_NEGATIVE:
				dialog.cancel();
				break;
			default:
				break;
			}

		}
	}

	static final String TAG = "MessageCreateActivity";

	public static final String EXTRA_FORWARD = "forward";
	public static final String EXTRA_REPLY = "reply";
	public static final String EXTRA_SUBJECT = "subject";

	private static final int FILE_SELECT_CODE = 5;

	private static final int MENU_ID_ATTACH = 10;
	private static final int MENU_ID_SEND = 11;

	private ViewHolder users;
	private ViewHolder title;
	private ViewHolder message;

	private final ViewHolder.Validator validator = new Validator() {

		@Override
		public ValidationResult onValidate(final ViewHolder vh,
				final CharSequence s) {
			return new ValidationResult();
		}

		@Override
		public ValidationResult onLoseFocus(final ViewHolder vh,
				final CharSequence s) {
			if (TextUtils.isEmpty(s)) {
				CharSequence err;
				if (vh.view.equals(users.view)) {
					err = getText(R.string.error_no_recepients);
				} else {
					err = getText(R.string.error_empty);
				}

				return new ValidationResult(false, err);
			}

			if (vh.view.equals(users.view)) {
				final String[] ids = ((Editable) s).getSpans(0, s.length(), String.class);
				final boolean idsMatches = ids.length <= 0;
				if (idsMatches) {
					return new ValidationResult(false, getString(R.string.error_no_user_selected));
				}
			}

			return new ValidationResult();
		}
	};
	private String threadId;

	private LinearLayout attachments;

	private MenuItem attachMenuItem;

	private String encodedFile;

	private String encodedFileName;

	private boolean mIsReply;

	private boolean mIsForward;

	private CommaTokenizer mTokenizer;

	private boolean mIsSended = false;

	private Thread mEncidongThread;

	private String mFileType;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_message_create);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		title = new ViewHolder((EditText) findViewById(R.id.title), validator,
				false);
		message = new ViewHolder((EditText) findViewById(R.id.message),
				validator, false);

		final int maxChars = getResources().getInteger(
				R.integer.max_message_chars);
		final TextView count = (TextView) findViewById(R.id.count);
		message.view.addTextChangedListener(new TextCounterListener(count,
				maxChars));
		count.setText(Integer.toString(maxChars));

		attachments = (LinearLayout) findViewById(R.id.attachments);

		final String type = getIntent().getType();
		if (type != null && type.indexOf("image/") != -1) {
			processImageFromData(getIntent());
			setLoginOnly(true);
		}

		threadId = getIntent().getStringExtra(Message.CONVERSATION_ID);

		users = new ViewHolder(
				(MultiAutoCompleteTextView) findViewById(R.id.users),
				validator, false);
		mIsReply = getIntent().getBooleanExtra(EXTRA_REPLY, false);
		mIsForward = getIntent().getBooleanExtra(EXTRA_FORWARD, false);
		if (mIsReply) {
			users.view.setVisibility(View.GONE);
			title.view.setVisibility(View.GONE);
			findViewById(R.id.title_label).setVisibility(View.GONE);
			findViewById(R.id.users_label).setVisibility(View.GONE);
			findViewById(R.id.btn_get_user).setVisibility(View.GONE);
			/*
			 * getSupportLoaderManager().initLoader(LOADER_MESSAGE,
			 * getIntent().getExtras(), this);
			 * getSupportLoaderManager().initLoader(LOADER_TITLE,
			 * getIntent().getExtras(), this);
			 */
		} else {
			final MultiAutoCompleteTextView m = (MultiAutoCompleteTextView) users.view;
			final CharSequenceArrayAdapter adapter = new CharSequenceArrayAdapter(
					this, android.R.layout.simple_dropdown_item_1line,
					android.R.id.text1, new AutocompleteNetworkProcessor(this),
					m);
			m.setAdapter(adapter);
			mTokenizer = new MultiAutoCompleteTextView.CommaTokenizer();
			m.addTextChangedListener(new MultiSuggestTextWatcher(m, mTokenizer));

			m.setTokenizer(mTokenizer);
		}

		if (mIsForward) {
			final String subject = getIntent().getStringExtra(EXTRA_SUBJECT);
			title.view.setText(getString(R.string.message_forward_subject, subject));
		}

		findViewById(R.id.btn_get_user).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(final View v) {
						final Intent i = new Intent(MessageCreateActivity.this,
								PeopleListActivity.class);
						i.setAction(PeopleListActivity.ACTION_GET_CONTACT);
						i.putExtra(PeopleListActivity.EXTRA_FRIEND_ONLY, true);
						startActivityForResult(i,
								PeopleListActivity.REQUEST_GET_CONTACT);

					}
				});

		final String autoAddSenderId = getIntent().getStringExtra(
				MenuMessageCreateFragment.EXTRA_SENDER_ID);
		final String autoAddSenderName = getIntent().getStringExtra(
				MenuMessageCreateFragment.EXTRA_SENDER_NAME);
		addUserToAutocomplete(autoAddSenderId, autoAddSenderName);
	}

	@Override
	protected void onNewIntent(final Intent intent) {
		setLoginOnly(true);
		super.onNewIntent(intent);

		final String type = getIntent().getType();
		if (type != null && type.indexOf("image/") != -1) {
			processImageFromData(intent);
		}
	}

	@Override
	protected void onResume() {
		setLoginOnly(true);
		super.onResume();
		((MHOApplication) getApplication()).getNodeService(this);
	}

	@Override
	protected void onPause() {
		if (mService != null) {
			mService.unregisterMessageCallback(mMessageReceiver);
			mService = null;
		}
		super.onPause();
	}

	@Override
	public void onNodeConnected(final NodeWorkerService service) {
		service.registerMessageCallback(mMessageReceiver);
		mService = service;
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		attachMenuItem = menu.add(Menu.NONE, MENU_ID_ATTACH, Menu.NONE,
				R.string.menu_attach);
		attachMenuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		attachMenuItem.setIcon(R.drawable.ic_message_attach);
		final MenuItem send = menu.add(Menu.NONE, MENU_ID_SEND, Menu.NONE,
				R.string.menu_send);
		send.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		if (encodedFile != null)
			attachMenuItem.setEnabled(false);
		attachMenuItem.setVisible(!mIsForward);

		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			if (!isIsSended()) {
				final ConfirmDialog d = new ConfirmDialog();
				final Bundle b = new Bundle(1);
				b.putBoolean(ConfirmDialog.EXTRA_BACK_PRESSED, false);
				d.setArguments(b);
				d.show(getSupportFragmentManager(), TAG);
			} else
				NavUtils.navigateUpFromSameTask(this);
			return true;
		case MENU_ID_ATTACH:
			showFileChooser();
			return true;
		case MENU_ID_SEND:
			sendMessage();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private boolean validate() {
		if (mIsReply)
			return message.isValid();
		else
			return (users.isValid() || !users.view.isEnabled())
					&& message.isValid() && title.isValid();
	}

	private void showFileChooser() {
		final Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setType("*/*");
		intent.addCategory(Intent.CATEGORY_OPENABLE);

		try {
			startActivityForResult(
					Intent.createChooser(intent, "Select a File to Upload"),
					FILE_SELECT_CODE);
		} catch (final android.content.ActivityNotFoundException ex) {
			// Potentially direct the user to the Market with a Dialog
			Toast.makeText(this, "Please install a File Manager.",
					Toast.LENGTH_SHORT).show();
		}
	}

	private void sendMessage() {
		if (validate()) {
			final Editable text = users.view.getText();
			final String[] ids = text.getSpans(0, text.length(), String.class);
			final String messageStr = message.view.getText().toString();
			final String titleStr = title.view.getText().toString();

			final Message.Entity message = new Message.Entity();
			message.message = messageStr;
			if (StringUtils.isNotEmpty(encodedFile))
				message.attachments = encodedFile;
			try {
				if (mEncidongThread != null && mEncidongThread.isAlive()) {
					// wait for a sec for decode ends, if deconding is not yet
					// finished
					mEncidongThread.join(1000);
				}
				if (mIsReply) {
					if (StringUtils.isNotEmpty(messageStr))
						mService.replyMessage(threadId, messageStr,
								encodedFile, encodedFileName);
				} else {
					mService.sendMessage(titleStr, ids, message,
							encodedFileName, mIsForward, threadId);
				}

				setIsSended(true);
				ToastUtil.showText(this, R.string.message_sent_successfully);
				finish();
			} catch (final JSONException e) {
				ToastUtil.showText(this, "Error sending message");
			} catch (final InterruptedException e) {
				ToastUtil.showText(this, "Error encoding file. Message not sended");
				e.printStackTrace();
			}

		}
	}

	private void addUserToAutocomplete(final String id, final String name) {
		if (StringUtils.isEmpty(id))
			return;

		if (!isUserAlreadyAdded(id)) {
			final CharSequence newUser = CharSequenceArrayAdapter
					.convertToString(this, id, name);
			Log.i(TAG, "::onActivityResult:" + "id = " + id + ", name = "
					+ name);

			final MultiAutoCompleteTextView edit = (MultiAutoCompleteTextView) users.view;
			CharSequence text = edit.getText();
			if (!TextUtils.isEmpty(text)) {
				text = mTokenizer.terminateToken(text);
			}
			final SpannableStringBuilder str = new SpannableStringBuilder(text);
			str.append(mTokenizer.terminateToken(newUser));
			edit.setFocusable(false);
			edit.setFocusableInTouchMode(false);
			edit.setText(str);
			edit.setFocusable(true);
			edit.setFocusableInTouchMode(true);
			edit.requestFocus();
		} else {
			ToastUtil.showText(this, R.string.error_user_already_in_the_list);
		}

	}

	@Override
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent data) {
		switch (requestCode) {
		case FILE_SELECT_CODE:
			if (resultCode == RESULT_OK) {
				processImageFromData(data);
			}
			break;
		case PeopleListActivity.REQUEST_GET_CONTACT:

			if (resultCode == RESULT_OK) {
				final Long id = data.getLongExtra(PeopleListActivity.EXTRA_ID,
						0);
				final String idStr = id.toString();
				final String name = data
						.getStringExtra(PeopleListActivity.EXTRA_NAME);
				addUserToAutocomplete(idStr, name);
			}

			break;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	private boolean isUserAlreadyAdded(final String id) {
		final Editable text = users.view.getText();
		final String[] ids = text.getSpans(0, text.length(), String.class);
		return ArrayUtils.contains(ids, id);
	}

	private void processImageFromData(final Intent data) {
		final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
		// Get the Uri of the selected file
		Uri uri = data.getData();
		mFileType = data.getType();
		if (uri == null) {
			uri = (Uri) data.getExtras().get(Intent.EXTRA_STREAM);
		}
		// Get the path
		final String path = FileUtils.getPath(this, uri);
		Log.d(TAG, "File Path: " + path);
		final File file = new File(path);

		if (file.length() > Settings.FILE_SIZE_LIMIT) {
			ToastUtil.showError(this, getString(R.string.error_file_too_big));
			return;
		}

		final View attach = inflater.inflate(R.layout.item_attachment,
				attachments);
		attach.setTag(file);

		final TextView filename = (TextView) attach.findViewById(R.id.filename);
		filename.setText(file.getName());
		encodedFileName = file.getName();

		String type = null;
		final String extension = MimeTypeMap.getFileExtensionFromUrl(path);
		if (extension != null) {
			final MimeTypeMap mime = MimeTypeMap.getSingleton();
			type = mime.getMimeTypeFromExtension(extension);
		}
		if (type != null && type.contains("image")) {
			final ImageView image = (ImageView) attach
					.findViewById(R.id.preview);
			final BitmapFactory.Options opts = new BitmapFactory.Options();
			opts.inSampleSize = 8;
			/*
			 * final Bitmap bmp = BitmapFactory.decodeFile(
			 * file.getAbsolutePath(), opts);
			 */
			java.io.InputStream is = null;
			try {
				is = getContentResolver().openInputStream(uri);
				final Bitmap bmp = BitmapFactory.decodeStream(is, null, opts);
				image.setImageBitmap(bmp);
				System.gc();
			} catch (final FileNotFoundException e) {
				e.printStackTrace();
			} finally {
				try {
					if (is != null)
						is.close();
				} catch (final IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		final Button cancel = (Button) attach.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				final ViewParent parent = v.getParent();
				attachments.removeView((View) parent);
				attachMenuItem.setEnabled(true);
			}
		});
		// do encoding in separate thread
		mEncidongThread = new Thread(new Runnable() {

			@Override
			public void run() {
				// encode file
				try {
					final String type;
					if (StringUtils.isEmpty(mFileType)) {
						final MimeTypeMap mime = MimeTypeMap.getSingleton();
						type = mime.getMimeTypeFromExtension(extension);
					} else {
						type = mFileType;
					}
					Log.i(TAG, "::run:" + "MIME filetype - " + type);
					Log.i(TAG, "::run:" + "MIME returned filetype - "
							+ mFileType);
					if (StringUtils.isEmpty(type)) {
						runOnUiThread(new Runnable() {

							@Override
							public void run() {
								ToastUtil.showText(MessageCreateActivity.this,
										"Could not detect file type of "
												+ encodedFileName);

							}
						});
					}
					encodedFile = "data:" + type + ";base64,";
					encodedFile += com.mobile.health.one.util.Base64.encodeFromFile(file
							.getAbsolutePath());
					//encodedFile += strBuilder.toString();
					Log.i(TAG, "BASE64: " + encodedFile);
				} catch (final IOException e) {
					e.printStackTrace();
				} catch (final OutOfMemoryError error) {
					System.gc();
					MessageCreateActivity.this.runOnUiThread(new Runnable() {
					    @Override
						public void run() {
					    	Toast.makeText(
					    			MessageCreateActivity.this,
									R.string.error_file_not_enough_memory,
									Toast.LENGTH_SHORT).show();
					    }
					});
				}

			}
		});
		mEncidongThread.start();

		if (attachMenuItem != null)
			attachMenuItem.setEnabled(false);
	}

	@Override
	public void onBackPressed() {
		if (!isIsSended()) {
			final ConfirmDialog d = new ConfirmDialog();
			final Bundle b = new Bundle(1);
			b.putBoolean(ConfirmDialog.EXTRA_BACK_PRESSED, true);
			d.setArguments(b);
			d.show(getSupportFragmentManager(), TAG);
		} else
			super.onBackPressed();
	}

	/**
	 * Service binder stuff
	 */

	public boolean isIsSended() {
		return mIsSended;
	}

	public void setIsSended(final boolean mIsSended) {
		this.mIsSended = mIsSended;
	}

	/** Messenger for communicating with the service. */
	NodeWorkerService mService = null;

	private Handler mHandler;
}

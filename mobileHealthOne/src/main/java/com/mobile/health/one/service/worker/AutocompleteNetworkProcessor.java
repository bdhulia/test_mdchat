package com.mobile.health.one.service.worker;

import android.content.Context;
import android.net.Uri;

import com.mobile.health.one.util.Settings;

/**
 * @author Igor Yanishevskiy
 */
public class AutocompleteNetworkProcessor extends AutocompleteSuggestProcessor {

	public AutocompleteNetworkProcessor(final Context context) {
		super(context);
	}

	private static final Uri URI = Uri
			.parse(Settings.URI_BASE + "/api/api_search_suggest/network");

	@Override
	public Uri getUri() {
		return URI;
	}

}

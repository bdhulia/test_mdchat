package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Fellowship {
	public static class Entity implements Parcelable {
		@SerializedName("speciality")
		public String speciality;
		@SerializedName("hospital")
		public String hospital;

		public Entity(final Parcel in) {
			speciality = in.readString();
			hospital = in.readString();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(hospital);
			dest.writeString(speciality);
		}
		
		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};
	}
}
package com.mobile.health.one.app;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.search.TextTask.Listener;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.worker.GetGroupsProcessor;
import com.mobile.health.one.util.CursorSaver;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.SearchParams.AliasVariant;
import com.mobile.health.one.util.SearchParams.SearchType;

public class GroupListActivity extends AppFragmentActivity implements OnNavigationListener, OnCheckedChangeListener, Listener {

	private SearchFragment mSearchFragment;

	private boolean startSearch = false;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// getSupportActionBar().setListNavigationCallbacks(this.adapter, this);
		setContentView(R.layout.activity_groups);

		final CharSequence searchText = AliasVariant.getSearchParam(getIntent());
		if (!TextUtils.isEmpty(searchText)) {
			startSearch = true;
			mSearchFragment = SearchFragment.newInstance(this, SearchType.GROUPS_MY, searchText);
			getSupportActionBar().setDisplayShowHomeEnabled(false);
		} else {
			mSearchFragment = SearchFragment.newInstance(this, SearchType.GROUPS_MY);
		}

		mGroupFragment = GroupListFragment.newInstance(startSearch);

		mFragmentManager = getSupportFragmentManager();
		mFragmentManager.beginTransaction()
				.add(R.id.list, mGroupFragment)
				.add(mSearchFragment, SearchFragment.TAG)
				.commit();

//		final RadioGroup filter = (RadioGroup) findViewById(R.id.filter);
//		filter.setOnCheckedChangeListener(this);

		//showTab(R.id.filter_contacts);
		mType = GetGroupsProcessor.TYPE_PRIVATE;
		mSearchFragment.setSearchType(SearchType.GROUPS_MY);

		mGroupFragment.setInvitesType(mType);
		if (mGroupFragment.isAdded())
			mGroupFragment.loadData();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menu_labels:
			final Intent i = new Intent(this, LabelsActivity.class);
			startActivityForResult(i, LabelsListFragment.REQUEST_GET_LABEL);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(final int itemPosition,
			final long itemId) {
		return false;
	}

	@Override
	public void onCheckedChanged(final RadioGroup group, final int checkedId) {
		//showTab(checkedId);
	}

	private GroupListFragment mGroupFragment;
	private int mType;
	private FragmentManager mFragmentManager;

//	private void showTab(final int id) {
//		switch (id) {
//		case R.id.filter_private:
//			mType = GetGroupsProcessor.TYPE_PRIVATE;
//			mSearchFragment.setSearchType(SearchType.GROUPS_MY);
//
//			break;
//		case R.id.filter_public:
//			mType = GetGroupsProcessor.TYPE_PUBLIC;
//			mSearchFragment.setSearchType(SearchType.GROUPS_PUBLIC);
//
//			break;
//		default:
//			mType = GetGroupsProcessor.TYPE_PRIVATE;
//			break;
//		}
//		mGroupFragment.setInvitesType(mType);
//		if (mGroupFragment.isAdded())
//			mGroupFragment.loadData();
//	}

	@Override
	public void onSearchStart() {
		setSupportProgressBarIndeterminateVisibility(true);
	}

	@Override
	public void onSearchComplete(final IResponce responce) {
		final CursorSaver r = (CursorSaver) responce;
		final Cursor c = r.saveToCursor();
		final GroupListFragment f = mGroupFragment;
		if (c != null && c.getCount() > 0) {
			f.setCursorSavingOld(c);
			f.setSearch(true);
		} else {
			f.setCursorSavingOld(null);
			f.setSearch(false);
		}

		if (f.getView() != null)
			f.setListShown(true);
		setSupportProgressBarIndeterminateVisibility(false);
	}

	@Override
	public void onSearchError(final String error) {
		setSupportProgressBarIndeterminateVisibility(false);
		ToastUtil.showText(this, error);
	}

	@Override
	public void onSearchRestart() {
		final GroupListFragment f = mGroupFragment;
		f.setSearch(false);
		f.setOldCursor();
	}

	@Override
	public void onSearchCancel() {
		/*final GroupListFragment f = mGroupFragment;
		f.setSearch(false);*/
	}

}

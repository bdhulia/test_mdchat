package com.mobile.health.one.app;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;

import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.app.ChangePinFragment.RegisterPinCallback;

public class ChangePinActivity extends AppFragmentActivity implements RegisterPinCallback {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.change_pin);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(android.R.id.content, new ChangePinFragment()).commit();
	}

	@Override
	public void onPinRegistered() {
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

}

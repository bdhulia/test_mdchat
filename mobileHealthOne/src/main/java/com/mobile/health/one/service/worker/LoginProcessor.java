package com.mobile.health.one.service.worker;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.util.Settings;

/**
 * @author Igor Yanishevskiy
 */
public class LoginProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_LOGIN = "username";
	public static final String EXTRA_PASS = "password";
	public static final String EXTRA_NEED_PIN = "need_pin";
	public static final String DESCRIPTION = "description";

	private static final Uri URI_BASE = Uri.parse(Settings.URI_BASE
			+ "/api_login");

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);
	private Uri uri;

	public LoginProcessor(final Context context) {
		super(context);
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		final RequestParams params = new BasicRequestParams();
		params.add(DESCRIPTION, this.getDeviceName());

		this.uri = URI_BASE;
		this.uri = this.uri.buildUpon()
				.appendPath(extras.getString(EXTRA_LOGIN))
				.appendPath(extras.getString(EXTRA_PASS))
				.appendPath(this.androidId).build();

		tryRequest(receiver, true, params, Responce.class, extras);

	}

	@Override
	public Uri getUri() {
		return this.uri;
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		final Responce resp = (Responce) result;
		final String token = resp.getData().get("token");
		final int needPin = Integer.parseInt(resp.getData().get("need_pin"));
		extras.putString(EXTRA_TOKEN, token);
		extras.putInt(EXTRA_NEED_PIN, needPin);
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

	private String getDeviceName() {
		String manufacturer = Build.MANUFACTURER;
		String model = Build.MODEL;
		
		StringBuffer deviceModel = new StringBuffer("Android ");
		
		if (model.startsWith(manufacturer)) {
			deviceModel.append(capitalize(model));
		} else {
			deviceModel.append(capitalize(manufacturer));
			deviceModel.append(" ");
			deviceModel.append(model);
		}
		
		return deviceModel.toString();
	}

	private String capitalize(String s) {
		if (s == null || s.length() == 0) {
			return "";
		}
		char first = s.charAt(0);
		if (Character.isUpperCase(first)) {
			return s;
		} else {
			return Character.toUpperCase(first) + s.substring(1);
		}
	}
}

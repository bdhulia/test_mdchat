package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.mobile.health.one.db.entity.User;

public class Group implements Parcelable {

	public static class Entity implements BaseColumns {
		public final static String SHORT_NAME = "short_name";
		public final static String LONG_NAME = "long_name";
		public final static String DESCRIPTION = "description";
		public final static String IS_PUBLIC = "is_public";
		public final static String STREET_ADDRESS_1 = "street_address_1";
		public final static String STREET_ADDRESS_2 = "street_address_2";
		public final static String CITY = "city";
		public final static String STATE = "state";
		public final static String ZIP_CODE = "zip_code";
        public final static String URL = "url";
		public final static String PHONE = "phone";
		public final static String FAX = "fax";
		public final static String AVATAR_LINK = "avatar_link";
		public final static String ADMIN_ID = "admin_id";
		public final static String ADMIN_NAME = "admin_name";
	}

	public final static String[] PROJECTION = new String[] {
		BaseColumns._ID,
		Entity.SHORT_NAME,
		Entity.LONG_NAME,
		Entity.DESCRIPTION,
		Entity.IS_PUBLIC,
		Entity.STREET_ADDRESS_1,
		Entity.STREET_ADDRESS_2,
		Entity.CITY,
		Entity.STATE,
		Entity.ZIP_CODE,
        Entity.URL,
		Entity.PHONE,
		Entity.FAX,
		Entity.AVATAR_LINK,
		Entity.ADMIN_NAME
	};

	public long id;
	public String short_name;
	public String long_name;
	public String description;
	public boolean is_public;
	public String street_address_1;
	public String street_address_2;
	public String city;
	public String state;
	public String zip_code;
	public String url;
	public String phone;
	public String fax;
	public String avatar_link;
	public User.Entity admin;

	public Group(final Parcel in) {
		id = in.readLong();
		short_name = in.readString();
		long_name = in.readString();
		description = in.readString();
		is_public = in.readByte() == 1 ? true : false;
		street_address_1 = in.readString();
		street_address_2 = in.readString();
		city = in.readString();
		state = in.readString();
		zip_code = in.readString();
		url = in.readString();
		phone = in.readString();
		fax = in.readString();
		avatar_link = in.readString();
		admin = in.readParcelable(User.Entity.class.getClassLoader());
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeLong(id);
		dest.writeString(short_name);
		dest.writeString(long_name);
		dest.writeString(description);
		dest.writeByte((byte) (is_public ? 1 : 0));
		dest.writeString(street_address_1);
		dest.writeString(street_address_2);
		dest.writeString(city);
		dest.writeString(state);
		dest.writeString(zip_code);
		dest.writeString(url);
		dest.writeString(phone);
		dest.writeString(fax);
		dest.writeString(avatar_link);
		dest.writeParcelable(admin, flags);
	}

	public static final Parcelable.Creator<Group> CREATOR = new Parcelable.Creator<Group>() {
		@Override
		public Group createFromParcel(final Parcel in) {
			return new Group(in);
		}

		@Override
		public Group[] newArray(final int size) {
			return new Group[size];
		}
	};

}

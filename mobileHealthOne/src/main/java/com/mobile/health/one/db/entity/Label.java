package com.mobile.health.one.db.entity;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

public class Label implements BaseColumns {

	public static class Entity implements Parcelable {
		public String id;
		public String labelName;
		public boolean isRoot;
		public String visible;
		public int count;

		public Entity(final Parcel in) {
			id = in.readString();
			labelName = in.readString();
			isRoot = (Boolean) in.readValue(Boolean.class.getClassLoader());
			visible = in.readString();
			count = in.readInt();
		}

		public Entity() {
		};

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(id);
			dest.writeString(labelName);
			dest.writeValue(isRoot);
			dest.writeString(visible);
			dest.writeInt(count);
		}

		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};

		public ContentValues prepareValues() {
			final ContentValues vals = new ContentValues();
			vals.put(REMOTE_ID, id);
			vals.put(TITLE, labelName);
			vals.put(COUNT, count);
			vals.put(IS_ROOT, isRoot);
			return vals;
		}


		@Override
		public String toString() {
			return id;
		}
	}

	public static final String PATH = "label";

	public static final String CREATED_DATE = "created";

	public static final String TITLE = "title";

	public static final String COUNT = "count";

	public static final String IS_ROOT = "is_root";

	public static final String REMOTE_ID = "remoteId";

}

package com.mobile.health.one.util;

import com.mobile.health.one.BuildConfig;

public class Settings {
	public final static String URI_BASE = "https://" + (BuildConfig.IS_DEMO ? "deddemo" : "www") + ".mdchat.com";
	//public final static String URI_BASE = "https://www.mdchat.com";
	public final static String SENDER_ID = "4424953717";
	public static final long INACTIVITY_TIME = 8 * 60 * 60 * 1000; // 8 h delay
	public static final long FILE_SIZE_LIMIT = 2 * 1024 * 1024; // 2 MB
}

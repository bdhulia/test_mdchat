/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.mobile.health.one.util.CursorSaver;
import com.mobile.health.one.util.DataSaver;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class ThreadsResponce implements Parcelable, IResponce, CursorSaver, DataSaver {

	public static class Data implements Parcelable, CursorSaver {
		Pagination pagination;
		MessageThread[] threads;
		// search returns messages sometimes
		MessageThread[] messages;

		public Data(final Parcel in) {
			pagination = in.readParcelable(Pagination.class.getClassLoader());
			threads = in.createTypedArray(MessageThread.CREATOR);
			messages = in.createTypedArray(MessageThread.CREATOR);
		}
		@Override
		public int describeContents() {
			return 0;
		}
		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeParcelable(pagination, flags);
			dest.writeTypedArray(threads, flags);
			dest.writeTypedArray(messages, flags);
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};

		@Override
		public MatrixCursor saveToCursor() {
			final MessageThread[] data = threads != null ? threads : messages;
			final MatrixCursor cursor = new MatrixCursor(MessageThread.PROJECTION, data.length);
			for (final MessageThread messageThread : data) {
				String user_name = messageThread.full_user_name;
				if (StringUtils.isEmpty(messageThread.full_user_name))
					user_name = messageThread.sender_name;

				String avatar = messageThread.avatar_small;
				if (StringUtils.isEmpty(messageThread.avatar_small))
					avatar = messageThread.sender_avatar;

				cursor.newRow()
					.add(messageThread.id)
					.add(messageThread.avatar_normal)
					.add(messageThread.message)
					.add(user_name)
					.add(messageThread.unreadMessages)
					.add(messageThread.subject)
					.add(messageThread.user_id)
					.add(messageThread.date)
					.add(messageThread.thread_id)
					.add(avatar);
			}
			return cursor;
		}
	}

	protected int code;

	private Data data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<ThreadsResponce> CREATOR = new Parcelable.Creator<ThreadsResponce>() {
		@Override
		public ThreadsResponce createFromParcel(final Parcel in) {
			return new ThreadsResponce(in);
		}

		@Override
		public ThreadsResponce[] newArray(final int size) {
			return new ThreadsResponce[size];
		}
	};

	public ThreadsResponce() {
		code = 0;
	}

	public ThreadsResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Data getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	/**
	 * Stub for saving to matrix cursor
	 */
	@Override
	public MatrixCursor saveToCursor() {
		return data.saveToCursor();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<HashMap<String, Object>> saveData() {
		final MessageThread[] data = getData().threads != null ? getData().threads : getData().messages;
		final ArrayList<HashMap<String, Object>> cursor = new ArrayList<HashMap<String, Object>>(data.length);
		for (final MessageThread messageThread : data) {
			String user_name = messageThread.full_user_name;
			if (StringUtils.isEmpty(messageThread.full_user_name))
				user_name = messageThread.sender_name;

			String avatar = messageThread.avatar_small;
			if (StringUtils.isEmpty(messageThread.avatar_small))
				avatar = messageThread.sender_avatar;

			final HashMap<String, Object> row = new HashMap<String, Object>();

			int i = 0;
			row.put(MessageThread.PROJECTION[i++], messageThread.id);
			row.put(MessageThread.PROJECTION[i++], messageThread.avatar_normal);
			row.put(MessageThread.PROJECTION[i++], messageThread.message);
			row.put(MessageThread.PROJECTION[i++], user_name);
			row.put(MessageThread.PROJECTION[i++], messageThread.unreadMessages);
			row.put(MessageThread.PROJECTION[i++], messageThread.subject);
			row.put(MessageThread.PROJECTION[i++], messageThread.user_id);
			row.put(MessageThread.PROJECTION[i++], messageThread.date);
			row.put(MessageThread.PROJECTION[i++], messageThread.thread_id);
			row.put(MessageThread.PROJECTION[i++], avatar);
		}
		return cursor;
	}
}

package com.mobile.health.one.widget;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;

import com.mobile.health.one.R;

public class TopBarLabelsView extends FrameLayout {

	private final TextView mTitleText;

	@SuppressWarnings("deprecation")
	public TopBarLabelsView(final Context context, final OnClickListener listener) {
		super(context);
		final Resources res = context.getResources();

		mTitleText = new TextView(context);
		mTitleText.setPadding(4, 4, 4, 4);
		mTitleText.setGravity(Gravity.CENTER);
		mTitleText.setBackgroundResource(R.drawable.top_bar_bg);
		mTitleText.setTextAppearance(context,
				android.R.style.TextAppearance_DeviceDefault_Medium);
		addView(mTitleText);
		final LayoutParams params = new FrameLayout.LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				res.getDimensionPixelSize(R.dimen.top_bar_height), Gravity.TOP);

		final ImageButton labelsBtn = new ImageButton(context);
		//labelsBtn.setId(R.id.labels);
		labelsBtn.setImageResource(R.drawable.btn_labels);
		labelsBtn.setBackgroundDrawable(null);
		labelsBtn.setOnClickListener(listener);
		addView(labelsBtn, new FrameLayout.LayoutParams(
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
				Gravity.LEFT | Gravity.CENTER_VERTICAL));
		setLayoutParams(params);
	}

	public void setTitleText(final CharSequence text) {
		if (TextUtils.isEmpty(text))
			mTitleText.setText(R.string.all_messages);
		else
			mTitleText.setText(text);
	}

	public void setVisible(final boolean isVisible) {
		this.setVisibility(isVisible ? View.VISIBLE : View.GONE);

	}

}

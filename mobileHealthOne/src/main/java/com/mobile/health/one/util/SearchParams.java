package com.mobile.health.one.util;

import java.util.EnumMap;

import org.apache.commons.lang.StringUtils;

import android.content.Context;
import android.content.Intent;

import com.mobile.health.one.R;
import com.mobile.health.one.app.GroupListActivity;
import com.mobile.health.one.app.MessageListActivity;
import com.mobile.health.one.app.PeopleListActivity;
import com.mobile.health.one.service.entity.GetContactsDataResponce;
import com.mobile.health.one.service.entity.GetDirectoryResponse;
import com.mobile.health.one.service.entity.GroupsResponce;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.MessagesResponce;
import com.mobile.health.one.service.entity.ThreadsResponce;
import com.mobile.health.one.service.worker.AutocompleteSearchProcessor;
import com.mobile.health.one.service.worker.SearchProcessor;
import com.mobile.health.one.util.SearchParams.AliasVariant.IntentBuilder;

public class SearchParams {

	public static class SearchVariant {
		private int nameResId;
		private String name;
		private String[] formFields;
		private String endpoint;

		SearchVariant(final String name, final String formField) {
			this.name = name;
			this.setFormField(formField);
		}

		SearchVariant(final int nameResId, final String formField) {
			this.nameResId = nameResId;
			this.setFormField(formField);
		}

		SearchVariant(final int nameResId, final String[] formFields) {
			this.nameResId = nameResId;
			this.setFormFields(formFields);
		}

		SearchVariant(final String name, final String[] formFields) {
			this.name = name;
			this.setFormFields(formFields);
		}

		SearchVariant(final String name, final String[] formFields,
				final String endpoint) {
			this.name = name;
			this.setFormFields(formFields);
			this.setEndpoint(endpoint);
		}

		public String getName(final Context c) {
			if (StringUtils.isEmpty(name)) {
				return c.getString(nameResId);
			} else {
				return name;
			}
		}

		public String getName() {
			return name;
		}

		public void setFormField(final String formField) {
			formFields = new String[] { formField };
		}

		public String[] getFormFields() {
			return formFields;
		}

		public void setFormFields(final String[] formFields) {
			this.formFields = formFields;
		}

		public String getEndpoint() {
			return endpoint;
		}

		public void setEndpoint(final String endpoint) {
			this.endpoint = endpoint;
		}
	}

	public interface Param {
		public Param setHintTextResource(final int hintTextResource);

		public int getHintTextResource();
	}

	public static class SearchParam implements Param {
		private String[] searches;
		private String[] autocompleteSearches;
		private String[] formFields;
		private String[] autocompleteFormFields;
		private String formName;
		private String endoint;
		private String autocompleteEndoint;
		private String[] defaultFormFields;
		private int advancedSearchLayout;
		private int hintTextResource;
		private Class<? extends IResponce> responce;
		private SearchVariant[] variants;
		private String[] simpleSearchFormFields;
		private String[] simpleSearchAutocompleteSearches;

		public String[] getSearches() {
			return searches;
		}

		public SearchParams.SearchParam setSearches(final String[] searches) {
			this.searches = searches;
			return this;
		}

		public String[] getAutocompleteSearches() {
			return autocompleteSearches;
		}

		public SearchParams.SearchParam setAutocompleteSearches(
				final String[] searches) {
			autocompleteSearches = searches;
			return this;
		}

		public String[] getFormFields() {
			return formFields;
		}

		public SearchParams.SearchParam setFormFields(final String[] formFields) {
			this.formFields = formFields;
			return this;
		}

		public String[] getAutocompleteFormFields() {
			return autocompleteFormFields;
		}

		public SearchParams.SearchParam setAutocompleteFormFields(
				final String[] formFields) {
			autocompleteFormFields = formFields;
			return this;
		}

		public String getFormName() {
			return formName;
		}

		public SearchParams.SearchParam setFormName(final String formName) {
			this.formName = formName;
			return this;
		}

		public String getEndoint() {
			return endoint;
		}

		public SearchParams.SearchParam setEndoint(final String endoint) {
			this.endoint = endoint;
			return this;
		}

		public String getAutocompleteEndoint() {
			return autocompleteEndoint;
		}

		public SearchParams.SearchParam setAutocompleteEndoint(
				final String endoint) {
			autocompleteEndoint = endoint;
			return this;
		}

		public Class<? extends IResponce> getResponce() {
			return responce;
		}

		public SearchParams.SearchParam setResponce(
				final Class<? extends IResponce> responce) {
			this.responce = responce;
			return this;
		}

		public String[] getDefaultFormFields() {
			return defaultFormFields;
		}

		public SearchParams.SearchParam setDefaultFormField(
				final String defaultFormField) {
			defaultFormFields = new String[] { defaultFormField };
			return this;
		}

		public SearchParams.SearchParam setDefaultFormFields(
				final String[] defaultFormFields) {
			this.defaultFormFields = defaultFormFields;
			return this;
		}

		public int getAdvancedSearchLayout() {
			return advancedSearchLayout;
		}

		public SearchParams.SearchParam setAdvancedSearchLayout(
				final int advancedSearchLayout) {
			this.advancedSearchLayout = advancedSearchLayout;
			return this;
		}

		@Override
		public int getHintTextResource() {
			return hintTextResource;
		}

		@Override
		public SearchParams.SearchParam setHintTextResource(
				final int hintTextResource) {
			this.hintTextResource = hintTextResource;
			return this;
		}

		public SearchVariant[] getVariants() {
			return variants;
		}

		public SearchParams.SearchParam setVariants(
				final SearchVariant[] variants) {
			this.variants = variants;
			return this;
		}

		public SearchParams.SearchParam setFormFieldsSimpleSearchOnly(
				final String[] simpleSearchFormFields) {
			this.simpleSearchFormFields = simpleSearchFormFields;
			return this;
		}

		public SearchParam setSearchesSimpleSearchOnly(final String[] searches) {
			simpleSearchAutocompleteSearches = searches;
			return this;
		}

		public String[] getFormFieldsSimpleSearchOnly() {
			return simpleSearchFormFields;
		}

		public String[] getSearchesSimpleSearchOnly() {
			return simpleSearchAutocompleteSearches;
		}
	}

	public static class AliasParam implements Param {
		private AliasVariant[] aliasVariants;
		private int hintTextResource;

		public AliasVariant[] getAliasVariants() {
			return aliasVariants;
		}

		public AliasParam setAliasVariants(final AliasVariant[] aliasVariants) {
			this.aliasVariants = aliasVariants;
			return this;
		}

		@Override
		public int getHintTextResource() {
			return hintTextResource;
		}

		@Override
		public AliasParam setHintTextResource(final int hintTextResource) {
			this.hintTextResource = hintTextResource;
			return this;
		}
	}

	public static class AliasVariant {

		public static class IntentBuilder {
			private static final String EXTRA_SEARCH_PARAM = "search_param";
			public static final String EXTRA_FROM_DASHBOARD = "fromDashboard";

			private Class<?> clazz;
			private CharSequence searchParam;

			public IntentBuilder setClass(final Class<?> clazz) {
				this.clazz = clazz;
				return this;
			}

			public IntentBuilder setSearchParam(final CharSequence search) {
				searchParam = search;
				return this;
			}

			public Intent build(final Context c) {
				final Intent i = new Intent(c, clazz);
				i.putExtra(EXTRA_FROM_DASHBOARD, true);
				i.putExtra(EXTRA_SEARCH_PARAM, searchParam);
				return i;
			}
		}

		private final IntentBuilder intentBuilder;
		private final String searchName;
		private int nameResId;

		AliasVariant(final String name, final IntentBuilder builder) {
			searchName = name;
			intentBuilder = builder;
		}

		AliasVariant(final int nameResId, final IntentBuilder builder) {
			this.nameResId = nameResId;
			intentBuilder = builder;
			searchName = null;
		}

		public static CharSequence getSearchParam(final Intent i) {
			return i.getCharSequenceExtra(IntentBuilder.EXTRA_SEARCH_PARAM);
		}

		public String getSearchName(final Context c) {
			if (searchName != null)
				return searchName;
			else
				return c.getString(nameResId);
		}

		public Intent getIntent(final Context c) {
			return intentBuilder.build(c);
		}

		public void setSearchParam(final CharSequence search) {
			intentBuilder.searchParam = search;
		}
	}

	public static enum SearchType {
		USERS, MEMBERS, HCP, GROUPS_MY, GROUPS_PUBLIC, THREADS, MESSAGES, DASHBOARD;

		public static final int size = SearchType.values().length;
	}

	private static EnumMap<SearchParams.SearchType, SearchParams.Param> mSearchParams;

	public static SearchParams.Param getParams(
			final SearchParams.SearchType type) {
		return mSearchParams.get(type);
	}

	static {
		mSearchParams = new EnumMap<SearchParams.SearchType, SearchParams.Param>(
				SearchParams.SearchType.class);
		mSearchParams.put(
				SearchType.USERS,
				new SearchParam()
						.setEndoint(SearchProcessor.URI_CONTACTS)
						.setAutocompleteEndoint(
								AutocompleteSearchProcessor.URI_CONTACTS)
						.setFormName("user_search")
						.setResponce(GetContactsDataResponce.class)
						.setAdvancedSearchLayout(R.layout.fragment_user_search)
						.setHintTextResource(R.string.search_people_hint)
						.setDefaultFormField("user_search[name]"));
		mSearchParams.put(
				SearchType.MEMBERS,
				new SearchParam()
						.setEndoint(SearchProcessor.URI_MEMBERS)
						.setAutocompleteEndoint(
								AutocompleteSearchProcessor.URI_MEMBERS)
						.setFormName("user_search")
						.setResponce(GetContactsDataResponce.class)
						.setHintTextResource(R.string.search_people_hint)
						.setAdvancedSearchLayout(R.layout.fragment_user_search)
						.setDefaultFormField("user_search[name]"));
		mSearchParams.put(
				SearchType.HCP,
				new SearchParam()
						.setEndoint(SearchProcessor.URI_MEMBERS)
						.setAutocompleteEndoint(
								AutocompleteSearchProcessor.URI_MEMBERS)
						.setFormName("user_search")
						.setFormFields(new String[] { "user_search[is_hcp]" })
						.setSearches(new String[] { "1" })
						.setAutocompleteFormFields(new String[] { "only_hcp" })
						.setAutocompleteSearches(new String[] { "1" })
						.setResponce(GetDirectoryResponse.class)
						.setAdvancedSearchLayout(
								R.layout.fragment_user_directory_search)
						.setHintTextResource(R.string.search_people_hint)
						.setDefaultFormField("user_search[name]"));
		mSearchParams
				.put(SearchType.GROUPS_MY,
						new SearchParam()
								.setEndoint(SearchProcessor.URI_GROUPS)
								.setAutocompleteEndoint(
										AutocompleteSearchProcessor.URI_GROUPS)
								.setFormName("group_search")
								.setFormFieldsSimpleSearchOnly(
										new String[] { "is_or_mode" })
								.setSearchesSimpleSearchOnly(
										new String[] { "1" })
								.setResponce(GroupsResponce.class)
								.setAdvancedSearchLayout(
										R.layout.fragment_group_search)
								.setHintTextResource(
										R.string.search_groups_hint)
								.setDefaultFormFields(
										new String[] { "group_search[name]",
												"group_search[description]" }));
		mSearchParams
				.put(SearchType.GROUPS_PUBLIC,
						new SearchParam()
								.setEndoint(SearchProcessor.URI_GROUPS)
								.setAutocompleteEndoint(
										AutocompleteSearchProcessor.URI_GROUPS)
								.setFormName("group_search")
								.setFormFields(
										new String[] { "group_search[is_public]" })
								.setSearches(new String[] { "1" })
								.setFormFieldsSimpleSearchOnly(
										new String[] { "is_or_mode" })
								.setSearchesSimpleSearchOnly(
										new String[] { "1" })
								.setResponce(GroupsResponce.class)
								.setAdvancedSearchLayout(
										R.layout.fragment_group_search)
								.setHintTextResource(
										R.string.search_groups_hint)
								.setDefaultFormFields(
										new String[] { "group_search[name]",
												"group_search[description]" }));
		mSearchParams
				.put(SearchType.MESSAGES,
						new SearchParam()
								.setEndoint(SearchProcessor.URI_MESSAGES)
								.setAutocompleteEndoint(
										AutocompleteSearchProcessor.URI_MESSAGES)
								.setFormName("local_message_search")
								.setFormFields(
										new String[] { "compose", "global" })
								.setSearches(new String[] { "1", "0" })
								.setFormFieldsSimpleSearchOnly(
										new String[] { "local_message_search[is_or_mode]" })
								.setSearchesSimpleSearchOnly(
										new String[] { "1" })
								.setResponce(MessagesResponce.class)
								.setAdvancedSearchLayout(
										R.layout.fragment_message_search)
								.setHintTextResource(
										R.string.search_messages_hint)
								.setDefaultFormFields(
										new String[] {
												"local_message_search[has_words]",
												"local_message_search[receiver_name]",
												"local_message_search[sender_name]" }
										));
		mSearchParams
				.put(SearchType.THREADS,
						new SearchParam()
								.setEndoint(SearchProcessor.URI_THREADS)
								.setAutocompleteEndoint(
										AutocompleteSearchProcessor.URI_THREADS)
								.setFormName("global_message_search")
								.setResponce(ThreadsResponce.class)
								.setFormFields(
										new String[] { "compose", "global" })
								.setSearches(new String[] { "1", "1" })
								.setFormFieldsSimpleSearchOnly(
										new String[] { "local_message_search[is_or_mode]" })
								.setSearchesSimpleSearchOnly(
										new String[] { "1" })
								.setAdvancedSearchLayout(
										R.layout.fragment_threads_search)
								.setHintTextResource(
										R.string.search_messages_hint)
								.setDefaultFormField(
										"global_message_search[subject]")
								.setVariants(
										new SearchVariant[] {
												// TODO: move text to
												// resources
												new SearchVariant("Subject",
														"global_message_search[subject]"),
												new SearchVariant("From",
														"global_message_search[from_user]"),
												new SearchVariant("To",
														"global_message_search[to_user]"),
												new SearchVariant(
														"All",
														new String[] {
																"local_message_search[has_words]",
																"local_message_search[receiver_name]",
																"local_message_search[sender_name]" },
														SearchProcessor.URI_MESSAGES)
												}));
		mSearchParams
				.put(SearchType.DASHBOARD,
						new AliasParam()
								.setAliasVariants(
										new AliasVariant[] {
												new AliasVariant(
														R.string.search_people_hint,
														new IntentBuilder()
																.setClass(PeopleListActivity.class)),
												new AliasVariant(
														R.string.search_messages_hint,
														new IntentBuilder()
																.setClass(MessageListActivity.class)),
												new AliasVariant(
														R.string.search_groups_hint,
														new IntentBuilder()
																.setClass(GroupListActivity.class)) })
								.setHintTextResource(R.string.search_hint));
	}
}
package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Academic {
	public static class Entity implements Parcelable {
		@SerializedName("school")
		public String school;
		@SerializedName("title")
		public String title;
		@SerializedName("start_year")
		public String startYear;
		@SerializedName("end_year")
		public String endYear;

		public Entity(final Parcel in) {
			school = in.readString();
			title = in.readString();
			startYear = in.readString();
			endYear = in.readString();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(school);
			dest.writeString(title);
			dest.writeString(startYear);
			dest.writeString(endYear);
		}
		
		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};
	}
}
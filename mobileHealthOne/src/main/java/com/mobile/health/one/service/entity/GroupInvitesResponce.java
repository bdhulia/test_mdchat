/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.List;

import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.mobile.health.one.util.CursorSaver;

/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class GroupInvitesResponce implements Parcelable, IResponce {

	public static class Data implements Parcelable, CursorSaver {
		public Pagination pagination;
		public GroupInvite[] invites;

		public Data(final Parcel in) {
			pagination = in.readParcelable(Pagination.class.getClassLoader());
			invites = in.createTypedArray(GroupInvite.CREATOR);
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeParcelable(pagination, flags);
			dest.writeTypedArray(invites, flags);
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};

		public static final String[] PROJECTION = new String[] { BaseColumns._ID,
				GroupInvite.Entity.GROUP_ID, GroupInvite.Entity.USER_ID,
				GroupInvite.Entity.GROUP_NAME,
				GroupInvite.Entity.AVATAR, GroupInvite.Entity.ADMIN_NAME,
				GroupInvite.Entity.MESSAGE, GroupInvite.Entity.STATUS };

		@Override
		public MatrixCursor saveToCursor() {
			if (invites != null) {
				final MatrixCursor cursor = new MatrixCursor(PROJECTION,
						invites.length);
				for (final GroupInvite invite : invites) {
					cursor.newRow()
					.add(invite.group_id)
					.add(invite.group_id)
					.add(invite.user_id)
					.add(invite.group_name)
					.add(invite.avatar_link)
					.add(invite.admin)
					.add(invite.message)
					.add(invite.status);
				}
				return cursor;
			} else
				return null;
		}
	}

	protected int code;

	private Data data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<GroupInvitesResponce> CREATOR = new Parcelable.Creator<GroupInvitesResponce>() {
		@Override
		public GroupInvitesResponce createFromParcel(final Parcel in) {
			return new GroupInvitesResponce(in);
		}

		@Override
		public GroupInvitesResponce[] newArray(final int size) {
			return new GroupInvitesResponce[size];
		}
	};

	public GroupInvitesResponce() {
		code = 0;
	}

	public GroupInvitesResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Data getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		return getData().invites.length;
	}
}

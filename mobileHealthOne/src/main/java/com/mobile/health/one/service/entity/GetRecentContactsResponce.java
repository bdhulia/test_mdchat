package com.mobile.health.one.service.entity;

import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.mobile.health.one.util.CursorSaver;


public class GetRecentContactsResponce implements Parcelable, IResponce, CursorSaver {

	private int code;

	private RecentUser.Entity[] data;

	public static final Parcelable.Creator<GetRecentContactsResponce> CREATOR = new Parcelable.Creator<GetRecentContactsResponce>() {
		@Override
		public GetRecentContactsResponce createFromParcel(final Parcel in) {
			return new GetRecentContactsResponce(in);
		}

		@Override
		public GetRecentContactsResponce[] newArray(final int size) {
			return new GetRecentContactsResponce[size];
		}
	};

	@Override
	public int getCode() {
		return code;
	}

	public GetRecentContactsResponce() {	}

	public GetRecentContactsResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.createTypedArray(RecentUser.Entity.CREATOR);
	}

	private String result;

	public String getResult() {
		return result;
	}

	public RecentUser.Entity[] getData() {
		return data;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeTypedArray(data, flags);
	}

	@Override
	public String getMessages() {
		// TODO Auto-generated method stub
		return null;
	}

	private static final String[] PROJECTION = new String[] { RecentUser._ID,
		RecentUser.FULL_NAME,
		RecentUser.SPECIALITY,
		RecentUser.STATUS,
		RecentUser.STATUS_TEXT,
		RecentUser.AVATAR_LINK};

	@Override
	public Cursor saveToCursor() {
		final MatrixCursor cursor = new MatrixCursor(PROJECTION, data.length);
		for (final RecentUser.Entity RecentUser : data) {
			final MatrixCursor.RowBuilder b = cursor.newRow();
			b.add(RecentUser.id);
			b.add(RecentUser.name);
			b.add(RecentUser.speciality);
			b.add(RecentUser.status);
			b.add(RecentUser.statusText);
			b.add(RecentUser.avatarLink);
		}
		return cursor;
	}

	@Override
	public int getCount() {
		return data.length;
	}
}
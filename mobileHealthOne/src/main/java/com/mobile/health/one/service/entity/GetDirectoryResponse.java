package com.mobile.health.one.service.entity;

import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.mobile.health.one.util.CursorSaver;

public class GetDirectoryResponse implements Parcelable, IResponce, CursorSaver {
	private int code;

	private Data data;

	@Override
	public int getCode() {
		return code;
	}

	public GetDirectoryResponse() {

	}

	public GetDirectoryResponse(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
	}

	private String result;

	public String getResult() {
		return result;
	}

	public Data getData() {
		return data;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, 0);
	}

	@Override
	public String getMessages() {
		return null;
	}

	private static final String[] PROJECTION = new String[] {
			DirectoryUserProfile._ID, DirectoryUserProfile.FULL_NAME,
			DirectoryUserProfile.SPECIALITY_AND_ADDRESS,
			DirectoryUserProfile.GENDER, DirectoryUserProfile.PHONE,
			DirectoryUserProfile.STATE, DirectoryUserProfile.CITY,
			DirectoryUserProfile.STREET, DirectoryUserProfile.ZIP_CODE,
			DirectoryUserProfile.FIRST_NAME, DirectoryUserProfile.LAST_NAME,
			DirectoryUserProfile.PN,
			DirectoryUserProfile.SPECIALITIES };

	@Override
	public MatrixCursor saveToCursor() {
		final MatrixCursor cursor = new MatrixCursor(PROJECTION,
				data.directoryUsers.length);
		for (final DirectoryUserProfile.Entity user : data.directoryUsers) {
			final MatrixCursor.RowBuilder b = cursor.newRow();

			user.transformData();

			b.add((long) user.hashCode());
			b.add(user.fullName);
			b.add(user.specialityWithAddress.toString());
			b.add(user.gender);
			b.add(user.phone);
			b.add(user.state);
			b.add(user.city);
			b.add(user.street);
			b.add(user.zipCode);
			b.add(user.firstName);
			b.add(user.lastName);
			b.add(user.pn);
			b.add(user.specialites.toString());
		}

		return cursor;
	}

	@Override
	public int getCount() {
		return getData().pagination.total_users_count;
	}

	public static class Data implements Parcelable {
		@SerializedName("pagination")
		public Pagination pagination;
		@SerializedName("directory")
		public DirectoryUserProfile.Entity[] directoryUsers;

		public Data(final Parcel in) {
			pagination = in.readParcelable(Pagination.class
					.getClassLoader());
			directoryUsers = in
					.createTypedArray(DirectoryUserProfile.Entity.CREATOR);
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeValue(pagination);
			dest.writeTypedArray(directoryUsers, 0);
		}

		public Pagination getPagination() {
			return pagination;
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};
	}

	public static final Parcelable.Creator<GetDirectoryResponse> CREATOR = new Parcelable.Creator<GetDirectoryResponse>() {
		@Override
		public GetDirectoryResponse createFromParcel(final Parcel in) {
			return new GetDirectoryResponse(in);
		}

		@Override
		public GetDirectoryResponse[] newArray(final int size) {
			return new GetDirectoryResponse[size];
		}
	};
}
package com.mobile.health.one.service.entity;

import java.util.HashMap;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

public class SendInvitesResponce implements Parcelable, IResponce {

	public static class Data implements Parcelable {

		public String form_name;
		public FormFields form_fields;
		public FormErrors form_errors;

		public Data(final Parcel in) {
			form_name = in.readString();
			form_fields = in.readParcelable(FormFields.class.getClassLoader());
			form_errors = in.readParcelable(FormErrors.class.getClassLoader());
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(form_name);
			dest.writeParcelable(form_fields, flags);
			dest.writeParcelable(form_errors, flags);
		}
	}

	public static class FormFields implements Parcelable {

		public String _token;
		public String[] invitee_users;
		public String message_list;
		public String invite_message;

		public FormFields(final Parcel in) {
			_token = in.readString();
			invitee_users = in.createStringArray();
			//in.readStringArray(this.invitee_users);
			message_list = in.readString();
			invite_message = in.readString();
		}

		public static final Parcelable.Creator<FormFields> CREATOR = new Parcelable.Creator<FormFields>() {
			@Override
			public FormFields createFromParcel(final Parcel in) {
				return new FormFields(in);
			}

			@Override
			public FormFields[] newArray(final int size) {
				return new FormFields[size];
			}
		};

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(_token);
			dest.writeStringArray(invitee_users);
			dest.writeString(message_list);
			dest.writeString(invite_message);
		}
	}

	public static class FormErrors implements Parcelable {

		public String[] global;
		public HashMap<String, String[]> fields;

		@SuppressWarnings("unchecked")
		public FormErrors(final Parcel in) {
			global = in.createStringArray();
			fields = in.readHashMap(String.class.getClassLoader());
		}

		public static final Parcelable.Creator<FormErrors> CREATOR = new Parcelable.Creator<FormErrors>() {
			@Override
			public FormErrors createFromParcel(final Parcel in) {
				return new FormErrors(in);
			}

			@Override
			public FormErrors[] newArray(final int size) {
				return new FormErrors[size];
			}
		};

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeStringArray(global);
			dest.writeMap(fields);
		}

	}

	protected int code;

	private Data data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<SendInvitesResponce> CREATOR = new Parcelable.Creator<SendInvitesResponce>() {
		@Override
		public SendInvitesResponce createFromParcel(final Parcel in) {
			return new SendInvitesResponce(in);
		}

		@Override
		public SendInvitesResponce[] newArray(final int size) {
			return new SendInvitesResponce[size];
		}
	};

	public SendInvitesResponce() {
		code = 0;
	}

	public SendInvitesResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Data getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}

}

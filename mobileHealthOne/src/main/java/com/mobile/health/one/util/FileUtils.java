package com.mobile.health.one.util;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

public class FileUtils {

	public static String getPath(final Context context,
			final Uri uri) {
		if ("content".equalsIgnoreCase(uri.getScheme())) {
			final String[] projection = { "_data" };
			Cursor cursor = null;

			try {
				cursor = context.getContentResolver().query(uri, projection,
						null, null, null);
				final int column_index = cursor.getColumnIndexOrThrow("_data");
				if (cursor.moveToFirst()) {
					return cursor.getString(column_index);
				}
			} catch (final Exception e) {
				// Eat it
			}
		}

		else if ("file".equalsIgnoreCase(uri.getScheme())) {
			return uri.getPath();
		}

		return null;
	}

}

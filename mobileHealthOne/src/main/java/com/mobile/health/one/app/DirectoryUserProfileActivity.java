package com.mobile.health.one.app;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;

public class DirectoryUserProfileActivity extends AppFragmentActivity {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle(R.string.profile);
		final DirectoryUserFragment fragment = new DirectoryUserFragment();
		fragment.setArguments(getIntent().getExtras());

		final FragmentTransaction ft = getSupportFragmentManager()
				.beginTransaction();
		ft.add(android.R.id.content, fragment).commit();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
package com.mobile.health.one.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobile.health.one.R;
import com.mobile.health.one.app.ViewHolder.Validator;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.service.worker.RegisterPinProcessor;
import com.mobile.health.one.util.SettingsManager;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class RegisterPinFragment extends SherlockFragment {

	public interface RegisterPinCallback {
		public void onPinRegistered();
		public void onPinRegisterError(String error);
	}

	protected class RegisterPinReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();
		private final SettingsManager settingsManager;

		public RegisterPinReceiver(final Context context) {
			settingsManager = new SettingsManager(context);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start pin registering");
				setProgress(true);
				break;
			case SyncService.STATUS_FINISHED:
				settingsManager.setPinRegistered(true);
				callback.onPinRegistered();
				Log.i(TAG, "Pin registered");
				setProgress(false);
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error registering pin");
				if (resultData != null) {
					//callback.onPinRegisterError(error);
					final Responce resp = resultData
							.getParcelable(RequestProcessor.EXTRA_RESPONSE);
					final String error = resultData.getString(RequestProcessor.EXTRA_ERROR_STRING);
					if (resp != null) {
						boolean controlWithErrorFound = false;
						if (resp.getInvalidParams() != null) {
							for (final String tag : resp.getInvalidParams()) {
								final TextView view = (TextView) getView()
										.findViewWithTag(tag);
								if (view != null) {
									view.setError(resp.getMessages());
									view.requestFocus();
									controlWithErrorFound = true;
								}
							}
						}
						// TODO: temporatory fix
						if ("Invalid access data".equals(error)) {
							controlWithErrorFound = false;
							ToastUtil.showText(getActivity(), "Your session expired. Please, login again");
							settingsManager.removeSessionId(); // remove old session id
						}
						// if error is not on this page, force user to relogin
						if (!controlWithErrorFound) {
							// if no back stack, create one
							if (getFragmentManager().getBackStackEntryCount() > 0) {
								getFragmentManager().popBackStack();
							} else {
								final LoginFragment loginFragment = new LoginFragment();
								getFragmentManager().beginTransaction()
									.replace(android.R.id.content, loginFragment)
									.commit();
							}
						}
					} else {
						ToastUtil.showText(getActivity(), error);
					}
				}
				setProgress(false);
				break;
			}
		}

	}

	protected static final String TAG = RegisterPinFragment.class
			.getCanonicalName();

	private final SparseArray<ViewHolder> views = new SparseArray<ViewHolder>();

	private CharSequence oldTitle;

	private RegisterPinCallback callback;

	private Button saveBtn;

	private ProgressBar progress;

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		final int PIN_COUNT = getResources().getInteger(R.integer.max_pin_chars);

		final ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.fragment_register_pin, container, false);

		final ViewHolder.Validator validator = new Validator() {

			@Override
			public ValidationResult onValidate(final ViewHolder vh,
					final CharSequence s) {
				Log.d(TAG, "Validate");

				final CharSequence pincodeText = views.get(R.id.pincode).view
						.getText();
				final CharSequence pinconfirmText = views.get(R.id.pinconfirm).view
						.getText();
				final boolean equals = TextUtils.equals(pincodeText,
						pinconfirmText);

				if (pincodeText.length() == PIN_COUNT
						&& pinconfirmText.length() == PIN_COUNT && equals) {

					return new ValidationResult();
				}

				if (pincodeText.length() == PIN_COUNT
						&& pinconfirmText.length() == PIN_COUNT && !equals) {
					return new ValidationResult(false,
							getText(R.string.error_pin_should_match));
				}

				if (vh.view.length() != PIN_COUNT) {
					return new ValidationResult(false,
							getString(R.string.error_pin_too_short, PIN_COUNT));
				}

				return new ValidationResult();
			}

			@Override
			public ValidationResult onLoseFocus(final ViewHolder vh,
					final CharSequence s) {
				Log.i(TAG, "Left control");

				final CharSequence pincodeText = views.get(R.id.pincode).view
						.getText();
				final CharSequence pinconfirmText = views.get(R.id.pinconfirm).view
						.getText();
				final boolean equals = TextUtils.equals(pincodeText,
						pinconfirmText);

				if (pincodeText.length() == PIN_COUNT
						&& pinconfirmText.length() == PIN_COUNT && equals) {

					return new ValidationResult();
				}

				if (pincodeText.length() == PIN_COUNT
						&& pinconfirmText.length() == PIN_COUNT && !equals) {
					return new ValidationResult(false,
							getText(R.string.error_pin_should_match));
				}

				if (vh.view.length() != PIN_COUNT) {
					return new ValidationResult(false,
							getString(R.string.error_pin_too_short, PIN_COUNT));
				}

				return new ValidationResult();
			}

		};

		final EditText enterPin = (EditText) view.findViewById(R.id.pincode);
		enterPin.setTag(RegisterPinProcessor.EXTRA_PIN);
		views.append(R.id.pincode, new ViewHolder(enterPin, validator, false));

		final EditText confirmPin = (EditText) view
				.findViewById(R.id.pinconfirm);
		views.append(R.id.pinconfirm, new ViewHolder(confirmPin, validator,
				false));

		progress = (ProgressBar) view.findViewById(android.R.id.progress);
		saveBtn = (Button) view.findViewById(R.id.save);
		saveBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				final ViewHolder pincode = views.get(R.id.pincode);
				final ViewHolder pinconfirm = views.get(R.id.pinconfirm);
				if (pincode.isValid() && pinconfirm.isValid()) {
					Log.v(getTag(), "Register pin sync service started");
					final Bundle extras = new Bundle();
					extras.putString(RegisterPinProcessor.EXTRA_PIN,
							pincode.view.getText().toString());
					SyncService.startSyncService(getActivity(),
							AppSyncService.REGISTER_PIN_REQUEST,
							new RegisterPinReceiver(getActivity()), extras);

				}
			}
		});

		return view;
	}

	public void setProgress(final boolean b) {
		saveBtn.setEnabled(!b);
		progress.setVisibility(b ? View.VISIBLE : View.INVISIBLE);
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		oldTitle = activity.getTitle();
		activity.setTitle(R.string.title_fragment_register_pin);
		if (activity instanceof RegisterPinCallback) {
			callback = (RegisterPinCallback) activity;
		} else {
			throw new ClassCastException(
					"Parent activity should implement callbacks");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();

		getActivity().setTitle(oldTitle);
	}

}

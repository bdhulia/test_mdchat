package com.mobile.health.one.service.worker;

import java.io.IOException;

import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.SendInvitesResponce;
import com.mobile.health.one.util.Settings;

public class SendInvitesProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";

	public static final String PARAM_INVITE_USERS = "archer_invite_welcome_message[invitee_users][]";
	public static final String PARAM_INVITE_MESSAGE = "archer_invite_welcome_message[invite_message]";
	public static final String PARAM_INVITE_TOKEN = "archer_invite_welcome_message[_token]";

	private static final Uri uri = Uri.parse(Settings.URI_BASE
			+ "/api/archer_invite_api/sendInvites");
	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public SendInvitesProcessor(final Context context) {
		super(context);
	}

	@Override
	protected void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras)
					throws IOException, HttpException, Exception {
		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, this.androidId);

		if (extras != null) {
			params.add(PARAM_INVITE_MESSAGE, extras.getString(PARAM_INVITE_MESSAGE));
			params.add(PARAM_INVITE_TOKEN, extras.getString(PARAM_INVITE_TOKEN));
			params.add(PARAM_INVITE_USERS, extras.getString(PARAM_INVITE_USERS));
		}
		tryRequest(receiver, false, params, SendInvitesResponce.class, extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

	@Override
	public Uri getUri() {
		return SendInvitesProcessor.uri;
	}

}

package com.mobile.health.one.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NavUtils;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;

import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.service.entity.Invite;

public class GroupInviteListActivity extends AppFragmentActivity implements OnNavigationListener, OnCheckedChangeListener {

	private GroupInviteListFragment mGroupInviteFragment;
	private FragmentManager mFragmentManager;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		// getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// getSupportActionBar().setListNavigationCallbacks(this.adapter, this);
		setContentView(R.layout.activity_group_invites);
		setTitle(R.string.group_invites);

		mGroupInviteFragment = new GroupInviteListFragment();
		//mSearchFragment = SearchFragment.newInstance(this, SearchType.GROUPS_MY);

		mFragmentManager = getSupportFragmentManager();
		mFragmentManager.beginTransaction()
		.add(MenuMessageCreateFragment.newInstance(R.id.compose),
				MenuMessageCreateFragment.TAG)
				.add(R.id.list, mGroupInviteFragment)
				.commit();

		final RadioGroup filter = (RadioGroup) findViewById(R.id.filter);
		filter.setOnCheckedChangeListener(this);
		final RadioGroup typeFilter = (RadioGroup) findViewById(R.id.filter_invite_type);
		typeFilter.setOnCheckedChangeListener(this);

		showTab(R.id.filter_contacts);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menu_labels:
			final Intent i = new Intent(this, LabelsActivity.class);
			startActivityForResult(i, LabelsListFragment.REQUEST_GET_LABEL);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(final int itemPosition,
			final long itemId) {
		return false;
	}

	private int mType;
	private int mStatus;

	@Override
	public void onCheckedChanged(final RadioGroup group, final int checkedId) {
		showTab(checkedId);
	}

	private void showTab(final int id) {
		switch (id) {
		case R.id.filter_pending:
			mStatus = Invite.Entity.STATUS_PENDING;
			break;
		case R.id.filter_all:
			mStatus = Invite.Entity.STATUS_ALL;
			break;
		case R.id.filter_accepted:
			mStatus = Invite.Entity.STATUS_ACCEPTED;
			break;
		case R.id.filter_declined:
			mStatus = Invite.Entity.STATUS_DECLINED;
			break;
		case R.id.filter_received:
			mType = Invite.Entity.TYPE_INCOMING;
			break;
		case R.id.filter_sent:
			mType = Invite.Entity.TYPE_OUTGOING;
			break;
		default:
			mStatus = Invite.Entity.STATUS_PENDING;
			mType = Invite.Entity.TYPE_INCOMING;
			break;
		}
		mGroupInviteFragment.setInvitesType(mType);
		mGroupInviteFragment.setInvitesStatusType(mStatus);
		if (mGroupInviteFragment.isAdded())
			mGroupInviteFragment.loadData();
	}

}

package com.mobile.health.one.service;


/**
 * @author Igor Yanishevskiy
 */
public class AppSyncService extends SyncService {

	public static final String EXTRA_REGISTRATION_NUMBER = "extra_registration_number";
	public static final String EXTRA_FORCE_UPDATE = "extra_force_update";

	private static final String TAG = AppSyncService.class.getSimpleName();

	//
	// requests for processing
	//
	public static final String LOGIN_REQUEST = "login";
	public static final String REGISTER_PIN_REQUEST = "registerPin";
	public static final String CHANGE_PIN_REQUEST = "changePin";
	public static final String UPDATE_SESSION_REQUEST = "updateSession";
	public static final String UNREGISTER_DEVICE_REQUEST = "unregisterDevice";
	public static final String GET_STATUS_TEXT_REQUEST = "getStatusText";
	public static final String SET_STATUS_TEXT_REQUEST = "setStatusText";
	public static final String GET_STATUS_REQUEST = "getStatus";
	public static final String SET_STATUS_REQUEST = "setStatus";
	public static final String GET_ALL_USER_THREADS_REQUEST = "getAllUserThreads";
	public static final String GET_CONTACTS_DATA_REQUEST = "getContactsData";
	public static final String USER_CONTACTS_SEARCH = "userContactsSearch";
	public static final String SEND_INVITES_REQUEST = "sendInvites";
	public static final String GET_INVITES_TEMPLATES = "getInvitesTemplates";
	public static final String GET_INVITES = "getInvites";
	public static final String GET_INVITES_COUNT = "getInvitesCount";
	public static final String GET_GROUP_INVITES_COUNT = "getGroupInvitesCount";
	public static final String CHANGE_INVITATION_STATUS = "changeInvitationStatus";
	public static final String DELETE_INVITATION_STATUS = "deleteInvitation";
	public static final String GET_GROUPS = "getGroups";
	public static final String GET_GROUP = "getGroup";
	public static final String GET_GROUP_INVITES = "getGroupInvites";
	public static final String CHANGE_GROUP_INVITATION_STATUS = "changeGroupInvitationStatus";
	public static final String GET_USER_PROFILE = "getUserProfile";
	public static final String SEARCH = "search";
	public static final String AUTOCOMPLETE_SEARCH = "autocompleteSearch";
	public static final String AUTOCOMPLETE_NETWORK = "autocompleteNetwork";
	public static final String AUTOCOMPLETE_INVITE = "autocompleteInvite";
	public static final String GET_USER_LABELS = "getUserLabels";
	public static final String SEND_SECRET_PUSH = "sendSecretPush";

	public static final String MARK_USER_WITH_LABEL = "markUserWithLabel";
	public static final String DELETE_USER_FROM_LABEL = "deleteUserFromLabel";
	public static final String DELETE_USER_FROM_NETWORK = "DeleteUserFromNetwork";

	public AppSyncService() {
		super(TAG);
	}
}
package com.mobile.health.one.service.worker;

import java.io.IOException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.GetInvitesTemplatesResponse;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Invite;
import com.mobile.health.one.util.Settings;

public class DeleteInvitationProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_IDS = "ids";
	public static final String EXTRA_INCOMING = "incoming";
	public static final String EXTRA_GROUP_ID = "group_id";
	public static final String EXTRA_USER_ID = "user_id";

	private static final Uri URI_INVITATION = Uri.parse(Settings.URI_BASE
			+ "/api/archer_invite_api/deleteInvitation");
	private static final Uri URI_GROUP_INVITATION = Uri.parse(Settings.URI_BASE
			+ "/api/groups_api/deleteSendingInvitation");
	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	private int inviteType;

	public DeleteInvitationProcessor(final Context context) {
		super(context);
	}

	@Override
	protected void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras)
					throws IOException, HttpException, Exception {
		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);

		inviteType = extras.getInt(Invite.Entity.EXTRA_TYPE,
				Invite.Entity.CONTACT_INVITES);

		if(inviteType == Invite.Entity.CONTACT_INVITES) {
			params.add(EXTRA_IDS, StringUtils.join(ArrayUtils.toObject(extras.getLongArray(EXTRA_IDS))));
			params.add(EXTRA_INCOMING, extras.getString(EXTRA_INCOMING));
		} else {
			if (extras.containsKey(EXTRA_USER_ID))
				params.add(EXTRA_USER_ID, extras.getString(EXTRA_USER_ID));
			params.add(EXTRA_GROUP_ID, extras.getString(EXTRA_GROUP_ID));
		}

		tryRequest(receiver, false, params, GetInvitesTemplatesResponse.class, extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

	@Override
	public Uri getUri() {
		switch (inviteType) {
		case Invite.Entity.CONTACT_INVITES:
			return URI_INVITATION;
		case Invite.Entity.GROUP_INVITES:
			return URI_GROUP_INVITATION;
		default:
			return URI_INVITATION;
		}
	}
}
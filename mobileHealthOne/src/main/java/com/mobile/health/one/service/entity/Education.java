package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Education {
	public static class Entity implements Parcelable {
		@SerializedName("colledge")
		public String school;
		@SerializedName("degree")
		public String degree;
		@SerializedName("gradutaion_date")
		public String graduationDate;

		public Entity(final Parcel in) {
			school = in.readString();
			degree = in.readString();
			graduationDate = in.readString();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeString(school);
			dest.writeString(degree);
			dest.writeString(graduationDate);
		}
		
		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};
	}
}
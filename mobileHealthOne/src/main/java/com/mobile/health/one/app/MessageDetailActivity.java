package com.mobile.health.one.app;

import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.util.Log;

import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.R;
import com.mobile.health.one.app.LabelsListFragment.LabelsListCallback;
import com.mobile.health.one.app.MessageDetailFragment.MessageDetailFragmentCallback;
import com.mobile.health.one.db.entity.Label.Entity;
import com.mobile.health.one.db.entity.Message;
import com.mobile.health.one.search.TextTask.Listener;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.DataSaver;
import com.mobile.health.one.util.SearchParams.SearchType;

public class MessageDetailActivity extends AppFragmentActivity
implements MessageDetailFragmentCallback, LabelsListCallback, Listener {

	@SuppressLint("ValidFragment")
	public class ConfirmDialog extends DialogFragment implements DialogInterface.OnClickListener, NodeServiceConnector {

		private String threadId;

		@Override
		public Dialog onCreateDialog(final Bundle savedInstanceState) {
			final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
			final String m = getString(R.string.message_delete_message);
			dialog.setMessage(m);
			dialog.setTitle(R.string.message_delete_title);
			dialog.setPositiveButton(R.string.message_delete_btn, this);
			dialog.setNegativeButton(R.string.btn_no, this);
			return dialog.create();
		}

		@Override
		public void onNodeConnected(final NodeWorkerService service) {
			if (service != null && threadId != null) {
				service.deleteMessageThread(threadId);
				setResult(RESULT_OK);
				finish();
			}

		}

		@Override
		public void onClick(final DialogInterface dialog, final int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				threadId = getIntent().getStringExtra(Message.CONVERSATION_ID);
				final MHOApplication app = (MHOApplication) getApplication();
				app.getNodeService(this);
				dialog.dismiss();

				break;
			case DialogInterface.BUTTON_NEGATIVE:
				dialog.cancel();
				break;
			default:
				break;
			}

		}
	}

	private MessageDetailFragment mDetails;
	private HashMap<String, String> mLabelsList;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		if (!getResources().getBoolean(R.bool.fragment_details_separate)) {
			// If the screen is now in landscape mode, we can show the
			// dialog in-line with the list so we don't need this activity.
			finish();
			return;
		}
		final String threadId = getIntent().getStringExtra(Message.CONVERSATION_ID);
		if (savedInstanceState == null) {
			final Bundle add = new Bundle(1);
			add.putString("local_message_search[thread_id]", threadId);
			mDetails = new MessageDetailFragment();
			mDetails.setArguments(getIntent().getExtras());
			getSupportFragmentManager().beginTransaction()
			.add(android.R.id.content, mDetails)
			.add(SearchFragment.newInstance(this, SearchType.MESSAGES, add), SearchFragment.TAG)
			.commit();
		}
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onLabelsLoaded(final HashMap<String, String> labels) {
		saveLabels(labels);
	}

	private void saveLabels(final HashMap<String, String> labels) {
		mLabelsList = labels;
		getIntent().putExtra(LabelsListFragment.EXTRA_LABEL_IDS, labels.keySet().toArray(new String[labels.size()]));
	}

	@Override
	public void onItemClick(final String remoteId, final String labelName) { /* nop */ }

	@Override
	public void onItemsSelected(final HashMap<String, String> labels) {
		mLabelsList = labels;
		final MessageDetailFragment f = mDetails;
		onLabelsLoaded(labels);
		f.setLabels(labels);
	}

	@Override
	public void onMenu(final int id) {
		switch (id) {
		case R.id.menu_label:
			LabelsListFragment list = new LabelsListFragment(true);
			list.setArguments(getIntent().getExtras());
			list.show(getSupportFragmentManager(), LabelsListFragment.TAG_DIALOG_LABELS);
			break;
		case R.id.menu_move:
			list = new LabelsListFragment(false);
			list.setArguments(getIntent().getExtras());
			list.show(getSupportFragmentManager(), LabelsListFragment.TAG_DIALOG_LABELS);
			break;
		case R.id.menu_reply:
			Intent i = new Intent(this, MessageCreateActivity.class);
			i.putExtra(MessageCreateActivity.EXTRA_REPLY, true);
			i.putExtras(getIntent().getExtras());
			startActivity(i);
			break;
		case R.id.menu_forward:
			i = new Intent(this, MessageCreateActivity.class);
			final String subject = mDetails.getSubject();
			if (StringUtils.isNotEmpty(subject)) {
				i.putExtra(MessageCreateActivity.EXTRA_SUBJECT, subject);
			}
			i.putExtra(MessageCreateActivity.EXTRA_FORWARD, true);
			i.putExtras(getIntent().getExtras());
			startActivity(i);
			break;
		case R.id.menu_delete:
			new ConfirmDialog().show(getSupportFragmentManager(), ConfirmDialog.class.getSimpleName());
			break;
		}

	}

	@Override
	public void onSearchStart() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSearchError(final String error) {
		Log.i(TAG, "::onSearchError:" + "");
		setSupportProgressBarIndeterminateVisibility(false);
		final MessageDetailFragment f = mDetails;
		f.setSearch(false);
	}

	@Override
	public void onSearchComplete(final IResponce responce) {

		final DataSaver resp = (DataSaver) responce;
		final ArrayList<HashMap<String, Object>> c = resp.saveData();
		final MessageDetailFragment f = mDetails;
		if (c != null && c.size() > 0) {
			f.setDataSavingOld(c);
			f.setSearch(true);
		} else {
			f.setDataSavingOld(null);
			f.setSearch(false);
		}
		setSupportProgressBarIndeterminateVisibility(false);

	}

	@Override
	public void onSearchRestart() {
		Log.i(TAG, "::onSearchRestart:" + "");
		final MessageDetailFragment f = mDetails;
		f.setSearch(false);
		f.setOldData();
	}

	@Override
	public void onSearchCancel() {
		// nop

	}

	@Override
	public void onRootLabel(final Entity label) {
		// nop

	}

	public HashMap<String, String> getLabelsList() {
		return mLabelsList;
	}

	public void setLabelsList(final HashMap<String, String> mLabelsList) {
		this.mLabelsList = mLabelsList;
	}

	@Override
	public void removeLabel(final String labelId) {
		mLabelsList.remove(labelId);
		saveLabels(mLabelsList);
	}

}

package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

public class GroupInvite implements Parcelable {

	public static interface Entity extends BaseColumns {
		public static final int STATUS_ALL = 3;
		public static final int STATUS_PENDING = 0;
		public static final int STATUS_ACCEPTED = 1;
		public static final int STATUS_DECLINED = 2;

		public static final int TYPE_INCOMING = 1;
		public static final int TYPE_OUTGOING = 2;

		public static final String EXTRA_TYPE = "type";

		public static final String GROUP_ID = "group_id";
		public static final String USER_ID = "user_id";
		public static final String GROUP_NAME = "group_name";
		public static final String AVATAR = "avatar";
		public static final String ADMIN_NAME = "admin_name";
		public static final String MESSAGE = "message";
		public static final String STATUS = "status";
	}

	public long group_id;
	public long user_id;
	public String group_name;
	public String avatar_link;
	public String admin;
	public int status;
	public String message;

	public GroupInvite(final Parcel in) {
		group_id = in.readLong();
		user_id = in.readLong();
		group_name = in.readString();
		avatar_link = in.readString();
		admin = in.readString();
		message = in.readString();
		status = in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeLong(group_id);
		dest.writeLong(user_id);
		dest.writeString(group_name);
		dest.writeString(avatar_link);
		dest.writeString(admin);
		dest.writeString(message);
		dest.writeInt(status);
	}

	public static final Parcelable.Creator<GroupInvite> CREATOR = new Parcelable.Creator<GroupInvite>() {
		@Override
		public GroupInvite createFromParcel(final Parcel in) {
			return new GroupInvite(in);
		}

		@Override
		public GroupInvite[] newArray(final int size) {
			return new GroupInvite[size];
		}
	};

}

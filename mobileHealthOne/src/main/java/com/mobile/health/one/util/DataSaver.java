package com.mobile.health.one.util;

import java.util.ArrayList;
import java.util.HashMap;

public interface DataSaver {
	public ArrayList<HashMap<String, Object>> saveData();
}

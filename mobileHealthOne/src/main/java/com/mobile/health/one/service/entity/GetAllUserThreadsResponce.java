package com.mobile.health.one.service.entity;

import java.util.HashMap;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;

import com.mobile.health.one.db.entity.Conversation;


public class GetAllUserThreadsResponce implements Parcelable, IResponce {

	@SuppressWarnings("unused")
	private static final String TAG = "service.entity.GetAllUserThreadsResponce";

	private int code;

	private String messages;

	private HashMap<String, Conversation.Entity> data;

	public static final Parcelable.Creator<GetAllUserThreadsResponce> CREATOR = new Parcelable.Creator<GetAllUserThreadsResponce>() {
		@Override
		public GetAllUserThreadsResponce createFromParcel(final Parcel in) {
			return new GetAllUserThreadsResponce(in);
		}

		@Override
		public GetAllUserThreadsResponce[] newArray(final int size) {
			return new GetAllUserThreadsResponce[size];
		}
	};

	@Override
	public int getCode() {
		return code;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public GetAllUserThreadsResponce() {
		data = new HashMap<String, Conversation.Entity>();
	}

	@SuppressWarnings("unchecked")
	public GetAllUserThreadsResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readHashMap(Conversation.Entity.class.getClassLoader());
		/*final int count = in.readInt();
		for (int i = 0; i < count; i++) {
			final ConversationEntity conversationEntity = new ConversationEntity();
			in.readStringArray(conversationEntity.labels);

			this.data.put(in.readString(), conversationEntity);
		}*/

		messages = in.readString();
	}

	private String result;

	public String getResult() {
		return result;
	}

	public Map<String, Conversation.Entity> getData() {
		return data;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeMap(data);
		/*dest.writeInt(this.data.size());
		for (final String s : this.data.keySet()) {
			final ConversationEntity conv = this.data.get(s);
			dest.writeParcelable(conv, 0);
		}*/
		dest.writeString(messages);

	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}
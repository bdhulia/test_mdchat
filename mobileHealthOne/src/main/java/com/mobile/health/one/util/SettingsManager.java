package com.mobile.health.one.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

/**
 * Utility class
 *
 */
public class SettingsManager {

	private static final String SETTINGS_NAME = "settings";

	private static final String PIN_REGISTERED_KEY = "pin_registered";
	private static final String PIN_KEY = "pin";
	private static final String LOGIN_KEY = "login";
	private static final String SESSION_ID_KEY = "sessionId";
	private static final String GCM_ID_KEY = "GCMId";
	private static final String NOTIFICATION_ID = "notificationId";
	//private static final String INVITE_TOKEN_KEY = "inviteToken";
	private static final String APP_VISIBLE_KEY = "loggedIn";
	private static final String SHOW_PASS_DIALOG = "show_pass_dialog";
	private static final String INVITES_COUNT = "invites_count";
	private static final String GROUP_INVITES_COUNT = "group_invites_count";
    private static final String LAST_LOGIN_EMAIL = "last_login_email";
    private static final String LAST_LOGIN_TIME = "last_login_time";

    private static Bundle settings;
    private static long lastLoginTime;
	private SharedPreferences prefs;

	/**
	 * Create and initialize a new SettingsManager
	 *
	 * @param context
	 */
	public SettingsManager(final Context context) {
		if (SettingsManager.settings == null) {
			SettingsManager.settings = new Bundle();
		}
		if (prefs == null) {
			prefs = context.getSharedPreferences(SETTINGS_NAME, Context.MODE_PRIVATE);
		}
	}

	//
	// pin registered
	//

	public boolean hasPinRegistered() {
		return prefs.contains(PIN_REGISTERED_KEY);
	}

	public void setPinRegistered(final boolean pin) {
		prefs.edit().putBoolean(PIN_REGISTERED_KEY, pin).commit();
	}

	public boolean isPinRegistered() {
		return prefs.getBoolean(PIN_REGISTERED_KEY, false);
	}

	public void removePinRegistered() {
		prefs.edit().remove(PIN_REGISTERED_KEY).commit();
	}

	//
	// pin
	//

	public boolean hasPin() {
		return SettingsManager.settings.containsKey(PIN_KEY);
	}

	public void setPin(final String pin) {
		SettingsManager.settings.putString(PIN_KEY, pin);
	}

	public String getPin() {
		return SettingsManager.settings.getString(PIN_KEY);
	}

	public void removePin() {
		SettingsManager.settings.remove(PIN_KEY);
	}

	//
	// pass dialog
	//

	public void setShowPassDialog(final boolean showPassDialog) {
		SettingsManager.settings.putBoolean(SHOW_PASS_DIALOG, showPassDialog);
	}

	public boolean getShowPassDialog() {
		return SettingsManager.settings.getBoolean(SHOW_PASS_DIALOG);
	}

	//
	// invites count
	//

	public void setInvitesCount(final int count) {
		SettingsManager.settings.putInt(INVITES_COUNT, count);
	}

	public int getInvitesCount() {
		return SettingsManager.settings.getInt(INVITES_COUNT);
	}

	public void setGroupInvitesCount(final int count) {
		SettingsManager.settings.putInt(GROUP_INVITES_COUNT, count);
	}

	public int getGroupInvitesCount() {
		return SettingsManager.settings.getInt(GROUP_INVITES_COUNT);
	}


	//
	// notification id
	//

	public boolean hasNotificationId() {
		return prefs.contains(NOTIFICATION_ID);
	}

	public void setNotificationId(final int notificationId) {
		prefs.edit().putInt(NOTIFICATION_ID, notificationId).commit();
	}

	public int getNotificationId() {
		return prefs.getInt(NOTIFICATION_ID, 0);
	}

	public void removeNotificationId() {
		prefs.edit().remove(NOTIFICATION_ID).commit();
	}

	//
	// login
	//

	public boolean hasLogin() {
		return SettingsManager.settings.containsKey(LOGIN_KEY);
	}

	public void setLogin(final String login) {
		SettingsManager.settings.putString(LOGIN_KEY, login);
	}

	public String getLogin() {
		return SettingsManager.settings.getString(LOGIN_KEY);
	}

	public void removeLogin() {
		SettingsManager.settings.remove(LOGIN_KEY);
	}

	//
	// login
	//

	public void setAppVisible(final boolean login) {
		SettingsManager.settings.putBoolean(APP_VISIBLE_KEY, login);
	}

	public boolean isAppVisible() {
		return SettingsManager.settings.getBoolean(APP_VISIBLE_KEY);
	}

	public void removeAppVisible() {
		SettingsManager.settings.remove(APP_VISIBLE_KEY);
	}

	//
	// sessionId
	//

	public boolean hasSessionId() {
		return prefs.contains(SESSION_ID_KEY);
	}

	public void setSessionId(final String sessionId) {
		prefs.edit().putString(SESSION_ID_KEY, sessionId).commit();
	}

	public String getSessionId() {
		return prefs.getString(SESSION_ID_KEY, null);
	}

	public void removeSessionId() {
		prefs.edit().remove(SESSION_ID_KEY).commit();
	}

    //
    // last login email
    //

    public boolean hasLastLoginEmail() {
        return prefs.contains(LAST_LOGIN_EMAIL);
    }

    public void setLastLoginEmail(final String lastLoginEmail) {
        prefs.edit().putString(LAST_LOGIN_EMAIL, lastLoginEmail).commit();
    }

    public String getLastLoginEmail() {
        return prefs.getString(LAST_LOGIN_EMAIL, null);
    }

    public void removeLastLoginEmail() {
        prefs.edit().remove(LAST_LOGIN_EMAIL).commit();
    }

    //
    // last login time
    //

    public boolean hasLastLoginTime() {
        return prefs.contains(LAST_LOGIN_TIME);
    }

    public void setLastLoginTime(final long lastLoginTime) {
        SettingsManager.lastLoginTime = lastLoginTime;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void saveLastLoginTime() {
        prefs.edit().putLong(LAST_LOGIN_TIME, lastLoginTime).commit();
    }

    public long restoreLastLoginTime() {
        lastLoginTime =  prefs.getLong(LAST_LOGIN_TIME, 0);
        return lastLoginTime;
    }

    public void removeLastLoginTime() {
        prefs.edit().remove(LAST_LOGIN_TIME).commit();
    }

	//
	// invites token
	//
	/*
	public boolean hasInviteToken() {
		return this.settings.contains(INVITE_TOKEN_KEY);
	}

	public void setInviteToken(final String inviteToken) {
		this.settings.edit().putString(INVITE_TOKEN_KEY, inviteToken).commit();
	}

	public String getInviteToken() {
		return this.settings.getString(INVITE_TOKEN_KEY, null);
	}

	public void removeInviteToken() {
		this.settings.edit().remove(INVITE_TOKEN_KEY).commit();
	}
	 */

	//
	// GCM id
	//

	public boolean hasGCMId() {
		return prefs.contains(GCM_ID_KEY);
	}

	public void setGCMId(final String sessionId) {
		prefs.edit().putString(GCM_ID_KEY, sessionId).commit();
	}

	public String getGCMId() {
		return prefs.getString(GCM_ID_KEY, null);
	}

	public void removeGCMId() {
		prefs.edit().remove(GCM_ID_KEY).commit();
	}

}

package com.mobile.health.one.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobile.health.one.R;
import com.mobile.health.one.app.ViewHolder.Validator;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.worker.ChangePinProcessor;
import com.mobile.health.one.util.SettingsManager;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class ChangePinFragment extends SherlockFragment {

	public interface RegisterPinCallback {
		public void onPinRegistered();
	}

	public boolean syncStarted;

	protected class RegisterPinReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();
		private final SettingsManager settingsManager;

		public RegisterPinReceiver(final Context context) {
			settingsManager = new SettingsManager(context);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start pin registering");
				syncStarted = true;
				setProgress(true);
				break;
			case SyncService.STATUS_FINISHED:
				settingsManager.setPinRegistered(true);
				callback.onPinRegistered();
				Log.i(TAG, "Pin registered");
				syncStarted = false;
				setProgress(false);
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error registering pin");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(getActivity(), error);
				}
				syncStarted = false;
				setProgress(false);
				break;
			}
		}

	}

	protected static final String TAG = ChangePinFragment.class
			.getCanonicalName();

	private final SparseArray<ViewHolder> views = new SparseArray<ViewHolder>();

	private CharSequence oldTitle;

	private RegisterPinCallback callback;

	private Button saveBtn;

	private ProgressBar progress;

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		final int PIN_COUNT = getResources().getInteger(R.integer.max_pin_chars);

		final ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.fragment_change_pin, container, false);

		final ViewHolder.Validator validator = new Validator() {

			@Override
			public ValidationResult onValidate(final ViewHolder vh,
					final CharSequence s) {
				Log.d(TAG, "Validate");

				final CharSequence pincodeText = views.get(R.id.pincode).view
						.getText();
				final CharSequence pinconfirmText = views.get(R.id.pinconfirm).view
						.getText();
				final boolean equals = TextUtils.equals(pincodeText,
						pinconfirmText);

				if (pincodeText.length() == PIN_COUNT
						&& pinconfirmText.length() == PIN_COUNT && equals) {

					return new ValidationResult();
				}

				if (pincodeText.length() == PIN_COUNT
						&& pinconfirmText.length() == PIN_COUNT && !equals) {
					return new ValidationResult(false,
							getText(R.string.error_pin_should_match));
				}

				if (vh.view.length() != PIN_COUNT) {
					return new ValidationResult(false,
							getString(R.string.error_pin_too_short, PIN_COUNT));
				}

				return new ValidationResult();
			}

			@Override
			public ValidationResult onLoseFocus(final ViewHolder vh,
					final CharSequence s) {
				Log.i(TAG, "Left control");

				final CharSequence pincodeText = views.get(R.id.pincode).view
						.getText();
				final CharSequence pinconfirmText = views.get(R.id.pinconfirm).view
						.getText();
				final boolean equals = TextUtils.equals(pincodeText,
						pinconfirmText);

				if (pincodeText.length() == PIN_COUNT
						&& pinconfirmText.length() == PIN_COUNT && equals) {

					return new ValidationResult();
				}

				if (pincodeText.length() == PIN_COUNT
						&& pinconfirmText.length() == PIN_COUNT && !equals) {
					return new ValidationResult(false,
							getText(R.string.error_pin_should_match));
				}

				if (vh.view.length() != PIN_COUNT) {
					return new ValidationResult(false,
							getString(R.string.error_pin_too_short, PIN_COUNT));
				}

				return new ValidationResult();
			}

		};

		final ViewHolder.Validator oldPinValidator = new Validator() {

			@Override
			public ValidationResult onValidate(final ViewHolder vh, final CharSequence s) {
				if (vh.view.length() != PIN_COUNT) {
					return new ValidationResult(false,
							getString(R.string.error_pin_too_short, PIN_COUNT));
				}
				return new ValidationResult();
			}

			@Override
			public ValidationResult onLoseFocus(final ViewHolder vh, final CharSequence s) {
				if (vh.view.length() != PIN_COUNT) {
					return new ValidationResult(false,
							getString(R.string.error_pin_too_short, PIN_COUNT));
				}
				return new ValidationResult();
			}

		};

		final EditText oldPin = (EditText) view
				.findViewById(R.id.oldpin);
		views.append(R.id.oldpin, new ViewHolder(oldPin, oldPinValidator,
				false));

		final EditText enterPin = (EditText) view.findViewById(R.id.pincode);
		views.append(R.id.pincode, new ViewHolder(enterPin, validator, false));

		final EditText confirmPin = (EditText) view
				.findViewById(R.id.pinconfirm);
		views.append(R.id.pinconfirm, new ViewHolder(confirmPin, validator,
				false));
		progress = (ProgressBar) view.findViewById(android.R.id.progress);
		saveBtn = (Button) view.findViewById(R.id.save);
		saveBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				final ViewHolder oldpin = views.get(R.id.oldpin);
				final ViewHolder pincode = views.get(R.id.pincode);
				final ViewHolder pinconfirm = views.get(R.id.pinconfirm);
				if (!syncStarted && oldpin.isValid() && pincode.isValid() && pinconfirm.isValid()) {
					syncStarted = true;
					Log.v(getTag(), "Change pin sync service started");
					final Bundle extras = new Bundle();
					extras.putCharSequence(ChangePinProcessor.EXTRA_OLD_PIN,
							oldpin.view.getText());
					extras.putString(ChangePinProcessor.EXTRA_NEW_PIN,
							pincode.view.getText().toString());
					extras.putString(ChangePinProcessor.EXTRA_PIN,
							pincode.view.getText().toString());
					SyncService.startSyncService(getActivity(),
							AppSyncService.CHANGE_PIN_REQUEST,
							new RegisterPinReceiver(getActivity()), extras);

				}
			}
		});

		return view;
	}

	public void setProgress(final boolean b) {
		saveBtn.setEnabled(!b);
		progress.setVisibility(b ? View.VISIBLE : View.INVISIBLE);
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		oldTitle = activity.getTitle();
		activity.setTitle(R.string.change_pin);
		if (activity instanceof RegisterPinCallback) {
			callback = (RegisterPinCallback) activity;
		} else {
			throw new ClassCastException(
					"Parent activity should implement RegisterPinCallback");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();

		getActivity().setTitle(oldTitle);
	}

}

package com.mobile.health.one.service.worker;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SettingsManager;

public class SendSecretPushProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_PIN = "pin";
	public static final String EXTRA_SECRET = "secret";
	public static final String EXTRA_DEVICE_TYPE = "device_type";

	public static final String DEVICE_TYPE_ANDROID = "2";

	private static final Uri URI = Uri.parse(Settings.URI_BASE
			+ "/api/archer_api.helper/setupSecretPushNotification");
	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	private final SettingsManager mSettings;

	public SendSecretPushProcessor(final Context context) {
		super(context);
		mSettings = new SettingsManager(context);
	}

	@Override
	protected void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras)
					throws IOException, HttpException, Exception {
		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);

		if (!mSettings.hasPin()) {
			throw new InvalidParameterException("No pin registered");
		}
		params.add(EXTRA_PIN, mSettings.getPin());
		params.add(EXTRA_SECRET, extras.getString(EXTRA_SECRET));
		params.add(EXTRA_DEVICE_TYPE, DEVICE_TYPE_ANDROID);

		tryRequest(receiver, false, params, Responce.class, extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

	@Override
	public Uri getUri() {
		return URI;
	}

}

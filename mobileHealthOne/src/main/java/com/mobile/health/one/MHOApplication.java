package com.mobile.health.one;

import java.lang.ref.WeakReference;
import java.util.Timer;
import java.util.TimerTask;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.bugsense.trace.BugSenseHandler;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.NodeWorkerService.NodeWorkerBinder;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.mobile.health.one.util.Settings;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class MHOApplication extends Application {

	public static final String APIKEY = "b1113b0b";
	private static final String TAG = MHOApplication.class.getSimpleName();

	/** Messenger for communicating with the service. */
	private NodeWorkerService mService = null;

	/** Flag indicating whether we have called bind on the service. */
	private boolean mBound;

	private Timer mUpdateSessionTimer;

	/**
	 * Class for interacting with the main interface of the service.
	 */
	private final ServiceConnection mConnection = new ServiceConnection() {
		@Override
		public void onServiceConnected(final ComponentName className, final IBinder service) {
			final NodeWorkerBinder binder = (NodeWorkerBinder) service;
			MHOApplication.this.setNodeService(binder.getService());
			if (mNodeConnector != null) {
				mNodeConnector.onNodeConnected(binder.getService());
			}
			mBound = true;
		}

		@Override
		public void onServiceDisconnected(final ComponentName className) {
			// This is called when the connection with the service has been
			// unexpectedly disconnected -- that is, its process crashed.
			MHOApplication.this.setNodeService(null);
			mBound = false;
		}
	};
	private NodeServiceConnector mNodeConnector;

	private void bindNodeService() {
		bindService(new Intent(getApplicationContext(), NodeWorkerService.class), mConnection,
				Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onCreate() {
		bindNodeService();
		super.onCreate();
		GCMIntentService.register(this);
		// launch bugsence only if in production
		final boolean isDebuggable = 0 != ( getApplicationInfo().flags &= ApplicationInfo.FLAG_DEBUGGABLE );
		if (!isDebuggable)
			BugSenseHandler.initAndStartSession(this, APIKEY);

		// Create global configuration and initialize ImageLoader with this configuration
		final DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
			.showImageForEmptyUri(R.drawable.avatar_stub)
        	.cacheInMemory()
        	.build();
        final ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
        	.defaultDisplayImageOptions(defaultOptions)
            .build();
        ImageLoader.getInstance().init(config);

        final long updateInterval = Settings.INACTIVITY_TIME - 3000; // we should send query before session expired
        mUpdateSessionTimer = new Timer(true);
        mUpdateSessionTimer.scheduleAtFixedRate(new UpdateSessionTimerTask(this), updateInterval, updateInterval);
	}

	@Override
	public void onTerminate() {
		if (mBound) {
			unbindService(mConnection);
			mBound = false;
		}
		super.onTerminate();
	}

	// FIXME shitcode
	private void waitForService() {
		int i = 2000;
		while (i-- > 0) {
			if (mService != null) {
				break;
			}
			try {
				Thread.sleep(2);
			} catch (final InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public interface NodeServiceConnector {
		public void onNodeConnected(NodeWorkerService service);
	}

	/**
	 *
	 * @hide
	 */
	public void getNodeService(final NodeServiceConnector connector) {
		mNodeConnector = connector;
		if (mService == null) {
			// if server was destroyed, try to rebind
			bindNodeService();
		} else {
			connector.onNodeConnected(mService);
		}
	}

	/*
	public NodeWorkerService getNodeService() {
		if (mService == null) {
			// if server was destroyed, try to rebind
			bindNodeService();
			// lets wait 4secs
			waitForService();
		}

		return mService;
	}*/

	public void setNodeService(final NodeWorkerService mService) {
		this.mService = mService;
	}

	public static class UpdateSessionTimerTask extends TimerTask {

		WeakReference<Context> mContext;

		public UpdateSessionTimerTask(final Context c) {
			mContext = new WeakReference<Context>(c);
			Log.i(TAG, "::UpdateSessionTimerTask:" + " task init");
		}

		@Override
		public void run() {
			final Context context = mContext.get();
			if (context != null) {
				SyncService.startSyncService(context,
					AppSyncService.UPDATE_SESSION_REQUEST, new UpdateSessionReceiver(this), Bundle.EMPTY);
			}

		}

	}

	protected static class UpdateSessionReceiver implements Receiver {
		public final String TAG = UpdateSessionReceiver.class.getCanonicalName();
		private final UpdateSessionTimerTask mTask;

		public UpdateSessionReceiver(final UpdateSessionTimerTask task) {
			mTask = task;
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
//			synchronized (mTask) {
//				mTask.notifyAll();
//			}
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start UpdateSession");

				break;
			case SyncService.STATUS_FINISHED:
				Log.i(TAG, "UpdateSession done");

				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error UpdateSession");

				break;
			}
		}
	}
}

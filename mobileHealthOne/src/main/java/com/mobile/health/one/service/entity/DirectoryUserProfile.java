package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.google.gson.annotations.SerializedName;
import com.mobile.health.one.db.entity.Gender;

public class DirectoryUserProfile implements BaseColumns {

	public static final String FULL_NAME = "full_name";
	public static final String PHONE = "phone";
	public static final String GENDER = "gender";
	public static final String SPECIALITY_AND_ADDRESS = "speciality_and_address";
	public static final String STATE = "state";
	public static final String CITY = "city";
	public static final String STREET = "street";
	public static final String ZIP_CODE = "zip_code";
	public static final String FIRST_NAME = "first_name";
	public static final String LAST_NAME = "last_name";
	public static final String PN = "pn";
	public static final String SPECIALITIES = "specialities";

	public static class Entity implements Parcelable {
		@SerializedName("ln")
		public String firstName;
		@SerializedName("fn")
		public String lastName;
		@SerializedName("full_name")
		public String fullName;
		@SerializedName("prac_phone")
		public String phone;
		@SerializedName("gender")
		public String gender;
		@SerializedName("pn")
		public String pn;
		@SerializedName("prac_state")
		public String state;
		@SerializedName("prac_city")
		public String city;
		@SerializedName("prac_zip")
		public String zipCode;
		@SerializedName("prac_1st_addr")
		public String street;
		@SerializedName("taxo_descr")
		public String[] speciality;

		public StringBuffer specialityWithAddress;
		public StringBuffer specialites;

		public Entity(final Parcel in) {
			fullName = in.readString();
			phone = in.readString();
			gender = Gender.get(in.readString());
			pn = in.readString();
			city = in.readString();
			state = in.readString();
			zipCode = in.readString();
			street = in.readString();
			firstName = in.readString();
			lastName = in.readString();
			speciality = (String[]) in.readArray(String.class
					.getClassLoader());
		}

		public void transformData() {
			gender = Gender.get(gender);
			specialityWithAddress = new StringBuffer();
			specialites = new StringBuffer();

			for (int i = 0; i < speciality.length; i++) {
				if(speciality[i] != null && !"".equals(speciality[i])) {
					specialites.append(speciality[i]);
					specialites.append(";");
				}
				specialityWithAddress.append(speciality[i]);
				if (i != speciality.length - 1) {
					specialityWithAddress.append(", ");
				}
			}

			specialityWithAddress.append(" ");
			specialityWithAddress.append(city);
			if (city != null) {
				specialityWithAddress.append(", ");
			}
			specialityWithAddress.append(state);
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(fullName);
			dest.writeString(phone);
			dest.writeString(gender);
			dest.writeString(pn);
			dest.writeString(city);
			dest.writeString(state);
			dest.writeString(street);
			dest.writeString(zipCode);
			dest.writeString(firstName);
			dest.writeString(lastName);
			dest.writeArray(speciality);
		}

		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};
	}
}
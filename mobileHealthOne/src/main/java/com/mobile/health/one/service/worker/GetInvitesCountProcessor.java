package com.mobile.health.one.service.worker;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.GetInvitesCountResponse;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Invite;
import com.mobile.health.one.util.Settings;

public class GetInvitesCountProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_IS_OUTGOING = "is_outgoing";
	public static final String EXTRA_STATUS = "status";

	private int inviteType;

	private static final Uri URI_INVITES_COUNTS = Uri.parse(String.format(
			"%s/api/archer_invite_api/%s", Settings.URI_BASE,
			AppSyncService.GET_INVITES_COUNT));
	private static final Uri URI_GROUP_INVITES_COUNTS = Uri.parse(String
			.format("%s/api/groups_api/%s", Settings.URI_BASE,
					AppSyncService.GET_GROUP_INVITES_COUNT));

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public GetInvitesCountProcessor(Context context) {
		super(context);
	}

	@Override
	protected void processInternal(ResultReceiver receiver,
			ContentResolver resolver, Bundle extras) throws IOException,
			HttpException, Exception {
		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);
		params.add(EXTRA_IS_OUTGOING, String.valueOf(0));
		params.add(EXTRA_STATUS, String.valueOf(Invite.Entity.STATUS_PENDING));

		this.inviteType = extras.getInt(Invite.Entity.EXTRA_TYPE,
				Invite.Entity.CONTACT_INVITES);

		tryRequest(receiver, false, params, GetInvitesCountResponse.class,
				extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			E result, Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			E result) {
		return result.getMessages();
	}

	@Override
	public Uri getUri() {
		switch (this.inviteType) {
		case Invite.Entity.CONTACT_INVITES:
			return URI_INVITES_COUNTS;
		case Invite.Entity.GROUP_INVITES:
			return URI_GROUP_INVITES_COUNTS;
		default:
			return URI_INVITES_COUNTS;
		}
	}
}
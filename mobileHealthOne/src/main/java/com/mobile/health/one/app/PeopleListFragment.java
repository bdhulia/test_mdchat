package com.mobile.health.one.app;

import android.os.Handler;
import android.support.v4.widget.CursorAdapter;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ProgressBar;

import com.mobile.health.one.service.NodeWorkerService.IStatusChangeReceiver;
import com.mobile.health.one.widget.TopBarLabelsView;

public abstract class PeopleListFragment extends LazyLoadFragment implements
		OnScrollListener, IStatusChangeReceiver {

	protected static final int ITEMS_PER_PAGE = 10;
	protected static final String EXTRA_CUR_CHOICE = "curChoice";
	protected static final String EXTRA_CUR_ID = "curId";
	
	public static final String PARAM_IS_LABEL_BAR_VISIBLE = "isLabelBarVisible";
	
	@Override
	public Handler getHandler() {
		return new Handler(getActivity().getMainLooper());
	}

	@Override
	public void onStatusTextChanged(long userId, String statusText) {

	}

	@Override
	public void onStatusChanged(long userId, boolean isAvailable) {

	}

	@Override
	protected void loadData(int page, boolean showProgress) {

	}

	@Override
	protected ProgressBar getLazyLoadingProgressView() {
		return null;
	}

	@Override
	protected CursorAdapter getCursorAdapter() {
		return null;
	}
	
	@Override
	public void onDetach() {
		if(this.getActivity() instanceof PeopleListActivity)
			((PeopleListActivity)this.getActivity()).onSearchCancel();
		super.onDetach();
	}
	
	protected abstract void loadData(final String labelId);
	
	public abstract TopBarLabelsView getTopBarLabelsView();
	
	public interface PeopleListListener {
		void onListItemClick(String name, long id);
	}
}
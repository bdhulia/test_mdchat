package com.mobile.health.one.app;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.app.PeopleListFragment.PeopleListListener;
import com.mobile.health.one.app.UserLabelListFragment.UserLabelClickListener;
import com.mobile.health.one.db.entity.User;
import com.mobile.health.one.search.TextTask.Listener;
import com.mobile.health.one.service.entity.GetContactsDataResponce;
import com.mobile.health.one.service.entity.GetDirectoryResponse;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.CursorSaver;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.SearchParams.AliasVariant;
import com.mobile.health.one.util.SearchParams.SearchType;
import com.mobile.health.one.widget.TopBarLabelsView;

public class PeopleListActivity extends AppFragmentActivity implements
OnNavigationListener, OnCheckedChangeListener, Listener, OnClickListener, UserLabelClickListener, PeopleListListener {

	public static final int REQUEST_GET_CONTACT = 1;
	public static final String ACTION_GET_CONTACT = MHOApplication.class.getPackage().getName() + ".GET_CONTACT";
	public static final String EXTRA_FRIEND_ONLY = "friendOnly";
	public static final String EXTRA_NAME = "name";
	public static final String EXTRA_ID = "id";
	public static final String EXTRA_SHOW_TAB = "show_tab";

	private SparseArray<PeopleListFragment> fragments;

	private int mCurrentFragmentId;

	private ViewSwitcher mSwitcher;
	private boolean startSearch = false;
	private SearchFragment mSearchFragment;
	private RadioGroup mFilter;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		// getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// getSupportActionBar().setListNavigationCallbacks(this.adapter, this);
		setContentView(R.layout.activity_people);
		mSwitcher = (ViewSwitcher) findViewById(R.id.switcher);

		final CharSequence searchText = AliasVariant.getSearchParam(getIntent());
		if (!TextUtils.isEmpty(searchText)) {
			startSearch  = true;
			mSearchFragment = SearchFragment.newInstance(this, SearchType.USERS, searchText);

		} else {
			mSearchFragment = SearchFragment.newInstance(this, SearchType.USERS);
		}

		// add top context menu
		getSupportFragmentManager()
		.beginTransaction()
			.add(mSearchFragment, SearchFragment.TAG)
			.commit();

		final boolean isFriendOnly = getIntent().getBooleanExtra(PeopleListActivity.EXTRA_FRIEND_ONLY, false);
		mFilter = (RadioGroup) findViewById(R.id.filter);
		if (!isFriendOnly) {
			mFilter.setOnCheckedChangeListener(this);
		} else {
			mFilter.setVisibility(View.GONE);
		}
		final int tab = getIntent().getIntExtra(EXTRA_SHOW_TAB, R.id.filter_contacts);

		showTab(tab);

	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menu_labels:
			final Intent i = new Intent(this, LabelsActivity.class);
			startActivityForResult(i, LabelsListFragment.REQUEST_GET_LABEL);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(final int itemPosition,
			final long itemId) {
		return false;
	}

	@Override
	public void onCheckedChanged(final RadioGroup group, final int checkedId) {
		showTab(checkedId);
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		if (resultCode == RESULT_OK) {
			getCurrentFragment().reset();
			getCurrentFragment().loadData();
		}
	};

	private void showTab(final int id) {
		final FragmentTransaction ft = getSupportFragmentManager()
				.beginTransaction();

		mCurrentFragmentId = id;
		if (fragments == null) {
			fragments = new SparseArray<PeopleListFragment>();
		}

		final Bundle params = new Bundle();
		Log.i(TAG, "::showTab:" + "startSearch " + startSearch);
		params.putBoolean(SearchParams.AliasVariant.IntentBuilder.EXTRA_FROM_DASHBOARD, startSearch);

		mFilter.check(id);

		final String action = getIntent().getAction();
		if (ACTION_GET_CONTACT.equals(action))
			params.putBoolean(action, true);

		switch (id) {
		case R.id.filter_contacts:
			params.putBoolean(User.IS_FRIEND, true);
			params.putBoolean(PeopleListFragment.PARAM_IS_LABEL_BAR_VISIBLE, true);
			mSearchFragment.setSearchType(SearchType.USERS);
			break;
		case R.id.filter_members:
			params.putInt(User.TYPE, User.TYPE_UNDEFINED);
			params.putBoolean(User.IS_FRIEND, false);
			params.putBoolean(PeopleListFragment.PARAM_IS_LABEL_BAR_VISIBLE, false);
			mSearchFragment.setSearchType(SearchType.MEMBERS);
			break;
		case R.id.filter_directory:
			params.putInt(User.TYPE, User.TYPE_HCP);
			params.putBoolean(User.IS_FRIEND, false);
			params.putBoolean(PeopleListFragment.PARAM_IS_LABEL_BAR_VISIBLE, false);
			mSearchFragment.setSearchType(SearchType.HCP);
			break;
		default:
			break;
		}
		getSupportActionBar().setDisplayShowHomeEnabled(true);
		final PeopleListFragment people;
		if (fragments.indexOfKey(id) < 0) {
			if(id == R.id.filter_directory)
				people = new DirectoryListFragment();
			else
				people = new ContactsListFragment();
			fragments.put(id, people);
			people.setArguments(params);
		} else {
			people = fragments.get(id);
		}
		people.reset();
		if (id == R.id.filter_contacts)
			ft.replace(R.id.labels, new UserLabelListFragment());

		ft.replace(R.id.list, people)
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
			.commit();
	}

	private PeopleListFragment getCurrentFragment() {
		return fragments.get(mCurrentFragmentId);
	}

	@Override
	public void onSearchStart() {
		Log.v(TAG, "::onSearchStart:" + "");
		final PeopleListFragment f = getCurrentFragment();
		//f.setSearch(true);
		if (f.getView() != null)
			f.setListShown(false);

	}

	@Override
	public void onSearchComplete(final IResponce responce) {
		final PeopleListFragment f = getCurrentFragment();

		final CursorSaver r = (CursorSaver) responce;
		final Cursor c = r.saveToCursor();

		if (isResponceFromSameFragment(f, responce)) {
			if (c != null && c.getCount() > 0) {
				f.setCursorSavingOld(c);
				f.setSearch(true);
			} else {
				f.setCursorSavingOld(null);
				f.setSearch(false);
			}
		}

		f.setListShown(true);

	}

	private boolean isResponceFromSameFragment(final PeopleListFragment f, final IResponce r) {
		return r instanceof GetContactsDataResponce && f instanceof ContactsListFragment
				|| r instanceof GetDirectoryResponse && f instanceof DirectoryListFragment;
	}

	@Override
	public void onSearchError(final String error) {
		ToastUtil.showText(this, error);
	}

	@Override
	public void onSearchRestart() {
		//final PeopleListFragment f = getCurrentFragment();
		//f.setSearch(false);
	}

	@Override
	public void onSearchCancel() {
		final PeopleListFragment f = getCurrentFragment();
		f.setSearch(false);
		f.setOldCursor();
	}

	/**
	 * Clicked on Labels button
	 */
	@Override
	public void onClick(final View v) {
		mSwitcher.showNext();
		getSupportFragmentManager().beginTransaction()
			.hide(mSearchFragment)
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
			.commit();
//		mOldTitle = getTitle();
		setTitle(R.string.title_activity_labels);
		mFilter.setVisibility(View.GONE);
	}

	@Override
	public void onUserLabeClick(final String id, final String namme) {
		final PeopleListFragment f = getCurrentFragment();
		f.reset();
		f.loadData(id);
		mSwitcher.showPrevious();
		getSupportFragmentManager().beginTransaction()
			.show(mSearchFragment)
			.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
			.commit();
//		setTitle(mOldTitle);
		mFilter.setVisibility(View.VISIBLE);
	}

	@Override
	public void onLabelTitle(final String title) {
		final TopBarLabelsView topBar = getCurrentFragment().getTopBarLabelsView();
		if (topBar != null)
			topBar.setTitleText(title);
	}

	@Override
	public void onListItemClick(final String name, final long id) {
		final Intent i = new Intent();
		i.putExtra(EXTRA_NAME, name);
		i.putExtra(EXTRA_ID, id);
		setResult(RESULT_OK, i);
		finish();
	}
}
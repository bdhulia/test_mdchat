package com.mobile.health.one.service.worker;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.Settings;

public class ChangeGroupInvitationStatusProcessor extends RequestProcessor {

	public static class Response implements IResponce, Parcelable {

		protected int code;

		private Boolean data;

		protected List<String> invalidParams;
		protected String messages;

		public static final Parcelable.Creator<Response> CREATOR = new Parcelable.Creator<Response>() {
			@Override
			public Response createFromParcel(final Parcel in) {
				return new Response(in);
			}

			@Override
			public Response[] newArray(final int size) {
				return new Response[size];
			}
		};

		public Response() {
			code = 0;
		}

		public Response(final Parcel in) {
			this();
			code = in.readInt();
			data = (Boolean) in.readValue(Boolean.class.getClassLoader());
			invalidParams = in.createStringArrayList();
			messages = in.readString();
		}

		@Override
		public int getCode() {
			return code;
		}

		public Boolean getData() {
			return data;
		}

		@Override
		public String getMessages() {
			return messages;
		}

		public List<String> getInvalidParams() {
			return invalidParams;
		}

		@Override
		public int describeContents() {
			return this.hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeInt(code);
			dest.writeValue(data);
			dest.writeStringList(invalidParams);
			dest.writeString(messages);
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 0;
		}

	}

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_STATUS = "status";
	public static final String EXTRA_GROUP_ID = "group_id";

	private static final Uri URI = Uri.parse(Settings.URI_BASE
			+ "/api/groups_api/changeInvitationStatus");
	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public ChangeGroupInvitationStatusProcessor(final Context context) {
		super(context);
	}

	@Override
	protected void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras)
					throws IOException, HttpException, Exception {
		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);

		params.add(EXTRA_STATUS, Integer.toString(extras.getInt(EXTRA_STATUS)));
		params.add(EXTRA_GROUP_ID, Long.toString(extras.getLong(EXTRA_GROUP_ID)));

		tryRequest(receiver, false, params, Response.class, extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

	@Override
	public Uri getUri() {
		return URI;
	}

}

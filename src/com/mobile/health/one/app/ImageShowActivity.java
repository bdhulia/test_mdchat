package com.mobile.health.one.app;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;

public class ImageShowActivity extends Activity implements ImageLoadingListener {
	private static final String TAG = ImageShowActivity.class.getSimpleName();

	/** Called when the activity is first created. */
	@Override
	public void onCreate(final Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);

	    final ImageLoader loader = ImageLoader.getInstance();
	    final ImageView view = new ImageView(this);
	    setContentView(view, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
	    loader.displayImage(getIntent().getStringExtra("url"), view, this);
	}

	@Override
	public void onLoadingStarted(final String imageUri, final View view) {
		Log.i(TAG, "::onLoadingStarted:" + imageUri);

	}

	@Override
	public void onLoadingFailed(final String imageUri, final View view,
			final FailReason failReason) {
		Log.i(TAG, "::onLoadingFailed:" + failReason.toString());

	}

	@Override
	public void onLoadingComplete(final String imageUri, final View view, final Bitmap loadedImage) {
		Log.i(TAG, "::onLoadingComplete:" + "");

	}

	@Override
	public void onLoadingCancelled(final String imageUri, final View view) {
		Log.i(TAG, "::onLoadingCancelled:" + "");

	}

}

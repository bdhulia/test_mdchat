package com.mobile.health.one.app;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filter;

import com.mobile.health.one.R;
import com.mobile.health.one.service.entity.SuggestResponce;
import com.mobile.health.one.service.entity.SuggestResponce.Element;
import com.mobile.health.one.service.worker.AutocompleteSuggestProcessor;

public class CharSequenceArrayAdapter extends ArrayAdapter<CharSequence> {

	private static final String TAG = CharSequenceArrayAdapter.class
			.getSimpleName();
	/**
	 *
	 */

	private class CharSequenceFilter extends Filter {

		@Override
		protected FilterResults performFiltering(final CharSequence constraint) {
			if (!TextUtils.isEmpty(constraint)) {
				final FilterResults results = new FilterResults();
				final AutocompleteSuggestProcessor processor = mProcessor;
				final Bundle extras = new Bundle(1);
				extras.putString(AutocompleteSuggestProcessor.EXTRA_QUERY,
						constraint.toString());
				final SuggestResponce responce = processor.doRequest(extras);
				final Element[] elements = responce.getData();
				final ArrayList<CharSequence> data = new ArrayList<CharSequence>(
						elements.length);

				final Editable text = mAutoCompleteView.getText();
				final String[] ids = text.getSpans(0, text.length(), String.class);

				for (final Element element : elements) {
//					Log.i(TAG, "::performFiltering:" + "key " + element.key);
//					Log.i(TAG, "::performFiltering:" + "val " + element.value);
					if (StringUtils.isAlphanumeric(element.key)) {
						final String id = element.key;
						final String value = element.value;
						if (!ArrayUtils.contains(ids, id))
							data.add(convertToString(mContext, id, value));
					}
				}
				results.values = data;
				results.count = data.size();
				return results;
			} else {
				return null;
			}
		}

		@SuppressWarnings("unchecked")
		@Override
		protected void publishResults(final CharSequence constraint,
				final FilterResults results) {
			Log.i(MessageCreateActivity.TAG, "::publishResults:"
					+ " constraint: " + constraint);
			// noinspection unchecked
			if (results != null) {
				clear();
				setData((List<CharSequence>) results.values);

				if (results.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}

		}

		@TargetApi(11)
		public void setData(final List<CharSequence> data) {
			clear();
			if (data != null) {
				// If the platform supports it, use addAll, otherwise add in
				// loop
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					addAll(data);
				} else {
					for (final CharSequence item : data) {
						add(item);
					}
				}
			}
		}

		@Override
		public CharSequence convertResultToString(final Object resultValue) {
			return (CharSequence) resultValue;
		}

	}

	private Filter mFilter;
	private final Context mContext;
	private final AutocompleteSuggestProcessor mProcessor;
	private final AutoCompleteTextView mAutoCompleteView;

	public CharSequenceArrayAdapter(
			final Context context, final int resource,
			final int textViewResourceId, final AutocompleteSuggestProcessor processor, final AutoCompleteTextView m) {
		super(context, resource, textViewResourceId);
		mContext = context;
		mProcessor = processor;
		mAutoCompleteView = m;
	}

	public static CharSequence convertToString(final Context context, final String id, final String name) {
		if (name != null && id != null) {
			final SpannableString str = new SpannableString(name);
			final BackgroundColorSpan colorSpan = new BackgroundColorSpan(
					context.getResources().getColor(R.color.label_bg));
			str.setSpan(id, 0, str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			str.setSpan(colorSpan, 0, str.length(),
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			return str;
		} else {
			return null;
		}
	}

	public static CharSequence convertToString(final Context context, final Long id, final String name) {
		if (name != null && id > 0) {
			final SpannableString str = new SpannableString(name);
			final BackgroundColorSpan colorSpan = new BackgroundColorSpan(
					context.getResources().getColor(R.color.label_bg));
			str.setSpan(id, 0, str.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			str.setSpan(colorSpan, 0, str.length(),
					Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			return str;
		} else {
			return null;
		}
	}

	@Override
	public Filter getFilter() {
		if (mFilter == null) {
			mFilter = new CharSequenceFilter();
		}
		return mFilter;
	}

}
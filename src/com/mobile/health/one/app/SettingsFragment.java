package com.mobile.health.one.app;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobile.health.one.R;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.util.SettingsManager;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class SettingsFragment extends SherlockFragment implements OnClickListener {

	protected static final String TAG = SettingsFragment.class
			.getCanonicalName();

	public static interface OnUnregisterDeviceListener {
		public void onUnregisterDevice();
		public void onUnregisterError(String error);
	}

	private static class UnregisterDeviceReceiver implements Receiver {

		private final SettingsManager settingsManager;
		private final Context context;


		public UnregisterDeviceReceiver(final Context context) {
			settingsManager = new SettingsManager(context);
			this.context = context;
		}

		@Override
		public void onReceiveResult(final int resultCode, final Bundle resultData) {
			OnUnregisterDeviceListener callback = null;
			if (context instanceof OnUnregisterDeviceListener) {
				callback = (OnUnregisterDeviceListener) context;
			}
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start Unregister Device");
				break;
			case SyncService.STATUS_FINISHED:
				settingsManager.removePinRegistered();
				settingsManager.removeLogin();
				settingsManager.removeSessionId();
				settingsManager.removePin();
				if (callback != null)
					callback.onUnregisterDevice();
				Log.i(TAG, "Device Unregistered");
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error Unregister Device");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					if (callback != null)
						callback.onUnregisterError(error);
				}
				break;
			}

		}

	}

	public static class ConfirmUnregisterDialog extends DialogFragment implements DialogInterface.OnClickListener {
		private final Context context;
		ConfirmUnregisterDialog(final Context c) {
			context = c;
		}
		@Override
		public Dialog onCreateDialog(final Bundle savedInstanceState) {
			final AlertDialog.Builder dialog = new AlertDialog.Builder(context);
			dialog.setMessage(R.string.unregister_message);
			dialog.setTitle(R.string.unregister_title);
			dialog.setPositiveButton(R.string.btn_unregister, this);
			dialog.setNegativeButton(R.string.btn_no, this);
			return dialog.create();
		}
		@Override
		public void onClick(final DialogInterface dialog, final int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				SyncService.startSyncService(context,
						AppSyncService.UNREGISTER_DEVICE_REQUEST,
						new SettingsFragment.UnregisterDeviceReceiver(context), Bundle.EMPTY);
				dialog.dismiss();
				break;
			case DialogInterface.BUTTON_NEGATIVE:
				dialog.cancel();
				break;
			default:
				break;
			}

		}
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
			final Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.fragment_settings, container, false);
		v.findViewById(R.id.change_passcode).setOnClickListener(this);
		v.findViewById(R.id.profile).setOnClickListener(this);
		v.findViewById(R.id.unregister_device).setOnClickListener(this);
		final TextView versionView = (TextView) v.findViewById(R.id.version);
		PackageInfo pInfo;
		try {
			pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
			versionView.setText(pInfo.versionName);
		} catch (final NameNotFoundException e) {
			// do nothing, because this should not happen
		}
		return v;
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.change_passcode:
			startActivity(new Intent(getActivity(), ChangePinActivity.class));
			break;
		case R.id.profile:
			UserProfileActivity.startActivity(getActivity(), true);
			break;
		case R.id.unregister_device:
			new ConfirmUnregisterDialog(getActivity()).show(getFragmentManager(), TAG);
			break;

		default:
			break;
		}

	}


}

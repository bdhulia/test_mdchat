package com.mobile.health.one.app;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.app.SettingsFragment.OnUnregisterDeviceListener;
import com.mobile.health.one.util.AlertDialogUtil;

public class MessageConfirmActivity extends AppFragmentActivity implements OnUnregisterDeviceListener {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.read_confirmations);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		final String messageId = getIntent().getStringExtra(MessageDetailFragment.EXTRA_ID);

		ft.add(android.R.id.content, MessageConfirmFragment.newInstance(messageId)).commit();
	}

	@Override
	public void onUnregisterDevice() {
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onUnregisterError(final String error) {
		AlertDialogUtil.errorDialog(this, error).show();
	}

}

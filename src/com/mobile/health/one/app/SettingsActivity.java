package com.mobile.health.one.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NavUtils;

import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.app.SettingsFragment.OnUnregisterDeviceListener;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.util.AlertDialogUtil;

public class SettingsActivity extends AppFragmentActivity implements OnUnregisterDeviceListener {

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle(R.string.menu_settings);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(android.R.id.content, new SettingsFragment()).commit();
		
		//FIXME DEBUG!!!
		// stopService(new Intent(this, NodeWorkerService.class));
		//END DEBUG
	}

	@Override
	public void onUnregisterDevice() {
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onUnregisterError(final String error) {
		AlertDialogUtil.errorDialog(this, error).show();
	}

}

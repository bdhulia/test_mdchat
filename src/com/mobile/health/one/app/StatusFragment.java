package com.mobile.health.one.app;

import java.lang.ref.WeakReference;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.internal.nineoldandroids.animation.AnimatorSet;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.service.worker.GetStatusProcessor;
import com.mobile.health.one.service.worker.GetStatusTextProcessor;
import com.mobile.health.one.service.worker.SetStatusProcessor;
import com.mobile.health.one.service.worker.SetStatusTextProcessor;
import com.mobile.health.one.util.TextCounterListener;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class StatusFragment extends SherlockFragment implements
OnFocusChangeListener, OnClickListener {
	AnimatorSet fadeOutAnimation;
	AnimatorSet fadeInAnimation;
	private EditText status;
	private InputMethodManager imm;
	private PopupWindow statusWindow;
	private ImageView btnStatus;
	private View bottomPanel;

	public interface StatusChangeCallback {
		public void onGetStatus();

		public void onSetStatus();

		public void onGetStatusText();

		public void onSetStatusText();
	}

	protected static class StatusReceiver implements Receiver {
		public static final int METHOD_GET_STATUS = 1;
		public static final int METHOD_SET_STATUS = 2;
		public static final int METHOD_GET_STATUS_TEXT = 3;
		public static final int METHOD_SET_STATUS_TEXT = 4;

		private final int method;
		private boolean available;
		private final WeakReference<StatusFragment> fragment;

		public StatusReceiver(final StatusFragment fragment, final int method) {
			this.fragment = new WeakReference<StatusFragment>(fragment);
			this.method = method;
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final StatusFragment f = fragment.get();
			if (f == null || !f.isVisible()) return; // do nothing if we lost fragment
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				f.setProgress(true);
				break;
			case SyncService.STATUS_FINISHED:
				if (resultData != null) {
					switch (method) {
					case METHOD_GET_STATUS_TEXT:
						final String text = resultData
						.getString(GetStatusTextProcessor.EXTRA_STATUS_TEXT);
						f.status.setText(text);
						break;
					case METHOD_SET_STATUS_TEXT:
						break;
					case METHOD_GET_STATUS:
						available = resultData.getBoolean(GetStatusProcessor.EXTRA_STATUS);
						f.btnStatus.setImageResource(available ? R.drawable.status_circle_on : R.drawable.status_circle_off);
						break;
					case METHOD_SET_STATUS:
						available = resultData.getBoolean(SetStatusProcessor.EXTRA_STATUS);
						f.btnStatus.setImageResource(available ? R.drawable.status_circle_on : R.drawable.status_circle_off);
						break;
					default:
						break;
					}
				}

				f.setProgress(false);
				break;
			case SyncService.STATUS_ERROR:
				if (resultData != null) {
					final Responce resp = resultData
							.getParcelable(RequestProcessor.EXTRA_RESPONSE);
					if (resp != null && resp.getCode() != RequestProcessor.STATUS_SUCCESS && f.getActivity() != null) {
						f.getActivity().finish();
						final String error = resultData
								.getString(RequestProcessor.EXTRA_ERROR_STRING);
						ToastUtil.showText(f.getActivity(), error);
					}
				}
				f.setProgress(false);
				break;
			}
		}

	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_status, container,
				false);
		status = (EditText) view.findViewById(R.id.status_text);
		bottomPanel = view.findViewById(R.id.bottomPanel);
		final Button btnCancel = (Button) view.findViewById(R.id.cancel);
		final Button btnSubmit = (Button) view.findViewById(R.id.submit);
		btnStatus = (ImageView) view.findViewById(R.id.status_icon);
		status.setOnFocusChangeListener(this);
		final TextView count = (TextView) view.findViewById(R.id.count);
		if (count != null) {
			final int maxChars = getResources().getInteger(R.integer.max_status_chars);
			status.addTextChangedListener(new TextCounterListener(count, maxChars));
		}
		btnSubmit.setOnClickListener(this);
		btnCancel.setOnClickListener(this);
		btnStatus.setOnClickListener(this);

		imm = (InputMethodManager) getActivity().getSystemService(
				Context.INPUT_METHOD_SERVICE);

		final LinearLayout popup = (LinearLayout) inflater.inflate(
				R.layout.popup_status, null, false);
		popup.findViewById(R.id.status_available).setOnClickListener(this);
		popup.findViewById(R.id.status_unavailable).setOnClickListener(this);

		statusWindow = prepareWindow(popup);

		return view;
	}

	private PopupWindow prepareWindow(final View v) {
		final PopupWindow w = new PopupWindow(v);
		w.setTouchInterceptor(new OnTouchListener() {
			@Override
			public boolean onTouch(final View v, final MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					w.dismiss();

					return true;
				}

				return false;
			}
		});

		// workaround for outside touch events to work
		w.setBackgroundDrawable(getResources().getDrawable(R.drawable.popup_status));
		w.setWidth(LayoutParams.WRAP_CONTENT);
		w.setHeight(LayoutParams.WRAP_CONTENT);
		w.setOutsideTouchable(true);

		return w;
	}

	@Override
	public void onDetach() {
		dismissPopup();
		super.onDetach();
	}

	private void dismissPopup() {
		if (statusWindow != null && statusWindow.isShowing()) {
			statusWindow.dismiss();
		}
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		SyncService.startSyncService(getActivity(),
				AppSyncService.GET_STATUS_TEXT_REQUEST, new StatusReceiver(this,
						StatusReceiver.METHOD_GET_STATUS_TEXT),
						Bundle.EMPTY);
		SyncService.startSyncService(getActivity(),
				AppSyncService.GET_STATUS_REQUEST, new StatusReceiver(this,
						StatusReceiver.METHOD_GET_STATUS),
						Bundle.EMPTY);
	}

	private void fadeOut() {
		bottomPanel.setVisibility(View.GONE);
		//status.setLines(1);
	}

	private void fadeIn() {
		bottomPanel.setVisibility(View.VISIBLE);
		//status.setLines(3);
	}

	public void setProgress(final boolean b) {
		final SherlockFragmentActivity a = getSherlockActivity();
		if (a != null) {
			a.setSupportProgressBarIndeterminateVisibility(b);
		}
	}

	@Override
	public void onFocusChange(final View v, final boolean hasFocus) {
		switch (v.getId()) {
		case R.id.status_text:
			if (hasFocus) {
				fadeIn();
			} else {
				fadeOut();
				imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
			}
			break;
		}

	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.submit:
			setStatusText(status.getText().toString());
			status.clearFocus();
			break;
		case R.id.cancel:
            SyncService.startSyncService(getActivity(),
                    AppSyncService.GET_STATUS_TEXT_REQUEST, new StatusReceiver(this,
                    StatusReceiver.METHOD_GET_STATUS_TEXT),
                    Bundle.EMPTY);
			status.clearFocus();
			break;
		case R.id.status_icon:
			statusWindow.showAsDropDown(v, 0, -v.getHeight());
			break;
		case R.id.status_available:
			setStatus(true);
			break;
		case R.id.status_unavailable:
			setStatus(false);
			break;
		default:
			break;
		}


	}

	public void setStatus(final boolean available) {
		final Bundle extras = new Bundle();
		extras.putBoolean(SetStatusProcessor.EXTRA_STATUS, available);
		SyncService.startSyncService(getActivity(),
				AppSyncService.SET_STATUS_REQUEST, new StatusReceiver(this,
						StatusReceiver.METHOD_SET_STATUS), extras);
		final FragmentActivity a = getActivity();
		if (a != null) {
			((MHOApplication) a.getApplication()).getNodeService(new NodeServiceConnector() {

				@Override
				public void onNodeConnected(final NodeWorkerService service) {
					service.changeIconStatus(available);
				}
			});
		}

		dismissPopup();
	}

	public void setStatusText(final String status) {
		final Bundle extras = new Bundle();
		extras.putString(SetStatusTextProcessor.EXTRA_STATUS_TEXT, status);
		SyncService.startSyncService(getActivity(),
				AppSyncService.SET_STATUS_TEXT_REQUEST, new StatusReceiver(this,
						StatusReceiver.METHOD_SET_STATUS_TEXT), extras);

		final FragmentActivity a = getActivity();
		if (a != null) {
			((MHOApplication) a.getApplication()).getNodeService(new NodeServiceConnector() {

				@Override
				public void onNodeConnected(final NodeWorkerService service) {
					service.changeTextStatus(status);
				}
			});
		}
	}
}

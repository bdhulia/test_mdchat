package com.mobile.health.one.app;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

/**
 * Class, that holds a set of text input objects
 * with ability to perform validation
 * 
 * @author Igor Yanishevskiy
 *
 */
public class ViewHolder {
	/**
	 * Interface to implement to perform validation
	 * 
	 * @author rimmer
	 *
	 */
	public static interface Validator {
		/**
		 * Object should be returned by {@link Validator#onValidate(ViewHolder, CharSequence, boolean)} callback
		 * @author rimmer
		 *
		 */
		public static class ValidationResult {
			boolean result;
			CharSequence error;

			/**
			 * Constructor for validation with errors
			 */
			public ValidationResult(final boolean result,
					final CharSequence error) {
				this.result = result;
				this.error = error;
			}

			/**
			 * Constructor for success validation
			 */
			public ValidationResult() {
				result = true;
				error = null;
			}
		}

		/**
		 * Callback called when text in input was changed
		 * 
		 * @param s
		 *            string to validate
		 * @return ValidationResult with error message on error and boolean
		 *         result
		 */
		Validator.ValidationResult onValidate(ViewHolder vh, CharSequence s);

		/**
		 * Callback called when text input lose focus
		 * 
		 * @param s
		 *            string to validate
		 * @return ValidationResult with error message on error and boolean
		 *         result
		 */
		Validator.ValidationResult onLoseFocus(ViewHolder vh, CharSequence s);
	}

	final EditText view;
	final ViewHolder.Validator validator;
	private boolean valid;
	private final boolean validateImmediately;

	/**
	 * Object for storing associated info
	 */
	Object tag;

	/**
	 * Creates a new {@link ViewHolder} with validate immediately option and no tag object
	 * 
	 * @param view a TextView validation is set to
	 * @param validator {@link Validator} object
	 */
	public ViewHolder(final EditText view, final ViewHolder.Validator validator) {
		this(view, validator, true, null);
	}

	/**
	 * Creates a new {@link ViewHolder} without tag object
	 * 
	 * @param view a TextView validation is set to
	 * @param validator {@link Validator} object
	 * @param validateImmediately this param says the validator
	 * 		to validate data when user is typying or after it
	 */
	public ViewHolder(final EditText view,
			final ViewHolder.Validator validator,
			final boolean validateImmediately) {
		this(view, validator, validateImmediately, null);
	}

	/**
	 * Creates a new {@link ViewHolder} without tag object
	 * 
	 * @param view a TextView validation is set to
	 * @param validator {@link Validator} object
	 * @param validateImmediately this param says the validator
	 * 		to validate data when user is typing or after it
	 * @param tag a simple object associated with holder
	 */
	public ViewHolder(final EditText view,
			final ViewHolder.Validator validator,
			final boolean validateImmediately, final Object tag) {
		this.view = view;
		this.validator = validator;
		this.tag = tag;
		this.validateImmediately = validateImmediately;

		this.view.addTextChangedListener(new ValidatorTextWatcher());
	}

	private void validate(final CharSequence s) {
		final Validator.ValidationResult res = this.validator.onLoseFocus(this, s);
		if (!res.result) {
			this.view.setError(res.error);
			this.valid = false;
		} else {
			this.view.setError(null);
			this.valid = true;
		}

	}

	private class ValidatorTextWatcher implements TextWatcher {
		@Override
		public void beforeTextChanged(final CharSequence s, final int start,
				final int count, final int after) { /* nop */
		}

		@Override
		public void onTextChanged(final CharSequence s, final int start,
				final int before, final int count) {
			if (ViewHolder.this.validateImmediately)
				ViewHolder.this.validator.onValidate(ViewHolder.this, s);
			// we wont set error state for now here
		}

		@Override
		public void afterTextChanged(final Editable s) {
			if (ViewHolder.this.validateImmediately && ViewHolder.this.view.getVisibility() != View.GONE)
				validate(s);
		}
	}

	/**
	 * Do the validation and return the result
	 * 
	 * @return boolean
	 */
	public boolean isValid() {
		// lets validate results
		if (this.view.getVisibility() != View.GONE)
			validate(this.view.getText());

		return this.valid;
	}

	public void setValid(final boolean valid) {
		this.valid = valid;
	}
}
package com.mobile.health.one.app;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.URLUtil;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobile.health.one.R;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.Group;
import com.mobile.health.one.service.entity.GroupResponce;
import com.mobile.health.one.service.worker.GetUserProfileProcessor;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.nostra13.universalimageloader.core.ImageLoader;

public class GroupDetailFragment extends SherlockFragment implements OnClickListener {
	private static final String TAG = "GroupDetailFragment";

	private class GroupReceiver implements Receiver {

		@Override
		public void onReceiveResult(final int resultCode, final Bundle resultData) {
			setLoading(resultCode);

			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				break;
			case SyncService.STATUS_FINISHED:
				Log.i(TAG, "Getting group start");
				final GroupResponce responce = resultData
				.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				if (responce != null) {
					setData(responce.getData());
				}

				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error getting group");
				break;
			}
		}

	}

	private SparseArray<TextView> mTextViewCache;
	private String mEditableTag;
	private ImageView mPhoneIcon;
	private ProgressBar mProgress;
	private TextView mEmptyView;
	private ImageView mUserAvatar;
	private ImageView mGroupAvatar;

	private void loadTextViewsCache(final View viewGroup) {
		if (viewGroup instanceof ViewGroup) {
			if (mEditableTag == null) {
				mEditableTag = getString(R.string.tag_editable);
			}
			final ViewGroup g = (ViewGroup) viewGroup;
			final int count = g.getChildCount();
			if (mTextViewCache == null) {
				mTextViewCache = new SparseArray<TextView>(count);
			}
			for (int i = 0; i < count; i++) {
				final View child = g.getChildAt(i);
				final Object tag = child.getTag();
				if (child instanceof TextView && tag instanceof String && mEditableTag.equals(tag)) {
					mTextViewCache.append(child.getId(), (TextView) child);
				} else if (child instanceof ViewGroup) {
					loadTextViewsCache(child);
				}
			}
		} else {
			throw new IllegalArgumentException("Parameter viewGroup should be ViewGroup type");
		}
	}

	public void setData(final Group data) {
		if (getActivity() != null) {
			getActivity().setTitle(data.short_name);
		}
		if (!data.is_public) {
			setText(R.id.group_type, getString(R.string.group_private));
		}
		setText(R.id.street_addr_1, data.street_address_1);
		setText(R.id.street_addr_2, data.street_address_2);
		setText(R.id.city, data.city);
		setText(R.id.state, data.state);
		setText(R.id.zipcode, data.zip_code);
		setTextUrl(R.id.url, data.url);
		setText(R.id.phone, data.phone);
		setText(R.id.fax, data.fax);
		setText(R.id.admin_name, data.admin.fullName);
		setText(R.id.full_desciption, data.description);
		if (data.admin.id == null) {
			data.admin.id = data.admin.id2;
		}
		setAdminClickListener(R.id.admin_name, data.admin.id);

		mPhoneIcon.setTag(data.phone);

		final ImageLoader loader = ImageLoader.getInstance();
		String uri = data.avatar_link;
		if (uri == null || !URLUtil.isValidUrl(uri)){
			uri = null;
		}
		loader.displayImage(uri, mGroupAvatar);

		uri = data.admin.avatarLink;
		if (uri == null || !URLUtil.isValidUrl(uri)){
			uri = null;
		}
		loader.displayImage(uri, mUserAvatar);
		mUserAvatar.setTag(data.admin.id);
		mUserAvatar.setOnClickListener(this);
	}

	private void setText(final int id, final CharSequence text) {
		final TextView view = mTextViewCache.get(id);
		if (TextUtils.isEmpty(text)) {
			view.setText(R.string.nop);
		} else {
			view.setText(text);
		}
	}

	private void setTextUrl(final int id, final CharSequence text) {
		final TextView view = mTextViewCache.get(id);
		if (TextUtils.isEmpty(text)) {
			view.setText(R.string.nop);
		} else {
			final String link = "<a href=\"" + text + "\" >" + text + "</a>";
			view.setMovementMethod(LinkMovementMethod.getInstance());
			view.setText(Html.fromHtml(link));
		}
	}

	private void setAdminClickListener(final int id, final Long userid) {
		final TextView view = mTextViewCache.get(id);
		view.setTag(userid);
		view.setOnClickListener(this);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.fragment_group_detail,
				container, false);
		mPhoneIcon = (ImageView) v.findViewById(R.id.ic_phone);
		mPhoneIcon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				final String phoneNumber = (String) v.getTag();
				if (phoneNumber != null) {
					final Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phoneNumber));
					startActivity(i);
				}
			}
		});
		final String nop = getString(R.string.nop);
		final OnClickListener listener = new OnClickListener() {

			@Override
			public void onClick(final View v) {
				final TextView textView = (TextView) v;
				final CharSequence phoneNumber = textView.getText();
				if (!TextUtils.isEmpty(phoneNumber) && !phoneNumber.equals(nop)) {
					final Intent i = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+phoneNumber));
					startActivity(i);
				}
			}
		};
		v.findViewById(R.id.phone).setOnClickListener(listener);
		v.findViewById(R.id.fax).setOnClickListener(listener);

		mUserAvatar = (ImageView) v.findViewById(R.id.user_avatar);
		mGroupAvatar = (ImageView) v.findViewById(R.id.avatar);
		initLoadingControls(v);
		loadTextViewsCache(v);

		return v;
	}

	@Override
	public void onAttach(final Activity activity) {
		super.onAttach(activity);
		SyncService.startSyncService(activity, AppSyncService.GET_GROUP, new GroupReceiver(), getArguments());
	}

	private void initLoadingControls(final View viewToLookIn) {
		mProgress = (ProgressBar) viewToLookIn.findViewById(android.R.id.progress);
		mEmptyView = (TextView) viewToLookIn.findViewById(android.R.id.empty);
	}
	private void setLoading(final int status) {
		switch (status) {
		case SyncService.STATUS_RUNNING:
			mProgress.setVisibility(View.VISIBLE);
			mEmptyView.setVisibility(View.VISIBLE);
			mEmptyView.setText(null);
			break;
		case SyncService.STATUS_FINISHED:
			mProgress.setVisibility(View.INVISIBLE);
			mEmptyView.setVisibility(View.INVISIBLE);
			//mEmptyView.setText(null);
			break;
		case SyncService.STATUS_ERROR:
			mProgress.setVisibility(View.INVISIBLE);
			mEmptyView.setVisibility(View.VISIBLE);
			mEmptyView.setText(R.string.error_getting_group_info);
			break;
		}
	}

	@Override
	public void onClick(final View v) {
		final Bundle args = new Bundle();
		final Long id = (Long) v.getTag();
		if (id != null) {
			args.putLong(GetUserProfileProcessor.EXTRA_USER_ID, (Long) v.getTag());
			final Intent intent = new Intent();
			intent.setClass(getActivity(), UserProfileActivity.class);
			intent.putExtras(args);
			startActivity(intent);
		}
	}
}

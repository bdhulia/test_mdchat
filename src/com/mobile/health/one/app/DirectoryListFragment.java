package com.mobile.health.one.app;

import java.lang.ref.WeakReference;

import org.apache.commons.lang.StringUtils;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.util.LongSparseArray;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mobile.health.one.R;
import com.mobile.health.one.app.RegisterPinFragment.RegisterPinReceiver;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.DirectoryUserProfile;
import com.mobile.health.one.service.entity.GetDirectoryResponse;
import com.mobile.health.one.service.worker.GetContactsDataProcessor;
import com.mobile.health.one.util.CursorSaver;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.mobile.health.one.widget.TopBarLabelsView;

public class DirectoryListFragment extends PeopleListFragment implements
		OnScrollListener {

	private static final String TAG = DirectoryListFragment.class.getName();

	private TopBarLabelsView mTopBarLabelsView;

	private boolean isTopBarVisible;

	private SimpleCursorAdapter mAdapter;

	private ProgressBar mProgress;

	private LongSparseArray<Integer> mIdToPositionMap;

	@Override
	protected void loadData(final String labelId) {
		loadData(getCurrentPage(), true);
	}

	@Override
	protected ProgressBar getLazyLoadingProgressView() {
		return mProgress;
	}

	@Override
	public TopBarLabelsView getTopBarLabelsView() {
		return mTopBarLabelsView;
	}

	@Override
	protected CursorAdapter getCursorAdapter() {
		return mAdapter;
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		init();
		loadData();
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		isTopBarVisible = getArguments().getBoolean(PARAM_IS_LABEL_BAR_VISIBLE);
		if (isTopBarVisible) {
			final ViewGroup v = (ViewGroup) super.onCreateView(inflater,
					container, savedInstanceState);
			final ListView list = (ListView) v.findViewById(android.R.id.list);
			final Resources res = getActivity().getResources();
			v.findViewById(android.R.id.list).setPadding(list.getPaddingLeft(),
					res.getDimensionPixelSize(R.dimen.top_bar_height),
					list.getPaddingRight(), list.getPaddingBottom());
			final LayoutAnimationController controller = new LayoutAnimationController(
					AnimationUtils.loadAnimation(getActivity(),
							android.R.anim.fade_in));
			list.setLayoutAnimation(controller);

			mTopBarLabelsView = new TopBarLabelsView(getActivity(),
					(OnClickListener) getActivity());
			v.addView(mTopBarLabelsView);
			return v;
		} else {
			return super.onCreateView(inflater, container, savedInstanceState);
		}
	}

	public void setResponce(final CursorSaver resp) {

		final MatrixCursor c = (MatrixCursor) resp.saveToCursor();
		final int idIndex = c.getColumnIndex(BaseColumns._ID);

		int position = 0;

		mIdToPositionMap = new LongSparseArray<Integer>(c.getCount());
		while (c.moveToNext()) {
			final Long id = c.getLong(idIndex);
			mIdToPositionMap.append(id, position++);
		}

		c.moveToFirst();
		setCursor(c);
	}

	private void init() {
		setEmptyText(getString(R.string.error_no_people));

		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);

		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new SimpleCursorAdapter(
				getActivity(),
				R.layout.item_directory_list,
				null,
				new String[] { DirectoryUserProfile.FULL_NAME,
						DirectoryUserProfile.SPECIALITY_AND_ADDRESS,
						DirectoryUserProfile.GENDER, DirectoryUserProfile.PHONE },
				new int[] { R.id.name, R.id.speciality, R.id.gender, R.id.phone },
				0);

		mProgress = new ProgressBar(getActivity());
		mProgress.setIndeterminate(true);
		mProgress.setVisibility(View.GONE);
		setListAdapter(mAdapter);
		getListView().setOnScrollListener(this);
		getListView().addFooterView(mProgress);

		isTopBarVisible = getArguments().getBoolean(PARAM_IS_LABEL_BAR_VISIBLE);
	}

	@Override
	protected void loadData(final int page, final boolean showProgress) {
		Log.v(TAG, "page: " + page);
		if (page > 0) {
			getArguments().putString(
					GetContactsDataProcessor.EXTRA_ITEMS_PER_PAGE,
					Integer.toString(ITEMS_PER_PAGE));
			getArguments().putString(
					GetContactsDataProcessor.EXTRA_CURRENT_PAGE,
					Integer.toString(page));
			SyncService.startSyncService(getActivity(),
					AppSyncService.GET_CONTACTS_DATA_REQUEST,
					new DirectoryUserReceiver(this), getArguments());

			if (getListAdapter().isEmpty())
				setListShown(false);
		}
	}

	@Override
	public void onListItemClick(final ListView l, final View v,
			final int position, final long id) {
		final Cursor cursor = (Cursor) getListAdapter().getItem(position);

		String fullName = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.FULL_NAME));
		final String pn = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.PN));
		final String gender = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.GENDER));
		final String state = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.STATE));
		final String city = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.CITY));
		final String zipCode = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.ZIP_CODE));
		final String phone = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.PHONE));
		final String street = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.STREET));
		final String specialities = cursor.getString(cursor
				.getColumnIndex(DirectoryUserProfile.SPECIALITIES));

		if (StringUtils.isNotEmpty(pn)) {
			fullName = pn + ", " + fullName;
		}

		final Bundle args = new Bundle();
		args.putString(DirectoryUserProfile.FULL_NAME, fullName);
		args.putString(DirectoryUserProfile.PHONE, phone);
		args.putString(DirectoryUserProfile.ZIP_CODE, zipCode);
		args.putString(DirectoryUserProfile.CITY, city);
		args.putString(DirectoryUserProfile.STATE, state);
		args.putString(DirectoryUserProfile.STREET, street);
		args.putString(DirectoryUserProfile.GENDER, gender);
		args.putString(DirectoryUserProfile.SPECIALITIES, specialities);

		final Intent intent = new Intent(getActivity(), DirectoryUserProfileActivity.class);
		intent.putExtras(args);
		startActivity(intent);
	}

	protected class DirectoryUserReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();
		private final WeakReference<DirectoryListFragment> fragment;

		public DirectoryUserReceiver(final DirectoryListFragment fragment) {
			this.fragment = new WeakReference<DirectoryListFragment>(fragment);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final DirectoryListFragment f = fragment.get();
			if (f == null)
				return; // no action if fragment destroyed
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Started directory users fetching");
				if (f.getPrevTotalItemCount() == 0) {
					if (getView() != null)
						f.setListShown(false);
				} else {
					f.mProgress.setVisibility(View.VISIBLE);
				}
				break;
			case SyncService.STATUS_FINISHED:
				f.mProgress.setVisibility(View.GONE);
				final GetDirectoryResponse response = resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				if (response != null) {
					Log.i(TAG, "Saving to db start");
					f.setTotalItemCount(response.getCount());
					if (f != null && f.isVisible()) {
						final CursorSaver resp = response;
						f.setResponce(resp);
						if (f.isResumed() && getView() != null) {
							f.setListShown(true);
							if (f.getPrevTotalItemCount() == 0)
								f.setListShown(true);
							else {
								f.mProgress.setVisibility(View.GONE);
							}
						} else {
							f.setListShownNoAnimation(true);
						}
					}
				}
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error getting directory users");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(f.getActivity(), error);
				}

				f.getActivity().finish();
				break;
			}
		}
	}
}
package com.mobile.health.one.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.app.UserLabelListFragment.UserLabelClickListener;

public class UserProfileActivity extends AppFragmentActivity implements UserLabelClickListener {

	public static final String EXTRA_IS_OWN_PROFILE = "own_profile";

	private UserProfileFragment mFragment;

	public static void startActivity(final Context context, final boolean ownProfile) {
		final Intent i = new Intent(context, UserProfileActivity.class);
		i.putExtra(EXTRA_IS_OWN_PROFILE, ownProfile);
		context.startActivity(i);
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(com.actionbarsherlock.view.Window.FEATURE_INDETERMINATE_PROGRESS);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		setTitle(R.string.profile);
		mFragment = new UserProfileFragment();
		mFragment.setArguments(getIntent().getExtras());

		final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
		ft.add(android.R.id.content, mFragment).commit();
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onUserLabeClick(final String id, final String name) {
		mFragment.addLabel(id, name);

	}

	@Override
	public void onLabelTitle(final String title) {
		// TODO Auto-generated method stub

	}
}
package com.mobile.health.one.app;

import android.database.Cursor;
import android.database.MergeCursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ProgressBar;

import com.actionbarsherlock.app.SherlockListFragment;

public abstract class LazyLoadFragment extends SherlockListFragment implements
		OnScrollListener {

	private static final String TAG = LazyLoadFragment.class.getSimpleName();

	private int mPrevTotalItemCount;

	public int getPrevTotalItemCount() {
		return mPrevTotalItemCount;
	}

	public void setPrevTotalItemCount(final int mPrevTotalItemCount) {
		this.mPrevTotalItemCount = mPrevTotalItemCount;
	}

	public int getTotalItemCount() {
		return mTotalItemCount;
	}

	private int mTotalItemCount = -1;

	public void setTotalItemCount(final int mTotalItemCount) {
		this.mTotalItemCount = mTotalItemCount;
	}

	private int mPage = 1;

	public void setPage(final int page) {
		mPage = page;
	}

	private Cursor mOldCursor;

	private boolean mIsSearch = false;

	@Override
	public void onScrollStateChanged(final AbsListView view, final int scrollState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onScroll(final AbsListView view, final int firstVisibleItem,
			final int visibleItemCount, final int totalItemCount) {
		//Log.i(TAG, "::onScroll:" + "isSearch " + isSearch());
		if (view.getAdapter() != null
				&& totalItemCount < mTotalItemCount
				&& firstVisibleItem + visibleItemCount >= totalItemCount
				&& totalItemCount != mPrevTotalItemCount
				&& !isSearch()) {
			mPrevTotalItemCount = totalItemCount;

			loadMoreData();
		}

	}

	public boolean isSearch() {
		return mIsSearch;
	}

	public void setSearch(final boolean search) {
		mIsSearch = search;
	}

	protected int getCurrentPage() {
		return mPage;
	}

	private void loadMoreData() {
		loadData(mPage++, true);
		Log.i(TAG, "::loadMoreData:" + "mPage" + mPage);
	}

	protected abstract void loadData(final int page, final boolean showProgress);

	protected abstract ProgressBar getLazyLoadingProgressView();

	protected abstract CursorAdapter getCursorAdapter();

	public void loadData() {
		loadData(true);
	}

	public void loadData(final boolean showProgress) {
		loadData(mPage++, showProgress);
	}

	public void setCursor(final Cursor c) {
		final CursorAdapter adapter = getCursorAdapter();
		if (mPage > 1 && !adapter.isEmpty()) {
			final MergeCursor merge = new MergeCursor(new Cursor[] { adapter.getCursor(), c });
			adapter.changeCursor(merge);
		} else {
			adapter.changeCursor(c);
		}
	}

	public void setData(final Cursor data) {

		setCursor(data);

		// The list should now be shown.
		if (isResumed()) {
			setListShown(true);
			if (mPrevTotalItemCount == 0)
				setListShown(true);
			else {
				getLazyLoadingProgressView().setVisibility(View.GONE);
			}
		} else {
			setListShownNoAnimation(true);
		}
	}


	/**
	 * if change status, reset page to 1
	 */
	protected void reset() {
		final CursorAdapter adapter = getCursorAdapter();
		mPage = 1;
		mPrevTotalItemCount = 0;
		setSearch(false);
		closeOldCursor();
		if (adapter != null)
			adapter.changeCursor(null);
	}

	protected void resetPageNumberOnly() {
		setPrevTotalItemCount(0);
		setSearch(false);
		setPage(1);
	}

	public void closeOldCursor() {
		if (mOldCursor != null && !mOldCursor.isClosed()) {
			mOldCursor.close();
			mOldCursor = null;
		}
	}

	public void setCursorSavingOld(final Cursor c) {
		final CursorAdapter adapter = getCursorAdapter();
		if (!isSearch()) {
			final Cursor old = adapter.swapCursor(c);
			if (old != null && !old.isClosed())
				mOldCursor = old;
		} else
			adapter.changeCursor(c);
	}

	public void setOldCursor() {
		if (mOldCursor != null && !mOldCursor.isClosed()) {
			final CursorAdapter adapter = getCursorAdapter();
			adapter.changeCursor(mOldCursor);
		}
	}

	public void setOldCursor(final Cursor c) {
		mOldCursor = c;
	}

}

package com.mobile.health.one.app;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.apache.http.Header;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetricsInt;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.BaseColumns;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.BackgroundColorSpan;
import android.text.style.DynamicDrawableSpan;
import android.util.Log;
import android.util.Pair;
import android.util.SparseBooleanArray;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockListFragment;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.R;
import com.mobile.health.one.db.entity.Conversation;
import com.mobile.health.one.db.entity.Label;
import com.mobile.health.one.db.entity.Message;
import com.mobile.health.one.db.entity.User;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.NodeWorkerService.IMessageInThreadReceiver;
import com.mobile.health.one.service.NodeWorkerService.IMessageReceiver;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.util.AlertDialogUtil;
import com.mobile.health.one.util.DateFormatUtil;
import com.mobile.health.one.util.ToastUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

public class MessageDetailFragment extends SherlockListFragment implements
		OnClickListener, OnLongClickListener, IMessageReceiver,
		IMessageInThreadReceiver, OnScrollListener, NodeServiceConnector {

	public interface MessageDetailFragmentCallback {
		void onLabelsLoaded(HashMap<String, String> labels);

		void onMenu(int id);

		void removeLabel(String labelId);
	}

	public static final int REQUEST_VIEW_FILE = 0;

	private class DownloadReceiver extends BroadcastReceiver {

		@TargetApi(Build.VERSION_CODES.GINGERBREAD)
		@Override
		public void onReceive(final Context context, final Intent intent) {
			final DownloadManager manager = (DownloadManager) getActivity()
					.getSystemService(Context.DOWNLOAD_SERVICE);
			final long id = intent.getLongExtra(
					DownloadManager.EXTRA_DOWNLOAD_ID, 0);
			if (id > 0) {
				final Query q = new Query().setFilterById(id);
				final Cursor c = manager.query(q);
				c.moveToFirst();
				final String type = c.getString(c
						.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));

				final int status = c.getInt(c
						.getColumnIndex(DownloadManager.COLUMN_STATUS));
				final int reason = c.getInt(c
						.getColumnIndex(DownloadManager.COLUMN_REASON));

				Log.v(TAG, "::onReceive:" + "filetype: " + type);
				final String loadlUri = c.getString(c.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
				String loadlFilename = c.getString(c
						.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
				switch (status) {
				case DownloadManager.STATUS_SUCCESSFUL:
					if (loadlUri != null) {
						Log.i(TAG, "::onReceive:" + loadlFilename);

						final Intent i = new Intent(Intent.ACTION_VIEW);
						loadlFilename = loadlFilename.toLowerCase();
						final String extention = loadlFilename.substring(loadlFilename.indexOf('.') + 1);
						final MimeTypeMap mimeMap = MimeTypeMap.getSingleton();
						String mime = mimeMap.getMimeTypeFromExtension(extention);
						if (StringUtils.isEmpty(mime)) {
							mime = type;
						}
						if (StringUtils.isEmpty(mime)) {
							i.setData(Uri.parse(loadlUri));
						} else {
							i.setDataAndType(Uri.parse(loadlUri), mime);
						}

						try {

							startActivity(Intent.createChooser(i, context.getString(R.string.choose_application_to_open_attach)));
						} catch (final ActivityNotFoundException e) {
							if ("application/pdf".equals(mime)) {
								AlertDialogUtil.alertDialog(context, R.string.error_cant_open_pdf, new Dialog.OnClickListener() {

									@Override
									public void onClick(final DialogInterface dialog, final int which) {
										final String READER = "https://play.google.com/store/apps/details?id=com.adobe.reader";
										final Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(READER));
										context.startActivity(i);
									}
								}, new Dialog.OnClickListener() {

									@Override
									public void onClick(final DialogInterface dialog,
											final int which) {
										dialog.dismiss();
									}

								}).show();
							} else {
								AlertDialogUtil.errorDialog(context, R.string.error_cant_open_type).show();
							}
						}
					}
					break;
				case DownloadManager.STATUS_FAILED:
					ToastUtil.showError(context, "Download failed.");
					//ToastUtil.showError(context, "Download failed; reason code: " + reason);
					Log.e(TAG, "Download failed; reason code: " + reason);
				default:
					break;
				}

				c.close();
			}
		}

	}

	public static final String EXTRA_ID = "id";

	private final String TAG = MessageDetailFragment.class.getCanonicalName();

	private MessageDetailFragmentCallback callback;

	private ProgressDialog progressDialog;

	private NodeWorkerService mService;

	private Handler mHandler;

	private SimpleAdapter mAdapter;

	private LinearLayout mLabels;

	private ArrayList<HashMap<String, Object>> mOldData;

	private SparseBooleanArray mMessageReadCache;

	private TextView mTitleView;

	private DownloadReceiver mReceiver;

	private ArrayList<HashMap<String, Object>> mData;

	private String mThreadId;

	private boolean mIsSearch = false;

	private String mAttachName;

	protected boolean mLoaded;

	private Runnable mTimeoutTask;

	private Handler mTimeoutHandler;

	private String mSubject;

	@Override
	public void onPause() {
		getActivity().unregisterReceiver(mReceiver);
		mTimeoutHandler.removeCallbacks(mTimeoutTask);
		super.onPause();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (mReceiver == null) {
			mReceiver = new DownloadReceiver();
		}
		final IntentFilter filter = new IntentFilter(
				DownloadManager.ACTION_DOWNLOAD_COMPLETE);
		getActivity().registerReceiver(mReceiver, filter);
		mLoaded = false;
		if (!isSearch())
			((MHOApplication) getActivity().getApplication()).getNodeService(this);
		maybeDeleteAttach(mAttachName);

		// if no responce, close activity
		mTimeoutTask = new Runnable() {

			@Override
			public void run() {
				if (getActivity() != null) {
					if (!mLoaded) {
						getActivity().startActivity(new Intent(getActivity(), MessageListActivity.class));
						getActivity().finish();
					}
				}
			}
		};

		mTimeoutHandler = new Handler(getActivity().getMainLooper());
		mTimeoutHandler.postDelayed(mTimeoutTask, 5000);
	}

	private void maybeDeleteAttach(final String attachName) {
		if (attachName != null) {
			final File file = new File(getDownloadDir() + attachName);
			Log.i(TAG, "::maybeDeleteAttach:" + "file " + file.getAbsolutePath());
			if (file.exists()) {
				file.delete();
			}
		}
	}

	private void createEmptyDownloadDir() throws IOException {
		final File file = new File(getDownloadDir());
		file.mkdirs(); // prepare dirs
	}

	private String getDownloadDir() {
		final Context c = getActivity();
		final char sep = File.separatorChar;

		final File publicDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		final File cacheDir = c.getExternalCacheDir();
		final File dir = cacheDir.exists() ? cacheDir : publicDir;
		//BugSenseHandler.sendEvent("publicDir " + publicDir + ", cache dir " + cacheDir);
		return dir.getAbsolutePath() + sep + c.getString(R.string.app_name) + sep;
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		getListView().setEmptyView(view.findViewById(android.R.id.empty));
		mLabels = (LinearLayout) view.findViewById(R.id.labels);
		mThreadId = getArguments().getString(Message.CONVERSATION_ID);
	}

	private class MessageDetailSimpleAdapter extends SimpleAdapter {

		private class OnConfirmationClickListener implements OnClickListener {

			@Override
			public void onClick(final View v) {
				final String messageId = (String) v.getTag();
				launchConfirmations(messageId);
			}

		}

		public MessageDetailSimpleAdapter(final Context context,
				final List<? extends Map<String, ?>> data, final int resource,
				final String[] from, final int[] to) {
			super(context, data, resource, from, to);
		}

		@Override
		public View getView(final int position, final View convertView, final ViewGroup parent) {
			final View view = super.getView(position, convertView, parent);
			final View readByAllView = view.findViewById(R.id.isReadByAll);
			if (readByAllView != null) {
				@SuppressWarnings("unchecked")
				final HashMap<String, Object> data = (HashMap<String, Object>) getItem(position);
				final String messageId = (String) data.get(Message.REMOTE_ID);
				readByAllView.setTag(messageId);
				readByAllView.setOnClickListener(new OnConfirmationClickListener());
			}
			return view;
		}

	}

	private void createAdapterAndSetData(
			final ArrayList<HashMap<String, Object>> data) {
		if (data != null && !data.isEmpty()) {
			mLoaded = true;
			mData = data;
			mAdapter = new MessageDetailSimpleAdapter(getActivity(), mData,
					R.layout.item_message_detail, new String[] {
							User.FULL_NAME, Message.BODY, Message.CREATED_DATE,
							Message.IS_READ_BY_ALL, User.AVATAR_LINK,
							Message.ATTACHMENT }, new int[] { R.id.name,
							R.id.message, R.id.date, R.id.isReadByAll,
							R.id.avatar, R.id.attachment });
			final ImageLoader loader = ImageLoader.getInstance();
			mAdapter.setViewBinder(new ViewBinder() {

				@Override
				public boolean setViewValue(final View view, final Object data,
						final String textRepresentation) {
					switch (view.getId()) {
					case R.id.date:
						final Long date = (Long) data;

						final CharSequence dateText = DateFormatUtil.getFormattedDate(new Date(date));
						((TextView) view).setText(dateText);

						return true;
					case R.id.isReadByAll:
						final Boolean isReadByAll = (Boolean) data;
						final ImageView isReadByAllView = (ImageView) view;
						if (isReadByAll != null) {
							if (!isReadByAll.booleanValue()) {
								isReadByAllView
										.setImageResource(R.drawable.ic_attachment_delete);
							}
						} else {
							isReadByAllView.setVisibility(View.GONE);
						}

						return true;
					case R.id.avatar:
						final String uri = (String) data;
						loader.displayImage(uri, (ImageView) view);
						return true;
					case R.id.attachment:
						@SuppressWarnings("unchecked")
						final Pair<String, String> attach = (Pair<String, String>) data;
						if (attach != null) {
							final String attachName = attach.first;
							final String attachUri = attach.second;
							((TextView) view).setText(attachName);
							((TextView) view).setTag(attachUri);
							((TextView) view).setOnClickListener(new OnClickListener() {

										@Override
										public void onClick(final View v) {
											final String uriStr = (String) v.getTag();
											final Uri uri = Uri.parse(uriStr);
											downloadAttachment(attachName, uri);
										}
									});
							((TextView) view).setVisibility(View.VISIBLE);
						} else {
							((TextView) view).setVisibility(View.GONE);
						}
						return true;

					default:
						return false;
					}
				}

			});

			setListAdapter(mAdapter);

			mSubject = (String) data.get(0).get(Conversation.TITLE);
			if (StringUtils.isNotEmpty(mSubject))
				mTitleView.setText(mSubject);
		} else {
			setListAdapter(null);
		}
	}

	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	private void downloadAttachment(final String attachName, final Uri uri) {
		final String name = uri.getLastPathSegment();
		final String extention = name.substring(name.indexOf('.') + 1).toLowerCase();
		final MimeTypeMap mimeMap = MimeTypeMap.getSingleton();
		final String mime = mimeMap.getMimeTypeFromExtension(extention);
		Log.i(TAG, "::downloadAttachment:" + "uri " + uri);
		final String fileName = String.format("%d.%s", System.currentTimeMillis(), extention);
		//attachName = attachName.toLowerCase();
		//attachName = "123.jpg";
		mAttachName = fileName;
		if (Build.VERSION.SDK_INT >= 9) {
			final DownloadManager manager = (DownloadManager) getActivity()
					.getSystemService(Context.DOWNLOAD_SERVICE);
			try {
				final Request request = new Request(uri);
				request.setTitle(attachName);
				request.setMimeType(mime);
				//request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, getString(R.string.app_name) + File.separatorChar + fileName);
				final Uri fileUri = Uri.fromFile(new File(getDownloadDir(), fileName));
				request.setDestinationUri(fileUri);
				maybeDeleteAttach(fileName); // ensure we will have exact filename
				createEmptyDownloadDir();
				final Header header = RequestProcessor.getCookieHeader();
				request.addRequestHeader(header.getName(), header.getValue());

				manager.enqueue(request);

				ToastUtil.showText(getActivity(), "Download started");
			} catch (final IllegalArgumentException e) {
				ToastUtil.showText(getActivity(),
								"HTTPS Secure downloads are not supported on your device");
			} catch (final SecurityException e) {
				ToastUtil
				.showText(getActivity(), "Could not save file to SDCard");
			} catch (final IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mThreadId = getArguments().getString(Message.CONVERSATION_ID);
		mMessageReadCache = new SparseBooleanArray();
	}

	@Override
	public void onStop() {
		if (mService != null)
			mService.getUnreadMessagesCount();
		super.onStop();
	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		final View v = inflater.inflate(R.layout.fragment_message_detail,
				container, false);
		final ListView list = (ListView) v.findViewById(android.R.id.list);
		list.setOnScrollListener(this);
		mTitleView = (TextView) v.findViewById(R.id.title);
		return prepareMenuButtonListeners(v);
	}

	private View prepareMenuButtonListeners(final View v) {
		final View delete = v.findViewById(R.id.menu_delete);
		final View label = v.findViewById(R.id.menu_label);
		final View move = v.findViewById(R.id.menu_move);
		final View forward = v.findViewById(R.id.menu_forward);
		final View reply = v.findViewById(R.id.menu_reply);

		delete.setOnClickListener(this);
		label.setOnClickListener(this);
		move.setOnClickListener(this);
		forward.setOnClickListener(this);
		reply.setOnClickListener(this);

		delete.setOnLongClickListener(this);
		label.setOnLongClickListener(this);
		move.setOnLongClickListener(this);
		forward.setOnLongClickListener(this);
		reply.setOnLongClickListener(this);

		return v;
	}

	// save it if we use cursor sometime
	@SuppressWarnings("unused")
	private CharSequence prepareLabelsText(final Cursor c) {
		try {
			if (c.moveToFirst()) {
				final CharSequence DIVIDER = " ";
				final StringBuilder str = new StringBuilder();
				final int titleIndex = c.getColumnIndex(Label.TITLE);
				final int idIndex = c.getColumnIndex(BaseColumns._ID);
				final int remoteIdIndex = c.getColumnIndex(Label.REMOTE_ID);
				do {
					final String remoteId = c.getString(remoteIdIndex);
					final SpannableString span = new SpannableString(
							c.getString(titleIndex));
					span.setSpan(remoteId, 0, span.length(),
							Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					str.append(span);
					if (!c.isLast()) {
						str.append(DIVIDER);
					}
				} while (c.moveToNext());
				return str.toString();
			} else {
				return null;
			}
		} catch (final NullPointerException e) {
			Log.e(TAG, "::prepareLabelsText:" + "no labels data");
			return null;
		}
	}

	@SuppressWarnings("unused")
	private CharSequence prepareLabelsText(final HashMap<String, String> labels) {
		try {
			final SpannableStringBuilder str = new SpannableStringBuilder();
			for (final String key : labels.keySet()) {
				final String title = labels.get(key);
				final SpannableString span = new SpannableString(title);
				span.setSpan(key, 0, span.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				// final BackroundImageSpan bg = new
				// BackroundImageSpan(getActivity(), R.drawable.label_bg);
				// span.setSpan(bg, 0, span.length(),
				// Spanned.SPAN_INCLUSIVE_INCLUSIVE);
				final BackgroundColorSpan bgColor = new BackgroundColorSpan(
						getResources().getColor(R.color.label_bg));
				span.setSpan(bgColor, 0, span.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				str.append(span);
				final BackgroundColorSpan bgCloseColor = new BackgroundColorSpan(
						getResources().getColor(R.color.label_dark));
				final SpannableString closeSpan = new SpannableString(" X ");
				closeSpan.setSpan(key, 0, closeSpan.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				closeSpan.setSpan(bgCloseColor, 0, closeSpan.length(),
						Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
				str.append(closeSpan);
				str.append(' ');
			}
			return str;
		} catch (final NullPointerException e) {
			Log.e(TAG, "::prepareLabelsText:" + "no labels data");
			return null;
		}
	}

	@SuppressWarnings("unused")
	private static class BackroundImageSpan extends DynamicDrawableSpan {

		private final Drawable mDrawable;
		private final Context mContext;

		BackroundImageSpan(final Context c, final Drawable d) {
			mDrawable = d;
			mContext = c;
			setBounds();
		}

		BackroundImageSpan(final Context c, final int resourceId) {
			mDrawable = c.getResources().getDrawable(resourceId);
			mContext = c;
			setBounds();
		}

		private void setBounds(int width) {
			if (width == 0) {
				width = mDrawable.getIntrinsicWidth();
			}
			mDrawable.setBounds(0, 0, width, mDrawable.getIntrinsicHeight());
		}

		private void setBounds() {
			setBounds(0);
		}

		@Override
		public Drawable getDrawable() {
			return mDrawable;
		}

		@Override
		public void draw(final Canvas canvas, final CharSequence text,
				final int start, final int end, final float x, final int top,
				final int y, final int bottom, final Paint paint) {

			final Drawable d = getDrawable();
			final int transY = y - d.getBounds().bottom;
			final Rect textBounds = new Rect();
			paint.getTextBounds(text.toString(), start, end, textBounds);
			if (d.getBounds().right < textBounds.right) {
				d.getBounds().right = textBounds.right;
			}
			final Rect padding = new Rect();
			d.getPadding(padding);
			// d.getBounds().left -= padding.left;
			d.getBounds().right += padding.right;
			final int textX = padding.left;
			final int textY = textBounds.height()
					+ (d.getBounds().height() - textBounds.height()) / 2;

			canvas.save();
			canvas.translate(x, transY);
			d.draw(canvas);
			canvas.drawText(text, start, end, textX, textY, paint);
			canvas.restore();
		}

		@Override
		public int getSize(final Paint paint, final CharSequence text,
				final int start, final int end, final FontMetricsInt fm) {
			final Drawable d = getDrawable();
			final Rect padding = new Rect();
			d.getPadding(padding);
			final int size = super.getSize(paint, text, start, end, fm);
			return size + padding.left + padding.right;
		}
	}

	@Override
	public void onAttach(final Activity activity) {
		if (activity instanceof MessageDetailFragmentCallback) {
			callback = (MessageDetailFragmentCallback) activity;
		}

		super.onAttach(activity);
	}

	@Override
	public void onNodeConnected(final NodeWorkerService service) {
		mService = service;
		mService.registerMessageCallback(this);
		mService.registerMessagesInThreadCallback(this);
		mService.getMessagesInThread(mThreadId);
	}

	@Override
	public void onDetach() {
		mService.unregisterMessageCallback(this);
		mService.unregisterMessagesInThreadCallback(this);
		super.onDetach();
	}

	private class DeleteLabelListener implements OnClickListener {

		@Override
		public void onClick(final View v) {
			if (getActivity() == null)
				return;

			if (mService != null) {
				final String labelId = (String) v.getTag();
				mService.removeLabelFromThread(labelId, mThreadId);
				final LinearLayout parent = (LinearLayout) v.getParent();
				parent.removeView(v);
				callback.removeLabel(labelId);
			} else {
				ToastUtil.showText(getActivity(), "Error removing label");
			}
		}
	}

	/**
	 * TODO: write something better, maybe own layout FIXME: shitcode
	 * Temporatory public to be used in user labels
	 *
	 * @param ll
	 * @param views
	 * @param mContext
	 */
	public static void populateText(final LinearLayout ll,
			final HashMap<String, String> labels, final Context context,
			final OnClickListener deleteLabelListener) {
		ll.removeAllViews();
		final int maxWidth = ll.getMeasuredWidth();
		// int maxWidth = display.getWidth() - 20;
		if (labels == null)
			return;

		LinearLayout.LayoutParams params;
		LinearLayout newLL = new LinearLayout(context);
		newLL.setLayoutParams(new LayoutParams(
				android.view.ViewGroup.LayoutParams.MATCH_PARENT,
				android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
		newLL.setGravity(Gravity.LEFT);
		newLL.setOrientation(LinearLayout.HORIZONTAL);

		int widthSoFar = 0;
		for (final String key : labels.keySet()) {
			final String title = labels.get(key);
			final TextView view = new TextView(context);
			view.setText(title);
			view.setBackgroundResource(R.drawable.label_bg);
			view.setTag(key);
			// TODO
			view.setOnClickListener(deleteLabelListener);
			final LinearLayout LL = new LinearLayout(context);
			LL.setOrientation(LinearLayout.HORIZONTAL);
			LL.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM);
			LL.setLayoutParams(new ListView.LayoutParams(
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT,
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT));

			view.measure(0, 0);
			params = new LinearLayout.LayoutParams(view.getMeasuredWidth(),
					android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
			params.setMargins(5, 0, 5, 0); // YOU CAN USE THIS
			// LL.addView(TV, params);
			LL.addView(view, params);
			LL.measure(0, 0);
			widthSoFar += view.getMeasuredWidth();// YOU MAY NEED TO ADD THE
			// MARGINS
			if (widthSoFar >= maxWidth) {
				ll.addView(newLL);

				newLL = new LinearLayout(context);
				newLL.setLayoutParams(new LayoutParams(
						android.view.ViewGroup.LayoutParams.MATCH_PARENT,
						android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
				newLL.setOrientation(LinearLayout.HORIZONTAL);
				newLL.setGravity(Gravity.LEFT);
				params = new LinearLayout.LayoutParams(LL.getMeasuredWidth(),
						LL.getMeasuredHeight());
				newLL.addView(LL, params);
				widthSoFar = LL.getMeasuredWidth();
			} else {
				newLL.addView(LL);
			}
		}
		ll.addView(newLL);
	}

	@Override
	public void onClick(final View v) {
		if (callback != null) {
			callback.onMenu(v.getId());
		}

	}

	@Override
	public boolean onLongClick(final View v) {
		final CharSequence viewText = ((ImageButton) v).getContentDescription();
		ToastUtil.showText(getActivity(), viewText);
		return true;
	}

	@Override
	public void onListItemClick(final ListView l, final View v,
			final int position, final long id) {
		final SimpleAdapter adapter = (SimpleAdapter) l.getAdapter();
		@SuppressWarnings("unchecked")
		final HashMap<String, Object> data = (HashMap<String, Object>) adapter
				.getItem(position);

		final String threadId = (String) data.get(Message.REMOTE_ID);
		launchConfirmations(threadId);

		super.onListItemClick(l, v, position, id);
	}

	private void launchConfirmations(final String threadId) {
		final Intent i = new Intent(getActivity(), MessageConfirmActivity.class);
		i.putExtra(EXTRA_ID, threadId);
		startActivity(i);
	}

	@Override
	public void onMessageCount(final int count) {
	}

	@Override
	public Handler getHandler() {
		if (mHandler == null) {
			mHandler = new Handler(getActivity().getMainLooper());
		}
		return mHandler;
	}

	@Override
	public void onReceiveMessages(
			final Pair<ArrayList<HashMap<String, Object>>, HashMap<String, String>> pair) {
		if (getActivity() == null)
			return;
		createAdapterAndSetData(pair.first);
		setOnLabelsReceive(pair.second);
		setLabels(pair.second);
		if (progressDialog != null) {
			progressDialog.cancel();
		}
	}

	public void setLabels(final HashMap<String, String> labels) {
		populateText(mLabels, labels, getActivity(), new DeleteLabelListener());
	}

	private void setOnLabelsReceive(final HashMap<String, String> labels) {
		if (callback != null)
			callback.onLabelsLoaded(labels);
	}

	public void setCursor(final ArrayList<HashMap<String, Object>> data) {
		if (!mAdapter.isEmpty() && mData != null) {
			mData.addAll(data);
			mAdapter.notifyDataSetChanged();
		} else {
			mData = data;
			mAdapter.notifyDataSetChanged();
		}
	}

	public void setDataSavingOld(final ArrayList<HashMap<String, Object>> data) {
		mOldData = mData;
		createAdapterAndSetData(data);
	}

	public void setOldData() {
		mData = mOldData;
		createAdapterAndSetData(mData);
	}

	@Override
	public void onScrollStateChanged(final AbsListView view,
			final int scrollState) {
	}

	@Override
	public void onScroll(final AbsListView view, final int firstVisibleItem,
			final int visibleItemCount, final int totalItemCount) {
		final SparseBooleanArray readCache = mMessageReadCache;
		if (mAdapter != null) {
			// final int count = firstVisibleItem + visibleItemCount;
			// Log.d(TAG, "firstVisibleItem: " + firstVisibleItem +
			// " visibleItemCount: "+ visibleItemCount);
			for (int i = firstVisibleItem; i < visibleItemCount; i++) {
				final long id = mAdapter.getItemId(i);
				// will use id as int, hope we never get id more then MAX_INT
				final boolean isRead = readCache.get((int) id, false);
				if (!isRead) {
					readCache.put((int) id, true);
					@SuppressWarnings("unchecked")
					final HashMap<String, Object> item = (HashMap<String, Object>) mAdapter
							.getItem(i);
					final String remoteId = (String) item
							.get(Message.REMOTE_ID);
					// TODO async quering to service
					if (mService != null)
						mService.markMessageRead(remoteId);
				}
			}
		}
	}

	public boolean isSearch() {
		return mIsSearch;
	}

	public void setSearch(final boolean mIsSearch) {
		this.mIsSearch = mIsSearch;
	}

	@Override
	public void onMessageReceived(final HashMap<String, Object> message) {
		// lets wait a bit
		final String messageId = (String) message.get(Message.REMOTE_ID);
		if (messageId != null && mService != null) {
			mService.markMessageRead(messageId);
		}
		new Timer().schedule(new TimerTask() {

			@Override
			public void run() {
				if (getActivity() != null) {
					getActivity().runOnUiThread(new Runnable() {

						@Override
						public void run() {
							mService.getMessagesInThread(getArguments()
									.getString(Message.CONVERSATION_ID));
							if (progressDialog == null) {
								progressDialog = ProgressDialog.show(
										getActivity(), "Loading messages",
										"Loading");
								progressDialog.setCancelable(true);
							} else {
								progressDialog.show();
							}

							// mData.add(0, message);
						}
					});
				}
			}
		}, 1000);

	}

	public String getSubject() {
		return mSubject;
	}
}
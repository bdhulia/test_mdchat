package com.mobile.health.one.app;

import java.lang.ref.WeakReference;

import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Bundle;
import android.provider.BaseColumns;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.mobile.health.one.R;
import com.mobile.health.one.app.RegisterPinFragment.RegisterPinReceiver;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.Group;
import com.mobile.health.one.service.entity.GroupsResponce;
import com.mobile.health.one.service.worker.GetGroupsProcessor;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.nostra13.universalimageloader.core.ImageLoader;

public class GroupListFragment extends LazyLoadFragment {

	protected static class GroupListReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();
		private final WeakReference<GroupListFragment> mFragment;

		GroupListReceiver(final GroupListFragment fragment) {
			mFragment = new WeakReference<GroupListFragment>(fragment);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final GroupListFragment f = mFragment.get();
			if (f == null)
				return; // do nothing, if fragment already destroyed
			boolean showProgress = true;
			if (resultData != null) {
				showProgress = resultData.getBoolean(EXTRA_SHOW_PROGRESS, true);
			}
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start group list");
                if (f.getView() != null) {
                    if (showProgress) {
                        // Start out with a progress indicator.
                        if (f.getPrevTotalItemCount() == 0)
                            f.setListShown(false);
                        else {
                            f.mProgress.setVisibility(View.VISIBLE);
                        }
                    }
                }
				break;
			case SyncService.STATUS_FINISHED:
				final GroupsResponce responce = resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				if (responce != null) {
					f.setTotalItemCount(responce.getData().getPagination().total_groups_count);
					if (f != null && f.isVisible()) {
						final MatrixCursor c = responce.getData()
								.saveToCursor();
						f.setCursor(c);
						if (!f.fromDashboard) {
							if (f.isResumed()) {
								f.setListShown(true);
							} else {
								f.setListShownNoAnimation(true);
							}
						} else {
							f.setListShown(false);
						}
					}
				}
				f.mProgress.setVisibility(View.GONE);
				Log.i(TAG, "Saving group start");
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error getting groups list");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(f.getActivity(), error);
				}
				f.mProgress.setVisibility(View.GONE);
				// TODO: make diffs on error code, now server always return 1
				f.getActivity().finish();
				break;
			}
		}

	}

	private static final String TAG = GroupListFragment.class.getSimpleName();

	private static final String EXTRA_CUR_CHOICE = "curChoice";
	private static final int ITEMS_PER_PAGE = 10;

	private static final String EXTRA_SHOW_PROGRESS = "show_progress";

	private static final String EXTRA_FROM_DASHBOARD = SearchParams.AliasVariant.IntentBuilder.EXTRA_FROM_DASHBOARD;

	private boolean dualPane;
	private int curCheckPosition = 0;
	private int groupsType = GetGroupsProcessor.TYPE_PRIVATE;

	// This is the Adapter being used to display the list's data.
	private SimpleCursorAdapter mAdapter;

	private ProgressBar mProgress;

	private boolean fromDashboard;

	public static GroupListFragment newInstance(final boolean fromDashboard) {
		final Bundle b = new Bundle();
		b.putBoolean(EXTRA_FROM_DASHBOARD, fromDashboard);
		final GroupListFragment f = new GroupListFragment();
		f.setArguments(b);
		return f;
	}

	@Override
	protected ProgressBar getLazyLoadingProgressView() {
		return mProgress;
	}

	@Override
	protected void loadData(final int page, final boolean showProgress) {
		Log.v(TAG, "page: " + page);
		Bundle args = getArguments();
		if (args == null) {
			args = new Bundle(4);
		}
		if (page > 0) {
			args.putInt(GetGroupsProcessor.EXTRA_ITEMS_PER_PAGE, ITEMS_PER_PAGE);
			args.putInt(GetGroupsProcessor.EXTRA_CURRENT_PAGE, page);
			args.putInt(GetGroupsProcessor.EXTRA_TYPE, groupsType);
			args.putBoolean(EXTRA_SHOW_PROGRESS, showProgress);
			SyncService.startSyncService(getActivity(),
					AppSyncService.GET_GROUPS, new GroupListReceiver(this),
					args);

			if (getListAdapter() != null && getListAdapter().isEmpty())
				setListShown(false);
		}
	}

	private void init() {
		// Create an empty adapter we will use to display the loaded data.
		mAdapter = new SimpleCursorAdapter(getActivity(),
				R.layout.item_group_list, null, new String[] {
						Group.Entity.SHORT_NAME, Group.Entity.DESCRIPTION,
						Group.Entity.ADMIN_NAME, Group.Entity.AVATAR_LINK }, new int[] { R.id.name,
						R.id.description, R.id.admin_name, R.id.avatar }, 0);

		mProgress = new ProgressBar(getActivity());
		mProgress.setIndeterminate(true);
		mProgress.setVisibility(View.GONE);

		final ImageLoader loader = ImageLoader.getInstance();
		mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {

			@Override
			public boolean setViewValue(final View view, final Cursor cursor,
					final int columnIndex) {
				switch (view.getId()) {
				case R.id.avatar:
					String uri = cursor.getString(columnIndex);
					if (uri == null || !URLUtil.isValidUrl(uri)){
						uri = null;
					}
					loader.displayImage(uri, (ImageView) view);

					return true;
				default:
					return false;
				}
			}
		});

		getListView().setOnScrollListener(this);
		//getListView().addFooterView(mProgress);
		setListAdapter(mAdapter);
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		setEmptyText(getString(R.string.error_no_groups));

		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);

		// Check to see if we have a frame in which to embed the details
		// fragment directly in the containing UI.
		final View detailsFrame = getActivity().findViewById(R.id.details);
		dualPane = detailsFrame != null
				&& detailsFrame.getVisibility() == View.VISIBLE;

		if (savedInstanceState != null) {
			// Restore last state for checked position.
			curCheckPosition = savedInstanceState.getInt(EXTRA_CUR_CHOICE, 0);
		}

		if (getArguments() != null)
			fromDashboard = getArguments().getBoolean(EXTRA_FROM_DASHBOARD, false);

		if (dualPane) {
			// In dual-pane mode, the list view highlights the selected item.
			getListView().setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			// Make sure our UI is in the correct state.
			showDetails(curCheckPosition);
		}
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		init();
		loadData();
	}

	@Override
	public void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(EXTRA_CUR_CHOICE, curCheckPosition);
	}

	@Override
	public void onListItemClick(final ListView l, final View v,
			final int position, final long id) {
		showDetails(id, position);
		Log.i("onListItemClick", "Item clicked: " + id);
	}

	/**
	 * Helper function to show the details of a selected item, either by
	 * displaying a fragment in-place in the current UI, or starting a whole new
	 * activity in which it is displayed.
	 */
	void showDetails(final long id, final int index) {
		curCheckPosition = index;
		// final long id = getListView().getAdapter().getItemId(index);
		long itemId;
		String groupName = null;
		if (id == -1) {
			final Cursor o = (Cursor) getListView().getAdapter().getItem(index);
			final int userIdIndex = o.getColumnIndexOrThrow(BaseColumns._ID);
			final int groupNameIndex = o.getColumnIndexOrThrow(Group.Entity.SHORT_NAME);
			itemId = o.getLong(userIdIndex);
			groupName = o.getString(groupNameIndex);
		} else {
			final Cursor o = (Cursor) getListView().getAdapter().getItem(index);
			final int groupNameIndex = o.getColumnIndexOrThrow(Group.Entity.SHORT_NAME);
			groupName = o.getString(groupNameIndex);
			itemId = id;
		}

		final Bundle args = new Bundle();
		args.putLong(BaseColumns._ID, itemId);
		if (groupName != null)
			args.putString(Group.Entity.SHORT_NAME, groupName);

		if (dualPane) {
			// We can display everything in-place with fragments, so update
			// the list to highlight the selected item and show the data.
			getListView().setItemChecked(index, true);
			// Check what fragment is currently shown, replace if needed.
			GroupDetailFragment details = (GroupDetailFragment) getFragmentManager()
					.findFragmentById(R.id.details);
			if (details == null) {
				// Make new fragment to show this selection.
				details = new GroupDetailFragment();
				details.setArguments(args);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				final FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.replace(R.id.details, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			final Intent intent = new Intent();
			intent.setClass(getActivity(), GroupDetailActivity.class);
			intent.putExtras(args);
			startActivity(intent);
		}
	}

	void showDetails(final int index) {
		showDetails(-1, index);
	}

	public int getInvitesType() {
		return groupsType;
	}

	public void setInvitesType(final int invitesType) {
		reset();
		groupsType = invitesType;
	}

	@Override
	protected CursorAdapter getCursorAdapter() {
		return mAdapter;
	}
}
package com.mobile.health.one.app;

import java.lang.ref.WeakReference;
import java.util.HashMap;

import org.apache.commons.lang.StringUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.MenuItem;
import com.google.android.apps.iosched.ui.widget.ExpandableLinearLayout;
import com.mobile.health.one.R;
import com.mobile.health.one.app.RegisterPinFragment.RegisterPinReceiver;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.Academic;
import com.mobile.health.one.service.entity.Affiliation;
import com.mobile.health.one.service.entity.Education;
import com.mobile.health.one.service.entity.Fellowship;
import com.mobile.health.one.service.entity.GetUserProfileResponse;
import com.mobile.health.one.service.entity.InsurancePlan;
import com.mobile.health.one.service.entity.MedicalEducation;
import com.mobile.health.one.service.entity.Publication;
import com.mobile.health.one.service.entity.Residency;
import com.mobile.health.one.service.entity.UserProfile;
import com.mobile.health.one.service.entity.UserProfileLanguage;
import com.mobile.health.one.service.entity.UserProfilePractice;
import com.mobile.health.one.service.worker.DeleteUserFromLabelProcessor;
import com.mobile.health.one.service.worker.DeleteUserFromNetworkProcessor;
import com.mobile.health.one.service.worker.GetUserProfileProcessor;
import com.mobile.health.one.service.worker.MarkUserWithLabelProcessor;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.mobile.health.one.util.ToastUtil;
import com.nostra13.universalimageloader.core.ImageLoader;

public class UserProfileFragment extends SherlockFragment implements OnClickListener {

	public class ConfirmDialog extends DialogFragment implements DialogInterface.OnClickListener {

		@Override
		public Dialog onCreateDialog(final Bundle savedInstanceState) {
			final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
			final String m = getString(R.string.user_delete_message, userName != null ? userName : "");
			dialog.setMessage(m);
			dialog.setTitle(R.string.user_delete_title);
			dialog.setPositiveButton(R.string.user_delete_btn, this);
			dialog.setNegativeButton(R.string.btn_no, this);
			return dialog.create();
		}
		@Override
		public void onClick(final DialogInterface dialog, final int which) {
			final UserProfileFragment f = UserProfileFragment.this;
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				f.getArguments().putString(DeleteUserFromNetworkProcessor.EXTRA_FRIEND_ID, f.userProfileId.toString());
				SyncService.startSyncService(getActivity(),
						AppSyncService.DELETE_USER_FROM_NETWORK,
						new DeleteUserReceiver(UserProfileFragment.this), f.getArguments());

				dismiss();

				break;
			case DialogInterface.BUTTON_NEGATIVE:
				dialog.cancel();
				break;
			default:
				break;
			}

		}
	}

	private static final String TAG = UserProfileFragment.class.getName();
	private Button homePhone;
	private Button cellPhone;
	private Button workPhone;

	private ProgressBar mProgress;
	private ExpandableLinearLayout practiceLayout;
	private ExpandableLinearLayout affilationLayout;
	private ExpandableLinearLayout personalLayout;
	private ExpandableLinearLayout educationLayout;
	private ExpandableLinearLayout academicLayout;
	private LinearLayout labels;
	private ImageView avatar;
	private HashMap<String, String> labelsMap;
	private Long userProfileId;
	private String userName;
	private boolean isOwnProfile = false;
	private View labelProfileButton;
	private View deleteProfileButton;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		mProgress = new ProgressBar(getActivity());
		mProgress.setIndeterminate(true);
		mProgress.setVisibility(View.VISIBLE);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
			final Bundle savedInstanceState) {

		final ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.profile, container, false);

		final Bundle bundle = this.getArguments();
		if (bundle != null) {
			bundle.putLong(GetUserProfileProcessor.EXTRA_USER_ID,
					bundle.getLong(GetUserProfileProcessor.EXTRA_USER_ID));
			userProfileId = bundle.getLong(GetUserProfileProcessor.EXTRA_USER_ID);
			isOwnProfile = bundle.getBoolean(UserProfileActivity.EXTRA_IS_OWN_PROFILE);
		}

		homePhone = (Button) layout.findViewById(
				R.id.callHomePhone);
		workPhone = (Button) layout.findViewById(
				R.id.callWorkPhone);
		cellPhone = (Button) layout.findViewById(
				R.id.callCellPhone);
		practiceLayout 		= (ExpandableLinearLayout) layout.findViewById(R.id.practiceLinearLayout);
		affilationLayout 	= (ExpandableLinearLayout) layout.findViewById(R.id.user_profile_affilation_layout);
		personalLayout 		= (ExpandableLinearLayout) layout.findViewById(R.id.user_profile_personal_layout);
		educationLayout 	= (ExpandableLinearLayout) layout.findViewById(R.id.user_profile_education_layout);
		academicLayout 		= (ExpandableLinearLayout) layout.findViewById(R.id.user_profile_academic_layout);
		avatar = (ImageView) layout.findViewById(R.id.avatar);

		labels = (LinearLayout) layout.findViewById(R.id.profile_labels);

		labelProfileButton = layout.findViewById(R.id.labelProfileButton);
		deleteProfileButton = layout.findViewById(R.id.deleteProfileButton);
		if (!isOwnProfile) {
			labelProfileButton.setVisibility(View.VISIBLE);
			deleteProfileButton.setVisibility(View.VISIBLE);
			labelProfileButton.setOnClickListener(this);
			deleteProfileButton.setOnClickListener(this);
		} else {
			labelProfileButton.setVisibility(View.GONE);
			deleteProfileButton.setVisibility(View.GONE);
		}

//		practiceLayout.init();
//		residencyLayout.init();

		if (getActivity() != null) {
			getActivity().setResult(Activity.RESULT_CANCELED);
		}

		return layout;
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		this.startUserProfileService();
		super.onViewCreated(view, savedInstanceState);
	}


	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.labelProfileButton:
			final UserLabelListFragment labels = new UserLabelListFragment();
			labels.show(getFragmentManager(), UserLabelListFragment.TAG);
			break;
		case R.id.deleteProfileButton:
			new ConfirmDialog().show(getSherlockActivity().getSupportFragmentManager(), TAG);
			break;
		default:
			break;
		}

	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			getFragmentManager().popBackStack();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void startUserProfileService() {
		SyncService.startSyncService(getActivity(),
				AppSyncService.GET_USER_PROFILE,
				new UserProfiletReceiver(this), getArguments());
	}

	private class OnProfileHomePhoneClickListener implements OnClickListener {
		private final String phone;

		public OnProfileHomePhoneClickListener(final String phone) {
			this.phone = phone;
		}

		@Override
		public void onClick(final View v) {
			Log.w(TAG, "Call home phone button action");
			callPhoneNumber(phone);
		}
	}

	private class OnProfileCellPhoneClickListener implements OnClickListener {

		private final String phone;

		public OnProfileCellPhoneClickListener(final String phone) {
			this.phone = phone;
		}

		@Override
		public void onClick(final View v) {
			Log.w(TAG, "Call cell phone button action");
			callPhoneNumber(phone);
		}
	}

	private class OnProfileWorkPhoneClickListener implements OnClickListener {

		private final String phone;

		public OnProfileWorkPhoneClickListener(final String phone) {
			this.phone = phone;
		}

		@Override
		public void onClick(final View v) {
			Log.w(TAG, "Call work phone button action");
			callPhoneNumber(phone);
		}
	}

	public void initLabels(final HashMap<String, String> labelsMap) {
		this.labelsMap = labelsMap;
		labels.removeAllViews();
		MessageDetailFragment.populateText(labels, labelsMap, getActivity(), new OnClickListener() {

			@Override
			public void onClick(final View v) {
				final String key = (String) v.getTag();
				final LinearLayout parent = (LinearLayout) v.getParent();
				parent.removeView(v);
				deleteLabel(key, false);
			}
		});
	}

	public void addLabel(final String id, final String name) {
		if (userProfileId  == 0)
			return;

		if (labelsMap == null) {
			labelsMap = new HashMap<String, String>();
		}
		labelsMap.put(id, name);
		initLabels(labelsMap);
		getArguments().putString(MarkUserWithLabelProcessor.EXTRA_LABEL_ID, id);
		getArguments().putString(MarkUserWithLabelProcessor.EXTRA_FRIEND_ID, userProfileId.toString());
		SyncService.startSyncService(getActivity(),
				AppSyncService.MARK_USER_WITH_LABEL,
				new OperationReceiver(this), getArguments());
	}

	public void deleteLabel(final String id, final boolean update) {
		if (userProfileId  == 0)
			return;

		labelsMap.remove(id);
		getArguments().putString(DeleteUserFromLabelProcessor.EXTRA_LABEL_ID, id);
		getArguments().putString(DeleteUserFromLabelProcessor.EXTRA_FRIEND_ID, userProfileId.toString());
		SyncService.startSyncService(getActivity(),
				AppSyncService.DELETE_USER_FROM_LABEL,
				new OperationReceiver(this), getArguments());
		if (update)
			initLabels(labelsMap);
	}

	private class PlaceCallOnClickListener implements View.OnClickListener {

		@Override
		public void onClick(final View v) {
			final TextView textView = (TextView) v;
			final CharSequence phoneNum = textView.getText();
			callPhoneNumber(phoneNum.toString());
		}

	}

	private void setText(final View childLayout, final int labelViewRes, final int textViewRes, final String text, final boolean shouldPlaceCall) {
		final TextView labelView = (TextView) childLayout.findViewById(labelViewRes);
		final TextView textView = (TextView) childLayout.findViewById(textViewRes);
		if (StringUtils.isNotEmpty(text)) {
			if (textView != null) {
				textView.setText(text);
				if (shouldPlaceCall) {
					textView.setOnClickListener(new PlaceCallOnClickListener());
				}
			}
		} else {
			if (labelView != null) {
				labelView.setVisibility(View.GONE);
			}
			if (textView != null) {
				textView.setVisibility(View.GONE);
			}
		}
	}

	private void setText(final View childLayout, final int labelViewRes, final int textViewRes, final String text) {
		setText(childLayout, labelViewRes, textViewRes, text, false);
	}

	private TextView prepateSubTitleLayout(final Context c, final String text) {
		final TextView subTitle = (TextView) View.inflate(c, R.layout.user_title_layout, null);
		subTitle.setText(text);
		return subTitle;
	}

	private void parseResponseData(final GetUserProfileResponse response) {
		final FragmentActivity activity = this.getActivity();

		final UserProfile.Entity user = response.getData().user;

		final MenuMessageCreateFragment menuFragment;
		if (user.inNetwork) {
			// exlude send_invite item from menu
			menuFragment = MenuMessageCreateFragment.newInstance(R.id.send_invite, userProfileId.toString(), user.name);
			labelProfileButton.setVisibility(View.VISIBLE);
			deleteProfileButton.setVisibility(View.VISIBLE);
		} else {
			// exlude compose item from menu
			menuFragment = MenuMessageCreateFragment.newInstance(R.id.compose, userProfileId.toString(), user.name);
			labelProfileButton.setVisibility(View.GONE);
			deleteProfileButton.setVisibility(View.GONE);
		}
		if (!isOwnProfile) {
			final FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
			ft.add(menuFragment, MenuMessageCreateFragment.TAG).commit();
		}

		final UserProfilePractice.Entity practice = response.getData().practice;
		final Residency.Entity residency = response.getData().residency;
		final MedicalEducation.Entity[] medicalEducation = response.getData().medicalEducation;
		final Education.Entity[] education = response.getData().education;
		final UserProfileLanguage.Entity[] languages = response.getData().languages;
		final Fellowship.Entity[] fellowships = response.getData().fellowships;
		final Academic.Entity[] academics = response.getData().academics;
		final Publication.Entity[] publications = response.getData().publications;
		final InsurancePlan.Entity[] insurancePlans = response.getData().insurancePlans;
		final Affiliation.Entity[] affiliations = response.getData().hospitalAffiliations;
		String insuranceAccepted = null;
		if (response.getData().insuranceAccepted != null)
			insuranceAccepted = response.getData().insuranceAccepted.equals("1") ? "Yes" : "No";
		initLabels(response.getData().labels);

		userName = user.name;

		if (practiceLayout.getChildCount() > 0)
			practiceLayout.removeAllViews();
		if (affilationLayout.getChildCount() > 0)
			affilationLayout.removeAllViews();
		if (personalLayout.getChildCount() > 0)
			personalLayout.removeAllViews();
		if (educationLayout.getChildCount() > 0)
			educationLayout.removeAllViews();
		if (academicLayout.getChildCount() > 0)
			academicLayout.removeAllViews();

		final ImageLoader loader = ImageLoader.getInstance();
		loader.displayImage(user.avatarLink, avatar);
		final View v = getView(); // main view

		if (practice == null) {
			practiceLayout.setVisibility(View.GONE);
		} else {
			Log.w(TAG, "::parseResponseData:" + "practiceLayout");
			practiceLayout.setVisibility(View.VISIBLE);

			final RelativeLayout childLayout = (RelativeLayout) View
					.inflate(activity,
							R.layout.user_practice_layout, null);

			String street = practice.street1;
			if (street != null && practice.street2 != null)
				street = String.format("%s, %s", street, practice.street2);
			else if (street == null)
				street = practice.street2;

			final String fullAddr = String.format("%s\n%s, %s %s",
					street,
					practice.city,
					practice.state,
					practice.zipCode);

			setText(childLayout, R.id.practiceAddressTextView, R.id.practiceAddress, fullAddr);
			setText(childLayout, R.id.practiceNameTextView, R.id.practiceName, practice.name);
			setText(childLayout, R.id.practiceOfficePhoneTextView, R.id.practiceOfficePhone, practice.officePhone, true);
			setText(childLayout, R.id.practiceOfficeFaxTextView, R.id.practiceOfficeFax, practice.officeFax, true);
			setText(childLayout, R.id.practicePagerTextView, R.id.practicePager, practice.officePager, true);
			setText(childLayout, R.id.practiceInsurancePlansTextView, R.id.practiceInsurancePlans, StringUtils.join(insurancePlans, ','));
			setText(childLayout, R.id.practiceInsuranceAcceptedTextView, R.id.practiceInsuranceAccepted, insuranceAccepted);

			practiceLayout.addView(childLayout);
			practiceLayout.init();
			practiceLayout.setTitle(R.string.profile_practice);
		}

		if (affiliations == null || affiliations.length == 0) {
			affilationLayout.setVisibility(View.GONE);
		} else {
			affilationLayout.setVisibility(View.VISIBLE);
			Log.w(TAG, "::parseResponseData:" + "affilationLayout");
			for (final Affiliation.Entity affiliation : affiliations) {
				final RelativeLayout childLayout = (RelativeLayout) View
						.inflate(activity, R.layout.user_affilation_layout, null);

				String street = affiliation.street1;
				if (street != null && affiliation.street1 != null)
					street = String.format("%s, %s", street, affiliation.street2);
				else if (street == null)
					street = affiliation.street2;

				setText(childLayout, R.id.affilationHospitalTextView, R.id.affilationHospital, affiliation.hospitalName);
				setText(childLayout, R.id.affilationDepartmentTextView, R.id.affilationDepartment, affiliation.department);
				setText(childLayout, R.id.affilationStreetTextView, R.id.affilationStreet, street);
				setText(childLayout, R.id.affilationCityTextView, R.id.affilationCity, affiliation.city);
				setText(childLayout, R.id.affilationStateTextView, R.id.affilationState, affiliation.state);
				setText(childLayout, R.id.affilationZipTextView, R.id.affilationZip, affiliation.zipCode);

				affilationLayout.addView(childLayout);
			}
			affilationLayout.init();
			affilationLayout.setTitle(R.string.profile_affilations);
		}

		if (user.email == null && user.homePhone == null && user.cellPhone == null && !isOwnProfile) {
			personalLayout.setVisibility(View.GONE);
		} else {
			Log.w(TAG, "::parseResponseData:" + "personalLayout");
			personalLayout.setVisibility(View.VISIBLE);

			final RelativeLayout childLayout = (RelativeLayout) View
					.inflate(activity,
							R.layout.user_personal_layout, null);

			String street = user.street1;
			if (street != null && user.street2 != null)
				street = String.format("%s, %s", street, user.street2);
			else if (street == null)
				street = user.street2;

			if (isOwnProfile) {
				setText(childLayout, R.id.pesonalCityTextView, R.id.pesonalCity, user.city);
				setText(childLayout, R.id.pesonalStateTextView, R.id.pesonalState, user.state);
				setText(childLayout, R.id.pesonalCountryTextView, R.id.pesonalCountry, user.country);
				setText(childLayout, R.id.pesonalStreetTextView, R.id.pesonalStreet, street);
				setText(childLayout, R.id.pesonalZipTextView, R.id.pesonalZip, user.zipCode);
			} else {
				setText(childLayout, R.id.pesonalCityTextView, R.id.pesonalCity, null);
				setText(childLayout, R.id.pesonalStateTextView, R.id.pesonalState, null);
				setText(childLayout, R.id.pesonalCountryTextView, R.id.pesonalCountry, null);
				setText(childLayout, R.id.pesonalStreetTextView, R.id.pesonalStreet, null);
				setText(childLayout, R.id.pesonalZipTextView, R.id.pesonalZip, null);
			}
			setText(childLayout, R.id.pesonalHomePhoneTextView, R.id.pesonalHomePhone, user.homePhone);
			setText(childLayout, R.id.pesonalCellPhoneTextView, R.id.pesonalCellPhone, user.cellPhone);
			setText(childLayout, R.id.pesonalEmailTextView, R.id.pesonalEmail, user.email);

			personalLayout.addView(childLayout);
			personalLayout.init();
			personalLayout.setTitle(R.string.profile_personal);
		}

		if (education != null && education.length != 0
				|| medicalEducation != null && medicalEducation.length != 0
				|| residency != null
				|| fellowships != null && fellowships.length != 0) {
			educationLayout.setVisibility(View.VISIBLE);
			Log.w(TAG, "::parseResponseData:" + "educationLayout");
			if (education != null && education.length != 0) {
				educationLayout.addView(prepateSubTitleLayout(activity, "Post Secondary Education"));
				for (final Education.Entity educationEntity : education) {
					final RelativeLayout childLayout = (RelativeLayout) View
							.inflate(activity, R.layout.user_education_layout, null);

					setText(childLayout, R.id.educationDegreeTextView, R.id.educationDegree, educationEntity.degree);
					setText(childLayout, R.id.educationGraduationTextView, R.id.educationGraduation, educationEntity.graduationDate);
					setText(childLayout, R.id.educationSchoolTextView, R.id.educationSchool, educationEntity.school);
					educationLayout.addView(childLayout);
				}
			}

			if (medicalEducation != null && medicalEducation.length != 0) {
				educationLayout.addView(prepateSubTitleLayout(activity, "Medical Education"));
				for (final MedicalEducation.Entity medicalEducationEntity : medicalEducation) {
					final RelativeLayout childLayout = (RelativeLayout) View
							.inflate(activity, R.layout.user_medical_education_layout, null);

					setText(childLayout, R.id.medicalEducationDegreeTextView, R.id.medicalEducationDegree, medicalEducationEntity.degree);
					setText(childLayout, R.id.medicalEducationGraduationTextView, R.id.medicalEducationGraduation, medicalEducationEntity.graduationDate);
					setText(childLayout, R.id.medicalEducationSchoolTextView, R.id.medicalEducationSchool, medicalEducationEntity.medicalSchool);

					educationLayout.addView(childLayout);
				}
			}

			if (residency != null) {
				educationLayout.addView(prepateSubTitleLayout(activity, "Residency"));

				final RelativeLayout childLayout = (RelativeLayout) View
						.inflate(activity, R.layout.user_residency_layout, null);
				Log.w(TAG, "::parseResponseData:" + "residencyLayout");
				setText(childLayout, R.id.residencyHospitalTextView, R.id.residencyHospital, residency.hospital);
				setText(childLayout, R.id.residencySpecialityTextView, R.id.residencySpeciality, residency.speciality);

				educationLayout.addView(childLayout);
			}

			if (fellowships != null && fellowships.length > 0) {
				educationLayout.addView(prepateSubTitleLayout(activity, "Fellowships"));
				for (final Fellowship.Entity fellowshipEntity : fellowships) {
					final RelativeLayout childLayout = (RelativeLayout) View
							.inflate(activity, R.layout.user_fellowship_layout,
									null);

					setText(childLayout, R.id.fellowshipHospitalTextView, R.id.fellowshipHospital, fellowshipEntity.hospital);
					setText(childLayout, R.id.fellowshipSpecialityTextView, R.id.fellowshipSpeciality, fellowshipEntity.speciality);

					educationLayout.addView(childLayout);

				}
			}

			educationLayout.init();
			educationLayout.setTitle(R.string.profile_education);
		} else
			educationLayout.setVisibility(View.GONE);


		if (academics != null && academics.length != 0 || publications != null && publications.length != 0) {
			academicLayout.setVisibility(View.VISIBLE);
			Log.w(TAG, "::parseResponseData:" + "academicLayout");
			if (academics != null && academics.length > 0) {
				academicLayout.addView(prepateSubTitleLayout(activity, "Appointments"));
				for (final Academic.Entity academicEntity : academics) {
					final RelativeLayout childLayout = (RelativeLayout) View
							.inflate(activity, R.layout.user_academic_layout, null);

					setText(childLayout, R.id.academicTitleTextView, R.id.academicTitle, academicEntity.title);
					setText(childLayout, R.id.academicSchoolTextView, R.id.academicSchool, academicEntity.school);
					setText(childLayout, R.id.academicStartYearTextView ,R.id.academicStartYear, academicEntity.startYear);
					setText(childLayout, R.id.academicEndYearTextView, R.id.academicEndYear, academicEntity.endYear);

					academicLayout.addView(childLayout);
				}
			}
			if (publications != null && publications.length > 0) {
				academicLayout.addView(prepateSubTitleLayout(activity, "Publications"));
				for (final Publication.Entity publicationEntity : publications) {
					final RelativeLayout childLayout = (RelativeLayout) View
							.inflate(activity,
									R.layout.user_profile_publication_layout, null);

					setText(childLayout, R.id.publicationTitleTextView, R.id.publicationTitle, publicationEntity.title);
					setText(childLayout, R.id.publicationJournalTextView, R.id.publicationJournal, publicationEntity.journal);
					setText(childLayout, R.id.publicationIssueTextView, R.id.publicationIssue, publicationEntity.issue);
					setText(childLayout, R.id.publicationVolumeTextView, R.id.publicationVolume, publicationEntity.volume);
					setText(childLayout, R.id.publicationPagesTextView, R.id.publicationPages, publicationEntity.pages);
					setText(childLayout, R.id.publicationAuthorsTextView, R.id.publicationAuthors, publicationEntity.authors);
					setText(childLayout, R.id.publicationDateTextView, R.id.publicationDate, publicationEntity.date);

					academicLayout.addView(childLayout);

				}
			}

			academicLayout.init();
			academicLayout.setTitle(R.string.profile_academic);
		} else {
			academicLayout.setVisibility(View.GONE);
		}

		Log.w(TAG, "::parseResponseData:" + "end");
		((TextView) v.findViewById(R.id.name)).setText(user.name);
		((TextView) v.findViewById(R.id.memberType)).setText(user.memberType);
		((TextView) v.findViewById(R.id.speciality)).setText(user.speciality);
		((TextView) v.findViewById(R.id.status_text)).setText(user.statusText);
		if (user.sex != null)
			setText(v, 0, R.id.sex, user.sex);

		if (user.dob != null) {
			final long dobDate = Long.parseLong(user.dob) * 1000;
			final CharSequence date = DateFormat.format("d of MMM, yyyy", dobDate);
			setText(v, 0, R.id.dob, getString(R.string.profile_dob, date));
		}


		if (!user.status.booleanValue())
			((ImageView) v.findViewById(R.id.status))
					.setImageResource(R.drawable.status_circle_off);

		if (user.homePhone != null && !isOwnProfile) {
			homePhone.setVisibility(View.VISIBLE);
			homePhone.setOnClickListener(new OnProfileHomePhoneClickListener(
							user.homePhone));
		}

		if (user.cellPhone != null && !isOwnProfile) {
			cellPhone.setVisibility(View.VISIBLE);
			cellPhone.setOnClickListener(new OnProfileCellPhoneClickListener(
							user.cellPhone));
		}
		if (response.getData().practice != null) {
			final String officePhone = response.getData().practice.officePhone;
			if (officePhone != null && !isOwnProfile) {
				workPhone.setVisibility(View.VISIBLE);
				workPhone.setOnClickListener(new OnProfileWorkPhoneClickListener(
						officePhone));
			}
		}

		if (languages != null) {
			String languagesStr = "Languages: ";
			for (int i = 0; i < languages.length; i++) {
				if (i == 0)
					languagesStr = String.format("%s%s", languagesStr,
							languages[i].name);
				else
					languagesStr = String.format("%s, %s", languagesStr,
							languages[i].name);
			}

			((TextView) v.findViewById(R.id.languages))
					.setText(languagesStr);
		}

	}

	private void callPhoneNumber(final String phoneNumber) {
		try {
			final Intent callIntent = new Intent(Intent.ACTION_CALL);
			callIntent.setData(Uri.parse(String.format("tel:%s", phoneNumber)));
			startActivity(callIntent);
		} catch (final ActivityNotFoundException e) {
			Log.e(TAG, "Cannot execute phone call", e);
		}
	}

	protected class UserProfiletReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();
		private final WeakReference<UserProfileFragment> fragment;

		public UserProfiletReceiver(final UserProfileFragment fragment) {
			this.fragment = new WeakReference<UserProfileFragment>(fragment);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final UserProfileFragment f = fragment.get();
			if (f == null || f.getActivity() == null)
				return; // no action if fragment destroyed
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.w(TAG, "Start user profile fetching");
				// Start out with a progress indicator.
				f.mProgress.setVisibility(View.VISIBLE);
				setProgress(f, true);
				break;
			case SyncService.STATUS_FINISHED:
				f.mProgress.setVisibility(View.GONE);
				setProgress(f, false);
				final GetUserProfileResponse response = resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				parseResponseData(response);
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error getting user profile");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(f.getActivity(), error);
				}
				setProgress(f, false);
				f.getActivity().finish();
				break;
			}
		}
	}

	private void setProgress(final UserProfileFragment f, final boolean b) {
		if (f != null && f.getSherlockActivity() != null)
			f.getSherlockActivity().setSupportProgressBarIndeterminateVisibility(b);
	}

	protected class OperationReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();
		private final WeakReference<UserProfileFragment> fragment;

		public OperationReceiver(final UserProfileFragment fragment) {
			this.fragment = new WeakReference<UserProfileFragment>(fragment);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final UserProfileFragment f = fragment.get();
			if (f == null)
				return; // no action if fragment destroyed
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.w(TAG, "Start MarkWithLabelReceiver");
				setProgress(f, true);
				break;
			case SyncService.STATUS_FINISHED:
				Log.w(TAG, "Finish MarkWithLabelReceiver");
				setProgress(f, false);
				break;
			case SyncService.STATUS_ERROR:
				Log.w(TAG, "Error MarkWithLabelReceiver");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(f.getActivity(), error);
				}
				setProgress(f, false);

				break;
			}
		}
	}

	protected class DeleteUserReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();
		private final WeakReference<UserProfileFragment> fragment;

		public DeleteUserReceiver(final UserProfileFragment fragment) {
			this.fragment = new WeakReference<UserProfileFragment>(fragment);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final UserProfileFragment f = fragment.get();
			if (f == null)
				return; // no action if fragment destroyed
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.w(TAG, "Start DeleteUserReceiver");
				setProgress(f, true);
				break;
			case SyncService.STATUS_FINISHED:
				Log.w(TAG, "Finish DeleteUserReceiver");
				setProgress(f, false);
				f.labelProfileButton.setVisibility(View.GONE);
				f.deleteProfileButton.setVisibility(View.GONE);
				if (f.getActivity() != null) {
					f.getActivity().setResult(Activity.RESULT_OK);
					f.getActivity().finish();
				}
				break;
			case SyncService.STATUS_ERROR:
				Log.w(TAG, "Error DeleteUserReceiver");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(f.getActivity(), error);
				}
				setProgress(f, false);

				break;
			}
		}
	}

}
package com.mobile.health.one.app;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.URLUtil;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.app.RegisterPinFragment.RegisterPinReceiver;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.NodeWorkerService.IInvitesReceiver;
import com.mobile.health.one.service.NodeWorkerService.ILabelsReceiver;
import com.mobile.health.one.service.entity.Invite;
import com.mobile.health.one.service.entity.InvitesResponce;
import com.mobile.health.one.service.worker.ChangeInvitationStatusProcessor;
import com.mobile.health.one.service.worker.DeleteInvitationProcessor;
import com.mobile.health.one.service.worker.GetInvitesProcessor;
import com.mobile.health.one.util.OverScrollDisabler;
import com.mobile.health.one.util.SettingsManager;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.nostra13.universalimageloader.core.ImageLoader;

public class InviteListFragment extends LazyLoadFragment implements IInvitesReceiver, NodeServiceConnector {

	public class ChangeStatusReceiver implements Receiver {

		@Override
		public void onReceiveResult(final int resultCode, final Bundle resultData) {
			final InviteListFragment f = InviteListFragment.this;
			if (f == null || !f.isVisible())
				return;
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start change status");

				setListShown(false);
				mProgress.setVisibility(View.VISIBLE);

				break;
			case SyncService.STATUS_FINISHED:
				reset();
				loadData(false);
				Log.i(TAG, "Finished change status");

				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error change status");

				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(getActivity(), error);
				}

				break;
			}
		}
	}

	public class DeleteInviteReceiver implements Receiver {

		@Override
		public void onReceiveResult(final int resultCode, final Bundle resultData) {
			final InviteListFragment f = InviteListFragment.this;
			if (f == null || !f.isVisible())
				return;
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start delete invite");

				setListShown(false);
				mProgress.setVisibility(View.VISIBLE);

				break;
			case SyncService.STATUS_FINISHED:
				reset();
				loadData(false);
				Log.i(TAG, "Finished delete invite");
				final SettingsManager m = new SettingsManager(getActivity());
				final int count = m.getInvitesCount();
				m.setInvitesCount(count-1);
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error delete invite");

				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(getActivity(), error);
				}

				break;
			}
		}
	}

	protected class InvitesListReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final InviteListFragment f = InviteListFragment.this;
			if (f == null || !f.isVisible())
				return;
			boolean showProgress = true;
			if (resultData != null) {
				showProgress = resultData.getBoolean(EXTRA_SHOW_PROGRESS, true);
			}
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start invites list");
				if (showProgress) {
				// Start out with a progress indicator.
					if (getPrevTotalItemCount() == 0)
						setListShown(false);
					else {
						mProgress.setVisibility(View.VISIBLE);
					}
				}
				break;
			case SyncService.STATUS_FINISHED:
				final InvitesResponce responce = resultData
				.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				if (responce != null) {
					setTotalItemCount(responce.getData().pagination.total_users_count);
					final MatrixCursor c = responce.getData().saveToCursor();
					setData(c);
				}
				Log.i(TAG, "Saving to db start");
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error getting invites list");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(getActivity(), error);
				}
				// TODO: make diffs on error code, now server always return 1
				getActivity().finish();
				break;
			}
		}

	}

	ILabelsReceiver mLabelsReceiver;

	private static final String EXTRA_CUR_CHOICE = "curChoice";
	private static final int ITEMS_PER_PAGE = 10;

	private static final String EXTRA_SHOW_PROGRESS = "show_progress";

	private boolean dualPane;
	private int curCheckPosition = 0;
	private int invitesStatusType = Invite.Entity.STATUS_ALL;
	private int invitesType = Invite.Entity.TYPE_INCOMING;

	// This is the Adapter being used to display the list's data.
	private SimpleCursorAdapter adapter;

	private NodeWorkerService mService;

	@Override
	protected void loadData(final int page, final boolean showProgress) {
		Log.v(TAG, "showProgress: " + showProgress);
		Bundle args = getArguments();
		if (args == null) {
			args = new Bundle(5);
		}
		if (page > 0) {
			args.putInt(
					GetInvitesProcessor.EXTRA_ITEMS_PER_PAGE,
					ITEMS_PER_PAGE);
			args.putInt(
					GetInvitesProcessor.EXTRA_CURRENT_PAGE,
					page);
			args.putInt(
					GetInvitesProcessor.EXTRA_STATUS,
					invitesStatusType);
			args.putInt(
					Invite.Entity.EXTRA_TYPE,
					invitesType);
			args.putBoolean(
					EXTRA_SHOW_PROGRESS,
					showProgress);
			SyncService.startSyncService(getActivity(),
					AppSyncService.GET_INVITES,
					new InvitesListReceiver(),
					args);
		}
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		setEmptyText(getString(R.string.error_no_people));

		// We have a menu item to show in action bar.
		setHasOptionsMenu(true);

		// Create an empty adapter we will use to display the loaded data.
		adapter = new SimpleCursorAdapter(getActivity(),
				R.layout.item_invite_list, null, new String[] { Invite.Entity.USER_NAME,
			Invite.Entity.STATUS, Invite.Entity.STATUS, Invite.Entity.STATUS, Invite.Entity.STATUS, Invite.Entity.INVITATION_TEXT, Invite.Entity.USER_AVATAR },
			new int[] { R.id.name, R.id.status, R.id.btn_delete, R.id.btn_decline, R.id.btn_accept, R.id.invite_text, R.id.avatar }, 0);
		final ImageLoader loader = ImageLoader.getInstance();
		adapter.setViewBinder(new ViewBinder() {

			@Override
			public boolean setViewValue(final View view, final Cursor cursor,
					final int columnIndex) {
				final boolean incoming = invitesType == Invite.Entity.TYPE_INCOMING;
				switch (view.getId()) {
				case R.id.status:
					final TextView statusView = (TextView) view;
					int status = cursor.getInt(columnIndex);
					switch (status) {
					case Invite.Entity.STATUS_ACCEPTED:
						int strId = incoming ? R.string.invite_accepted : R.string.invite_accepted_out;
						statusView.setText(Html.fromHtml(getString(strId)));
						break;
					case Invite.Entity.STATUS_DECLINED:
						strId = incoming ? R.string.invite_declined : R.string.invite_declined_out;
						statusView.setText(Html.fromHtml(getString(strId)));
						break;
					case Invite.Entity.STATUS_PENDING:
						strId = incoming ? R.string.invite_pending : R.string.invite_pending_out;
						statusView.setText(strId);
						break;
					}
					return true;
				case R.id.btn_delete:
					status = cursor.getInt(columnIndex);
					Button b = (Button) view;
					long id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
					setButtonDeleteOnClickListener(b, id);
					return true;
				case R.id.btn_decline:
					if (incoming) {
						status = cursor.getInt(columnIndex);
						id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
						b = (Button) view;
						setButtonOnClickListener(b, id, Invite.Entity.STATUS_DECLINED);
						setViewVisibility(status, view);
					} else {
						view.setVisibility(View.GONE);
					}
					return true;
				case R.id.btn_accept:
					if (incoming) {
						status = cursor.getInt(columnIndex);
						id = cursor.getLong(cursor.getColumnIndex(BaseColumns._ID));
						b = (Button) view;
						setButtonOnClickListener(b, id, Invite.Entity.STATUS_ACCEPTED);
						setViewVisibility(status, view);
					} else {
						view.setVisibility(View.GONE);
					}
					return true;
				case R.id.avatar:
					String uri = cursor.getString(columnIndex);
					if (uri == null || !URLUtil.isValidUrl(uri)){
						uri = null;
					}
					loader.displayImage(uri, (ImageView) view);

					return true;
				default:
					return false;
				}

			}

			private void setViewVisibility(final int status, final View view) {
				int visibility;

				if (status == Invite.Entity.STATUS_PENDING)
					visibility = View.VISIBLE;
				else
					visibility = View.GONE;

				view.setVisibility(visibility);
			}

			private void setButtonOnClickListener(final Button button, final long invitationId, final int status) {
				final OnClickListener click = new OnClickListener() {
					private long mId;
					private int mStatus;

					{
						mId = invitationId;
						mStatus = status;
					}

					@Override
					public void onClick(final View v) {
						resetPageNumberOnly();
						final Bundle extras = new Bundle(2);
						extras.putInt(ChangeInvitationStatusProcessor.EXTRA_STATUS, mStatus);
						extras.putLongArray(ChangeInvitationStatusProcessor.EXTRA_IDS, new long[] { mId });
						SyncService.startSyncService(getActivity(),
								AppSyncService.CHANGE_INVITATION_STATUS,
								new ChangeStatusReceiver(), extras);

					}
				};
				button.setOnClickListener(click);
			}

			private void setButtonDeleteOnClickListener(final Button button, final long invitationId) {
				final OnClickListener click = new OnClickListener() {
					private long mId;
					private int mType;

					{
						mId = invitationId;
						mType = invitesType;
					}

					@Override
					public void onClick(final View v) {
						resetPageNumberOnly();
						final Bundle extras = new Bundle(2);
						extras.putLongArray(DeleteInvitationProcessor.EXTRA_IDS, new long[] { mId });
						extras.putString(DeleteInvitationProcessor.EXTRA_INCOMING, mType == Invite.Entity.TYPE_INCOMING ? "1" : null);

						SyncService.startSyncService(getActivity(),
							AppSyncService.DELETE_INVITATION_STATUS,
								new DeleteInviteReceiver(), extras);

					}
				};
				button.setOnClickListener(click);
			}
		});

		mProgress = new ProgressBar(getActivity());
		mProgress.setIndeterminate(true);
		mProgress.setVisibility(View.GONE);
		if(Build.VERSION.SDK_INT >= 9) {
		    OverScrollDisabler.disableOverScroll(getListView());
		}
		setListAdapter(adapter);
		getListView().setOnScrollListener(this);
		getListView().addFooterView(mProgress);

		// Check to see if we have a frame in which to embed the details
		// fragment directly in the containing UI.
		final View detailsFrame = getActivity().findViewById(R.id.details);
		dualPane = detailsFrame != null
				&& detailsFrame.getVisibility() == View.VISIBLE;

		if (savedInstanceState != null) {
			// Restore last state for checked position.
			curCheckPosition = savedInstanceState.getInt(EXTRA_CUR_CHOICE, 0);
		}

		if (dualPane) {
			// In dual-pane mode, the list view highlights the selected item.
			getListView().setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
			// Make sure our UI is in the correct state.
			showDetails(curCheckPosition);
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		loadData();
	}

	@Override
	public void onAttach(final Activity activity) {
		if (mService == null)
			((MHOApplication) getActivity().getApplication()).getNodeService(this);

		super.onAttach(activity);
	}

	@Override
	public void onNodeConnected(final NodeWorkerService service) {
		mService = service;
		mService.registerInvitesCallback(this);
	}

	@Override
	public void onDetach() {
		if (mService != null)
			mService.unregisterInvitesCallback(this);
		super.onDetach();
	}

	@Override
	public void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(EXTRA_CUR_CHOICE, curCheckPosition);
	}

	@Override
	public void onListItemClick(final ListView l, final View v,
			final int position, final long id) {
		final TextView inviteText = (TextView) v.findViewById(R.id.invite_text);
		final int visibility = inviteText.getVisibility() == View.VISIBLE ? View.GONE : View.VISIBLE;
		inviteText.setVisibility(visibility);
		Log.i("onListItemClick", "Item clicked: " + id);
	}

	/**
	 * Helper function to show the details of a selected item, either by
	 * displaying a fragment in-place in the current UI, or starting a whole new
	 * activity in which it is displayed.
	 */
	void showDetails(final int index) {
		curCheckPosition = index;
		// final long id = getListView().getAdapter().getItemId(index);
		final Cursor o = (Cursor) getListView().getAdapter().getItem(index);
		final int userIdIndex = o.getColumnIndexOrThrow(BaseColumns._ID);

		final Bundle args = new Bundle();
		args.putLong(BaseColumns._ID, o.getLong(userIdIndex));

		if (dualPane) {
			// We can display everything in-place with fragments, so update
			// the list to highlight the selected item and show the data.
			getListView().setItemChecked(index, true);
			// Check what fragment is currently shown, replace if needed.
			MessageDetailFragment details = (MessageDetailFragment) getFragmentManager()
					.findFragmentById(R.id.details);
			if (details == null) {
				// Make new fragment to show this selection.
				details = new MessageDetailFragment();
				details.setArguments(args);

				// Execute a transaction, replacing any existing fragment
				// with this one inside the frame.
				final FragmentTransaction ft = getFragmentManager()
						.beginTransaction();
				ft.replace(R.id.details, details);
				ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
				ft.commit();
			}

		} else {
			// Otherwise we need to launch a new activity to display
			// the dialog fragment with selected text.
			final Intent intent = new Intent();
			intent.setClass(getActivity(), MessageDetailActivity.class);
			intent.putExtras(args);
			// /startActivity(intent);
		}
	}

	private static final String TAG = InviteListFragment.class.getSimpleName();
	private ProgressBar mProgress;

	public int getInvitesStatusType() {
		return invitesStatusType;
	}

	public void setInvitesStatusType(final int invitesStatusType) {
		reset();
		this.invitesStatusType = invitesStatusType;
	}

	public int getInvitesType() {
		return invitesType;
	}

	public void setInvitesType(final int invitesType) {
		reset();
		this.invitesType = invitesType;
	}

	@Override
	public Handler getHandler() {
		return new Handler(getActivity().getMainLooper());
	}

	@Override
	public void onNewInvite() {
		loadData(false);
	}

	@Override
	public void onInvitesCount(final int count) {
		return;
	}

	@Override
	public void onNewGroupInvite() {
		// nop
	}

	@Override
	protected ProgressBar getLazyLoadingProgressView() {
		return mProgress;
	}

	@Override
	protected CursorAdapter getCursorAdapter() {
		return adapter;
	}
}
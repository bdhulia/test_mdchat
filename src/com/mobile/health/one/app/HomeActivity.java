package com.mobile.health.one.app;

import java.util.HashMap;

import android.app.Dialog;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.GCMIntentService;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.search.TextTask.Listener;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.NodeWorkerService.IInvitesReceiver;
import com.mobile.health.one.service.NodeWorkerService.IMessageReceiver;
import com.mobile.health.one.service.entity.GetInvitesCountResponse;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Invite;
import com.mobile.health.one.util.AlertDialogUtil;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;
import com.mobile.health.one.util.SearchParams.SearchType;

public class HomeActivity extends AppFragmentActivity implements
		IMessageReceiver, IInvitesReceiver, Listener, NodeServiceConnector {

	public static final String EXTRA_SHOW_WELCOME = "show_welcome";
	private static final int DIALOG_WELCOME = 0;
	private NodeWorkerService mService;
	private DashboardFragment dashboard;
	private Handler mHandler;
	private int mBackPressedCounter;
	public boolean mGotInvites;
	public boolean mGotGroupInvites;
	public Integer mInvitesCount;
	public Integer mGroupInvitesCount;

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		getSupportActionBar(); // stub for ability to hide progress
		setSupportProgressBarIndeterminateVisibility(false);

		mHandler = new Handler();

		final FragmentTransaction ta = getSupportFragmentManager()
				.beginTransaction();
		if (getIntent().getBooleanExtra(EXTRA_SHOW_WELCOME, false)) {
			ta.add(DIALOG_WELCOME, new AfterRegisterDialog());
		}
		dashboard = new DashboardFragment();

		ta.add(SearchFragment.newInstance(this, SearchType.DASHBOARD),
				SearchFragment.TAG);
		ta.add(R.id.root, new StatusFragment());
		ta.add(R.id.root, dashboard);
		ta.commit();

		GCMIntentService.registerOnServer(this);

		//LauncherUtils.generateNotification(this, "Test", getIntent());
	}

	@Override
	public void onNodeConnected(final NodeWorkerService service) {
		//service.serverAuth(getSettings().getSessionId());
		service.registerMessageCallback(this);
		service.registerInvitesCallback(this);
		service.getUnreadMessagesCount();

		mService = service;
	}

	@Override
	public void onResume() {
		super.onResume();
		startGetInvites();
		mBackPressedCounter = 0;
		((MHOApplication) getApplication()).getNodeService(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mService != null) {
			mService.unregisterMessageCallback(this);
			mService.unregisterInvitesCallback(this);
		}
	}

	@SuppressWarnings("unused")
	private void setInvitesFromSettings() {
		final int invites = getSettings().getInvitesCount();
		final int groupInvites = getSettings().getGroupInvitesCount();
		dashboard.resetInvitesCount();
		dashboard.setGroupInvitesCount(invites);
		dashboard.setGroupInvitesCount(groupInvites);
	}

	private void startGetInvites() {
		final Bundle bundle = new Bundle();
		bundle.putInt(Invite.Entity.EXTRA_TYPE, Invite.Entity.GROUP_INVITES);

		SyncService.startSyncService(this,
				AppSyncService.GET_INVITES_COUNT,
				new GroupInvitesCountReceiver(), bundle);

		SyncService.startSyncService(this, AppSyncService.GET_INVITES_COUNT,
				new InvitesCountReceiver(), getIntent().getExtras());
	}

	@Override
	protected void onDestroy() {
		// GCMRegistrar.onDestroy(this);
		super.onDestroy();
	}

	public static class AfterRegisterDialog extends SherlockDialogFragment {

		@Override
		public Dialog onCreateDialog(final Bundle savedInstanceState) {
			return AlertDialogUtil.alertDialog(getActivity(),
					R.string.dialog_registered_title,
					R.string.dialog_registered_message);
		}
	}

	@Override
	public void onMessageReceived(final HashMap<String, Object> message) {
		if (dashboard != null && mService != null) {
			// dashboard.incMessageCount();
			mService.getUnreadMessagesCount();
		}
	}

	@Override
	public void onMessageCount(final int count) {
		if (dashboard != null) {
			dashboard.setMessageCount(count);
		}
	}

	@Override
	public void onInvitesCount(final int count) {
		if (dashboard != null) {
			Log.i(TAG, "COUNT INVITES: " + count);
		}
	}

	@Override
	public Handler getHandler() {
		return mHandler;
	}

	@Override
	public void onSearchStart() {
	}

	@Override
	public void onSearchRestart() {
	}

	@Override
	public void onSearchError(final String error) {
	}

	@Override
	public void onSearchComplete(final IResponce responce) {
	}

	@Override
	public void onSearchCancel() {
	}

	@Override
	public void onNewInvite() {
		startGetInvites();
	}

	@Override
	public void onNewGroupInvite() {
		startGetInvites();
	}

	private class InvitesCountReceiver implements Receiver {
		private final String TAG = InvitesCountReceiver.class
				.getCanonicalName();

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start invites count");
				break;
			case SyncService.STATUS_FINISHED:
				final GetInvitesCountResponse response = resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);

				if(dashboard != null && response.getData() != null) {
					Log.i(TAG, "Got invites count: " + response.getData().count);
					mInvitesCount = response.getData().count;
					mGotInvites = true;
					updateInvitesCount();
				}

				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error getting invites count");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(HomeActivity.this, error);
				}
				break;
			}
		}
	}

	private class GroupInvitesCountReceiver implements Receiver {
		private final String TAG = GroupInvitesCountReceiver.class
				.getCanonicalName();

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start group invites count");
				break;
			case SyncService.STATUS_FINISHED:
				final GetInvitesCountResponse response = resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);

				if(dashboard != null && response.getData() != null) {
					Log.i(TAG, "Got group invites count: " + response.getData().count);
					mGroupInvitesCount = response.getData().count;
					mGotGroupInvites = true;
					updateInvitesCount();
				}
				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error getting invites count");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(HomeActivity.this, error);
				}
				break;
			}
		}
	}

	private void updateInvitesCount() {
		if(dashboard != null && mGotInvites && mGotGroupInvites) {
			dashboard.resetInvitesCount();
			dashboard.setGroupInvitesCount(mGroupInvitesCount);
			dashboard.setContactInvitesCount(mInvitesCount);
			dashboard.updateInvitesCount();
			mGotInvites = false;
			mGotGroupInvites = false;
		}
	}

	@Override
	public void onBackPressed() {
		if (mBackPressedCounter++ == 2) {
			//super.onBackPressed(); // back press not allowed
			ToastUtil.showText(this, "To relogin please unregister your device in Settings");
		} else if (mBackPressedCounter > 3) {
			super.onBackPressed();
		}
	}
}
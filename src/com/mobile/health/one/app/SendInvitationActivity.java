package com.mobile.health.one.app;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.app.ViewHolder.Validator;
import com.mobile.health.one.search.SuggestTextWatcher;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.GetInvitesTemplatesResponse;
import com.mobile.health.one.service.entity.SendInvitesResponce;
import com.mobile.health.one.service.worker.AutocompleteInviteProcessor;
import com.mobile.health.one.service.worker.SendInvitesProcessor;
import com.mobile.health.one.util.TextCounterListener;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class SendInvitationActivity extends AppFragmentActivity {

	/**
	 * Valid cause never called from layout or even other unit
	 */
	@SuppressLint("ValidFragment")
	public class ConfirmDialog extends DialogFragment implements DialogInterface.OnClickListener {

		public static final String EXTRA_BACK_PRESSED = "back_pressed";
		private boolean isBackPressed;

		@Override
		public void onCreate(final Bundle savedInstanceState) {
			isBackPressed = getArguments().getBoolean(EXTRA_BACK_PRESSED);
			super.onCreate(savedInstanceState);
		}

		@Override
		public Dialog onCreateDialog(final Bundle savedInstanceState) {
			final AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
			dialog.setMessage(R.string.message_discard_message);
			dialog.setTitle(R.string.message_discard_title);
			dialog.setPositiveButton(R.string.message_discard_btn, this);
			dialog.setNegativeButton(R.string.btn_no, this);
			return dialog.create();
		}
		@Override
		public void onClick(final DialogInterface dialog, final int which) {
			switch (which) {
			case DialogInterface.BUTTON_POSITIVE:
				dialog.dismiss();

				if (isBackPressed)
					SendInvitationActivity.this.finish();
				else
					NavUtils.navigateUpFromSameTask(SendInvitationActivity.this);
				break;
			case DialogInterface.BUTTON_NEGATIVE:
				dialog.cancel();
				break;
			default:
				break;
			}

		}
	}

	private static final String TEMPLATE_TITLE = "title";
	private static final String TEMPLATE_TEXT = "text";
	private static final String EXTRA_GET_TOKEN_ONLY = "get_token_only";
	private static final int MENU_ID_SEND = 10;

	private ViewHolder users;
	private ViewHolder message;

	private final ViewHolder.Validator validator = new Validator() {

		@Override
		public ValidationResult onValidate(final ViewHolder vh,
				final CharSequence s) {
			return new ValidationResult();
		}

		@Override
		public ValidationResult onLoseFocus(final ViewHolder vh,
				final CharSequence s) {
			if (TextUtils.isEmpty(s)) {
				CharSequence err;
				if (vh.view.equals(users.view)) {
					err = getText(R.string.error_no_recepients);
				} else {
					err = getText(R.string.error_empty);
				}

				return new ValidationResult(false, err);
			}
			if (vh.view.equals(users.view)) {
				final String errStr = getString(R.string.error_no_user_selected);

				final String[] ids = ((Editable) s).getSpans(0, s.length(), String.class);
				final boolean emailMathes = Patterns.EMAIL_ADDRESS.matcher(s).matches();
				final boolean idsMatches = ids.length <= 0;
				if (idsMatches && !emailMathes) {
					return new ValidationResult(false, errStr);
				}
				if (!idsMatches && emailMathes) {
					return new ValidationResult(false, errStr);
				}
			}
			final ValidationResult res = new ValidationResult(); // all ok
			return res;
		}
	};

	public String mInviteToken;
	private Bundle mExtras;
	private Spinner mTemplates;

	protected static class GetInvitesTemplatesReceiver implements Receiver {

		private final WeakReference<SendInvitationActivity> mActivity;

		GetInvitesTemplatesReceiver(final SendInvitationActivity a) {
			mActivity = new WeakReference<SendInvitationActivity>(a);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final SendInvitationActivity a = mActivity.get();
			// do nothing, if activity stopped
			if (a != null) {
				switch (resultCode) {
				case SyncService.STATUS_RUNNING:
					a.setSupportProgressBarIndeterminateVisibility(true);
					a.mTemplates.setEnabled(false);
					break;
				case SyncService.STATUS_FINISHED:
					fillSpinner(resultData);
					a.mTemplates.setEnabled(true);
					a.setSupportProgressBarIndeterminateVisibility(false);
					break;
				case SyncService.STATUS_ERROR:
					a.setSupportProgressBarIndeterminateVisibility(false);
					break;
				}
			}
		}

		private void fillSpinner(final Bundle resultData) {
			final SendInvitationActivity a = mActivity.get();
			if (resultData != null && a != null) {
				final GetInvitesTemplatesResponse resp = resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				final GetInvitesTemplatesResponse.Message[] messages = resp
						.getData().messages;
				final List<Map<String, Object>> data = new ArrayList<Map<String, Object>>(
						messages.length);
				for (final GetInvitesTemplatesResponse.Message message : messages) {
					final Map<String, Object> m = new HashMap<String, Object>();
					m.put(TEMPLATE_TITLE, message.title);
					m.put(TEMPLATE_TEXT, message.description);
					data.add(m);
				}
				final SimpleAdapter adapterTemplate = new SimpleAdapter(a,
						data, R.layout.spinner_item, new String[] {
						TEMPLATE_TITLE, TEMPLATE_TEXT }, new int[] {
						android.R.id.title, android.R.id.content });
				a.mTemplates.setAdapter(adapterTemplate);
				a.mTemplates
				.setOnItemSelectedListener(new OnItemSelectedListener() {
					@Override
					public void onItemSelected(
							final AdapterView<?> parent,
							final View view, final int position,
							final long id) {
						a.message.view
						.setText(messages[position].description);
					}

					@Override
					public void onNothingSelected(
							final AdapterView<?> arg0) {
						a.message.view.setText("");
					}
				});
			}
		}

	}

	protected class SendInvitationReceiver implements Receiver {

		private final WeakReference<SendInvitationActivity> mActivity;

		public SendInvitationReceiver(final SendInvitationActivity a) {
			mActivity = new WeakReference<SendInvitationActivity>(a);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final SendInvitationActivity a = mActivity.get();
			if (resultData != null && a != null) {
				final SendInvitesResponce resp = resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				String inviteToken = "";
				if (resp != null && resp.getData() != null && resp.getData().form_fields != null) {
					inviteToken = resp.getData().form_fields._token;
				}
				// if only get token, exit
				if (resultData.getBoolean(EXTRA_GET_TOKEN_ONLY, false)) {
					resultData.putBoolean(EXTRA_GET_TOKEN_ONLY, false);
					resultData.putString(
							SendInvitesProcessor.PARAM_INVITE_TOKEN,
							inviteToken);
					SyncService.startSyncService(a,
							AppSyncService.SEND_INVITES_REQUEST,
							new SendInvitationReceiver(a), resultData);
					return;
				}

				switch (resultCode) {
				case SyncService.STATUS_RUNNING:
					a.setSupportProgressBarIndeterminateVisibility(true);
					break;
				case SyncService.STATUS_FINISHED:
					a.setSupportProgressBarIndeterminateVisibility(false);
					finish();
					break;
				case SyncService.STATUS_ERROR:
					a.setSupportProgressBarIndeterminateVisibility(false);
					break;
				}
			}
		}
	}

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setSupportProgressBarIndeterminateVisibility(false);
		setContentView(R.layout.activity_send_invitation);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		users = new ViewHolder(
				(AutoCompleteTextView) findViewById(R.id.users),
				validator, false);
		final AutoCompleteTextView m = (AutoCompleteTextView) users.view;
		final CharSequenceArrayAdapter adapter = new CharSequenceArrayAdapter(
				this,
				android.R.layout.simple_dropdown_item_1line,
				android.R.id.text1,
				new AutocompleteInviteProcessor(this), m);
		m.setAdapter(adapter);
		m.addTextChangedListener(new SuggestTextWatcher(m));

		message = new ViewHolder((EditText) findViewById(R.id.message),
				validator, false);

		mTemplates = (Spinner) findViewById(R.id.templates);

		final int maxChars = getResources().getInteger(R.integer.max_message_chars);
		final TextView count = (TextView) findViewById(R.id.count);
		message.view.addTextChangedListener(new TextCounterListener(count, maxChars));
		count.setText(Integer.toString(maxChars));

		findViewById(R.id.btn_get_user).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				final Intent i = new Intent(SendInvitationActivity.this, PeopleListActivity.class);
				i.setAction(PeopleListActivity.ACTION_GET_CONTACT);
				i.putExtra(PeopleListActivity.EXTRA_SHOW_TAB, R.id.filter_members);
				startActivityForResult(i, PeopleListActivity.REQUEST_GET_CONTACT);

			}
		});

		SyncService.startSyncService(this,
				AppSyncService.GET_INVITES_TEMPLATES,
				new GetInvitesTemplatesReceiver(this), Bundle.EMPTY);

		final String autoAddSenderId = getIntent().getStringExtra(MenuMessageCreateFragment.EXTRA_SENDER_ID);
		final String autoAddSenderName = getIntent().getStringExtra(MenuMessageCreateFragment.EXTRA_SENDER_NAME);
		addUserToAutocomplete(autoAddSenderId, autoAddSenderName);

	}

	private void addUserToAutocomplete(final String id, final String name) {
		final CharSequence newUser = CharSequenceArrayAdapter.convertToString(this, id, name);
		Log.i(TAG, "::onActivityResult:" + "id = " + id + ", name = " + name);

		final AutoCompleteTextView edit = (AutoCompleteTextView) users.view;

		edit.setFocusable(false);
		edit.setFocusableInTouchMode(false);
		edit.setText(newUser);
		edit.setFocusable(true);
		edit.setFocusableInTouchMode(true);
		edit.requestFocus();
	}

	@Override
	protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
		switch (requestCode) {
		case PeopleListActivity.REQUEST_GET_CONTACT:

			if (resultCode == RESULT_OK) {
				final Long id = data.getLongExtra(PeopleListActivity.EXTRA_ID, 0);
				final String name = data.getStringExtra(PeopleListActivity.EXTRA_NAME);
				addUserToAutocomplete(id.toString(), name);
			}

			break;

		default:
			break;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		final MenuItem send = menu.add(Menu.NONE, MENU_ID_SEND, Menu.NONE, R.string.menu_send);
		send.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			final ConfirmDialog d = new ConfirmDialog();
			final Bundle b = new Bundle(1);
			b.putBoolean(ConfirmDialog.EXTRA_BACK_PRESSED, false);
			d.setArguments(b);
			d.show(getSupportFragmentManager(), TAG);
			return true;
		case MENU_ID_SEND:
			sendInvitation();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		final ConfirmDialog d = new ConfirmDialog();
		final Bundle b = new Bundle(1);
		b.putBoolean(ConfirmDialog.EXTRA_BACK_PRESSED, true);
		d.setArguments(b);
		d.show(getSupportFragmentManager(), TAG);
		//super.onBackPressed();
	}

	private boolean validate() {
		return (users.isValid() || !users.view.isEnabled())
				&& message.isValid();
	}

	private void sendInvitation() {
		if (validate()) {
			final Editable text = users.view.getText();
			final String[] ids = text.getSpans(0, text.length(), String.class);
			final String emailsStr = getEmailFromAutocomplete().toString();
			final String idsStr = ids.length > 0 ? ids[0] : "";

			String usersStr = idsStr;
			// previously validated email
			if (TextUtils.isEmpty(idsStr)) {
				usersStr = emailsStr;
			}

			final String messageStr = message.view.getText().toString();

			mExtras = new Bundle();
			mExtras.putString(SendInvitesProcessor.PARAM_INVITE_USERS,
					usersStr);
			mExtras.putString(SendInvitesProcessor.PARAM_INVITE_MESSAGE,
					messageStr);
			mExtras.putBoolean(EXTRA_GET_TOKEN_ONLY, true);
			SyncService.startSyncService(this,
					AppSyncService.SEND_INVITES_REQUEST,
					new SendInvitationReceiver(this), mExtras);
		}
	}

	private CharSequence getEmailFromAutocomplete() {
        return users.view.getText();
	}
}

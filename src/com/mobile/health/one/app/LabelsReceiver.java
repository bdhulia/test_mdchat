package com.mobile.health.one.app;

import java.lang.ref.WeakReference;
import java.util.List;

import android.app.Activity;
import android.os.Handler;

import com.mobile.health.one.db.entity.Label.Entity;
import com.mobile.health.one.service.NodeWorkerService.ILabelsReceiver;

class LabelsReceiver implements ILabelsReceiver {

	WeakReference<Activity> mActivity;
	List<Entity> mLabels;

	private final Handler mHandler;

	LabelsReceiver(final Activity activity) {
		mActivity = new WeakReference<Activity>(activity);
		mHandler = new Handler(activity.getMainLooper());
	}

	@Override
	public void onLabelsReceived(final List<Entity> labels) {
		//final Activity a = mActivity.get();
		mLabels = labels;
		// to nothing for now

	}

	@Override
	public Handler getHandler() {
		return mHandler;
	}

	@Override
	public void onMoveThreadToLabel(final List<Entity> labels, final Entity newLabel) { }

}
package com.mobile.health.one.app;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;

import com.actionbarsherlock.app.SherlockListFragment;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.db.entity.Confirm;
import com.mobile.health.one.db.entity.User;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.NodeWorkerService.IConfirmReceiver;

public class MessageConfirmFragment extends SherlockListFragment implements
		IConfirmReceiver {

	private NodeWorkerService mService;
	private Handler mHandler;
	private SimpleAdapter mAdapter;

	public static MessageConfirmFragment newInstance(final String messageId) {
		final MessageConfirmFragment fragment = new MessageConfirmFragment();
		final Bundle extras = new Bundle();
		extras.putString(MessageDetailFragment.EXTRA_ID, messageId);
		fragment.setArguments(extras);

		return fragment;
	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final String messageId = getArguments().getString(
				MessageDetailFragment.EXTRA_ID);

		((MHOApplication) getActivity().getApplication())
				.getNodeService(new NodeServiceConnector() {

					@Override
					public void onNodeConnected(final NodeWorkerService service) {
						try {
							service.getConfirmMessages(messageId);
						} catch (final JSONException e) {
							e.printStackTrace();
						}
						service.registerConfirmCallback(MessageConfirmFragment.this);
						mService = service;
					}
				});
	}

	@Override
	public void onDestroy() {
		if (mService != null)
			mService.unregisterConfirmCallback(this);
		super.onDestroy();
	}

	@Override
	public void onConfirmsReceived(final List<Confirm.Entity> confirms) {
		if (getActivity() == null)
			return;
		final SimpleDateFormat formatter = new SimpleDateFormat(getSherlockActivity().getString(R.string.message_read_date_format));
		setEmptyText(getString(R.string.error_no_people));

		final List<HashMap<String, Object>> data = new ArrayList<HashMap<String, Object>>();

		for (final Confirm.Entity confirmEntity : confirms) {
			final String formattedDate = confirmEntity.date > 0 ? formatter.format(new Date(confirmEntity.date)) : getSherlockActivity().getString(R.string.message_read_unconfirmed);
			final HashMap<String, Object> map = new HashMap<String, Object>();
			map.put(Confirm.USER_NAME, confirmEntity.userName);
			map.put(Confirm.STATUS, confirmEntity.status);
			map.put(User.STATUS_TEXT, formattedDate);

			data.add(map);
		}

		mAdapter = new SimpleAdapter(getActivity(), data,
				R.layout.item_confirm_list, new String[] { Confirm.USER_NAME,
						Confirm.STATUS, User.STATUS_TEXT }, new int[] {
						R.id.name, R.id.status, R.id.status_text });

		mAdapter.setViewBinder(new ViewBinder() {

			@Override
			public boolean setViewValue(final View view, final Object data,
					final String textRepresentation) {
				switch (view.getId()) {
				case R.id.status:
					final Boolean status = (Boolean) data;

					final ImageView image = (ImageView) view;
					final int drawableRes = status ? R.drawable.status_circle_on
							: R.drawable.status_circle_off;
					image.setImageResource(drawableRes);

					return true;

				default:
					return false;
				}
			}
		});

		setListAdapter(mAdapter);

	}

	@Override
	public Handler getHandler() {
		if (mHandler == null) {
			mHandler = new Handler(getActivity().getMainLooper());
		}
		return mHandler;
	}
}
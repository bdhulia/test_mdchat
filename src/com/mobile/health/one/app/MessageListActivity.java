package com.mobile.health.one.app;

import java.util.HashMap;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.widget.SimpleCursorAdapter;
import android.text.TextUtils;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.ViewSwitcher;

import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.Window;
import com.mobile.health.one.AppFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.app.LabelsListFragment.LabelsListCallback;
import com.mobile.health.one.db.entity.Label;
import com.mobile.health.one.db.entity.Label.Entity;
import com.mobile.health.one.search.TextTask.Listener;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.CursorSaver;
import com.mobile.health.one.util.SearchParams.AliasVariant;
import com.mobile.health.one.util.SearchParams.SearchType;

public class MessageListActivity extends AppFragmentActivity implements
		OnNavigationListener, LabelsListCallback, OnTouchListener,
		OnClickListener, Listener {

	private static final String TAG = "MessageListActivity";

	private static final int SWITCHER_ID = 10;

	private class MessageGestureListener extends SimpleOnGestureListener {
		@Override
		public boolean onFling(final MotionEvent e1, final MotionEvent e2,
				final float velocityX, final float velocityY) {
			Log.i(TAG, "::onFling:" + " velosityX " + velocityX
					+ ", velocityY " + velocityY);
			return super.onFling(e1, e2, velocityX, velocityY);
		}
	}

	// private static final String[] PROJECTION = new String[] { Label._ID,
	// Label.TITLE };

	private SimpleCursorAdapter adapter;

	private GestureDetector mGestureDetector;

	private MessageListFragment mMessageListFragment;

	private LabelsListFragment mLabelsListFragment;

	private boolean mIsLabelsShown = false;

	private boolean startSearch = false;

	private SearchFragment mSearchFragment;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		getSupportActionBar(); // stub for ability to hide progress
		setSupportProgressBarIndeterminateVisibility(false);

		adapter = new SimpleCursorAdapter(getSupportActionBar()
				.getThemedContext(), android.R.layout.simple_list_item_1, null,
				new String[] { Label.TITLE }, new int[] { android.R.id.text1 },
				0);

		/*
		mAnimLeftIn = AnimationUtils.loadAnimation(this, R.anim.fade_left_in);
		mAnimLeftOut = AnimationUtils.loadAnimation(this, R.anim.fade_left_out);
		mAnimRightIn = AnimationUtils.loadAnimation(this, R.anim.fade_right_in);
		mAnimRightOut = AnimationUtils.loadAnimation(this,
				R.anim.fade_right_out);*/

		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setDisplayShowTitleEnabled(true);
		// getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		getSupportActionBar().setListNavigationCallbacks(adapter, this);

		final ViewSwitcher switcher = new ViewSwitcher(this);
		switcher.setId(SWITCHER_ID);
		setContentView(switcher);

		mLabelsListFragment = new LabelsListFragment();

		final CharSequence searchText = AliasVariant
				.getSearchParam(getIntent());
		if (!TextUtils.isEmpty(searchText)) {
			startSearch = true;
			mSearchFragment = SearchFragment.newInstance(this,
					SearchType.THREADS, searchText);
			getSupportActionBar().setDisplayShowHomeEnabled(false);
		} else {
			mSearchFragment = SearchFragment.newInstance(this,
					SearchType.THREADS);
		}

		mMessageListFragment = MessageListFragment.newInstance(startSearch);

		switcher.setOnTouchListener(this);

		if (switcher.getChildCount() == 0) {
			// add top context menu
			getSupportFragmentManager().beginTransaction()
					.disallowAddToBackStack()
					.add(mSearchFragment, SearchFragment.TAG)
					.add(android.R.id.content, mMessageListFragment, MessageListFragment.TAG)
					.add(android.R.id.content, mLabelsListFragment, LabelsListFragment.TAG)
					.hide(mLabelsListFragment)
					.commit();
		}

		mGestureDetector = new GestureDetector(this,
				new MessageGestureListener());

		// FIXME those shoud be removed and called MessageDetailActivity directly
		/*final String threadId = getIntent().getStringExtra(Message.CONVERSATION_ID);
		if (StringUtils.isNotEmpty(threadId)) {
			final Intent i = new Intent(getIntent());
			i.setClass(this, MessageDetailActivity.class);
			startActivity(i);
		}*/
	}

	@Override
	protected void onRestoreInstanceState(final Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	public boolean onTouch(final View v, final MotionEvent event) {
		Log.i(TAG, "::onTouch:" + "");
		return mGestureDetector.onTouchEvent(event);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			NavUtils.navigateUpFromSameTask(this);
			return true;
		case R.id.menu_labels:
			showPrevious();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(final int itemPosition,
			final long itemId) {
		return false;
	}

	@Override
	public void onItemClick(final String remoteId, final String labelName) {
		if (mMessageListFragment != null) {
			mMessageListFragment.loadThreads(remoteId, labelName);
			showNext();
		}
	}

	@Override
	public void onItemsSelected(final HashMap<String, String> labels) {
	}

	@Override
	public void onClick(final View v) {
		showPrevious();
	}

	private void showPrevious() {
		//mSwitcher.setOutAnimation(mAnimRightOut);
		//mSwitcher.setInAnimation(mAnimRightIn);
		//mSwitcher.showPrevious();
		mIsLabelsShown = true;
		getSupportFragmentManager().beginTransaction()
			.hide(mSearchFragment)
			.show(mLabelsListFragment)
			.hide(mMessageListFragment)
			//.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
			.commit();
	}

	private void showNext() {
		//mSwitcher.setOutAnimation(mAnimLeftOut);
		//mSwitcher.setInAnimation(mAnimLeftIn);
		//mSwitcher.showNext();
		mIsLabelsShown = false;
		getSupportFragmentManager().beginTransaction()
			.show(mSearchFragment)
			.hide(mLabelsListFragment)
			.show(mMessageListFragment)
			//.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
			.commit();
	}

	@Override
	public void onSearchStart() {
		setSupportProgressBarIndeterminateVisibility(true);
		if (startSearch) {
			getSupportActionBar().setDisplayShowHomeEnabled(false);
		}
	}

	@Override
	public void onSearchComplete(final IResponce responce) {
		Log.i(TAG, "::onSearchComplete:" + "");
		final CursorSaver r = (CursorSaver) responce;
		final Cursor c = r.saveToCursor();
		final MessageListFragment f = mMessageListFragment;
		if (c != null && c.getCount() > 0) {
			f.setCursorSavingOld(c);
			f.setSearch(true);
		} else {
			f.setCursorSavingOld(null);
			f.setSearch(false);
		}
		setSupportProgressBarIndeterminateVisibility(false);
		if (f.getView() != null)
			f.setListShown(true);
	}

	@Override
	public void onSearchError(final String error) {
		Log.i(TAG, "::onSearchError:" + "");
		setSupportProgressBarIndeterminateVisibility(false);
		final MessageListFragment f = mMessageListFragment;
		f.setSearch(false);
	}

	@Override
	public void onSearchRestart() {
		Log.i(TAG, "::onSearchRestart:" + "");
		final MessageListFragment f = mMessageListFragment;
		f.setSearch(false);
		f.setOldCursor();
	}

	@Override
	public void onSearchCancel() {

	}

	@Override
	public void onRootLabel(final Entity label) {
		// mMessageListFragment.setTitleText(label.labelName + " (" +
		// label.count + ")");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		if (mIsLabelsShown)
			showNext();
		else
			super.onBackPressed();
	}

}

package com.mobile.health.one.app;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.MenuItem;
import com.mobile.health.one.R;
import com.mobile.health.one.service.entity.DirectoryUserProfile;
import com.google.android.apps.iosched.ui.widget.ExpandableLinearLayout;

public class DirectoryUserFragment extends SherlockFragment {
	private ExpandableLinearLayout officeLayout;
	private ExpandableLinearLayout specialitiesLayout;
	private LayoutInflater inflater;

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		inflater = getActivity().getLayoutInflater();
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
			final Bundle savedInstanceState) {
		this.inflater = inflater;

		final ViewGroup mainView = (ViewGroup) inflater.inflate(R.layout.directory_profile, container, false);
		officeLayout = (ExpandableLinearLayout) mainView.findViewById(R.id.officesLinearLayout);
		specialitiesLayout = (ExpandableLinearLayout) mainView.findViewById(R.id.specialitiesLinearLayout);

		return mainView;
	}

	@Override
	public void onViewCreated(final View view, final Bundle savedInstanceState) {
		this.parseDirectoryData();
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			getFragmentManager().popBackStack();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private void parseDirectoryData() {
		final Bundle extras = this.getArguments();

		final String fullName = extras.getString(DirectoryUserProfile.FULL_NAME);
		final String gender = extras.getString(DirectoryUserProfile.GENDER);

		final TextView nameTextView = (TextView) this.getActivity().findViewById(
				R.id.name);
		final TextView genderTextView = (TextView) this.getActivity().findViewById(
				R.id.gender);

		nameTextView.setText(fullName);
		genderTextView.setText(gender);

		this.initOfficesSection();
		this.initSpecialitiesSection();
	}

	private void initOfficesSection() {
		final Bundle extras = this.getArguments();
		final ViewGroup offices = (ViewGroup) inflater.inflate(R.layout.directory_offices_layout, officeLayout, false);

		final String city = extras.getString(DirectoryUserProfile.CITY);
		final String state = extras.getString(DirectoryUserProfile.STATE);
		final String phone = extras.getString(DirectoryUserProfile.PHONE);
		final String street = extras.getString(DirectoryUserProfile.STREET);
		final String zipCode = extras.getString(DirectoryUserProfile.ZIP_CODE);

		final StringBuffer address = new StringBuffer();
		if (street != null) {
			address.append(street);
			if (city != null || state != null || zipCode != null)
				address.append("\n");
		}
		if (city != null) {
			address.append(city);
			if (state != null || zipCode != null)
				address.append(", ");
		}
		if (state != null) {
			address.append(state);
			if (zipCode != null)
				address.append(", ");
		}
		if (zipCode != null)
			address.append(zipCode);

		((TextView) offices.findViewById(R.id.address))
				.setText(address.toString());
		((TextView) offices.findViewById(R.id.phone)).setText(phone);

		//officeLayout.setEnabled(false);
		officeLayout.addView(offices);
		officeLayout.init();
		officeLayout.setTitle(R.string.offices);
	}

	private void initSpecialitiesSection() {
		final Bundle extras = this.getArguments();
		final ViewGroup specialitiesView = (ViewGroup) inflater.inflate(R.layout.directory_specialities_layout, specialitiesLayout, false);

		final String specialities = extras
				.getString(DirectoryUserProfile.SPECIALITIES);

		((TextView) specialitiesView.findViewById(R.id.speciality))
				.setText(specialities);

		//specialitiesLayout.setEnabled(false);
		specialitiesLayout.addView(specialitiesView);
		specialitiesLayout.init();
		specialitiesLayout.setTitle(R.string.specialities_and_licenses);
	}
}
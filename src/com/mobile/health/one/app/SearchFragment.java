package com.mobile.health.one.app;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import org.apache.commons.lang.StringUtils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragment;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.actionbarsherlock.view.MenuItem.OnMenuItemClickListener;
import com.mobile.health.one.R;
import com.mobile.health.one.search.AutocompleteListenerTextSetter;
import com.mobile.health.one.search.AutocompleteTextSearchTask;
import com.mobile.health.one.search.SearchTextWatcher;
import com.mobile.health.one.search.TextSearchReceiver;
import com.mobile.health.one.search.TextSearchTask;
import com.mobile.health.one.search.TextTask.Listener;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.worker.SearchProcessor;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.SearchParams.AliasParam;
import com.mobile.health.one.util.SearchParams.AliasVariant;
import com.mobile.health.one.util.SearchParams.SearchParam;
import com.mobile.health.one.util.SearchParams.SearchType;
import com.mobile.health.one.util.SearchParams.SearchVariant;

public class SearchFragment extends SherlockFragment {

	public static final String EXTRA_AUTOCOMPLETE = "autocomplete";

	public Button mCancelButton;

	private class AdvancedSearchDialogFragment extends SherlockDialogFragment implements OnClickListener, OnEditorActionListener {

		private class UserInterractionDialog extends Dialog {

			private final WeakReference<Activity> mActivityRef;

			public UserInterractionDialog(final Activity context, final int theme) {
				super(context, theme);
				mActivityRef = new WeakReference<Activity>(context);
			}

			@Override
			public boolean dispatchTouchEvent(final MotionEvent ev) {
				final Activity activity = mActivityRef.get();
				if (activity != null) {
					activity.onUserInteraction();
				}
				return super.dispatchTouchEvent(ev);
			}

			@Override
			public boolean onKeyUp(final int keyCode, final KeyEvent event) {
				final Activity activity = mActivityRef.get();
				if (activity != null) {
					activity.onUserInteraction();
				}
				return super.onKeyUp(keyCode, event);
			}
		}

		private ArrayList<AutoCompleteTextView> mTextViewCache;
		private ArrayList<CheckBox> mCheckBoxCache;
		private ArrayList<DatePicker> mDatePickerCache;
		private ArrayList<Spinner> mSpinnerCache;

		@Override
		public void onCreate(final Bundle savedInstanceState) {
			setStyle(STYLE_NORMAL, R.style.Theme_AdvancedSearch);
			super.onCreate(savedInstanceState);
		}

		@Override
		public void onDetach() {
			if (mCancelButton.getVisibility() == View.VISIBLE)
				mCancelButton.performClick();
			super.onDetach();
		}

		@Override
		public Dialog onCreateDialog(final Bundle savedInstanceState) {

			final UserInterractionDialog dialog = new UserInterractionDialog(getActivity(), R.style.Theme_AdvancedSearch);
			dialog.setOwnerActivity(getActivity());
			//dialog.setContentView(advancedSearchViewContainer);
			dialog.getWindow().setGravity(Gravity.TOP);

			return dialog;
		}

		@Override
		public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
				final Bundle savedInstanceState) {
			final ViewGroup advancedSearchViewContainer = (ViewGroup) inflater.inflate(R.layout.advanced_search, null, false);
			final View advancedSearchView = inflater.inflate(((SearchParam) getSearchParam()).getAdvancedSearchLayout(), advancedSearchViewContainer);

			initOnSearchClickListener(advancedSearchView);
            initOnResetClickListener(advancedSearchView);
			initSearchViewListeners(advancedSearchView);

			return advancedSearchViewContainer;
		}

		public void clearPopupFields() {
        	Log.i(TAG, "::clearPopupFields:" + "");
        	for (final AutoCompleteTextView edit : mTextViewCache) {
                edit.setText(null);
            }
        }

		private void initSearchViewListeners(final View advancedSearchView) {

			if (advancedSearchView instanceof ViewGroup) {
				final ViewGroup g = (ViewGroup) advancedSearchView;
				final int count = g.getChildCount();
				if (mTextViewCache == null) {
					mTextViewCache = new ArrayList<AutoCompleteTextView>(count);
				}
				if (mCheckBoxCache == null) {
					mCheckBoxCache = new ArrayList<CheckBox>(2);
				}
				if (mDatePickerCache == null) {
					mDatePickerCache = new ArrayList<DatePicker>(2);
				}
				if (mDatePickerCache == null) {
					mDatePickerCache = new ArrayList<DatePicker>(2);
				}
				if (mSpinnerCache == null) {
					mSpinnerCache = new ArrayList<Spinner>(1);
				}
				for (int i = 0; i < count; i++) {
					final View child = g.getChildAt(i);
					final Object tag = child.getTag();
					final boolean isTagString = tag instanceof String;
					if (child instanceof AutoCompleteTextView && isTagString) {
						final AutoCompleteTextView edit = (AutoCompleteTextView) child;
						mTextViewCache.add(edit);
						initSearchViewListener(edit);
					} else if (child instanceof CheckBox && isTagString) {
						final CheckBox check = (CheckBox) child;
						mCheckBoxCache.add(check);
					} else if (child instanceof DatePicker && isTagString) {
						final DatePicker date = (DatePicker) child;
						mDatePickerCache.add(date);
					} else if (child instanceof Spinner && isTagString) {
						final Spinner spinner = (Spinner) child;
						mSpinnerCache.add(spinner);
					} else if (child instanceof ViewGroup) {
						initSearchViewListeners(child);
					}
				}
			} else {
				throw new IllegalArgumentException("Parameter viewGroup should be ViewGroup type");
			}
		}

		private void initSearchViewListener(final AutoCompleteTextView view) {
			final AutocompleteTextSearchTask.Creator creator = new AutocompleteTextSearchTask.Creator();
			creator.setType(mType);
			final AutocompleteListenerTextSetter setter = new AutocompleteListenerTextSetter(view.getContext(), view);
			view.addTextChangedListener(new SearchTextWatcher(view, setter, creator));
			view.setOnEditorActionListener(this);
		}

		@Override
		public boolean onEditorAction(final TextView v, final int actionId, final KeyEvent event) {
			boolean handled = false;
			switch (actionId) {
			case EditorInfo.IME_ACTION_SEND:
	            onClick(v);
	            handled = true;
	            break;
			case EditorInfo.IME_ACTION_SEARCH:
				startAdvancedSearch(v);
			}
	        return handled;
		}

		private void initOnSearchClickListener(final View v) {
			final View searchBtn = v.findViewById(R.id.btn_close);
			searchBtn.setOnClickListener(this);
		}

        private void initOnResetClickListener(final View v) {
            final View resetBtn = v.findViewById(R.id.btn_reset);
            resetBtn.setOnClickListener(this);
        }

        private void startAdvancedSearch(final View v) {
        	Log.i(TAG, "::startAdvancedSearch:" + "");
        	final Bundle extras = new Bundle();
            final Listener l = getListener();
            if (l != null) {
                final int size = mTextViewCache.size() + mCheckBoxCache.size() + mDatePickerCache.size();
                final ArrayList<String> formFields  = new ArrayList<String>(size);
                final ArrayList<String> searches  = new ArrayList<String>(size);

                final SearchParam param = (SearchParam) mSearchParam;

                for (final AutoCompleteTextView edit : mTextViewCache) {
                    String formField = (String) edit.getTag();
                    formField = String.format(SearchProcessor.EXTRA_PARAM_FORM_NAME, param.getFormName(), formField);
                    final String search = edit.getText().toString();
                    if (StringUtils.isNotEmpty(formField)) {
                        formFields.add(formField);
                        searches.add(search);
                    }
                }
                for (final CheckBox check : mCheckBoxCache) {
                    String formField = (String) check.getTag();
                    formField = String.format(SearchProcessor.EXTRA_PARAM_FORM_NAME, param.getFormName(), formField);
                    final String search = check.isChecked() ? "1" : "0";
                    if (StringUtils.isNotEmpty(formField)) {
                        formFields.add(formField);
                        searches.add(search);
                    }
                }
                for (final DatePicker date : mDatePickerCache) {
                    String formField = (String) date.getTag();
                    formField = String.format(SearchProcessor.EXTRA_PARAM_FORM_NAME, param.getFormName(), formField);
                    final String search = String.format(Locale.getDefault(),"%d-%02d-%02d", date.getYear(), date.getMonth(), date.getDayOfMonth());
                    if (StringUtils.isNotEmpty(formField)) {
                        formFields.add(formField);
                        searches.add(search);
                    }
                }
                for (final Spinner spinner : mSpinnerCache) {
                    String formField = (String) spinner.getTag();
                    formField = String.format(SearchProcessor.EXTRA_PARAM_FORM_NAME, param.getFormName(), formField);
                    final String search = (String) spinner.getSelectedItem();
                    if (StringUtils.isNotEmpty(formField)) {
                        formFields.add(formField);
                        searches.add(search);
                    }
                }

                extras.putStringArray(SearchProcessor.EXTRA_FORM_FIELDS, formFields.toArray(new String[formFields.size()]));
                extras.putStringArray(SearchProcessor.EXTRA_SEARCH, searches.toArray(new String[formFields.size()]));
                extras.putInt(SearchProcessor.EXTRA_TYPE, mType.ordinal());

                SyncService.startSyncService(
                        v.getContext(),
                        AppSyncService.SEARCH,
                        new TextSearchReceiver(l),
                        extras);

                dismiss(true);
                mSearchView.setFocusable(false);
                mCancelButton.setVisibility(View.VISIBLE);
            }
        }

		@Override
		public void onClick(final View v) {

			switch (v.getId()) {
			case R.id.btn_close:
				clearPopupFields();
            	dismiss();
            	break;
			case R.id.btn_reset:
				clearPopupFields();
				break;
			}
		}

		public void dismiss(final boolean addToStack) {
			if (addToStack) {
				final FragmentTransaction ft = getFragmentManager().beginTransaction();
	            ft.remove(this);
	            ft.addToBackStack(null);
	            ft.commit();
			} else {
				super.dismiss();
			}
        }
	}

	public class AdvancedSearchMenuClick implements OnMenuItemClickListener {
		private AdvancedSearchDialogFragment mAdvancedSearchDialog = null;
		private LayoutInflater viewInflater;

		AdvancedSearchMenuClick() {
		}

		@Override
		public boolean onMenuItemClick(final MenuItem item) {
			if (viewInflater == null) {
				viewInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			}
			if (mAdvancedSearchDialog == null) {
				mAdvancedSearchDialog = new AdvancedSearchDialogFragment();
			}

			if (!mAdvancedSearchDialog.isVisible()) {
				final String tag = AdvancedSearchDialogFragment.class.getSimpleName();
				final FragmentTransaction ft = getFragmentManager().beginTransaction();
				ft.addToBackStack(tag);
				mAdvancedSearchDialog.show(ft, tag);
			} else {
				mAdvancedSearchDialog.dismiss();
			}

			mCancelButton.setText(R.string.btn_close);
			return true;
		}

		public void clearPopupFields() {
			if (mAdvancedSearchDialog != null)
				mAdvancedSearchDialog.clearPopupFields();
		}

	}

	private class ProxyListener implements Listener {

		private final WeakReference<Listener> mListener;

		public ProxyListener(final Listener l) {
			mListener = new WeakReference<Listener>(l);
		}

		@Override
		public void onSearchStart() {
			final Listener l = mListener.get();
			if (l != null)
				l.onSearchStart();
			setProgressVisible(true);
		}

		@Override
		public void onSearchRestart() {
			final Listener l = mListener.get();
			if (l != null)
				l.onSearchRestart();
			setProgressVisible(false);
		}

		@Override
		public void onSearchError(final String error) {
			final Listener l = mListener.get();
			if (l != null)
				l.onSearchError(error);
			setProgressVisible(false);
		}

		@Override
		public void onSearchComplete(final IResponce responce) {
			final Listener l = mListener.get();
			if (l != null)
				l.onSearchComplete(responce);
			setProgressVisible(false);
		}

		@Override
		public void onSearchCancel() {
			final Listener l = mListener.get();
			if (l != null)
				l.onSearchCancel();
			setProgressVisible(false);
		}

	}

	public static final String TAG = SearchFragment.class.getSimpleName();

	private ProxyListener mListener;
	private SearchParams.SearchType mType;
	private SearchParams.Param mSearchParam;

	private AutoCompleteTextView mSearchView;

	private SearchTextWatcher mTextWatcher;

	private PopupWindow mSearchVariantsPopup;

	private CharSequence mSearchText;

	private ProgressBar mSearchProgress;

	private Menu mMenu;

	private AdvancedSearchMenuClick mAdvancedSeachMenuClick;

	private Bundle mAdditionalParams;

	public static SearchFragment newInstance(final Listener listener, final SearchParams.SearchType type) {
		final SearchFragment f = new SearchFragment();
		f.setListener(listener);
		f.setSearchType(type);

		return f;
	}

	public static SearchFragment newInstance(final Listener listener, final SearchParams.SearchType type, final Bundle additionalParams) {
		final SearchFragment f = new SearchFragment();
		f.setListener(listener);
		f.setSearchType(type);
		f.setAdditionalParams(additionalParams);

		return f;
	}

	private void setAdditionalParams(final Bundle additionalParams) {
		mAdditionalParams = additionalParams;
	}

	public static SearchFragment newInstance(final Listener listener, final SearchParams.SearchType type, final CharSequence searchText) {
		final SearchFragment f = new SearchFragment();
		f.setListener(listener);
		f.setSearchType(type);
		f.setSearchText(searchText);

		return f;
	}

	public Listener getListener() {
		return mListener;
	}

	public void setListener(final Listener listener) {
		mListener = new ProxyListener(listener);
	}

	public void setSearchType(final SearchType type) {
		mType = type;
		setSearchParam(SearchParams.getParams(type));
		initTextChangedListener(mSearchView);
	}

	public SearchType getSearchType() {
		return mType;
	}

//	public void showAdvancedSearchDialog() {
//		final Dialog d = mAdvancedSeachMenuClick.mAdvancedSearchDialog;
//		if (d != null)
//			d.show();
//	}

	@Override
	public void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setSearchParam(SearchParams.getParams(getSearchType()));
		setHasOptionsMenu(true);
	}

	@Override
	public void onDestroy() {
		clearPopup();
		super.onDestroy();
	}

	private void setProgressVisible(final boolean isVisible) {
		if (mSearchProgress != null)
			mSearchProgress.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
	}

	private View prepareSearchView(final MenuItem advancedSearch) {
		final Context c = getSherlockActivity().getSupportActionBar().getThemedContext();
		final LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		final View layout = inflater.inflate(R.layout.search_view, null);

		mSearchProgress = (ProgressBar) layout.findViewById(R.id.search_progress);
		mSearchView = (AutoCompleteTextView) layout.findViewById(R.id.search_query);
		mCancelButton = (Button) layout.findViewById(R.id.btn_cancel);
		mCancelButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				//layout.requestFocus();
				mSearchView.clearFocus();
				mSearchView.setFocusable(false);
				//mCancelButton.requestFocus(View.FOCUS_UP);
				mSearchView.setText(null);
				mCancelButton.setText(R.string.btn_cancel);
				hideInputMethod();
				mCancelButton.setVisibility(View.GONE);
				mAdvancedSeachMenuClick.clearPopupFields();
				getListener().onSearchCancel();

			}
		});
		final ImageButton more = (ImageButton) layout.findViewById(R.id.btn_moreflow);
		getActivity().registerForContextMenu(more);
		more.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(final View v) {
				//getActivity().openOptionsMenu();
				mMenu.performIdentifierAction(R.id.create_menu, 0);
			}
		});
		//more.setVisibility(View.GONE);
		if(getSearchParam() != null)
			mSearchView.setHint(getSearchParam().getHintTextResource());

		if (mSearchView != null) {
			final ActionBar ab = getSherlockActivity().getSupportActionBar();
			initTextChangedListener(mSearchView);
			mSearchView.setOnFocusChangeListener(new OnFocusChangeListener() {

				private boolean mVariantPopupVisible;

				@Override
				public void onFocusChange(final View v, final boolean hasFocus) {
					mCancelButton.setVisibility(hasFocus ? View.VISIBLE : View.GONE);
					if(hasFocus)
						showInputMethod(mSearchView.findFocus());

					if (getSearchParam() instanceof SearchParam) {
						if(getActivity() instanceof MessageListActivity || getActivity() instanceof MessageDetailActivity)
							advancedSearch.setVisible(false);
						else
							advancedSearch.setVisible(hasFocus);

						final SearchVariant[] variants = ((SearchParam) getSearchParam()).getVariants();
						/*if (!hasFocus)
							getListener().onSearchCancel();*/

						if (variants != null) {
							final PopupWindow popup = getVariantsPopup(c, variants);
							Log.i(TAG, "::onFocusChange:" + "focus " + hasFocus);
							//else
							// TODO if (!TextUtils.isEmpty(mSearchView.getText()))
							final Runnable showPopup = new Runnable() {

								private boolean mHasFocus;

								{
									mHasFocus = hasFocus;
								}

								@Override
								public void run() {
									Log.i(TAG, "::onFocusChange:" + "POPUP! focus " + mHasFocus);
									if (mHasFocus)
										popup.showAsDropDown(mSearchView, -(mSearchView.getWidth() / 3), 8);
								}
							};
							mSearchView.removeCallbacks(showPopup);
							if (!hasFocus) {
								popup.dismiss();
								mVariantPopupVisible = false;
							} else if (!mVariantPopupVisible) {
								Log.i(TAG, "::onFocusChange:" + "post");
								mSearchView.post(showPopup);
								mVariantPopupVisible = true;
							}
						}
					} else {
						final AliasVariant[] variants = ((AliasParam) getSearchParam()).getAliasVariants();
						if (!hasFocus)
							getListener().onSearchCancel();

						if (variants != null) {
							final PopupWindow popup = getAliasVariantsPopup(c, variants);
							if (!hasFocus) {
								popup.dismiss();
							} else {
								final int padding = mSearchView.getWidth() / 6;
								popup.setWidth(mSearchView.getWidth() + padding);
								popup.showAsDropDown(mSearchView, -padding, 0);
							}
						}
					}
					ab.setDisplayShowHomeEnabled(!hasFocus);
					Log.i(TAG, "::onFocusChange:" + hasFocus + ", " + getSherlockActivity().getClass().getSimpleName());
				}
			});
			mSearchView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(final View v) {
					mSearchView.setFocusable(true);
					mSearchView.setFocusableInTouchMode(true);
					mSearchView.requestFocus();
				}
			});
			//mSearchView.setClickable(true);
			if (TextUtils.isEmpty(mSearchText)) {
				mSearchView.setFocusable(false);
			} else {
				mSearchView.setText(mSearchText);

				mSearchView.performClick();
				mSearchView.post(new Runnable() {

					@Override
					public void run() {
						ab.setDisplayShowHomeEnabled(false);
						mCancelButton.setVisibility(View.VISIBLE);
					}
				});
			}
		}

		return layout;
	}

	private static boolean isEqualFormFields(final String[] formFields1, final String[] variantFormFields1) {
		return Arrays.equals(formFields1, variantFormFields1);
	}

	private PopupWindow getVariantsPopup(final Context c, final SearchVariant[] variants) {
		if (mSearchVariantsPopup == null) {
			final SearchParam param = (SearchParam) getSearchParam();
			final LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final RadioGroup popup = (RadioGroup) inflater.inflate(
					R.layout.popup_search_variants, null, false);

			final OnClickListener onClick = new OnClickListener() {

				@Override
				public void onClick(final View v) {
					final SearchVariant searchVariant = (SearchVariant) v.getTag();
					if (!isEqualFormFields(param.getDefaultFormFields(), searchVariant.getFormFields())) {
						param.setDefaultFormFields(searchVariant.getFormFields());
						// launch search
						mTextWatcher.afterTextChanged(mSearchView.getText());
					}
				}

			};
			for (final SearchVariant searchVariant : variants) {
				final RadioButton button = (RadioButton) inflater.inflate(R.layout.popup_search_variants_item, popup, false);
				button.setText(searchVariant.getName(c));
				button.setTag(searchVariant);
				button.setOnClickListener(onClick);
				popup.addView(button);
				// that works fast because of very small arrays
				button.setChecked(Arrays.equals(searchVariant.getFormFields(), param.getDefaultFormFields()));
			}
			final PopupWindow w = new PopupWindow(popup);
			w.setWidth(LayoutParams.WRAP_CONTENT);
			w.setHeight(LayoutParams.WRAP_CONTENT);
			w.setOutsideTouchable(false);

			mSearchVariantsPopup = w;
		}

		return mSearchVariantsPopup;
	}

	private PopupWindow getAliasVariantsPopup(final Context c, final AliasVariant[] variants) {
		if (mSearchVariantsPopup == null) {
			final LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			final LinearLayout popup = (LinearLayout) inflater.inflate(
					R.layout.popup_search_alias, null, false);

			final OnClickListener onClick = new OnClickListener() {

				@Override
				public void onClick(final View v) {
					final AliasVariant searchVariant = (AliasVariant) v.getTag();
					searchVariant.setSearchParam(mSearchView.getText());
					c.startActivity(searchVariant.getIntent(c));
					mSearchVariantsPopup.dismiss();
					mCancelButton.performClick();
				}

			};
			for (final AliasVariant searchVariant : variants) {
				final TextView button = (TextView) inflater.inflate(R.layout.popup_search_alias_item, popup, false);
				button.setText(searchVariant.getSearchName(c));
				button.setTag(searchVariant);
				button.setOnClickListener(onClick);
				popup.addView(button);
			}
			final PopupWindow w = new PopupWindow(popup);
			w.setWidth(getResources().getDimensionPixelSize(R.dimen.search_alias_width));
			w.setHeight(LayoutParams.WRAP_CONTENT);
			w.setOutsideTouchable(false);

			mSearchVariantsPopup = w;
		}

		return mSearchVariantsPopup;
	}

	private void clearPopup() {
		if (mSearchVariantsPopup != null) {
			mSearchVariantsPopup.dismiss();
			mSearchVariantsPopup = null;
		}
	}

	private void initTextChangedListener(final AutoCompleteTextView search) {
		// only if search view already prepared
		if (search != null) {
			if (mTextWatcher != null)
				search.removeTextChangedListener(mTextWatcher);
			final TextSearchTask.Creator creator = new TextSearchTask.Creator();
			creator.setType(mType);
			creator.setAdditionalParams(mAdditionalParams);
			mTextWatcher = new SearchTextWatcher(search, getListener(), creator);
			search.addTextChangedListener(mTextWatcher);
		}
	}

	private void hideInputMethod() {
		if (getActivity() != null) {
			final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
			inputMethodManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
		}
	}

	private void showInputMethod(final View view) {
        final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null) {
        	inputMethodManager.showSoftInput(view, 0);
        }
    }

	@Override
	public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
		// Place an action bar item for searching.
		inflater.inflate(R.menu.message_create, menu);

		//final MenuItem createMenuItem = menu.findItem(R.id.create_menu);
		mMenu = menu;
		menu.findItem(R.id.compose).setIntent(new Intent(getActivity(), MessageCreateActivity.class));
		menu.findItem(R.id.send_invite).setIntent(new Intent(getActivity(), SendInvitationActivity.class));

		final MenuItem item = menu.add(Menu.CATEGORY_SYSTEM, Menu.NONE, 3,
				R.string.search_people_hint);
		item.setIcon(R.drawable.ic_action_search);
		item.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

		final ActionBar ab = getSherlockActivity().getSupportActionBar();
		ab.setDisplayShowTitleEnabled(false);

		final MenuItem advancedSearch = menu.add(Menu.CATEGORY_SYSTEM, Menu.NONE, 2, R.string.menu_advanced_search);
		advancedSearch.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		advancedSearch.setIcon(R.drawable.ic_advanced_search);
		advancedSearch.setVisible(false);
		mAdvancedSeachMenuClick = new AdvancedSearchMenuClick();
		advancedSearch.setOnMenuItemClickListener(mAdvancedSeachMenuClick);

		item.setActionView(prepareSearchView(advancedSearch));

		//ab.setDisplayShowCustomEnabled(true);
		//ab.setCustomView(prepareSearchView(advancedSearch));
	}

	public CharSequence getSearchText() {
		return mSearchText;
	}

	public void setSearchText(final CharSequence mSearchText) {
		this.mSearchText = mSearchText;
	}

	public SearchParams.Param getSearchParam() {
		if (mSearchParam == null) {
			mSearchParam = SearchParams.getParams(getSearchType());
		}
		return mSearchParam;
	}

	public void setSearchParam(final SearchParams.Param mSearchParam) {
		this.mSearchParam = mSearchParam;
	}

}

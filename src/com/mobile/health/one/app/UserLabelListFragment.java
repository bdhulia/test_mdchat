package com.mobile.health.one.app;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.mobile.health.one.R;
import com.mobile.health.one.app.RegisterPinFragment.RegisterPinReceiver;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.UserLabelsResponce;
import com.mobile.health.one.service.entity.UserLabelsResponce.Label;
import com.mobile.health.one.service.worker.GetUserLabelsProcessor;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class UserLabelListFragment extends SherlockDialogFragment implements OnItemClickListener {

	public interface UserLabelClickListener {
		void onUserLabeClick(final String id, String name);
		void onLabelTitle(final String title);
	}

	public static class LabelViewBinder implements ViewBinder {

		@Override
		public boolean setViewValue(final View view, final Object data,
				final String textRepresentation) {
			switch (view.getId()) {
			case android.R.id.text1:
				final TextView textView = (TextView) view;
				final Resources res = view.getContext().getResources();
				final Drawable left = res.getDrawable(R.drawable.ic_folder);
				textView.setCompoundDrawablePadding(res
						.getDimensionPixelSize(R.dimen.standart_padding));
				textView.setCompoundDrawablesWithIntrinsicBounds(left, null, null,
						null);

				return false;
			case android.R.id.text2:
				final Integer count = (Integer) data;
				if (count == -1)
					view.setVisibility(View.INVISIBLE);
				return false;
			default:
				return false;
			}
		}

	}

	protected static class UserLabelListReceiver implements Receiver {
		private final String TAG = RegisterPinReceiver.class.getCanonicalName();
		private final WeakReference<UserLabelListFragment> mFragment;

		UserLabelListReceiver(final UserLabelListFragment fragment) {
			mFragment = new WeakReference<UserLabelListFragment>(fragment);
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final UserLabelListFragment f = mFragment.get();
			if (f == null || !f.isVisible()) return; // do nothing, if fragment already destroyed
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start group list");

				// Start out with a progress indicator.
				if (f != null && f.isVisible()) {
					f.setListShown(false);
				}
				break;
			case SyncService.STATUS_FINISHED:
				final UserLabelsResponce responce = resultData.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				if (f != null && f.isVisible()) {
					if (responce != null) {
						final ArrayList<HashMap<String, Object>> data = responce.saveToArray(f.getShowsDialog());
						f.initAdapterAndSetData(data);

						if (f.getActivity() instanceof UserLabelClickListener) {
							final UserLabelClickListener listener = (UserLabelClickListener) f.getActivity();
							final Label label = responce.getRootLabel();
							if (label != null) //if root label found
								listener.onLabelTitle(label.is_root ? label.label_name : prepareLabelCount(label.label_name, label.count));
						}

					}
					f.setListShown(true);
				}

				Log.i(TAG, "Saving group start");
				break;
			case SyncService.STATUS_ERROR:
				f.setListShown(true);
				Log.e(TAG, "Error getting user labels list");
				if (resultData != null) {
					final String error = resultData
							.getString(RequestProcessor.EXTRA_ERROR_STRING);
					ToastUtil.showText(f.getActivity(), error);
				}

				break;
			}
		}

	}

	public static final String TAG = UserLabelListFragment.class
			.getSimpleName();

	private SimpleAdapter mAdapter;

	private ListView mList;
	private TextView mEmptyView;
	private ProgressBar mProgressContainer;
	private FrameLayout mListContainer;
    boolean mListShown;

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		loadData();
	}


	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container,
			final Bundle savedInstanceState) {
		mList = new ListView(getActivity());
		mList.setOnItemClickListener(this);
		mEmptyView = new TextView(getActivity());
		mListContainer = new FrameLayout(getActivity());
		mProgressContainer = new ProgressBar(getActivity());

		mList.setVisibility(View.INVISIBLE);
		mProgressContainer.setVisibility(View.GONE);
		mEmptyView.setVisibility(View.INVISIBLE);

		final Resources r = getResources();
		final int minHeight = r.getDimensionPixelSize(R.dimen.dialog_min_height);
		mEmptyView.setText(R.string.error_no_labels);
		final int padding = r.getDimensionPixelSize(R.dimen.double_padding);
		mEmptyView.setPadding(padding, padding, padding, padding);
		mEmptyView.setMinHeight(minHeight);
		mEmptyView.setGravity(Gravity.CENTER);
		mList.setMinimumHeight(minHeight);
		final LayoutParams pFull = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		final FrameLayout.LayoutParams pCenter = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.CENTER);
		mListContainer.addView(mList, pFull);
		mListContainer.addView(mProgressContainer, pCenter);
		mListContainer.addView(mEmptyView, pCenter);

		return mListContainer;
	}

	public void setListShown(final boolean b) {
		mList.setVisibility(b ? View.VISIBLE : View.INVISIBLE);
		mProgressContainer.setVisibility(!b ? View.VISIBLE : View.GONE);
	}

	protected void loadData() {
		Bundle args = getArguments();
		if (args == null) {
			args = new Bundle(1);
		}
		if (!getShowsDialog())
			args.putString(GetUserLabelsProcessor.EXTRA_ADD_GROUP_LABELS, "1");
		SyncService.startSyncService(getActivity(),
				AppSyncService.GET_USER_LABELS,
				new UserLabelListReceiver(this), args);
	}

	public void initAdapterAndSetData(final ArrayList<HashMap<String, Object>> data) {
		final int layout = R.layout.item_label;
		if (data != null && !data.isEmpty()) {
			mEmptyView.setVisibility(View.INVISIBLE);
			mAdapter = new SimpleAdapter(getActivity(), data, layout, new String[] { Label.Entity.NAME, Label.Entity.COUNT }, new int[] { android.R.id.text1, android.R.id.text2 });
			mAdapter.setViewBinder(new LabelViewBinder());
			setListAdapter(mAdapter);
		} else {
			mEmptyView.setVisibility(View.VISIBLE);
			setListShown(false);
		}

	}

	private void setListAdapter(final SimpleAdapter adapter) {
		final boolean hadAdapter = mAdapter != null;
        mAdapter = adapter;
        if (mList != null) {
            mList.setAdapter(adapter);
            if (!mListShown && !hadAdapter) {
                // The list was hidden, and previously didn't have an
                // adapter.  It is now time to show it.
                setListShown(true);
            }
        }

	}

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final Dialog d = new Dialog(getActivity(), R.style.Theme_EnterPinDialog);
		//d.setContentView(getView());
		return d;
	}

	private static String prepareLabelCount(final String name, final int count) {
		return name + " (" + count + ")";
	}

	@Override
	public void onItemClick(final AdapterView<?> parent, final View view, final int position,
			final long id) {
		final Activity a = getActivity();
		// just think that our parent activity implements it

		if (a instanceof UserLabelClickListener) {
			@SuppressWarnings("unchecked")
			final HashMap<String, Object> c = (HashMap<String, Object>) mAdapter.getItem(position);
			final String labelId = (String) c.get(Label.Entity.LABEL_ID);
			final String name = (String) c.get(Label.Entity.NAME);
			final int count = (Integer) c.get(Label.Entity.COUNT);
			final boolean isRoot = (Boolean) c.get(Label.Entity.IS_ROOT);
			final UserLabelClickListener listener = (UserLabelClickListener) a;
			listener.onUserLabeClick(labelId, name);
			listener.onLabelTitle(isRoot ? name : prepareLabelCount(name, count));
			if (getShowsDialog()) {
				dismiss();
			}
		}

	}

}

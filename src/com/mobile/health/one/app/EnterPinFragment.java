package com.mobile.health.one.app;

import java.lang.ref.WeakReference;
import java.util.Date;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.service.worker.RegisterPinProcessor;
import com.mobile.health.one.util.DateFormatUtil;
import com.mobile.health.one.util.DeviceIdUtils;
import com.mobile.health.one.util.SettingsManager;
import com.mobile.health.one.util.ToastUtil;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class EnterPinFragment extends SherlockDialogFragment implements TextWatcher {

	public static interface EnterPinListener {
		public void onPinEnter();
	}

	public static final String TAG = EnterPinFragment.class.getSimpleName();

	public static final String EXTRA_NOT_BLOCKED = "not_blocked";

	private int PIN_COUNT;
	private EditText passw;

	protected static class EnterPinReceiver implements Receiver {
		public final String TAG = EnterPinReceiver.class.getCanonicalName();
		private final WeakReference<EnterPinFragment> mFragmentRef;
		private final Context mContext;
		private final EnterPinListener callback;
		private SettingsManager mSettingsManager;

		public EnterPinReceiver(final EnterPinFragment fragment) {
			mFragmentRef = new WeakReference<EnterPinFragment>(fragment);
			final FragmentActivity a = fragment.getActivity();
			if (a != null && a instanceof EnterPinListener) {
				callback = (EnterPinListener) fragment.getActivity();
				mContext = a.getApplicationContext();
				mSettingsManager = new SettingsManager(a);
			} else {
				callback = null;
				mContext = null;
			}
		}

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			final EnterPinFragment f = mFragmentRef.get();
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start pin registering");

				setProgress(true);
				break;
			case SyncService.STATUS_FINISHED:
				Log.i(TAG, "Pin sended");
				if (resultData != null) {
					final Responce resp = resultData
							.getParcelable(RequestProcessor.EXTRA_RESPONSE);
					if (resp != null && resp.getData().get("success").equals("true")) {
						launchHome();
						if (mSettingsManager != null)
							mSettingsManager.setPinRegistered(true);
					}
				}
				setProgress(false);

				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error sending pin");
				if (resultData != null && f != null) {
					final Responce resp = resultData
							.getParcelable(RequestProcessor.EXTRA_RESPONSE);
					if (resp != null) {
						boolean controlWithErrorFound = false;
						if (resp.getInvalidParams() != null) {
							for (final String tag : resp.getInvalidParams()) {
								final TextView view = (TextView) f.getView()
										.findViewWithTag(tag);
								if (view != null) {
									view.setError(resp.getMessages());
									view.requestFocus();
									controlWithErrorFound = true;
								}
							}
						}
						// if error is not on this page, show error or do smth
						if (!controlWithErrorFound) {
							final Activity a = f.getActivity();
							if (f.getShowsDialog()) {
								f.dismiss();
								if (a != null) {
									NavUtils.navigateUpTo(a, new Intent(a, LoginActivity.class));
								}
							} else {
								if (a != null) {
									ToastUtil.showNoNetwork(a);
								}
							}
						}
					} else {
						ToastUtil.showError(mContext, resultData.getString(RequestProcessor.EXTRA_ERROR_STRING));
					}
				} else if (f != null && mContext != null) {
					NavUtils.navigateUpTo(f.getSherlockActivity(), new Intent(mContext, LoginActivity.class));
				} else {
					final Activity a = f.getActivity();
					ToastUtil.showNoNetwork(a);
				}

				setProgress(false);
				break;
			}
		}

		private void launchHome() {
			final EnterPinFragment f = mFragmentRef.get();
			if (f != null) {
				if (f.getShowsDialog()) {
					f.dismiss();
				} else if (callback != null) {
					callback.onPinEnter();
				}
			}
		}

		private void setProgress(final boolean show) {
			final EnterPinFragment f = mFragmentRef.get();
			if (f != null)
				f.setProgress(show);
		}

	}

	public static EnterPinFragment newInstance(final boolean isInactivityBlocker) {
		final Bundle b = new Bundle(1);
		b.putBoolean(EXTRA_NOT_BLOCKED, !isInactivityBlocker);
		final EnterPinFragment pin = new EnterPinFragment();
		pin.setArguments(b);

		return pin;
	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		PIN_COUNT = getResources().getInteger(R.integer.max_pin_chars);
		final ViewGroup view = (ViewGroup) inflater.inflate(
				R.layout.fragment_enter_pin, container, false);
		passw = (EditText) view.findViewById(R.id.pincode);
		passw.addTextChangedListener(this);
		passw.setTag(RegisterPinProcessor.EXTRA_PIN);

		final boolean nonTimeoutBlocker = getArguments() != null && getArguments().getBoolean(EXTRA_NOT_BLOCKED, false);
        TextView lastLoginView = (TextView) view.findViewById(R.id.last_login);
        SettingsManager manager = new SettingsManager(getActivity());
        String lastLoginEmail = manager.getLastLoginEmail();
        if (lastLoginEmail != null) {
            long lastLoginTime = manager.getLastLoginTime();
            String lastLogin = getString(R.string.last_login_label, DateFormatUtil.getFormattedDate(new Date(lastLoginTime)), lastLoginEmail, Build.MODEL, DeviceIdUtils.getDeviceId(getActivity()));
            lastLoginView.setText(lastLogin);
        } else {
            lastLoginView.setVisibility(View.GONE);
        }

		if (getShowsDialog() && !nonTimeoutBlocker) {
			view.findViewById(R.id.pin_blocked).setVisibility(View.VISIBLE);
		}

		return view;
	}

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final Dialog d = super.onCreateDialog(savedInstanceState);
		d.setTitle(R.string.pin_enter);
		d.setCancelable(false);
		d.setCanceledOnTouchOutside(false);
		setCancelable(false);
		return d;
	}

	@Override
	public void onResume() {
		super.onResume();
		passw.setText(null);
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start,
			final int count, final int after) { /* nop */
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start,
			final int before, final int count) {
		Log.d(getTag(), s.toString());
		Log.d(getTag(), "Char count " + count);

		final int length = s.length();
		if (length >= PIN_COUNT) {
			Log.v(getTag(), "Pin sync service started");
			final Bundle extras = new Bundle();
			extras.putString(RegisterPinProcessor.EXTRA_PIN, s.toString());
			SyncService.startSyncService(getActivity(),
					AppSyncService.UPDATE_SESSION_REQUEST, new EnterPinReceiver(
							this), extras);
		}
	}

	@Override
	public void afterTextChanged(final Editable s) { /* nop */
	}

	public void setProgress(final boolean b) {
		final SherlockFragmentActivity a = getSherlockActivity();
		if (a != null)
			a.setSupportProgressBarIndeterminateVisibility(b);
	}
}

/**
 *
 */
package com.mobile.health.one.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.mobile.health.one.R;

/**
 * @author rimmer
 *
 */
public class DashboardFragment extends SherlockFragment implements
		OnClickListener {

	private TextView mMessageCountView;
	private TextView mInvitesCountView;
	private PopupWindow mInviteTypePopup;

	private LinearLayout popupLayout;

	private int invitesCount = 0;

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		final View view = inflater.inflate(R.layout.fragment_dashboard,
				container, false);

		view.findViewById(R.id.dash_messages).setOnClickListener(this);
		view.findViewById(R.id.dash_people).setOnClickListener(this);
		view.findViewById(R.id.dash_groups).setOnClickListener(this);
		view.findViewById(R.id.dash_invitation_send).setOnClickListener(this);
		view.findViewById(R.id.dash_invitations).setOnClickListener(this);
		view.findViewById(R.id.dash_message_compose).setOnClickListener(this);
		view.findViewById(R.id.dash_messages).setOnClickListener(this);
		view.findViewById(R.id.dash_profile).setOnClickListener(this);
		view.findViewById(R.id.dash_recently_added).setOnClickListener(this);
		view.findViewById(R.id.dash_settings).setOnClickListener(this);
		mMessageCountView = (TextView) view
				.findViewById(R.id.dash_messages_badge);
		mInvitesCountView = (TextView) view
				.findViewById(R.id.dash_invites_badge);

		mInviteTypePopup = prepareWindow(inflater);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		mInviteTypePopup.dismiss();

		invitesCount = 0;
	}

	@Override
	public void onClick(final View v) {
		switch (v.getId()) {
		case R.id.dash_messages:
			Intent i = new Intent(getActivity(), MessageListActivity.class);
			startActivity(i);
			break;
		case R.id.dash_people:
			i = new Intent(getActivity(), PeopleListActivity.class);
			startActivity(i);
			break;
		case R.id.dash_message_compose:
			i = new Intent(getActivity(), MessageCreateActivity.class);
			startActivity(i);
			break;
		case R.id.dash_invitations:
			mInviteTypePopup.showAsDropDown(v, -60, -v.getHeight());
			break;
		case R.id.dash_invitation_send:
			i = new Intent(getActivity(), SendInvitationActivity.class);
			startActivity(i);
			break;
		case R.id.dash_groups:
			i = new Intent(getActivity(), GroupListActivity.class);
			startActivity(i);
			break;
		case R.id.dash_recently_added:
			i = new Intent(getActivity(), RecentListActivity.class);
			startActivity(i);
			break;
		case R.id.dash_settings:
			i = new Intent(getActivity(), SettingsActivity.class);
			startActivity(i);
			break;
		case R.id.dash_profile:
			UserProfileActivity.startActivity(getActivity(), true);
			break;
		case R.id.people_invites:
			i = new Intent(getActivity(), InviteListActivity.class);
			startActivity(i);
			break;
		case R.id.group_invites:
			i = new Intent(getActivity(), GroupInviteListActivity.class);
			startActivity(i);
			break;
		default:
			break;
		}

	}

	public void setMessageCount(final Integer count) {
		if (count > 0) {
			if (mMessageCountView.getVisibility() != View.VISIBLE) {
				mMessageCountView.setVisibility(View.VISIBLE);
			}
			mMessageCountView.startAnimation(AnimationUtils.loadAnimation(
					getActivity(), R.anim.bubble));
			mMessageCountView.setText(count.toString());
		} else {
			mMessageCountView.setVisibility(View.GONE);
		}
	}

	public void resetInvitesCount() {
		invitesCount = 0;

	}

	public void updateInvitesCount() {
		if (invitesCount > 0) {
			if (mInvitesCountView.getVisibility() != View.VISIBLE) {
				mInvitesCountView.setVisibility(View.VISIBLE);
			}
			mInvitesCountView.startAnimation(AnimationUtils.loadAnimation(
					getActivity(), R.anim.bubble));
			mInvitesCountView.setText(String.valueOf(invitesCount));
		} else {
			mInvitesCountView.setVisibility(View.GONE);
		}
	}

	public void setContactInvitesCount(final Integer count) {
		if (getActivity() != null) {
			final TextView peopleInvitesTextView = (TextView) popupLayout.findViewById(R.id.people_invites);

			final String peopleInvitesText = this.getString(R.string.people_invites);
			peopleInvitesTextView.setText(peopleInvitesText + " (" + count.intValue() + ")");

			invitesCount += count.intValue();

			//this.updateInvitesCount();
		}
	}

	public void setGroupInvitesCount(final Integer count) {
		if (isVisible()) {
			final TextView groupInvitesTextView = (TextView) popupLayout.findViewById(R.id.group_invites);

			final String groupInvitesText = this.getString(R.string.group_invites);
			groupInvitesTextView.setText(groupInvitesText + " (" + count.intValue() + ")");

			invitesCount += count.intValue();
		}

		//this.updateInvitesCount();
	}

	public void incMessageCount() {
		final Integer count = Integer.parseInt(mMessageCountView.getText()
				.toString());
		if (count != null) {
			setMessageCount(count + 1);
		}
	}

	private PopupWindow prepareWindow(final LayoutInflater inflater) {
		popupLayout = (LinearLayout) inflater.inflate(
				R.layout.popup_invites_type, null, false);
		popupLayout.findViewById(R.id.people_invites).setOnClickListener(this);
		popupLayout.findViewById(R.id.group_invites).setOnClickListener(this);

		final PopupWindow w = new PopupWindow(popupLayout);
		w.setTouchInterceptor(new OnTouchListener() {
			@Override
			public boolean onTouch(final View v, final MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
					w.dismiss();

					return true;
				}

				return false;
			}
		});

		// workaround for outside touch events to work
		w.setBackgroundDrawable(getResources().getDrawable(
				R.drawable.popup_invites_type_bg));
		w.setWidth(LayoutParams.WRAP_CONTENT);
		w.setHeight(LayoutParams.WRAP_CONTENT);
		w.setOutsideTouchable(true);

		return w;
	}
}
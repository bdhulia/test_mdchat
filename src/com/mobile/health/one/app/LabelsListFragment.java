package com.mobile.health.one.app;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.ArrayUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.provider.BaseColumns;
import android.util.Log;
import android.util.Pair;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleAdapter.ViewBinder;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.mobile.health.one.MHOApplication;
import com.mobile.health.one.R;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.db.entity.Label;
import com.mobile.health.one.db.entity.Message;
import com.mobile.health.one.db.entity.Label.Entity;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.service.NodeWorkerService.ILabelsReceiver;

public class LabelsListFragment extends SherlockDialogFragment implements
		OnItemClickListener, OnClickListener, ILabelsReceiver, NodeServiceConnector {

	public static final String TAG = LabelsListFragment.LabelsListCallback.class
			.getSimpleName();

	public interface LabelsListCallback {
		void onItemClick(String remoteId, String labelName);

		void onItemsSelected(final HashMap<String, String> labels);

		void onRootLabel(Label.Entity label);
	}

	public static class LabelViewBinder implements ViewBinder {

		@Override
		public boolean setViewValue(final View view, final Object data,
				final String textRepresentation) {
			switch (view.getId()) {
			case android.R.id.text1:
				final TextView textView = (TextView) view;
				final Resources res = view.getContext().getResources();
				final Drawable left = res.getDrawable(R.drawable.ic_folder);
				textView.setCompoundDrawablePadding(res
						.getDimensionPixelSize(R.dimen.standart_padding));
				textView.setCompoundDrawablesWithIntrinsicBounds(left, null,
						null, null);

				return false;
			case android.R.id.text2:
				/*isRoot = (Boolean) data;

				if (isRoot)
					view.setVisibility(View.GONE);*/
				return false;
			default:
				return false;
			}
		}

	}

	private class MySimpleCursorAdapter extends SimpleAdapter {

		public MySimpleCursorAdapter(final Context context,
				final List<? extends Map<String, ?>> data, final int resource,
				final String[] from, final int[] to) {
			super(context, data, resource, from, to);
		}

		@Override
		public boolean isEnabled(final int position) {
			@SuppressWarnings("unchecked")
			final HashMap<String,Object> label = (HashMap<String, Object>) getItem(position);
			final Boolean isRoot = (Boolean) label.get(Label.IS_ROOT);
			if (getShowsDialog())
				return !isRoot;
			else
				return true;
		}

		@Override
		public long getItemId(final int position) {
			@SuppressWarnings("unchecked")
			final HashMap<String,Object> label = (HashMap<String, Object>) getItem(position);
			final Long id = (Long) label.get(BaseColumns._ID);
			return id;
		}

		@Override
		public View getView(final int position, final View convertView, final ViewGroup parent) {
			final View v = super.getView(position, convertView, parent);
			v.setEnabled(isEnabled(position));
			return v;
		}

		@Override
		public boolean hasStableIds() {
			return true;
		}

	}

	public static final int REQUEST_GET_LABEL = 1;
	public static final String TAG_DIALOG_LABELS = "labels";
	public static final String EXTRA_LABEL_IDS = "labelIds";
	public static final String EXTRA_THREAD_ID = "threadId";

	private MySimpleCursorAdapter mAdapter;
	private ListView mList;

	private LabelsListCallback mCallback;
	private boolean multiple;
	private String[] mCheckedIds;
	private SparseArray<String> remoteIds;
	private NodeWorkerService mService;
	private Handler mHandler;
	private String mThreadId;

	private Label.Entity mRootLabel;
	private HashMap<String, String> labelsHash;

	public LabelsListFragment() {
		this(false);
	}

	public LabelsListFragment(final boolean multiple) {
		super();
		setMultiple(multiple);
	}

	@Override
	public void onAttach(final Activity activity) {
		mHandler = new Handler(getActivity().getMainLooper());
		if (activity instanceof LabelsListCallback) {
			mCallback = (LabelsListCallback) activity;
		}
		/*if (mService == null)
			((MHOApplication) getActivity().getApplication()).getNodeService(this);*/

		if (getArguments() != null) {
			mCheckedIds = getArguments().getStringArray(EXTRA_LABEL_IDS);
			mThreadId = getArguments().getString(Message.CONVERSATION_ID);
		}
		super.onAttach(activity);
	}

	@Override
	public void onNodeConnected(final NodeWorkerService service) {
		mService = service;
		mService.registerLabelsCallback(this);
		mService.getAllLabels();
	}

	@Override
	public void onStart() {
		((MHOApplication) getActivity().getApplication()).getNodeService(this);
		super.onStart();
	}

	public void registerCallback(final LabelsListCallback callback) {
		mCallback = callback;
	}

	public void unregisterCallback() {
		mCallback = null;
	}

	@Override
	public void onDetach() {
		if (mService != null)
			mService.unregisterLabelsCallback(this);
		super.onDetach();
	}

	void setMultiple(final boolean multiple) {
		this.multiple = multiple;
	}

	@Override
	public void onActivityCreated(final Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mList.setOnItemClickListener(this);
		mList.setItemsCanFocus(false);

		mList.setBackgroundResource(R.color.background);

		if (getShowsDialog())
			getDialog().setTitle(R.string.choose_labels);
	}

	private void createAdapterAndSetData(
			final ArrayList<HashMap<String, Object>> cursor) {
		int layout, choiceMode;
		if (!getShowsDialog()) {
			layout = R.layout.item_label;
			choiceMode = AbsListView.CHOICE_MODE_SINGLE;
		} else if (multiple) {
			layout = android.R.layout.simple_list_item_multiple_choice;
			choiceMode = AbsListView.CHOICE_MODE_MULTIPLE;
		} else {
			layout = android.R.layout.simple_list_item_1;
			choiceMode = AbsListView.CHOICE_MODE_SINGLE;
		}

		// Create an empty adapter we will use to display the loaded data.
		if (!getShowsDialog()) {
			mAdapter = new MySimpleCursorAdapter(getActivity(), cursor, layout,
					new String[] { Label.TITLE, Label.COUNT }, new int[] {
							android.R.id.text1, android.R.id.text2 });
		} else {
			mAdapter = new MySimpleCursorAdapter(getActivity(), cursor, layout,
					new String[] { Label.TITLE },
					new int[] { android.R.id.text1 });
		}
		mAdapter.setViewBinder(new LabelViewBinder());

		mList.setChoiceMode(choiceMode);
		mList.setAdapter(mAdapter);
	}

	@Override
	public void onItemClick(final AdapterView<?> parent, final View view,
			final int position, final long id) {
		if (getShowsDialog() && !multiple) {
			setNewIds(new long[] { id });
			getDialog().dismiss();
		} else {
			@SuppressWarnings("unchecked")
			final HashMap<String, Object> c = (HashMap<String, Object>) parent.getItemAtPosition(position);
			final String remoteId = (String) c.get(Label.REMOTE_ID);
			final int count = (Integer) c.get(Label.COUNT);
			final boolean isRoot = (Boolean) c.get(Label.IS_ROOT);

			String labelName = (String) c.get(Label.TITLE);

			if (!isRoot)
				labelName = labelName + " (" + count + ")";

			if (mCallback != null)
				mCallback.onItemClick(remoteId, labelName);
		}

	}


	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(
				getActivity());
		builder.setView(mList = new ListView(getActivity()));
		if (multiple)
			builder.setPositiveButton(R.string.save, this);
		return builder.create();
	}

	@Override
	public View onCreateView(final LayoutInflater inflater,
			final ViewGroup container, final Bundle savedInstanceState) {
		View v;
		if (!getShowsDialog()) {
			v = mList = new ListView(getActivity());
		} else {
			v = super.onCreateView(inflater, container, savedInstanceState);
		}

		return v;
	}

	@Override
	public void onClick(final DialogInterface dialog, final int which) {
		setNewIds(mList.getCheckedItemIds());
	}

	private void setNewIds(final long[] ids) {
		if (mThreadId != null) {
			final String[] newCheckedIds = new String[ids.length];

			for (int i = 0; i < ids.length; i++) {
				newCheckedIds[i] = remoteIds.get((int) ids[i]); // hash anyway
			}
			Log.d(this.getClass().getSimpleName(),
					"checkedids: " + Arrays.toString(mCheckedIds)
							+ "\nnewCheckedIds: "
							+ Arrays.toString(newCheckedIds));
			final Pair<ArrayList<String>, ArrayList<String>> data = calcDiffIds(
					mCheckedIds, newCheckedIds);
			for (final String id : data.first) {
				mService.addLabelToThread(id, mThreadId);
			}
			for (final String id : data.second) {
				mService.removeLabelFromThread(id, mThreadId);
			}
			if (mCallback != null) {
				final HashMap<String, String> l = new HashMap<String, String>(newCheckedIds.length);
				for (final String id : newCheckedIds) {
					l.put(id, labelsHash.get(id));
				}
				mCallback.onItemsSelected(l);
			}
		} else {
			Log.e(this.getClass().getSimpleName(), "No thread id provided");
		}
	}

	/**
	 *
	 * @param ids1
	 * @param ids2
	 * @return Pair.first - add, Pair.second - remove
	 */
	private Pair<ArrayList<String>, ArrayList<String>> calcDiffIds(
			final String[] ids1, final String[] ids2) {
		final ArrayList<String> add = new ArrayList<String>();
		final ArrayList<String> remove = new ArrayList<String>();

		for (final String id1 : ids1) {
			final boolean match = ArrayUtils.contains(ids2, id1);
			if (!match)
				remove.add(id1);
		}
		for (final String id2 : ids2) {
			final boolean match = ArrayUtils.contains(ids1, id2);

			if (!match)
				add.add(id2);
		}

		final Pair<ArrayList<String>, ArrayList<String>> result = new Pair<ArrayList<String>, ArrayList<String>>(
				add, remove);

		return result;
	}

	@Override
	public Handler getHandler() {
		return mHandler;
	}

	@Override
	public void onLabelsReceived(final List<Entity> labels) {
		final String[] projection = new String[] { BaseColumns._ID,
				Label.REMOTE_ID, Label.IS_ROOT, Label.TITLE, Label.COUNT };
		final ArrayList<HashMap<String, Object>> cursor = new ArrayList<HashMap<String, Object>>(labels.size());
		for (final Entity entity : labels) {

			final HashMap<String, Object> columnValues = new HashMap<String, Object>(projection.length);
			columnValues.put(BaseColumns._ID, new Long(entity.id.hashCode()));
			columnValues.put(Label.REMOTE_ID, entity.id);
			columnValues.put(Label.IS_ROOT, new Boolean(entity.isRoot));
			columnValues.put(Label.TITLE, entity.labelName);
			columnValues.put(Label.COUNT, entity.count);
			cursor.add(columnValues);

			if (entity.isRoot) {
				setRootLabel(entity);
				mCallback.onRootLabel(entity);
			}
		}
		createAdapterAndSetData(cursor);

		if (mCheckedIds != null) {
			final int count = mList.getCount();
			remoteIds = new SparseArray<String>(labels.size());
			labelsHash = new HashMap<String, String>();
			for (final Entity label : labels) {
				remoteIds.append(label.id.hashCode(), label.id);
				labelsHash.put(label.id, label.labelName);
			}

			final long[] hashIds = new long[mCheckedIds.length];
			int i = 0;
			for (final String checkedId : mCheckedIds) {
				hashIds[i++] = checkedId.hashCode();
			}

			for (i = 0; i < count; i++) {
				final long itemId = mList.getItemIdAtPosition(i);

				mList.setItemChecked(i, ArrayUtils.contains(hashIds, itemId));
			}
		}
	}

	@Override
	public void onMoveThreadToLabel(final List<Entity> labels,
			final Entity newLabel) { /* nop */
	}

	public Label.Entity getRootLabel() {
		return mRootLabel;
	}

	private void setRootLabel(final Label.Entity mRootLabel) {
		this.mRootLabel = mRootLabel;
	}
}
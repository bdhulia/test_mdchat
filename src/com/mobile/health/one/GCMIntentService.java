package com.mobile.health.one;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;
import com.google.android.gcm.GCMRegistrar;
import com.mobile.health.one.app.GroupInviteListActivity;
import com.mobile.health.one.app.InviteListActivity;
import com.mobile.health.one.app.LoginActivity;
import com.mobile.health.one.app.MessageDetailActivity;
import com.mobile.health.one.db.entity.Message;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.service.worker.SendSecretPushProcessor;
import com.mobile.health.one.util.LauncherUtils;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SettingsManager;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class GCMIntentService extends GCMBaseIntentService {

	public GCMIntentService() {
		super(Settings.SENDER_ID);
	}

	protected static class SendSecretPushReceiver implements Receiver {
		public final String TAG = SendSecretPushReceiver.class.getCanonicalName();

		@Override
		public void onReceiveResult(final int resultCode,
				final Bundle resultData) {
			switch (resultCode) {
			case SyncService.STATUS_RUNNING:
				Log.i(TAG, "Start SendSecretPushReceiver");

				break;
			case SyncService.STATUS_FINISHED:
				Log.i(TAG, "SendSecretPushReceiver success");

				break;
			case SyncService.STATUS_ERROR:
				Log.e(TAG, "Error SendSecretPushReceiver");
				if (resultData != null) {
					final Responce resp = resultData
							.getParcelable(RequestProcessor.EXTRA_RESPONSE);
					if (resp != null) {

					}
				}
				break;
			}
		}
	}

	private static final String TAG = GCMIntentService.class.getSimpleName();

	@Override
	protected void onError(final Context arg0, final String arg1) {
		Log.i(TAG, "::onError:" + "");

	}

	public static final String EXTRA_TYPE = "messageType";
	public static final String TYPE_THREAD_ID = "threadId";
	public static final String TYPE_INVITE_ID = "contactInvite";
	public static final String TYPE_GROUP_ID = "groupInvite";

	@Override
	protected void onMessage(final Context arg0, final Intent arg1) {
		Log.i(TAG, "::onMessage:" + arg1.getExtras().toString());
		try {
			final SettingsManager settings = new SettingsManager(arg0);
			final JSONObject messageJson = new JSONObject(arg1.getStringExtra("message"));
			final JSONObject infoJson = messageJson.getJSONObject("info");

			final String message = messageJson.getString("message");
			final String type = infoJson.getString("type");
			final Intent intent = getIntentForType(arg0, type);
			intent.putExtra(Message.CONVERSATION_ID, infoJson.optString("id"));

			if (!settings.isAppVisible() && intent != null)
				LauncherUtils.generateNotification(arg0, message, intent);
		} catch (final JSONException e) {
			e.printStackTrace();
		}
	}

	private Intent getIntentForType(final Context context, final String type) {
		Intent i = null;
		if (TYPE_THREAD_ID.equals(type)) {
			i = new Intent(context, MessageDetailActivity.class);
			i.putExtra(LoginActivity.EXTRA_LOGIN_ONLY, true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		} else if (TYPE_INVITE_ID.equals(type)) {
			i = new Intent(context, InviteListActivity.class);
			i.putExtra(LoginActivity.EXTRA_LOGIN_ONLY, true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		} else if (TYPE_GROUP_ID.equals(type)) {
			i = new Intent(context, GroupInviteListActivity.class);
			i.putExtra(LoginActivity.EXTRA_LOGIN_ONLY, true);
			i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		}
		return i;
	}

	@Override
	protected void onRegistered(final Context context, final String arg1) {
		Log.i(TAG, "::onRegistered:" + "");
		final String regId = GCMRegistrar.getRegistrationId(context);

		final SettingsManager sm = new SettingsManager(context);
		sm.setGCMId(regId);
		registerOnServer(context);
	}

	@Override
	protected void onUnregistered(final Context arg0, final String arg1) {
		Log.i(TAG, "::onUnregistered:" + "");

	}

	public static void registerOnServer(final Context context) {
		final SettingsManager sm = new SettingsManager(context);
		if (sm.hasGCMId()) {
			final String regId = sm.getGCMId();
			final Bundle extras = new Bundle();
			extras.putString(SendSecretPushProcessor.EXTRA_SECRET, regId);
			SyncService.startSyncService(context,
					AppSyncService.SEND_SECRET_PUSH, new SendSecretPushReceiver(), extras);
		}
	}

	public static void register(final Context context) {
		GCMRegistrar.checkDevice(context);
		GCMRegistrar.checkManifest(context);
		final String regId = GCMRegistrar.getRegistrationId(context);
		if (regId.equals("")) {
			GCMRegistrar.register(context, Settings.SENDER_ID);
		} else {
			Log.v(TAG, "Already registered");
		}
	}

}

package com.mobile.health.one.search;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import com.mobile.health.one.search.TextTask.Listener;
import com.mobile.health.one.service.entity.AutocompleteResponce;
import com.mobile.health.one.service.entity.IResponce;

public final class AutocompleteListenerTextSetter implements Listener {

	private static final String TAG = AutocompleteListenerTextSetter.class
			.getSimpleName();

	private final AutoCompleteTextView view;
	private final Context context;
	public AutocompleteListenerTextSetter(final Context context, final AutoCompleteTextView view) {
		this.view = view;
		this.context = context;
	}

	@Override
	public void onSearchStart() {	}

	@Override
	public void onSearchError(final String error) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSearchRestart() {
		// nop

	}

	@Override
	public void onSearchComplete(final IResponce responce) {
		final AutocompleteResponce r = (AutocompleteResponce) responce;
		final ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, r.getData());
		if (view.isShown() && view.hasFocus()) {
			view.setAdapter(adapter);
			view.showDropDown();
			Log.i(TAG, "::onSearchComplete:" + "showDropDown");
		}

	}

	@Override
	public void onSearchCancel() {
		// nop
	}

}
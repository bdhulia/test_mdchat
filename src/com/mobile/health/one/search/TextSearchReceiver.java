package com.mobile.health.one.search;

import android.os.Bundle;
import android.util.Log;

import com.mobile.health.one.search.TextTask.Listener;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

public class TextSearchReceiver implements Receiver {

	private static final String TAG = TextSearchReceiver.class.getSimpleName();
	private final Listener listener;

	public TextSearchReceiver(final Listener listener) {
		this.listener = listener;
	}

	@Override
	public void onReceiveResult(final int resultCode, final Bundle resultData) {
		switch (resultCode) {
		case SyncService.STATUS_RUNNING:
			Log.i(TAG, "Start search");
			// Start out with a progress indicator.
			listener.onSearchStart();
			break;
		case SyncService.STATUS_FINISHED:
			if (resultData == null) return; // for debugging

			final Listener l = listener;
			IResponce responce = resultData
					.getParcelable(RequestProcessor.EXTRA_RESPONSE);
			Log.i(TAG, "User autocomplete finished " + responce.toString());
			l.onSearchComplete(responce);

			break;
		case SyncService.STATUS_ERROR:
			final String ERROR = "Error search";
			if (resultData != null) {
				responce = resultData
						.getParcelable(RequestProcessor.EXTRA_RESPONSE);
				if (responce != null) {
					listener.onSearchError(responce.getMessages());
				}
				final String err = resultData.getString(RequestProcessor.EXTRA_ERROR_STRING);
				if (err != null)
					Log.e(TAG, err);
			} else {
				listener.onSearchError(ERROR);
			}
			Log.e(TAG, ERROR);
			// TODO: make diffs on error code, now server always return 1

			break;
		}

	}

}
package com.mobile.health.one.search;

import java.util.Timer;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;

import com.mobile.health.one.app.SearchFragment;
import com.mobile.health.one.search.TextTask.Listener;

public final class SearchTextWatcher implements TextWatcher {
	private final int TIMEOUT = 1000;

	private Timer mTimer;
	private final TextTask.Listener mListener;

	private final TextTask.Creator mTextTaskFactory;

	private TextTask mTextTask;

	public SearchTextWatcher(final AutoCompleteTextView searchView, final Listener l, final TextTask.Creator factory) {
		mListener = l;
		searchView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(final AdapterView<?> parent, final View view,
					final int position, final long id) {
				if (mTimer != null)
					mTimer.cancel();
			}
		});
		factory.setListener(l).setView(searchView);
		mTextTaskFactory = factory;
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
		Log.d(SearchFragment.TAG, "onTextChanged");
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start, final int count,
			final int after) {		}

	@Override
	public void afterTextChanged(final Editable s) {
		if (mTimer != null) mTimer.cancel();
		searchText(s);
	}

	private void searchText(final Editable s) {
		final Listener l = mListener;
		if (l != null) {
			if (TextUtils.isEmpty(s)) {
				l.onSearchRestart();
				if (mTimer != null) mTimer.cancel();
			} else {
				final TextTask.Creator f = mTextTaskFactory;
				f.setSearch(s);
				mTimer = new Timer(true);
				mTextTask = f.create();
				mTimer.schedule(mTextTask, TIMEOUT);
			}
			Log.d(SearchFragment.TAG, "aterTextChange");
		}
	}

	@SuppressWarnings("unused")
	private void autocompleteText(final Editable s) {
		mTimer = new Timer(true);
		//mTimer.schedule(new AutocompleteTextSearchTask(new AutocompleteListenerTextSetter(searchView.getContext(), searchView), searchView, s, mType), TIMEOUT);
	}

	public TextTask getTextTask() {
		return mTextTask;
	}
}
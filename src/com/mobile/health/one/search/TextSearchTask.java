package com.mobile.health.one.search;

import org.apache.commons.lang.StringUtils;

import android.os.Bundle;
import android.view.View;

import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.worker.SearchProcessor;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.SearchParams.Param;
import com.mobile.health.one.util.SearchParams.SearchParam;

public class TextSearchTask extends TextTask {

	public static class Creator extends TextTask.Creator {

		private Bundle params;

		private SearchParams.SearchType type;

		public Creator setType(final SearchParams.SearchType type) {
			this.type = type;
			return this;
		}

		public Creator setAdditionalParams(final Bundle params) {
			this.params = params;
			return this;
		}

		@Override
		public TextTask create() {
			return new TextSearchTask(getListener(), getView(), getSearch(), type, params);
		}

	}

	private final SearchParams.SearchType mType;

	private final Bundle mParams;

	private TextSearchTask(final Listener listener, final View view, final CharSequence s, final SearchParams.SearchType type, final Bundle params) {
		super(listener, view, s);
		mType = type;
		mParams = params;
	}

	@Override
	protected Bundle getExtras() {
		final Bundle extras = new Bundle();
		final String formField = (String) getView().getTag();
		if (StringUtils.isNotEmpty(formField)) {
			final String[] formFields = new String[] { formField };
			extras.putStringArray(SearchProcessor.EXTRA_FORM_FIELDS, formFields);
		}
		final Param p = SearchParams.getParams(mType);
		final String[] searches = new String[] { getText().toString() };

		Bundle extraParams = mParams;
		if (p instanceof SearchParam) {
			final SearchParam param = (SearchParam) p;
			final String[] fieldsSimpleOnly = param.getFormFieldsSimpleSearchOnly();
			final String[] searchesSimpleOnly = param.getSearchesSimpleSearchOnly();

			if (fieldsSimpleOnly != null && searchesSimpleOnly != null) {
				extraParams = mParams != null ? mParams : new Bundle(fieldsSimpleOnly.length);
				for (int i = 0; i < fieldsSimpleOnly.length; i++) {
					extraParams.putString(fieldsSimpleOnly[i], searchesSimpleOnly[i]);
				}
			}
		}

		extras.putStringArray(SearchProcessor.EXTRA_SEARCH, searches);
		extras.putInt(SearchProcessor.EXTRA_TYPE, mType.ordinal());
		extras.putBundle(SearchProcessor.EXTRA_ADDITIONAL, extraParams);
		return extras;
	}

	@Override
	protected String getSearchWorkerName() {
		return AppSyncService.SEARCH;
	}

}
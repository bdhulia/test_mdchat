package com.mobile.health.one.search;

import android.os.Bundle;
import android.view.View;

import com.mobile.health.one.app.SearchFragment;
import com.mobile.health.one.service.AppSyncService;
import com.mobile.health.one.service.worker.AutocompleteSearchProcessor;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.SearchParams.SearchType;

public class AutocompleteTextSearchTask extends TextTask {

	public static class Creator extends TextTask.Creator {

		public Creator setType(final SearchParams.SearchType type) {
			this.type = type;
			return this;
		}

		private SearchParams.SearchType type;

		@Override
		public TextTask create() {
			return new AutocompleteTextSearchTask(getListener(), getView(), getSearch(), type);
		}

	}

	private final SearchParams.SearchType type;

	public AutocompleteTextSearchTask(final Listener listener, final View view,
			final CharSequence s, final SearchType type) {
		super(listener, view, s);
		this.type = type;
	}

	@Override
	protected Bundle getExtras() {
		final Bundle extras = new Bundle();
		final String formField = (String) getView().getTag();

		extras.putString(AutocompleteSearchProcessor.EXTRA_PARAM_FIELD, formField);
		extras.putString(AutocompleteSearchProcessor.EXTRA_PARAM_SEARCH, getText().toString());
		extras.putInt(AutocompleteSearchProcessor.EXTRA_TYPE, type.ordinal());
		extras.putBoolean(SearchFragment.EXTRA_AUTOCOMPLETE, true);
		return extras;
	}

	@Override
	protected String getSearchWorkerName() {
		return AppSyncService.AUTOCOMPLETE_SEARCH;
	}

}
package com.mobile.health.one.search;

import java.util.Timer;
import java.util.TimerTask;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AutoCompleteTextView;

import com.mobile.health.one.app.CharSequenceArrayAdapter;
import com.mobile.health.one.app.SearchFragment;

public final class SuggestTextWatcher implements TextWatcher {
	private final int TIMEOUT = 1000;

	private Timer mTimer;
	private final AutoCompleteTextView mSearchView;

	public SuggestTextWatcher(final AutoCompleteTextView searchView) {
		searchView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(final AdapterView<?> parent, final View view,
					final int position, final long id) {
				if (mTimer != null)
					mTimer.cancel();
			}
		});
		mSearchView = searchView;
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
		Log.d(SearchFragment.TAG, "onTextChanged");
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start, final int count,
			final int after) {		}

	@Override
	public void afterTextChanged(final Editable s) {
		if (mTimer != null) mTimer.cancel();
		searchText(s);
	}

	private void searchText(final Editable s) {
		if (TextUtils.isEmpty(s)) {
			if (mTimer != null) mTimer.cancel();
		} else {
			mTimer = new Timer(true);
			mTimer.schedule(new TimerTask() {

				@Override
				public void run() {
					final CharSequenceArrayAdapter adapter = (CharSequenceArrayAdapter) mSearchView.getAdapter();
					adapter.getFilter().filter(s);
				}
			}, TIMEOUT);
		}
		Log.d(SearchFragment.TAG, "aterTextChange");
	}

}
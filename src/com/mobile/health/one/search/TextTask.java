package com.mobile.health.one.search;

import java.util.TimerTask;

import android.os.Bundle;
import android.view.View;

import com.mobile.health.one.service.SyncService;
import com.mobile.health.one.service.entity.IResponce;

public abstract class TextTask extends TimerTask {

	public static interface Listener {
		public void onSearchStart();

		public void onSearchRestart();

		public void onSearchError(String error);

		public void onSearchComplete(IResponce responce);

		/**
		 * Happens, when user clicks Cancel button
		 */
		public void onSearchCancel();
	}

	public static abstract class Creator {

		private Listener listener;
		private View view;
		private CharSequence search;

		public Listener getListener() {
			return listener;
		}

		public View getView() {
			return view;
		}

		public CharSequence getSearch() {
			return search;
		}

		public Creator setListener(final Listener listener) {
			this.listener = listener;
			return this;
		}

		public Creator setView(final View view) {
			this.view = view;
			return this;
		}

		public Creator setSearch(final CharSequence search) {
			this.search = search;
			return this;
		}

		public abstract TextTask create();

	}

	private final CharSequence text;
	private final View view;
	private final Listener listener;

	protected abstract Bundle getExtras();

	protected abstract String getSearchWorkerName();

	public CharSequence getText() {
		return text;
	}

	public View getView() {
		return view;
	}

	public Listener getListener() {
		return listener;
	}

	public TextTask(final Listener listener, final View view,
			final CharSequence s) {
		this.listener = listener;
		text = s;
		this.view = view;
	}

	@Override
	public final void run() {
		SyncService.startSyncService(getView().getContext(),
				getSearchWorkerName(), new TextSearchReceiver(getListener()),
				getExtras());
	}
}
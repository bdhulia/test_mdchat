package com.mobile.health.one;

import java.lang.ref.WeakReference;
import java.util.Timer;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.mobile.health.one.R;
import com.mobile.health.one.MHOApplication.NodeServiceConnector;
import com.mobile.health.one.MHOApplication.UpdateSessionTimerTask;
import com.mobile.health.one.app.EnterPinFragment;
import com.mobile.health.one.app.LoginActivity;
import com.mobile.health.one.service.NodeWorkerService;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SettingsManager;

public class AppFragmentActivity extends SherlockFragmentActivity implements
		NodeServiceConnector {

	public static class InactivityTimerTask implements Runnable {

		WeakReference<AppFragmentActivity> activity;

		public InactivityTimerTask(final AppFragmentActivity a) {
			activity = new WeakReference<AppFragmentActivity>(a);
			Log.i(TAG, "::InactivityTimerTask:" + " task init");
		}

		@Override
		public void run() {
			synchronized (activity) {
				Log.i(TAG, "::InactivityTimerTask:" + " task run");
				final AppFragmentActivity a = activity.get();
				if (a != null) {

					final FragmentManager fm = a.getSupportFragmentManager();
					final Fragment oldFragment = fm
							.findFragmentByTag(EnterPinFragment.TAG);
					// a.getSettings().setShowPassDialog(false);
					if (oldFragment == null || !oldFragment.isVisible()) {
						if (!a.isLostState()) {
							a.showEnterPinDialog();
							Log.i(TAG, "::InactivityTimerTask:" + "show dialog");
						} else {
							// a.getSettings().setShowPassDialog(true);
							// Log.i(TAG, "::InactivityTimerTask:"
							// + "setShowPassDialog(true)");
						}
					}
				}
			}
		}

	}

	private SettingsManager settings;

	private boolean mLostState;
	private Handler mUserIntarractionTimer;
	private InactivityTimerTask task;

	private boolean mAlreadyShownInCreate;
	private boolean mLoginOnly;

	// do them static to use across activities
	// should use this gently
	private static long mUpdateInteractionTime;
	private static long mCountdownTime = Long.MAX_VALUE;

	protected static final String TAG = AppFragmentActivity.class
			.getSimpleName();

	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		setSettings(new SettingsManager(getApplicationContext()));
		super.onCreate(savedInstanceState);
		final MHOApplication app = (MHOApplication) getApplication();
		mUpdateInteractionTime = System.currentTimeMillis();

		//if (!mLoginOnly)
		//	ensureCredentials(mLoginOnly);
		getSettings().setAppVisible(true);
		if (ensureTimeout()) {
			mAlreadyShownInCreate = true;
		} else {
			app.getNodeService(new NodeServiceConnector() {

				@Override
				public void onNodeConnected(final NodeWorkerService service) {
					if (isLoginOnly())
						service.serverAuth(getSettings().getSessionId());
					getSettings().setAppVisible(true);
					if (ensureTimeout()) {
						mAlreadyShownInCreate = true;
					}
				}
			});
		}

		initCountDownTime();
	}

	// Remember time before on pause and count 3 mins on onResume

	@Override
	protected void onDestroy() {
		cancelTimer();
		try {
			super.onDestroy();
		} catch (final IllegalStateException e) {
			Log.e(TAG, "Cannot destroy activity", e);
		}
	}

	public SettingsManager getSettings() {
		return settings;
	}

	public void setSettings(final SettingsManager settings) {
		this.settings = settings;
	}

	@Override
	protected void onResume() {
		Log.i(TAG, "AppFragmentActivity::onResume");
		super.onResume();
		setLostState(false);
		setLoginOnly(getIntent().getBooleanExtra(LoginActivity.EXTRA_LOGIN_ONLY, false));
		// if ensureCredentials did not work onCreate
		// then show only login and hide it after success
		ensureCredentials(mLoginOnly);
		getSettings().setAppVisible(true);
		if (!mAlreadyShownInCreate)
			ensureTimeout();
		updateInteractionTimer();
        settings.restoreLastLoginTime();
	}

	protected void ensureCredentials(final boolean loginOnly) {
		if (!getSettings().hasPin()) {
			Log.e(TAG, "Please enter pin. loginOnly " + loginOnly);
			final Intent i = new Intent(this, LoginActivity.class);
			i.putExtra(LoginActivity.EXTRA_LOGIN_ONLY, loginOnly);
			startActivity(i);
			if (!loginOnly)
				finish();
		}
		if (!getSettings().hasPinRegistered() && !loginOnly) {
			Log.e(TAG, "Please register pin");
			finish();
		}
	}

	@SuppressWarnings("unused")
	private void ensureCredentials() {
		ensureCredentials(false);
	}

	protected boolean ensureTimeout() {
		Log.i(TAG, "AppFragmentActivity::ensureTimeout");
		final long now = System.currentTimeMillis();
		if (now - mUpdateInteractionTime > mCountdownTime) {
			Log.i(TAG, "AppFragmentActivity::ShowPassDialog");
			showEnterPinDialog();
			return true;
		}

		return false;
	}

	private void showEnterPinDialog() {
		DialogFragment pin = (DialogFragment) getSupportFragmentManager()
				.findFragmentByTag(EnterPinFragment.TAG);
		if (pin == null || !pin.isVisible()) {
			pin = new EnterPinFragment();
			pin.setStyle(DialogFragment.STYLE_NORMAL,
					R.style.Theme_EnterPinDialog);
			pin.show(getSupportFragmentManager(), EnterPinFragment.TAG);
		}
	}

	@Override
	protected void onPause() {
		getSettings().setAppVisible(false);
		// getSettings().setShowPassDialog(false);
		setLostState(true);
		initCountDownTime();
        settings.saveLastLoginTime();
		Log.i(TAG, "::onPause:" + "mCountdownTime = " + mCountdownTime);
		super.onPause();
	}

	private void initCountDownTime() {
		mCountdownTime = Settings.INACTIVITY_TIME
				- (System.currentTimeMillis() - mUpdateInteractionTime);
	}

	@Override
	protected void onSaveInstanceState(final Bundle outState) {
		super.onSaveInstanceState(outState);
		setLostState(true);
	}

	private void updateInteractionTimer() {
		mUpdateInteractionTime = System.currentTimeMillis();
		updateInactivityTimer(this);
	}

	public void updateInactivityTimer(final AppFragmentActivity a) {
		final Handler timer = getInactivityTimer();
		cancelTimer();
		task = new InactivityTimerTask(a);
		timer.postDelayed(task, Settings.INACTIVITY_TIME);
	}

	public Handler getInactivityTimer() {
		if (mUserIntarractionTimer == null)
			mUserIntarractionTimer = new Handler(getMainLooper());

		return mUserIntarractionTimer;
	}

	private void cancelTimer() {
		if (mUserIntarractionTimer != null)
			mUserIntarractionTimer.removeCallbacks(task);
	}

	@Override
	public void onUserInteraction() {
		updateInteractionTimer();
        settings.setLastLoginTime(System.currentTimeMillis());
		super.onUserInteraction();
	}

	public boolean isLostState() {
		return mLostState;
	}

	public void setLostState(final boolean lostState) {
		mLostState = lostState;
	}

	public static class ProgressDialogFragment extends DialogFragment {
		@Override
		public Dialog onCreateDialog(final Bundle savedInstanceState) {
			final ProgressDialog d = new ProgressDialog(getActivity());
			d.setMessage(getActivity().getString(
					R.string.error_trying_to_restore_user_session));
			return d;
		}
	}

	/**
	 * Used for extraordinary update session
	 *
	 * @param showProgress
	 */
	public void updateSession(final boolean showProgress) {
		final Timer updateSessionTimer = new Timer();
		final UpdateSessionTimerTask updateSessionTask = new UpdateSessionTimerTask(
				this);
		updateSessionTimer.schedule(updateSessionTask, 100);
		if (showProgress) {
			final ProgressDialogFragment progress = new ProgressDialogFragment();
			progress.show(getSupportFragmentManager(), TAG);
			final Handler handler = new Handler(getMainLooper());
			new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						updateSessionTask.wait(1 * 60 * 1000); // wait 1 min
					} catch (final InterruptedException e) {

					} finally {
						handler.post(new Runnable() {

							@Override
							public void run() {
								progress.dismiss();
							}
						});
					}

				}
			});
		}
	}

	/**
	 * Override this to get NodeJS Service
	 *
	 * @param service
	 *            NodeWorkerService
	 */
	@Override
	public void onNodeConnected(final NodeWorkerService service) {

	}

	public boolean isLoginOnly() {
		return mLoginOnly;
	}

	public void setLoginOnly(final boolean mLoginOnly) {
		this.mLoginOnly = mLoginOnly;
	}

}

package com.mobile.health.one.db.entity;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

import android.annotation.SuppressLint;

public enum Gender {

	MALE("M"), FEMALE("F");

	@SuppressLint("UseSparseArrays")
	private static final Map<String, Gender> lookupCodes = new HashMap<String, Gender>();

	static {
		for (Gender gender : EnumSet.allOf(Gender.class)) {
			lookupCodes.put(gender.getCode(), gender);
		}
	}

	private String code;

	private Gender(String code) {
		this.code = code;
	}

	public String getCode() {
		return this.code;
	}

	public static String get(String code) {
		String value = lookupCodes.get(code).name().toLowerCase();

		StringBuffer buffer = new StringBuffer(value);
		buffer.replace(0, 1, value.substring(0, 1).toUpperCase());

		return buffer.toString();
	}
}
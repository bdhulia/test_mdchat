package com.mobile.health.one.db.entity;

import org.apache.commons.lang.StringUtils;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.google.gson.annotations.SerializedName;

// The database table
public class User implements BaseColumns {

	public static class Entity implements Parcelable {
		@SerializedName("userId")
		public Long id;
		@SerializedName("id")
		public Long id2;
		public String firstName;
		public String lastName;
		public String middleName;
		@SerializedName("full_name")
		public String fullName;
		public Boolean status;
		@SerializedName("status_text")
		public String statusText;
		public String speciality;
		@SerializedName("avatar_link")
		public String avatarLink;

		public int type;
		public Boolean isFriend;

		public Entity(final Parcel in) {
			id = in.readLong();
			firstName = in.readString();
			lastName = in.readString();
			middleName = in.readString();
			fullName = in.readString();
			status = (Boolean) in.readValue(Boolean.class.getClassLoader());
			statusText = in.readString();
			speciality = in.readString();
			avatarLink = in.readString();
		}
		public Entity() {	}
		//String privacy;
		@Override
		public int describeContents() {
			return hashCode();
		}
		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeLong(id);
			dest.writeString(firstName);
			dest.writeString(lastName);
			dest.writeString(middleName);
			dest.writeString(fullName);
			dest.writeValue(status);
			dest.writeString(statusText);
			dest.writeString(speciality);
			dest.writeString(avatarLink);
		}
		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};

		@Override
		public String toString() {
			return id.toString();
		}

		public ContentValues prepareValues() {
			final ContentValues cv = new ContentValues();
			//FIXME: stub for old format
			if (id != null && id > 0)
				cv.put(_ID, id);
			else
				cv.put(_ID, id2);

			cv.put(FIRST_NAME, firstName);
			cv.put(LAST_NAME, lastName);
			cv.put(MIDDLE_NAME, middleName);
			if (StringUtils.isEmpty(fullName)) {
				cv.put(FULL_NAME, lastName);
				cv.put(LAST_NAME, lastName);
			} else {
				cv.put(FULL_NAME, fullName);
			}
			cv.put(STATUS, status);
			cv.put(STATUS_TEXT, statusText);
			cv.put(SPECIALITY, speciality);
			cv.put(AVATAR_LINK, avatarLink);

			cv.put(IS_FRIEND, isFriend);
			cv.put(TYPE, type);

			return cv;
		}
	}

	// This defines the path component of the content URI.
	// For most instances, it's best to just use the classname here:
	public static final String PATH = "user";
	public static final String FILTER_PATH = "user/filter";

	// Column definitions ///////////////////////////////////

	/**
	 * Message date
	 */
	public static final String CREATED_DATE = "created";


	/**
	 * User name
	 */
	public static final String FIRST_NAME = "first_name";

	/**
	 * User name
	 */
	public static final String MIDDLE_NAME = "middle_name";

	/**
	 * User family name
	 */
	public static final String LAST_NAME = "last_name";

	/**
	 * User full name
	 */
	public static final String FULL_NAME = "full_name";

	public static final String STATUS = "status";

	public static final String STATUS_TEXT = "status_text";

	public static final String SPECIALITY = "speciality";

	public static final String AVATAR_LINK = "avatar_link";

	public static final String IN_NETWORK = "in_network";


	public static final int TYPE_UNDEFINED = 0;
	public static final int TYPE_HCP = 1;
	public static final int TYPE_RECENT = 2;

	public static final String TYPE = "type";

	public static final String IS_FRIEND = "is_friend";

}

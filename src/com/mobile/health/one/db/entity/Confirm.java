package com.mobile.health.one.db.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

// The database table
public class Confirm implements BaseColumns {
	public static final String PATH = "confirm";
	public static final String FILTER_PATH = "filter";
	public static final String CONVERSATIONS_PATH = "confirms";
	public static final String DATE_CONF = "dateConf";
	public static final String DATE = "date";
	public static final String USER_NAME = "userName";
	public static final String STATUS = "availability_status";

	// Column definitions ///////////////////////////////////

	public static final String READ_DATE = "read";


	public static class Entity implements Parcelable {
		public Long id;
		public String dateConf;
		public Long date;
		public String userName;
		public Boolean status;

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeLong(id);
			dest.writeString(dateConf);
			dest.writeLong(date);
			dest.writeString(userName);
			dest.writeInt(status ? 1 : 0);
		}
	}
}
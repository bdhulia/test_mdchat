package com.mobile.health.one.db.entity;

import android.content.ContentValues;
import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.util.SparseArray;

// The database table
public class Message implements BaseColumns {
	public static class Entity implements Parcelable {
		public String id;
		public String message;
		public long createDate;
		public String attachments;
		public Long sendBy;
		public SparseArray<Long> reads;
		public boolean isRead;
		public Entity(final Parcel in) {
			id = in.readString();
			message = in.readString();
			createDate = in.readLong();
			attachments = in.readString();
			sendBy = in.readLong();
			// TODO : reads
		}
		public Entity() { };
		@Override
		public int describeContents() {
			return hashCode();
		}
		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(id);
			dest.writeString(message);
			dest.writeLong(createDate);
			dest.writeString(attachments);
			dest.writeLong(sendBy);
			// TODO : reads
		}
		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};

		public ContentValues prepareValues() {
			final ContentValues cv = new ContentValues(4);
			cv.put(Message.BODY, message);
			cv.put(Message.CREATED_DATE, createDate * 1000);
			cv.put(Message.USER_ID_FROM, sendBy);
			cv.put(Message.REMOTE_ID, id);
			cv.put(Message.IS_READ_BY_ALL, isRead);
			return cv;
		}

	}

	// Column definitions ///////////////////////////////////
	/**
	 * Message read state
	 */
	public static final String IS_READ_BY_ALL = "is_read_all";

	/**
	 * Message date
	 */
	public static final String CREATED_DATE = "created";

	/**
	 * Message body
	 */
	public static final String BODY = "body";

	/**
	 * User id, that this message is from
	 */
	public static final String USER_ID_FROM = "userIdFrom";

	/**
	 * User names comma separated string, that this message corresponds to
	 * need this for optimizations
	 */
	public static final String USER_NAMES_TO = "userNamesTo";

	public static final String CONVERSATION_ID = "conversationId";

	public static final String REMOTE_ID = "remoteId";

	public static final String ATTACHMENT = "attachment";

	// //////////////////////////////////////////////////////

}

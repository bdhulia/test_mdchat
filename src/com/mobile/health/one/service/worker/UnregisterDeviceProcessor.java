package com.mobile.health.one.service.worker;

import java.security.InvalidParameterException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;
import android.util.Log;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SettingsManager;

/**
 * @author Igor Yanishevskiy
 */
public class UnregisterDeviceProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";

	private static final Uri URI = Uri
			.parse(Settings.URI_BASE + "/api/device_api/unregisterDevice");

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	private final SettingsManager settings;

	public UnregisterDeviceProcessor(final Context context) {
		super(context);
		settings = new SettingsManager(context);
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		if (!settings.hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}
		final String token = settings.getSessionId();

		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		Log.i(TAG, "::processInternal:" + " token " + token);
		params.add(EXTRA_DEVICE_ID, androidId);

		tryRequest(receiver, false, params, Responce.class, extras);
	}

	@Override
	public Uri getUri() {
		return URI;
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(final E result,
			final Bundle extras) {
		// nothing to do here, we don't receive any data, just status
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(final E result) {
		return result.getMessages();
	}

}

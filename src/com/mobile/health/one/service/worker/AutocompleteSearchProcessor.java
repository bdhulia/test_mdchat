package com.mobile.health.one.service.worker;

import java.security.InvalidParameterException;
import java.util.Arrays;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.AutocompleteResponce;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.SearchParams;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SearchParams.SearchParam;
import com.mobile.health.one.util.SearchParams.SearchType;

/**
 * @author Igor Yanishevskiy
 */
public class AutocompleteSearchProcessor extends RequestProcessor {

	public static final String EXTRA_PARAM_TOKEN = "token";
	public static final String EXTRA_PARAM_DEVICE_ID = "device_id";

	public static final String EXTRA_PARAM_FIELD = "field";
	public static final String EXTRA_PARAM_SEARCH = "query";

	public static final String EXTRA_TYPE = "type";

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public static final String URI_CONTACTS = Settings.URI_BASE
			+ "/api/api_search_suggest/contacts";

	public static final String URI_MEMBERS = Settings.URI_BASE
			+ "/api/api_search_suggest/members";

	public static final String URI_GROUPS = Settings.URI_BASE
			+ "/api/api_search_suggest/groups";

	public static final String URI_MESSAGES = Settings.URI_BASE
			+ "/api/api_search_suggest/messages";

	public static final String URI_THREADS = Settings.URI_BASE
			+ "/api/api_search_suggest/threads";

	private Uri mUri;

	public AutocompleteSearchProcessor(final Context context) {
		super(context);
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		final SearchType type = SearchType.values()[extras.getInt(EXTRA_TYPE,
				SearchType.MESSAGES.ordinal())];
		final SearchParams.SearchParam sp = (SearchParam) SearchParams.getParams(type);
		mUri = Uri.parse(sp.getAutocompleteEndoint());

		final String[] fields = sp.getAutocompleteFormFields();
		String[] searchVals = sp.getAutocompleteSearches();

		final RequestParams params = new BasicRequestParams();

		String field;
		if (extras.containsKey(EXTRA_PARAM_FIELD)) {
			field = extras.getString(EXTRA_PARAM_FIELD);
		} else {
			throw new InvalidParameterException(
					"EXTRA_PARAM_FIELD should be passed");
		}
		final String search = extras.getString(EXTRA_PARAM_SEARCH);

		if (StringUtils.isEmpty(search))
			throw new InvalidParameterException(
					"EXTRA_PARAM_SEARCH should be passed");

		final String[] searches;
		final String[] formFields;
		if (searchVals != null && fields != null) {
			if (searchVals.length == 1 && searchVals.length < fields.length) {
				final String searchValue = searchVals[0];
				searchVals = new String[fields.length];
				Arrays.fill(searchVals, searchValue);
			}
			searches = (String[]) ArrayUtils.add(searchVals, search);
			formFields = (String[]) ArrayUtils.add(fields, field);
		} else {
			searches = new String[] { search };
			formFields = new String[] { field };
		}

		for (int i = 0; i < searches.length; i++) {
			final String searchText = searches[i];
			final String formField = formFields[i];
			params.add(formField, searchText);
		}

		final String token = getSettings().getSessionId();

		params.add(EXTRA_PARAM_TOKEN, token);
		params.add(EXTRA_PARAM_DEVICE_ID, androidId);
		params.add(EXTRA_PARAM_SEARCH, search);
		params.add(EXTRA_PARAM_FIELD, field);
		// params.add("current_page", "0");
		// params.add("per_page", "1000");

		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		tryRequest(receiver, false, params, AutocompleteResponce.class, extras);

	}

	@Override
	public Uri getUri() {
		return mUri;
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

}

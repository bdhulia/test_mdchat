package com.mobile.health.one.service.worker;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.mobile.health.one.R;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.UserLabelsResponce;
import com.mobile.health.one.util.Settings;
import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;

public class GetUserLabelsProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_USER_ID = "user_id";
	public static final String EXTRA_ADD_GROUP_LABELS = "add_groups_labels";
	public static final String ROOT_TITLE = "root_title";
	public static final String ROOT_COUNT = "root_count";

	private final Context context;

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	private static final Uri URI = Uri.parse(Settings.URI_BASE
			+ "/api/user.labels_api/getUserLabels");

	public GetUserLabelsProcessor(final Context context) {
		super(context);
		this.context = context;
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) throws IOException,
			HttpException, Exception {
		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();

		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);
		final String addGroupLabels = extras.getString(EXTRA_ADD_GROUP_LABELS);
		if (StringUtils.isNotEmpty(addGroupLabels)) {
			params.add(EXTRA_ADD_GROUP_LABELS, addGroupLabels);
		}

		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		tryRequest(receiver, false, params, UserLabelsResponce.class,
				extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return context.getResources().getString(R.string.error_get_user_profile);
	}

	@Override
	public Uri getUri() {
		return URI;
	}
}
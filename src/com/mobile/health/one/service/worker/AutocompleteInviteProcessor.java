package com.mobile.health.one.service.worker;

import android.content.Context;
import android.net.Uri;

import com.mobile.health.one.util.Settings;

/**
 * @author Igor Yanishevskiy
 */
public class AutocompleteInviteProcessor extends AutocompleteSuggestProcessor {

	private static final Uri URI = Uri
			.parse(Settings.URI_BASE + "/api/api_search_suggest/invitee");

	public AutocompleteInviteProcessor(final Context context) {
		super(context);
	}

	@Override
	public Uri getUri() {
		return URI;
	}

}

package com.mobile.health.one.service.worker;

import java.security.InvalidParameterException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.util.Settings;

/**
 * @author Igor Yanishevskiy
 */
public class SetStatusProcessor extends RequestProcessor {

	public static final String EXTRA_METHOD = "method";
	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_STATUS = "status";

	private static final Uri URI = Uri
			.parse(Settings.URI_BASE + "/api/api_profile_status/setStatus");

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public SetStatusProcessor(final Context context) {
		super(context);
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		final String token = getSettings().getSessionId();

		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, this.androidId);
		params.add(EXTRA_STATUS, extras.getBoolean(EXTRA_STATUS)? "1" : "0");

		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		tryRequest(receiver, false, params, Responce.class, extras);

	}

	@Override
	public Uri getUri() {
		return URI;
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(final E result, final Bundle extras) {
		final Responce res = (Responce) result;
		extras.putBoolean(EXTRA_STATUS, res.getData().get(EXTRA_STATUS).equals("true"));
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(final E result) {
		return result.getMessages();
	}

}

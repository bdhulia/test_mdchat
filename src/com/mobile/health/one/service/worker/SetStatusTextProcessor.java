package com.mobile.health.one.service.worker;

import java.security.InvalidParameterException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Responce;
import com.mobile.health.one.util.Settings;

/**
 * @author Igor Yanishevskiy
 */
public class SetStatusTextProcessor extends RequestProcessor {

	public static final String EXTRA_METHOD = "method";
	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_STATUS = "status";
	public static final String EXTRA_STATUS_TEXT = "status_text";

	private static final Uri URI = Uri
			.parse(Settings.URI_BASE + "/api/api_profile_status/setStatusText");

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public SetStatusTextProcessor(final Context context) {
		super(context);
	}

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		final String token = getSettings().getSessionId();

		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, this.androidId);
		params.add(EXTRA_STATUS_TEXT, extras.getString(EXTRA_STATUS_TEXT));

		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		tryRequest(receiver, false, params, Responce.class, extras);

	}

	@Override
	public Uri getUri() {
		return URI;
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(final E result, final Bundle extras) {
		// do nothing here
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(final E result) {
		return result.getMessages();
	}

}

package com.mobile.health.one.service.worker;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.Invite;
import com.mobile.health.one.service.entity.InvitesResponce;
import com.mobile.health.one.util.Settings;

public class GetInvitesProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_CURRENT_PAGE = "page";
	public static final String EXTRA_ITEMS_PER_PAGE = "per_page";
	public static final String EXTRA_STATUS = "invite_status";

	private static final Uri URI_INCOMING = Uri.parse(Settings.URI_BASE
			+ "/api/archer_invite_api/getIncomingInvitations");
	private static final Uri URI_OUTGOING = Uri.parse(Settings.URI_BASE
			+ "/api/archer_invite_api/getOutgoingInvitations");
	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);
	private int mType;

	public GetInvitesProcessor(final Context context) {
		super(context);
	}

	@Override
	protected void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras)
			throws IOException, HttpException, Exception {
		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);

		if (extras != null) {
			params.add(EXTRA_CURRENT_PAGE,
					Integer.toString(extras.getInt(EXTRA_CURRENT_PAGE)));
			params.add(EXTRA_ITEMS_PER_PAGE,
					Integer.toString(extras.getInt(EXTRA_ITEMS_PER_PAGE)));
			final int status = extras.getInt(EXTRA_STATUS);
			if (status != Invite.Entity.STATUS_ALL)
				params.add(EXTRA_STATUS,
						Integer.toString(extras.getInt(EXTRA_STATUS)));
				mType = extras.getInt(Invite.Entity.EXTRA_TYPE,
					Invite.Entity.TYPE_INCOMING);
		}

		tryRequest(receiver, false, params, InvitesResponce.class, extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

	@Override
	public Uri getUri() {
		switch (mType) {
		case Invite.Entity.TYPE_INCOMING:
			return URI_INCOMING;
		case Invite.Entity.TYPE_OUTGOING:
			return URI_OUTGOING;
		default:
			return URI_INCOMING;
		}
	}
}
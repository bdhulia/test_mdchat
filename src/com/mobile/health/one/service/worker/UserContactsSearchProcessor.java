package com.mobile.health.one.service.worker;

import java.security.InvalidParameterException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.GetContactsDataResponce;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.Settings;

/**
 * @author Igor Yanishevskiy
 */
public class UserContactsSearchProcessor extends RequestProcessor {

	public static final String EXTRA_PARAM_TOKEN = "token";
	public static final String EXTRA_PARAM_DEVICE_ID = "device_id";
	public static final String EXTRA_SEARCH_TYPE = "search_type";

	public static final String EXTRA_PARAM_USER_SEARCH = "user_search[%s]";
	public static final String EXTRA_PARAM_FORM_NAME = "form_name[%s]";
	public static final String EXTRA_FORM_FIELDS = "form_fields";

	public static final int SEARCH_TYPE_USERS = 1;
	public static final int SEARCH_TYPE_MEMBERS = 2;
	public static final int SEARCH_TYPE_DIRECTORY = 3;

	private static final Uri URI_CONTACTS = Uri
			.parse(Settings.URI_BASE + "/api/api_search/contacts");

	private static final Uri URI_MEMBERS = Uri
			.parse(Settings.URI_BASE + "/api/api_search/members");

	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public UserContactsSearchProcessor(final Context context) {
		super(context);
	}

	private int mType;

	@Override
	public void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras) {

		mType = extras.getInt(EXTRA_SEARCH_TYPE, SEARCH_TYPE_USERS);
		final String[] searches = extras.getStringArray(EXTRA_PARAM_USER_SEARCH);
		final String[] formFields = extras.getStringArray(EXTRA_FORM_FIELDS);

		try {
			if (searches.length != formFields.length)
				throw new InvalidParameterException("EXTRA_USER_SEARCH and EXTRA_FORM_FIELDS arrays should be the same length");
		} catch (final NullPointerException e) {
			throw new InvalidParameterException("EXTRA_USER_SEARCH and EXTRA_FORM_FIELDS arrays should be passed");
		}

		final String token = getSettings().getSessionId();

		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_PARAM_TOKEN, token);
		params.add(EXTRA_PARAM_DEVICE_ID, androidId);
		//params.add("current_page", "0");
		//params.add("per_page", "1000");
		for (int i = 0; i < searches.length; i++) {
			final String searchText = searches[i];
			final String formField = formFields[i];
			final String formattedParam = String.format(EXTRA_PARAM_USER_SEARCH, formField);
			params.add(formattedParam, searchText);
			//params.add("user_search[speciality]", searchText);
		}

		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		tryRequest(receiver, false, params, GetContactsDataResponce.class, extras);

	}

	@Override
	public Uri getUri() {
		switch (mType) {
		case SEARCH_TYPE_USERS:
			return URI_CONTACTS;
		case SEARCH_TYPE_MEMBERS:
		case SEARCH_TYPE_DIRECTORY:
			return URI_MEMBERS;
		default:
			return URI_CONTACTS;
		}
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(final E result) {
		return "Error receiving status text";
	}

}

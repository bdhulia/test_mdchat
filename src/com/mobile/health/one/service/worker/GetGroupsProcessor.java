package com.mobile.health.one.service.worker;

import java.io.IOException;
import java.security.InvalidParameterException;

import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.GroupsResponce;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.Settings;

public class GetGroupsProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_CURRENT_PAGE = "page";
	public static final String EXTRA_ITEMS_PER_PAGE = "per_page";
	public static final String EXTRA_TYPE = "type";

	public static final int TYPE_PUBLIC = 1;
	public static final int TYPE_PRIVATE = 2;

	private static final Uri URI_PUBLIC = Uri.parse(Settings.URI_BASE
			+ "/api/groups_api/getPublicGroups");
	private static final Uri URI_USER = Uri.parse(Settings.URI_BASE
			+ "/api/groups_api/getGroups");
	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);
	private int mType;

	public GetGroupsProcessor(final Context context) {
		super(context);
	}

	@Override
	protected void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras)
					throws IOException, HttpException, Exception {
		if (!getSettings().hasSessionId()) {
			throw new InvalidParameterException("Please login first");
		}

		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);

		if (extras != null) {
			params.add(EXTRA_CURRENT_PAGE, Integer.toString(extras.getInt(EXTRA_CURRENT_PAGE)));
			params.add(EXTRA_ITEMS_PER_PAGE, Integer.toString(extras.getInt(EXTRA_ITEMS_PER_PAGE)));
			mType = extras.getInt(EXTRA_TYPE, TYPE_PUBLIC);
		}

		tryRequest(receiver, false, params, GroupsResponce.class, extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

	@Override
	public Uri getUri() {
		switch (mType) {
		case TYPE_PUBLIC:
			return URI_PUBLIC;
		case TYPE_PRIVATE:
			return URI_USER;
		default:
			return URI_PUBLIC;
		}
	}

}

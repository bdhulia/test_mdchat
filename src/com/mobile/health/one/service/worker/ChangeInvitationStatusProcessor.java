package com.mobile.health.one.service.worker;

import java.io.IOException;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpException;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.os.ResultReceiver;
import android.provider.Settings.Secure;

import com.google.resting.component.RequestParams;
import com.google.resting.component.impl.BasicRequestParams;
import com.mobile.health.one.service.RequestProcessor;
import com.mobile.health.one.service.entity.GetInvitesTemplatesResponse;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.util.Settings;

public class ChangeInvitationStatusProcessor extends RequestProcessor {

	public static final String EXTRA_TOKEN = "token";
	public static final String EXTRA_DEVICE_ID = "device_id";
	public static final String EXTRA_STATUS = "status";
	public static final String EXTRA_IDS = "ids";

	private static final Uri URI = Uri.parse(Settings.URI_BASE
			+ "/api/archer_invite_api/changeInvitationStatus");
	private final String androidId = Secure.getString(getContext()
			.getContentResolver(), Secure.ANDROID_ID);

	public ChangeInvitationStatusProcessor(final Context context) {
		super(context);
	}

	@Override
	protected void processInternal(final ResultReceiver receiver,
			final ContentResolver resolver, final Bundle extras)
					throws IOException, HttpException, Exception {
		final String token = getSettings().getSessionId();
		final RequestParams params = new BasicRequestParams();
		params.add(EXTRA_TOKEN, token);
		params.add(EXTRA_DEVICE_ID, androidId);

		params.add(EXTRA_STATUS, Integer.toString(extras.getInt(EXTRA_STATUS)));
		params.add(EXTRA_IDS, StringUtils.join(ArrayUtils.toObject(extras.getLongArray(EXTRA_IDS))));

		// FIXME: change class of response
		tryRequest(receiver, false, params, GetInvitesTemplatesResponse.class, extras);
	}

	@Override
	protected <E extends IResponce & Parcelable> Bundle prepareExtrasFromResult(
			final E result, final Bundle extras) {
		return extras;
	}

	@Override
	protected <E extends IResponce & Parcelable> String prepareErrorFromResult(
			final E result) {
		return result.getMessages();
	}

	@Override
	public Uri getUri() {
		return URI;
	}

}

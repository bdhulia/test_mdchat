package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import com.mobile.health.one.db.entity.Label;

public class MessageThread implements Parcelable {

	public static class Entity implements BaseColumns {
		public final static String AVATAR_NORMAL = "avatar_normal";
		public final static String MESSAGE = "message";
		public final static String FULL_USER_NAME = "full_user_name";
		public final static String UNREADMESSAGES = "unreadMessages";
		public final static String SUBJECT = "subject";
		public final static String LABELS = "labels";
		public final static String USER_ID = "user_id";
		public final static String DATE = "date";
		public final static String THREAD_ID = "thread_id";
		public final static String AVATAR_SMALL = "avatar_small";
		public final static String MESSAGE_ID = "message_id";
	}

	public final static String[] PROJECTION = new String[] {
		BaseColumns._ID,
		Entity.AVATAR_NORMAL,
		Entity.MESSAGE,
		Entity.FULL_USER_NAME,
		Entity.UNREADMESSAGES,
		Entity.SUBJECT,
		Entity.USER_ID,
		Entity.DATE,
		Entity.THREAD_ID,
		Entity.AVATAR_SMALL
	};

	public long id;
	public String avatar_normal;
	public String message;
	public String full_user_name;
	public String sender_name;
	public int unreadMessages;
	public String subject;
	public Label.Entity[] labels;
	public long user_id;
	public String date;
	public String thread_id;
	public String avatar_small;
	public String sender_avatar;
	public String message_id; // stub to use in messages

	public MessageThread() { }

	public MessageThread(final Parcel in) {
		id = in.readLong();
		avatar_normal = in.readString();
		message = in.readString();
		full_user_name = in.readString();
		unreadMessages = in.readInt();
		subject = in.readString();
		labels = in.createTypedArray(Label.Entity.CREATOR);
		user_id = in.readLong();
		date = in.readString();
		thread_id = in.readString();
		avatar_small = in.readString();
		message_id = in.readString();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeLong(id);
		dest.writeString(avatar_normal);
		dest.writeString(message);
		dest.writeString(full_user_name);
		dest.writeInt(unreadMessages);
		dest.writeString(subject);
		dest.writeTypedArray(labels, flags);
		dest.writeLong(user_id);
		dest.writeString(date);
		dest.writeString(thread_id);
		dest.writeString(avatar_small);
		dest.writeString(message_id);
	}

	public static final Parcelable.Creator<MessageThread> CREATOR = new Parcelable.Creator<MessageThread>() {
		@Override
		public MessageThread createFromParcel(final Parcel in) {
			return new MessageThread(in);
		}

		@Override
		public MessageThread[] newArray(final int size) {
			return new MessageThread[size];
		}
	};

}

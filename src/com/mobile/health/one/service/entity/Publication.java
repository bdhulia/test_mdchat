package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Publication {
	public static class Entity implements Parcelable {
		@SerializedName("title")
		public String title;
		@SerializedName("journal")
		public String journal;
		@SerializedName("volume")
		public String volume;
		@SerializedName("issue")
		public String issue;
		@SerializedName("pages")
		public String pages;
		@SerializedName("authors")
		public String authors;
		@SerializedName("date")
		public String date;

		public Entity(final Parcel in) {
			title = in.readString();
			journal = in.readString();
			volume = in.readString();
			issue = in.readString();
			pages = in.readString();
			authors = in.readString();
			date = in.readString();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(title);
			dest.writeString(journal);
			dest.writeString(volume);
			dest.writeString(issue);
			dest.writeString(pages);
			dest.writeString(authors);
			dest.writeString(date);
		}

		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};
	}
}

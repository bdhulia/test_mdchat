package com.mobile.health.one.service.entity;

import java.util.HashMap;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class GetUserProfileResponse implements Parcelable, IResponce {

	private int code;

	private Data data;

	@Override
	public int getCode() {
		return code;
	}

	public GetUserProfileResponse() {
	}

	public GetUserProfileResponse(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
	}

	private String result;

	public String getResult() {
		return result;
	}

	public Data getData() {
		return data;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, 0);
	}

	@Override
	public String getMessages() {
		return null;
	}

	public static class Data implements Parcelable {
		@SerializedName("medical_education")
		public MedicalEducation.Entity[] medicalEducation;
		@SerializedName("user")
		public UserProfile.Entity user;
		@SerializedName("practice")
		public UserProfilePractice.Entity practice;
		@SerializedName("insurance_plans")
		public InsurancePlan.Entity[] insurancePlans;
		@SerializedName("insurance_accepted")
		public String insuranceAccepted;
		@SerializedName("hospital_affiliations")
		public Affiliation.Entity[] hospitalAffiliations;
		@SerializedName("labels")
		public HashMap<String, String> labels;
		@SerializedName("education")
		public Education.Entity[] education;
		@SerializedName("languages")
		public UserProfileLanguage.Entity[] languages;
		@SerializedName("residency")
		public Residency.Entity residency;
		@SerializedName("fellowships")
		public Fellowship.Entity[] fellowships;
		@SerializedName("academic")
		public Academic.Entity[] academics;
		@SerializedName("publications")
		public Publication.Entity[] publications;

		@SuppressWarnings("unchecked")
		public Data(final Parcel in) {
			user = in.readParcelable(UserProfile.Entity.class.getClassLoader());
			practice = in.readParcelable(UserProfilePractice.Entity.class.getClassLoader());
			residency = in.readParcelable(Residency.class.getClassLoader());
			medicalEducation = in.createTypedArray(MedicalEducation.Entity.CREATOR);
			education = in.createTypedArray(Education.Entity.CREATOR);
			labels = in.readHashMap(String.class.getClassLoader());
			languages = in.createTypedArray(UserProfileLanguage.Entity.CREATOR);
			fellowships = in.createTypedArray(Fellowship.Entity.CREATOR);
			academics = in.createTypedArray(Academic.Entity.CREATOR);
			publications = in.createTypedArray(Publication.Entity.CREATOR);
			insurancePlans = in.createTypedArray(InsurancePlan.Entity.CREATOR);
			insuranceAccepted = in.readString();
			hospitalAffiliations = in.createTypedArray(Affiliation.Entity.CREATOR);
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeParcelable(user, flags);
			dest.writeParcelable(practice, flags);
			dest.writeParcelable(residency, flags);
			dest.writeTypedArray(medicalEducation, flags);
			dest.writeTypedArray(education, flags);
			dest.writeMap(labels);
			dest.writeTypedArray(languages, flags);
			dest.writeTypedArray(fellowships, flags);
			dest.writeTypedArray(academics, flags);
			dest.writeTypedArray(publications, flags);
			dest.writeTypedArray(insurancePlans, flags);
			dest.writeString(insuranceAccepted);
			dest.writeTypedArray(hospitalAffiliations, flags);
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}
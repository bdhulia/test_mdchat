package com.mobile.health.one.service.entity;

import com.google.gson.annotations.SerializedName;
import com.mobile.health.one.service.entity.GetContactsDataResponce.Data;

import android.os.Parcel;
import android.os.Parcelable;

public class GetInvitesCountResponse implements Parcelable, IResponce {

	@SuppressWarnings("unused")
	private static final String TAG = GetInvitesCountResponse.class.getName();

	private int code;

	private String messages;

	private Data data;

	@Override
	public int getCode() {
		return this.code;
	}

	@Override
	public int getCount() {
		return 0;
	}

	@Override
	public String getMessages() {
		return this.messages;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(this.code);
		dest.writeParcelable(this.data, 0);
	}
	
	public Data getData() {
		return this.data;
	}

	public static class Data implements Parcelable {
		@SerializedName("count")
		public Integer count;

		public Data(final Parcel in) {
			this.count = in.readInt();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
		}
	}
}
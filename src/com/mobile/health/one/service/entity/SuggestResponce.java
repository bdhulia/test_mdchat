/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class SuggestResponce implements Parcelable, IResponce {

	public static class Element implements Parcelable {

		public String key;
		public String value;

		public Element(final Parcel in) {
			key = in.readString();
			value = in.readString();
		}

		@Override
		public int describeContents() {
			return key.hashCode() + value.hashCode();
		}

		@Override
		public void writeToParcel(final Parcel out, final int flags) {
			out.writeString(key);
			out.writeString(value);
		}

		public static final Parcelable.Creator<Element> CREATOR = new Parcelable.Creator<Element>() {
			@Override
			public Element createFromParcel(final Parcel in) {
				return new Element(in);
			}

			@Override
			public Element[] newArray(final int size) {
				return new Element[size];
			}
		};

	}

	protected int code;

	private Element[] data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<SuggestResponce> CREATOR = new Parcelable.Creator<SuggestResponce>() {
		@Override
		public SuggestResponce createFromParcel(final Parcel in) {
			return new SuggestResponce(in);
		}

		@Override
		public SuggestResponce[] newArray(final int size) {
			return new SuggestResponce[size];
		}
	};

	public SuggestResponce() {
		code = 0;
	}

	public SuggestResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.createTypedArray(Element.CREATOR);
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Element[] getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeTypedArray(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		return data.length;
	}
}

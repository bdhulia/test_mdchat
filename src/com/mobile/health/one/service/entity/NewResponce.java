/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class NewResponce implements Parcelable, IResponce {

	protected int code;

	private Map<String, String> data;

	protected List<String> invalidParams;
	protected String[] messages;

	public static final Parcelable.Creator<NewResponce> CREATOR = new Parcelable.Creator<NewResponce>() {
		@Override
		public NewResponce createFromParcel(final Parcel in) {
			return new NewResponce(in);
		}

		@Override
		public NewResponce[] newArray(final int size) {
			return new NewResponce[size];
		}
	};

	public NewResponce() {
		code = 0;
		data = new HashMap<String, String>();
	}

	public NewResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = new HashMap<String, String>();
		final int count = in.readInt();
		for (int i = 0; i < count; i++) {
			data.put(in.readString(), in.readString());
		}
		in.readStringList(invalidParams);
		messages = in.createStringArray();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Map<String, String> getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages[0];
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeInt(data.size());
		for (final String s : data.keySet()) {
			dest.writeString(s);
			dest.writeString(data.get(s));
		}
		dest.writeStringList(invalidParams);
		dest.writeStringArray(messages);
	}

	@Override
	public int getCount() {
		return data.size();
	}
}

/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.List;

import android.database.MatrixCursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.mobile.health.one.util.CursorSaver;

/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class InvitesResponce implements Parcelable, IResponce {

	public static class Data implements Parcelable, CursorSaver {
		public Pagination pagination;
		Invite[] invites;

		public Data(final Parcel in) {
			pagination = in.readParcelable(Pagination.class.getClassLoader());
			invites = in.createTypedArray(Invite.CREATOR);
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeParcelable(pagination, flags);
			dest.writeTypedArray(invites, flags);
		}

		public static final Parcelable.Creator<Data> CREATOR = new Parcelable.Creator<Data>() {
			@Override
			public Data createFromParcel(final Parcel in) {
				return new Data(in);
			}

			@Override
			public Data[] newArray(final int size) {
				return new Data[size];
			}
		};

		@Override
		public MatrixCursor saveToCursor() {
			if (invites != null) {
				final MatrixCursor cursor = new MatrixCursor(
						Invite.Entity.PROJECTION, invites.length);
				for (final Invite invite : invites) {
					cursor.newRow().add(invite.invitation_id)
							.add(invite.user_id).add(invite.user_name)
							.add(invite.user_avatar)
							.add(invite.invitation_text)
							.add(invite.invitation_id).add(invite.status);
				}
				return cursor;
			} else
				return null;
		}
	}

	protected int code;

	private Data data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<InvitesResponce> CREATOR = new Parcelable.Creator<InvitesResponce>() {
		@Override
		public InvitesResponce createFromParcel(final Parcel in) {
			return new InvitesResponce(in);
		}

		@Override
		public InvitesResponce[] newArray(final int size) {
			return new InvitesResponce[size];
		}
	};

	public InvitesResponce() {
		code = 0;
	}

	public InvitesResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Data.class.getClassLoader());
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Data getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}

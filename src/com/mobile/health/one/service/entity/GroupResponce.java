/**
 *
 */
package com.mobile.health.one.service.entity;

import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class GroupResponce implements Parcelable, IResponce {

	protected int code;

	private Group data;

	protected List<String> invalidParams;
	protected String messages;

	public static final Parcelable.Creator<GroupResponce> CREATOR = new Parcelable.Creator<GroupResponce>() {
		@Override
		public GroupResponce createFromParcel(final Parcel in) {
			return new GroupResponce(in);
		}

		@Override
		public GroupResponce[] newArray(final int size) {
			return new GroupResponce[size];
		}
	};

	public GroupResponce() {
		code = 0;
	}

	public GroupResponce(final Parcel in) {
		this();
		code = in.readInt();
		data = in.readParcelable(Group.class.getClassLoader());
		invalidParams = in.createStringArrayList();
		messages = in.readString();
	}

	@Override
	public int getCode() {
		return code;
	}

	public Group getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	public List<String> getInvalidParams() {
		return invalidParams;
	}

	@Override
	public int describeContents() {
		return this.hashCode();
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeParcelable(data, flags);
		dest.writeStringList(invalidParams);
		dest.writeString(messages);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}

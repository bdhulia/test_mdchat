/**
 *
 */
package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;


/**
 * General class for json response entities
 *
 * @author Igor Yanishevskiy
 *
 */
public class AutocompleteResponce implements Parcelable, IResponce {

	private int code;
	private String messages;
	private String[] data;

	public static final Parcelable.Creator<AutocompleteResponce> CREATOR = new Parcelable.Creator<AutocompleteResponce>() {
		@Override
		public AutocompleteResponce createFromParcel(final Parcel in) {
			return new AutocompleteResponce(in);
		}

		@Override
		public AutocompleteResponce[] newArray(final int size) {
			return new AutocompleteResponce[size];
		}
	};

	public AutocompleteResponce() {
		code = 0;
	}

	public AutocompleteResponce(final Parcel in) {
		this();
		code = in.readInt();
		messages = in.readString();
		data = in.createStringArray();
	}

	@Override
	public int getCode() {
		return code;
	}

	public String[] getData() {
		return data;
	}

	@Override
	public String getMessages() {
		return messages;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(code);
		dest.writeString(messages);
		dest.writeStringArray(data);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return 0;
	}
}

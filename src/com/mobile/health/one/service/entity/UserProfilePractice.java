package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserProfilePractice {

	public static class Entity implements Parcelable {
		@SerializedName("name")
		public String name;
		@SerializedName("street_1")
		public String street1;
		@SerializedName("street_2")
		public String street2;
		@SerializedName("city")
		public String city;
		@SerializedName("state")
		public String state;
		@SerializedName("zip_code")
		public String zipCode;
		@SerializedName("county")
		public String country;
		@SerializedName("office_phone")
		public String officePhone;
		@SerializedName("office_fax")
		public String officeFax;
		@SerializedName("office_pager")
		public String officePager;

		public Entity(final Parcel in) {
			name = in.readString();
			street1 = in.readString();
			street2 = in.readString();
			city = in.readString();
			state = in.readString();
			country = in.readString();
			zipCode = in.readString();
			officePhone = in.readString();
			officeFax = in.readString();
			officePager = in.readString();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(name);
			dest.writeString(country);
			dest.writeString(city);
			dest.writeString(state);
			dest.writeString(zipCode);
			dest.writeString(street1);
			dest.writeString(street2);
			dest.writeString(officePhone);
			dest.writeString(officeFax);
			dest.writeString(officePager);
		}
	}
}
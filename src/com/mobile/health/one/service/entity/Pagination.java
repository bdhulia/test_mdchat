package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

public class Pagination implements Parcelable {
	public int current_page;
	public int per_page;
	public int total_users_count;
	public int total_groups_count;

	public Pagination(final Parcel in) {
		current_page = in.readInt();
		per_page = in.readInt();
		total_users_count = in.readInt();
		total_groups_count = in.readInt();
	}

	@Override
	public int describeContents() {
		return hashCode();
	}
	@Override
	public void writeToParcel(final Parcel dest, final int flags) {
		dest.writeInt(current_page);
		dest.writeInt(per_page);
		dest.writeInt(total_users_count);
		dest.writeInt(total_groups_count);
	}

	public static final Parcelable.Creator<Pagination> CREATOR = new Parcelable.Creator<Pagination>() {
		@Override
		public Pagination createFromParcel(final Parcel in) {
			return new Pagination(in);
		}

		@Override
		public Pagination[] newArray(final int size) {
			return new Pagination[size];
		}
	};
}
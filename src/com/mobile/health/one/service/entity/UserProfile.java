package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class UserProfile {

	public static class Entity implements Parcelable {
		@SerializedName("user_name")
		public String name;
		@SerializedName("sex")
		public String sex;
		@SerializedName("dob")
		public String dob;
		@SerializedName("home_phone")
		public String homePhone;
		@SerializedName("work_phone")
		public String workPhone;
		@SerializedName("cell_phone")
		public String cellPhone;
		@SerializedName("email")
		public String email;

		@SerializedName("street_1")
		public String street1;
		@SerializedName("street_2")
		public String street2;
		@SerializedName("city")
		public String city;
		@SerializedName("state")
		public String state;
		@SerializedName("zip_code")
		public String zipCode;
		@SerializedName("country")
		public String country;

		@SerializedName("status")
		public Boolean status;
		@SerializedName("status_text")
		public String statusText;
		@SerializedName("avatar_normal")
		public String avatarLink;
		@SerializedName("member_type")
		public String memberType;
		@SerializedName("provider_subtype")
		public String providerSubtype;
		@SerializedName("speciality")
		public String speciality;
		@SerializedName("in_network")
		public boolean inNetwork;

		public Entity(final Parcel in) {
			name = in.readString();
			status = (Boolean) in.readValue(Boolean.class.getClassLoader());
			statusText = in.readString();
			avatarLink = in.readString();
			email = in.readString();

			street1 = in.readString();
			street2 = in.readString();
			city = in.readString();
			state = in.readString();
			zipCode = in.readString();
			country = in.readString();

			homePhone = in.readString();
			cellPhone = in.readString();
			memberType = in.readString();
			providerSubtype = in.readString();
			workPhone = in.readString();
			speciality = in.readString();
			inNetwork = in.readByte() == 1 ? true : false;
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(name);
			dest.writeValue(status);
			dest.writeString(statusText);
			dest.writeString(avatarLink);
			dest.writeString(homePhone);
			dest.writeString(cellPhone);
			dest.writeString(email);
			dest.writeString(memberType);
			dest.writeString(workPhone);
			dest.writeString(speciality);
			dest.writeByte((byte) (inNetwork ? 1 : 0));
		}
	}
}
package com.mobile.health.one.service.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class InsurancePlan {

	public static class Entity implements Parcelable {
		@SerializedName("name")
		public String name;

		public Entity(final Parcel in) {
			name = in.readString();
		}

		@Override
		public int describeContents() {
			return hashCode();
		}

		@Override
		public void writeToParcel(final Parcel dest, final int flags) {
			dest.writeString(name);
		}

		public static final Parcelable.Creator<Entity> CREATOR = new Parcelable.Creator<Entity>() {
			@Override
			public Entity createFromParcel(final Parcel in) {
				return new Entity(in);
			}

			@Override
			public Entity[] newArray(final int size) {
				return new Entity[size];
			}
		};

		@Override
		public String toString() {
			return name;
		};
	}
}
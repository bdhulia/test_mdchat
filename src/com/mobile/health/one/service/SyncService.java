package com.mobile.health.one.service;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.http.HttpException;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

import com.mobile.health.one.util.DebugUtil;
import com.mobile.health.one.util.DetachableResultReceiver;
import com.mobile.health.one.util.DetachableResultReceiver.Receiver;

/**
 * @author Denis Migol
 */
public abstract class SyncService extends IntentService {

	public static final String EXTRA_RECEIVER = "extra_receiver";
	public static final String EXTRA_REQUEST_CODE = "request_code";
	public static final String EXTRA_ERROR_MESSAGE = "error_message";
	public static final String EXTRA_CODE = "extra_code";

	public static final int STATUS_RUNNING = 1;
	public static final int STATUS_ERROR = 2;
	public static final int STATUS_FINISHED = 3;

	private final String name;
	private static boolean online = false;

	public SyncService(final String name) {
		super(name);
		this.name = name;
	}

	protected String getServiceName() {
		return this.name;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		DebugUtil.logServiceCreated(this.name);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		DebugUtil.logServiceDestroyed(this.name);
	}

	/**
	 * Name convention must be respected<br/>
	 *  request parameter is first part of processor will be called.
	 *  <p>
	 *  for ex. <code>request = "login";</code> that means <b>\this package\.worker.LoginProcessor</b> will be called<br/>
	 *  processor must be named <b>\request name\Processor</b>
	 * @param receiver
	 * @param request name part of request processor
	 */
	@SuppressLint("DefaultLocale")
	protected final void process(final Intent intent) {
		final ResultReceiver receiver = intent.getParcelableExtra(EXTRA_RECEIVER);
		String request = intent.getStringExtra(EXTRA_REQUEST_CODE);

		request = request.substring(0, 1).toUpperCase() + request.substring(1); // convert first letter to upper case

		Class<?> clazz;
		final String url = "";
		RequestProcessor processor = null;
		try {
			clazz = Class.forName(SyncService.class.getPackage().getName() + ".worker." + request + "Processor");
			final Constructor<?> ctor = clazz.getConstructor(Context.class);
			processor = (RequestProcessor) ctor.newInstance(new Object[] { this });

		} catch (final ClassNotFoundException e1) {
			DebugUtil.logErrorProcessing(this.name, url, e1);
		} catch (final NoSuchMethodException e) {
			DebugUtil.logErrorProcessing(this.name, url, e);
		} catch (final IllegalArgumentException e) {
			DebugUtil.logErrorProcessing(this.name, url, e);
		} catch (final InstantiationException e) {
			DebugUtil.logErrorProcessing(this.name, url, e);
		} catch (final IllegalAccessException e) {
			DebugUtil.logErrorProcessing(this.name, url, e);
		} catch (final InvocationTargetException e) {
			DebugUtil.logErrorProcessing(this.name, url, e);
		}

		DebugUtil.logStartProcessing(this.name, url);
		//receiver.send(STATUS_RUNNING, null);

		try {

			processor.process(receiver, getContentResolver(), intent.getExtras());

			//receiver.send(STATUS_FINISHED, null);
		} catch (final IOException e) {
			handleException(receiver, url, "Couldn't connect to server", e);
		} catch (final HttpException e) {
			handleException(receiver, url, "Server error", e);
		} catch (final Exception e) {
			handleException(receiver, url, "Unexpected error", e);
		}
		DebugUtil.logEndProcessing(this.name, url);
	}

	protected final void handleException(final ResultReceiver receiver, final String url, final String message,
			final Exception e) {
		DebugUtil.logErrorProcessing(this.name, url, e);

		final Bundle extras = new Bundle();
		extras.putString(EXTRA_ERROR_MESSAGE, message);
		receiver.send(STATUS_ERROR, extras);
	}

	@Override
	protected void onHandleIntent(final Intent intent) {
		process(intent);
	}

	/**
	 * Callback for storing status of sync operation
	 * offline - no network or can't connect to the server
	 * online - everything is ok
	 * 
	 * the default implementation - set static variable
	 * @param online
	 */
	protected void onChangeOnlineStatus(final boolean online) {
		SyncService.online = online;
	}

	public static boolean isOnline() {
		return online;
	}


	/**
	 * Starts SyncService
	 * (static version for using in other context rather that nested activity)
	 * 
	 * @param uri
	 * @param serviceRequest
	 * @param receiver
	 * @param extras
	 * @param startQuery
	 */
	public static void startSyncService(final Context context, final String serviceRequest, final Receiver receiver,
			final Bundle extras) {
		final DetachableResultReceiver resultReceiver = new DetachableResultReceiver(new Handler(context.getMainLooper()));
		resultReceiver.setReceiver(receiver);

		final Intent syncIntent = new Intent(context, AppSyncService.class);
		syncIntent.putExtra(SyncService.EXTRA_RECEIVER, resultReceiver);
		syncIntent.putExtra(SyncService.EXTRA_REQUEST_CODE, serviceRequest);
		if (extras != null) {
			syncIntent.putExtras(extras);
		}
		context.startService(syncIntent);
	}

	public static void stopSyncService(final Context context) {
		final Intent syncIntent = new Intent(context, AppSyncService.class);
		context.stopService(syncIntent);
	}
}

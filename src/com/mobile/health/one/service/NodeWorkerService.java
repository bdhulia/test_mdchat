package com.mobile.health.one.service;

import java.io.IOException;
import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Parcelable;
import android.provider.BaseColumns;
import android.util.Log;
import android.util.Pair;

import com.codebutler.android_websockets.SocketIOClient;
import com.google.gson.Gson;
import com.mobile.health.one.db.entity.Confirm;
import com.mobile.health.one.db.entity.Conversation;
import com.mobile.health.one.db.entity.Label;
import com.mobile.health.one.db.entity.Message;
import com.mobile.health.one.db.entity.User;
import com.mobile.health.one.service.entity.IResponce;
import com.mobile.health.one.service.entity.MessageThread;
import com.mobile.health.one.util.DateFormatUtil;
import com.mobile.health.one.util.Settings;
import com.mobile.health.one.util.SettingsManager;

public class NodeWorkerService extends Service {
	private static final String TAG = "NodeWorkerService";

	private interface CallbackReceiver {
		public Handler getHandler();
	}

	public interface IMessageReceiver extends CallbackReceiver {

		public void onMessageCount(int count);

		public void onMessageReceived(HashMap<String, Object> message);
	}

	public interface IConfirmReceiver extends CallbackReceiver {
		public void onConfirmsReceived(List<Confirm.Entity> confirms);
	}

	public interface IMessageInThreadReceiver extends CallbackReceiver {
		/**
		 * @param pair
		 *            a pair of received messages and labels pair.first -
		 *            messages pair.second - labels
		 */
		public void onReceiveMessages(
				Pair<ArrayList<HashMap<String, Object>>, HashMap<String, String>> pair);
	}

	public interface ILabelsReceiver extends CallbackReceiver {
		public void onLabelsReceived(List<Label.Entity> labels);

		public void onMoveThreadToLabel(List<Label.Entity> labels,
				Label.Entity newLabel);
	}

	public interface IThreadsReceiver extends CallbackReceiver {
		public void onReceiveThreads(List<MessageThread> conversations);
	}

	public interface IInvitesReceiver extends CallbackReceiver {
		public void onNewInvite();

		public void onNewGroupInvite();

		public void onInvitesCount(int count);
	}

	public interface IStatusChangeReceiver extends CallbackReceiver {
		public void onStatusTextChanged(long userId, String statusText);

		public void onStatusChanged(long userId, boolean isAvailable);
	}

	/** Command to the service to display a message */
	static final int MSG_SEND_MESSAGE = 1;

	private static final URI URI = java.net.URI.create(Settings.URI_BASE
			+ ":8888");
	// private static final URI URI = java.net.URI.create("https://google.com");

	private SocketIOClient client;

	private HashSet<IMessageReceiver> messageCallbackRef;

	private HashSet<IConfirmReceiver> confirmCallbackRef;

	private HashSet<ILabelsReceiver> labelsCallbackRef;

	private HashSet<IThreadsReceiver> threadsCallbackRef;

	private HashSet<IMessageInThreadReceiver> messagesInThreadCallbackRef;

	private HashSet<IInvitesReceiver> invitesCallbackRef;

	private HashSet<IStatusChangeReceiver> statusCallbackRef;

	private final SimpleDateFormat df = new SimpleDateFormat(
			"yyyy-MM-dd kk:mm:ss z", Locale.getDefault());

	private class DefaultSocketHandler implements SocketIOClient.Handler {

		private static final int RECONNECT_PERIOD = 3 * 1000; // 3 secs

		private boolean mConnected;

		private Timer mReconnectTimer;

		@Override
		public void onConnect() {
			Log.v("DefaultSocketHandler", "connected");
			mConnected = true;
			final SettingsManager settings = new SettingsManager(getApplicationContext());
			if (settings.hasPin() && settings.hasSessionId()) {
				serverAuth(settings.getSessionId());
			}
		}

		@Override
		public void on(final String event, final JSONArray arguments) {
			Log.i("DefaultSocketHandler", "event: " + event + ", args: "
					+ arguments.toString());
			if ("message".equals(event)) {
				callOnMessage(arguments);
			} else if ("unread messages count".equals(event)) {
				try {
					callOnMessageCount(arguments);
				} catch (final JSONException e) {
					e.printStackTrace();
				}
			} else if ("get all labels response".equals(event)) {
				callOnGetAllLabels(arguments);
			} else if ("move thread to label response".equals(event)) {
				callOnMoveThreadToLabel(arguments);
			} else if ("get threads in label response".equals(event)) {
				callOnGetThreadsInLabel(arguments);
			} else if ("confirmation messages response".equals(event)) {
				callOnConfirmMessage(arguments);
			} else if ("get messages in thread response".equals(event)) {
				callOnGetMessagesInThread(arguments);
			} else if ("new invite".equals(event)) {
				callOnInvite(arguments);
			} else if ("new group invite".equals(event)) {
				callOnGroupInvite(arguments);
			} else if ("confirmation messages".equals(event)) {
				callOnConfirmMessage(arguments);
			} else if ("change icon status response".equals(event)) {
				callOnStatusResponce(arguments);
			} else if ("change text status response".equals(event)) {
				callOnStatusTextResponce(arguments);
			} else if ("unread invitations count".equals(event)) {
				try {
					callOnInvitesCount(arguments);
				} catch (final JSONException e) {
					e.printStackTrace();
				}
			}
		}

		@Override
		public void onDisconnect(final int code, final String reason) {
			Log.v("DefaultSocketHandler", "disconnected. trying to reconnect");
			mConnected = false;
			reconnect();
		}

		@Override
		public void onError(final Exception error) {
			Log.e("DefaultSocketHandler", "error", error);
			mConnected = false;
			// reconnect on error
			reconnect();
		}

		/**
		 * Trying to reconnect every RECONNECT_PERIOD secs
		 */
		private void reconnect() {
			if (mReconnectTimer != null) {
				mReconnectTimer.cancel();
				mReconnectTimer = null;
			}

			mReconnectTimer = new Timer();
			try {
				mReconnectTimer.scheduleAtFixedRate(new TimerTask() {

					@Override
					public void run() {
						if (mConnected) {
							mReconnectTimer.cancel();
						} else {
							client.connect();
						}
					}
				}, RECONNECT_PERIOD, RECONNECT_PERIOD); // schedule at
			} catch (final IllegalStateException e) {
				Log.e(TAG, "Timer already cancelled. This should not happed because new timer created");
			}
			// RECONNECT_PERIOD time
		}

		@Override
		public void onJSON(final JSONObject json) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMessage(final String message) {
			// TODO Auto-generated method stub

		}

	}

	/**
	 * Use local binding, we don't use cross process communication
	 *
	 * @author rimmer
	 *
	 */
	public class NodeWorkerBinder extends Binder {
		public NodeWorkerService getService() {
			return NodeWorkerService.this;
		}
	}

	@Override
	public IBinder onBind(final Intent intent) {
		return new NodeWorkerBinder();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		messageCallbackRef = new HashSet<NodeWorkerService.IMessageReceiver>();
		confirmCallbackRef = new HashSet<NodeWorkerService.IConfirmReceiver>();
		labelsCallbackRef = new HashSet<NodeWorkerService.ILabelsReceiver>();
		threadsCallbackRef = new HashSet<NodeWorkerService.IThreadsReceiver>();
		messagesInThreadCallbackRef = new HashSet<NodeWorkerService.IMessageInThreadReceiver>();
		invitesCallbackRef = new HashSet<NodeWorkerService.IInvitesReceiver>();
		statusCallbackRef = new HashSet<NodeWorkerService.IStatusChangeReceiver>();
		client = new SocketIOClient(URI, new DefaultSocketHandler());
		client.connect();
		// WebSocketClient.setTrustManagers(new TrustManager[] { new
		// LocalSSLTrustManager() });
		Log.v("NodeWorkerService", "created");
	}

	/**
	 * Auth on server with token
	 *
	 * @param token
	 * @throws JSONException
	 */
	public void serverAuth(final String token) {
		Log.i(TAG, "::serverAuth:" + "");
		final JSONObject params = new JSONObject();
		try {
			params.put("token", token);
			client.emit("token", new JSONArray().put(params));
		} catch (final JSONException e) {
			e.printStackTrace();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	// / Messages ----------------------------------------------------

	/**
	 * Create thread with specified message
	 *
	 * @param conversation
	 * @param message
	 * @throws JSONException
	 */
	public void sendMessage(final String subject, final String[] users,
			final Message.Entity message, final String filename, final boolean forward, final String threadId)
			throws JSONException {
		final JSONObject obj = new JSONObject();
		obj.put("users", StringUtils.join(users, ','));
		obj.put("subject", subject);
		obj.put("message", message.message);
		obj.put("forward", forward ? 1 : 0);
		if (forward) {
			obj.put("threadId", threadId);
		}
		if (message.attachments != null) {
			obj.put("file", message.attachments);
			obj.put("fileName", filename);
		}
		try {
			client.emit("create thread", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(final String subject, final String[] users,
			final Message.Entity message, final String filename)
			throws JSONException {
		sendMessage(subject, users, message, filename, false, null);
	}


	/**
	 * Reply message on specified thread
	 *
	 * @param threadId
	 * @param message
	 * @throws JSONException
	 */
	public void replyMessage(final String threadId, final String message,
			final String encodedFile, final String filename)
			throws JSONException {
		final JSONObject obj = new JSONObject();
		obj.put("threadId", threadId);
		obj.put("message", message);
		obj.put("file", encodedFile);
		obj.put("fileName", filename);
		try {
			client.emit("reply message", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Mark message as read. No answer from server
	 *
	 * @param messageId
	 * @throws JSONException
	 */
	public void markMessageRead(final String messageId) {
		final JSONObject obj = new JSONObject();
		try {
			obj.put("message", messageId);
			client.emit("read message", new JSONArray().put(obj));
		} catch (final JSONException e) {
			Log.e(TAG, "Error in markMessageRead", e);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Mark message as read. No answer from server
	 *
	 * @param messageId
	 * @throws JSONException
	 */
	public void deleteMessageThread(final String threadId) {
		final JSONObject obj = new JSONObject();
		try {
			obj.put("threadId", threadId);
			client.emit("delete_messages_in_thread", new JSONArray().put(obj));
		} catch (final JSONException e) {
			Log.e(TAG, "Error in markMessageRead", e);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Mark message as read. No answer from server
	 *
	 * @param messageId
	 * @throws JSONException
	 */
	public void getUnreadMessagesCount() {
		final JSONObject obj = new JSONObject();
		try {
			client.emit("get unread messages", new JSONArray().put(obj));
		} catch (final JSONException e) {
			Log.e(TAG, "Error in markMessageRead", e);
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void unregisterMessageCallback(final IMessageReceiver messageCallback) {
		messageCallbackRef.remove(messageCallback);
	}

	public void registerMessageCallback(final IMessageReceiver messageCallback) {
		messageCallbackRef.add(messageCallback);
	}

	// / labels ----------------------------------------------------

	public void getAllLabels() {
		try {
			client.emit("get all labels", new JSONArray());
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void getThreadsInLabel(final String labelId, final int offset,
			final int count) {
		try {
			final JSONObject obj = new JSONObject();
			if (StringUtils.isNotEmpty(labelId))
				obj.put("labelId", labelId);
			obj.put("offset", Integer.toString(offset));
			obj.put("count", Integer.toString(count));

			client.emit("get threads in label", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void getAllThreadsInLabel(final String labelId) {
		getThreadsInLabel(labelId, 0, 0);
	}

	public void getAllThreads() {
		getAllThreadsInLabel(null);
	}

	public void addLabelToThread(final String labelId, final String threadId) {
		try {
			final JSONObject obj = new JSONObject();
			obj.put("labelId", labelId);
			obj.put("threadId", threadId);

			client.emit("add label to thread", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void moveThreadToLabel(final String labelId, final String threadId) {
		try {
			final JSONObject obj = new JSONObject();
			obj.put("labelId", labelId);
			obj.put("threadId", threadId);

			client.emit("move thread to label", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void removeLabelFromThread(final String labelId,
			final String threadId) {
		try {
			final JSONObject obj = new JSONObject();
			obj.put("labelId", labelId);
			obj.put("threadId", threadId);

			client.emit("remove label from thread", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void unregisterLabelsCallback(final ILabelsReceiver labelsCallback) {
		labelsCallbackRef.remove(labelsCallback);
	}

	public void registerLabelsCallback(final ILabelsReceiver labelsCallback) {
		labelsCallbackRef.add(labelsCallback);
	}

	public void registerThreadsCallback(final IThreadsReceiver threadsReceiver) {
		threadsCallbackRef.add(threadsReceiver);
	}

	public void unregisterThreadsCallback(final IThreadsReceiver threadsReceiver) {
		threadsCallbackRef.remove(threadsReceiver);
	}

	// / messages in thread -----------------------------------------------

	public void getMessagesInThread(final String threadId, final int offset,
			final int count) {
		try {
			final JSONObject obj = new JSONObject();
			obj.put("threadId", threadId);
			obj.put("offset", Integer.toString(offset));
			obj.put("count", Integer.toString(count));

			client.emit("get messages in thread", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void getMessagesInThread(final String threadId) {
		getMessagesInThread(threadId, 0, Integer.MAX_VALUE);
	}

	public void registerMessagesInThreadCallback(
			final IMessageInThreadReceiver messagesReceiver) {
		messagesInThreadCallbackRef.add(messagesReceiver);
	}

	public void unregisterMessagesInThreadCallback(
			final IMessageInThreadReceiver messagesReceiver) {
		messagesInThreadCallbackRef.remove(messagesReceiver);
	}

	// / confirmations ----------------------------------------------------
	public void getConfirmMessages(final String messageId) throws JSONException {
		final JSONObject obj = new JSONObject();
		obj.put("messageId", messageId);
		try {
			client.emit("confirmation messages", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void unregisterConfirmCallback(final IConfirmReceiver confirmCallback) {
		confirmCallbackRef.remove(confirmCallback);
	}

	public void registerConfirmCallback(final IConfirmReceiver confirmCallback) {
		confirmCallbackRef.add(confirmCallback);
	}

	// / invites ----------------------------------------------------------
	public void unregisterInvitesCallback(final IInvitesReceiver invitesCallback) {
		invitesCallbackRef.remove(invitesCallback);
	}

	public void registerInvitesCallback(final IInvitesReceiver invitesCallback) {
		invitesCallbackRef.add(invitesCallback);
	}

	// / status changes
	// Subscribe to text status changes

	public void unregisterStatusChangesCallback(
			final IStatusChangeReceiver statusCallback) {
		statusCallbackRef.remove(statusCallback);
	}

	public void registerStatusChangesCallback(
			final IStatusChangeReceiver statusCallback, final String userId) {
		addListenTextStatus(userId);
		addListenIconStatus(userId);
		statusCallbackRef.add(statusCallback);
	}

	public void addListenTextStatus(final String userId) {
		try {
			final JSONObject obj = new JSONObject();
			obj.put("userId", userId);

			client.emit("add listen text status", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void addListenIconStatus(final String userId) {
		try {
			final JSONObject obj = new JSONObject();
			obj.put("userId", userId);

			client.emit("add listen icon status", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void changeTextStatus(final String status) {
		try {
			final JSONObject obj = new JSONObject();
			obj.put("status", status);

			client.emit("change text status", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	public void changeIconStatus(final boolean status) {
		try {
			final JSONObject obj = new JSONObject();
			obj.put("status", status ? "1" : "0");

			client.emit("change icon status", new JSONArray().put(obj));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * change status response
	 *
	 * @param arguments
	 */
	private void callOnStatusResponce(final JSONArray arguments) {
		if (statusCallbackRef != null) {
			// TODO: implement message receive
			try {
				final JSONObject statusObj = arguments.getJSONObject(0);
				for (final IStatusChangeReceiver sr : statusCallbackRef) {
					if (sr != null) {
						final long userId = statusObj.getLong("userId");
						final String statusStr = statusObj.getString("status");
						final boolean status = statusStr.equals("1") || statusStr.equals("true") ? true
								: false;
						sr.getHandler().post(new Runnable() {

							@Override
							public void run() {
								sr.onStatusChanged(userId, status);
							}
						});
					}
				}
			} catch (final JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * change text status response
	 *
	 * @param arguments
	 */
	private void callOnStatusTextResponce(final JSONArray arguments) {
		if (statusCallbackRef != null) {
			try {
				final JSONObject statusObj = arguments.getJSONObject(0);
				for (final IStatusChangeReceiver sr : statusCallbackRef) {
					if (sr != null) {
						final long userId = statusObj.getLong("userId");
						final String status = statusObj.getString("status");
						sr.getHandler().post(new Runnable() {

							@Override
							public void run() {
								sr.onStatusTextChanged(userId, status);
							}
						});
					}
				}
			} catch (final JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	// / ------------------------------
	@Override
	public void onDestroy() {
		try {
			client.disconnect();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		super.onDestroy();
	}

	@SuppressWarnings("unused")
	private <E extends IResponce & Parcelable> E getFromJson(final String json,
			final Class<E> responce) {
		final Gson gson = new Gson();
		return gson.fromJson(json, responce);
	}

	/**
	 * callback func calls
	 */

	// / messages ----------------------------------------------------
	private void callOnMessage(final JSONArray arguments) {

		if (messageCallbackRef != null) {
			final JSONObject jsonThread;
			//TODO
			try {
				jsonThread = arguments.getJSONObject(0);

				final HashMap<String, Object> message = new HashMap<String, Object>();

				final String subject = jsonThread.optString("subject", "");

				final String messageId = jsonThread.getString("message_id");
				message.put(BaseColumns._ID, messageId.hashCode());
				message.put(Message.REMOTE_ID, messageId);
				message.put(Message.BODY, jsonThread.getString("message"));
				message.put(Message.IS_READ_BY_ALL,
						jsonThread.optBoolean("read"));
				final String user = jsonThread.optString("user");
				message.put(User.FULL_NAME, jsonThread.optString("full_name", user));
				message.put(Conversation.TITLE, subject);
				// message.put(User._ID, jsonThread.getString("user_id"));

				message.put(User.AVATAR_LINK,
						jsonThread.getString("avatar_small"));
				/*
				final JSONObject attachJSON = jsonThread
						.optJSONObject("attachments");
				Pair<String, String> attach = null;
				String attachName;
				String attachLink;
				if (attachJSON != null) {
					attachName = attachJSON.optString("fileName");
					attachLink = attachJSON.optString("link");
					if (!StringUtils.isEmpty(attachName) && !StringUtils.isEmpty(attachLink)) {
						attach = new Pair<String, String>(attachName, attachLink);
					}
				}

				message.put(Message.ATTACHMENT, attach);

				// add date at the end
				final String dateStr = jsonThread.getString("create_at_api");
				final Date date = df.parse(dateStr);
				message.put(Message.CREATED_DATE, date.getTime());
				*/
				for (final IMessageReceiver mc : messageCallbackRef) {
					if (mc != null)
						mc.getHandler().post(new Runnable() {

							@Override
							public void run() {
								mc.onMessageReceived(message);
							}
						});

				}

			} catch (final JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			/*} catch (final ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();*/
			}


		}
		/*
		final HashMap<String, Object> message = new HashMap<String, Object>();
		for (final IMessageReceiver mc : messageCallbackRef) {
				if (mc != null)
					mc.getHandler().post(new Runnable() {

						@Override
						public void run() {
							mc.onMessageReceived(message);
						}
					});

			}
		}
		*/
	}

	private void callOnMessageCount(final JSONArray arguments)
			throws JSONException {
		if (messageCallbackRef != null) {
			final int count = arguments.getInt(0);
			for (final IMessageReceiver mc : messageCallbackRef) {
				if (mc != null) {
					mc.getHandler().post(new Runnable() {

						@Override
						public void run() {
							mc.onMessageCount(count);
						}
					});
				}
			}
		}
	}

	private void callOnInvitesCount(final JSONArray arguments)
			throws JSONException {
		if (invitesCallbackRef != null) {
			final int count = arguments.getInt(0);
			for (final IInvitesReceiver receiver : invitesCallbackRef) {
				if (receiver != null) {
					receiver.getHandler().post(new Runnable() {

						@Override
						public void run() {
							receiver.onInvitesCount(count);
						}
					});
				}
			}
		}
	}

	// / labels ----------------------------------------------------
	private void callOnGetThreadsInLabel(final JSONArray arguments) {
		if (threadsCallbackRef != null) {

			final JSONArray array = arguments.optJSONObject(0).optJSONArray(
					"threads");
			final int length = array.length();
			final List<MessageThread> conversations = new ArrayList<MessageThread>(
					length);
			for (int i = 0; i < length; i++) {
				final MessageThread conversation = new MessageThread();

				JSONObject jsonThread;
				try {
					jsonThread = array.getJSONObject(i);
					conversation.thread_id = jsonThread.getString("thread_id");
					conversation.subject = jsonThread.getString("subject");
					conversation.message = jsonThread.getString("message");
					conversation.full_user_name = jsonThread
							.getString("full_user_name");
					conversation.unreadMessages = jsonThread
							.getInt("unreadMessages");
					conversation.user_id = jsonThread.getLong("user_id");

					conversation.date = jsonThread.getString("date");
					conversation.avatar_small = jsonThread
							.getString("avatar_small");
				} catch (final JSONException e) {
					e.printStackTrace();
				}
				conversations.add(conversation);

			}
			for (final IThreadsReceiver tr : threadsCallbackRef) {
				if (tr != null) {
					tr.getHandler().post(new Runnable() {

						@Override
						public void run() {
							tr.onReceiveThreads(conversations);
						}
					});
				}
			}
		}
	}

	private void callOnGetMessagesInThread(final JSONArray arguments) {
		if (threadsCallbackRef != null) {

			try {
				final JSONObject jsonThread = arguments.getJSONObject(0);
				final JSONObject messagesJson = jsonThread
						.getJSONObject("messages");
				final JSONArray names = messagesJson.names();
				if (names == null)
					throw new IllegalStateException("message already deleted");

				final int lenght = names.length();

				final String[] columnNames = new String[] { BaseColumns._ID,
						Message.REMOTE_ID, Message.BODY,
						Message.IS_READ_BY_ALL, User.FULL_NAME,
						Conversation.TITLE, User.AVATAR_LINK,
						Message.ATTACHMENT, Message.CREATED_DATE, };
				final String subject = jsonThread.getString("subject");
				final ArrayList<HashMap<String, Object>> arrayData = new ArrayList<HashMap<String, Object>>(
						lenght);
				for (int i = 0; i < lenght; i++) {
					final HashMap<String, Object> message = new HashMap<String, Object>(
							columnNames.length);

					final String count = names.getString(i);
					final JSONObject insideObj = new JSONObject(
							messagesJson.getString(count));
					final String messageId = insideObj.getString("message_id");
					message.put(BaseColumns._ID, messageId.hashCode());
					message.put(Message.REMOTE_ID, messageId);
					message.put(Message.BODY, insideObj.getString("message"));
					message.put(Message.IS_READ_BY_ALL,
							insideObj.getBoolean(Message.IS_READ_BY_ALL));
					message.put(User.FULL_NAME,
							insideObj.getString("sender_name"));
					message.put(Conversation.TITLE, subject);
					// message.put(User._ID, jsonThread.getString("user_id"));

					message.put(User.AVATAR_LINK,
							insideObj.getString("avatar_small"));

					final JSONObject attachJSON = insideObj
							.optJSONObject("attachments");
					Pair<String, String> attach = null;
					String attachName;
					String attachLink;
					if (attachJSON != null) {
						attachName = attachJSON.getString("fileName");
						attachLink = attachJSON.getString("link");
						if (StringUtils.isNotEmpty(attachName)) {
							attach = new Pair<String, String>(attachName,
									attachLink);
						}
					}

					message.put(Message.ATTACHMENT, attach);

					// add date at the end
					final String dateStr = insideObj.getString("create_at");
					final Date date = df.parse(dateStr);
					message.put(Message.CREATED_DATE, date.getTime());

					arrayData.add(message);
				}
				// TODO: not the fastest and smartest way to sort data. should
				// implement something more optimized
				// for example sort only date and use CursorWrapper to pass
				// sorted indexes
				Collections.sort(arrayData,
						new Comparator<HashMap<String, Object>>() {

							@Override
							public int compare(
									final HashMap<String, Object> lhs,
									final HashMap<String, Object> rhs) {
								final Long date1 = (Long) lhs
										.get(Message.CREATED_DATE); // date
								final Long date2 = (Long) rhs
										.get(Message.CREATED_DATE); // date
								return date2.compareTo(date1);
							}
						});

				final JSONObject labelsJson = jsonThread
						.getJSONObject("labels");
				final JSONArray labelIds = labelsJson.names();
				final HashMap<String, String> labels = new HashMap<String, String>(
						labelsJson.length());
				if (labelIds != null) {
					for (int i = 0; i < labelIds.length(); i++) {
						final String key = labelIds.getString(i);
						final String value = labelsJson.getString(key);
						labels.put(key, value);
					}
				}

				for (final IMessageInThreadReceiver mr : messagesInThreadCallbackRef) {
					if (mr != null) {
						mr.getHandler().post(new Runnable() {

							@Override
							public void run() {
								final Pair<ArrayList<HashMap<String, Object>>, HashMap<String, String>> pair = new Pair<ArrayList<HashMap<String, Object>>, HashMap<String, String>>(
										arrayData, labels);
								mr.onReceiveMessages(pair);
							}
						});
					}
				}
			} catch (final JSONException e) {
				e.printStackTrace();
			} catch (final ParseException e) {
				e.printStackTrace();
			} catch (final IllegalStateException e) {
				e.printStackTrace();
			}
		}
	}

	private void callOnGetAllLabels(final JSONArray arguments) {
		if (labelsCallbackRef != null) {
			Log.i(TAG, "::labelsCallbackRef:" + arguments.toString());
			final List<Label.Entity> labels = new ArrayList<Label.Entity>();
			for (int i = 0; i < arguments.length(); i++) {
				try {
					final JSONObject o = new JSONObject(arguments.getString(i));
					final JSONArray names = o.names();
					Label.Entity rootLabel = null;
					for (int j = 0; j < names.length(); j++) {
						final String remoteId = names.getString(j);
						final JSONObject insideObj = new JSONObject(
								o.getString(remoteId));
						final Label.Entity label = new Label.Entity();
						label.id = remoteId;
						//final JSONArray threads = insideObj.optJSONArray("threads");
						label.count = insideObj.optInt("count");
						label.labelName = insideObj.getString("labelName");
						label.isRoot = insideObj.getBoolean("isRoot");
						label.visible = insideObj.getString("visible");
						if (!label.isRoot)
							labels.add(label);
						else
							rootLabel = label;
					}
					if (rootLabel != null)
						labels.add(0, rootLabel);

				} catch (final JSONException e) {
					e.printStackTrace();
				}

			}
			for (final ILabelsReceiver lr : labelsCallbackRef) {
				if (lr != null) {
					lr.getHandler().post(new Runnable() {

						@Override
						public void run() {
							lr.onLabelsReceived(labels);
						}
					});
				}
			}
		}
	}

	private void callOnMoveThreadToLabel(final JSONArray arguments) {
		if (labelsCallbackRef != null) {
			Log.i(TAG, "::labelsCallbackRef:" + arguments.toString());
			final List<Label.Entity> labels = new ArrayList<Label.Entity>();
			for (int i = 0; i < arguments.length(); i++) {
				try {
					final JSONObject o = new JSONObject(arguments.getString(i));
					final JSONArray names = o.names();
					for (int j = 0; j < names.length(); j++) {
						final String remoteId = names.getString(j);
						final JSONObject insideObj = new JSONObject(
								o.getString(remoteId));
						final Label.Entity label = new Label.Entity();
						label.id = remoteId;
						label.count = insideObj.getInt("count");
						label.labelName = insideObj.getString("labelName");

						labels.add(label);
					}
				} catch (final JSONException e) {
					e.printStackTrace();
				}

			}
			for (final ILabelsReceiver lr : labelsCallbackRef) {
				if (lr != null) {
					lr.getHandler().post(new Runnable() {

						@Override
						public void run() {
							lr.onLabelsReceived(labels);
						}
					});
				}
			}
		}
	}

	// / confirmations ----------------------------------------------------
	private void callOnConfirmMessage(final JSONArray arguments) {
		if (confirmCallbackRef != null) {
			Log.i(TAG, "::callOnConfirmMessage:" + arguments.toString());
			final List<Confirm.Entity> confirms = new ArrayList<Confirm.Entity>();

			try {
				final JSONArray jsonConfirm = arguments.getJSONArray(0);
				final int length = jsonConfirm.length();

				for (int i = 0; i < length; i++) {
					final JSONObject object = jsonConfirm.getJSONObject(i);

					final Confirm.Entity confirm = new Confirm.Entity();
					confirm.dateConf = object.getString(Confirm.DATE_CONF);
					final String dateStr = object.getString(Confirm.DATE);
					confirm.date = "false".equals(dateStr) ? 0 : DateFormatUtil.getFormattedDateEpoch(dateStr).getTime();
					confirm.id = object.getLong("userId");
					confirm.userName = object.getString(Confirm.USER_NAME);

					final String status = object.optString(Confirm.STATUS, "0");
					confirm.status = status.equals("1") || status.equals("true") ? true : false;

					confirms.add(confirm);
				}
			} catch (final JSONException e) {
				e.printStackTrace();
			}

			for (final IConfirmReceiver confirmReceiver : confirmCallbackRef) {
				if (confirmReceiver != null) {
					confirmReceiver.getHandler().post(new Runnable() {
						@Override
						public void run() {
							confirmReceiver.onConfirmsReceived(confirms);
						}
					});
				}
			}
		}
	}

	// / invites -----------------------------------------------------------

	private void callOnInvite(final JSONArray arguments) {
		if (invitesCallbackRef != null) {

			for (final IInvitesReceiver rc : invitesCallbackRef) {
				if (rc != null) {
					rc.getHandler().post(new Runnable() {

						@Override
						public void run() {
							rc.onNewInvite();
						}
					});
				}
			}

		}
	}

	private void callOnGroupInvite(final JSONArray arguments) {
		if (invitesCallbackRef != null) {

			for (final IInvitesReceiver rc : invitesCallbackRef) {
				if (rc != null) {
					rc.getHandler().post(new Runnable() {

						@Override
						public void run() {
							rc.onNewGroupInvite();
						}
					});
				}
			}

		}
	}

}

package com.mobile.health.one.util;

import android.util.Log;

/**
 * @author Denis Migol
 */
public final class DebugUtil {
	private static String TAG = "Debug";
	
	private DebugUtil() {
	}

	public static final void logServiceCreated(final String serviceName) {
		Log.d(TAG, "Service [" + serviceName + "] has been created...");
	}

	public static final void logServiceDestroyed(final String serviceName) {
		Log.d(TAG, "Service [" + serviceName + "] has been destroyed...");
	}

	public static final void logStartProcessing(final String serviceName, final String url) {
		if (serviceName != null && url != null) {
			Log.d(TAG, "Service [" + serviceName + "] start processing " + url);
		}
	}

	public static final void logErrorProcessing(final String serviceName, final String url, final Exception e) {
		if (serviceName != null && url != null) {
			Log.e(TAG, 
					"Service [" + serviceName + "] has an exception during processing " + url + "\n" + e.getMessage(),
					e);
		}
	}

	public static final void logEndProcessing(final String serviceName, final String url) {
		if (serviceName != null && url != null) {
			Log.d(TAG, "Service [" + serviceName + "] end processing " + url);
		}
	}
}

package com.mobile.health.one.util;

import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class ResizeAnimation extends Animation {
	private final View mView;
	private final float mToHeight;
	private final float mFromHeight;

	private final float mToWidth;
	private final float mFromWidth;

	public ResizeAnimation(final View v, final float fromWidth, final float fromHeight,
			final float toWidth, final float toHeight) {
		mToHeight = toHeight;
		mToWidth = toWidth;
		mFromHeight = fromHeight;
		mFromWidth = fromWidth;
		mView = v;
		setDuration(300);
	}

	@Override
	protected void applyTransformation(final float interpolatedTime, final Transformation t) {
		final float height = (mToHeight - mFromHeight) * interpolatedTime
				+ mFromHeight;
		final float width = (mToWidth - mFromWidth) * interpolatedTime + mFromWidth;
		final LayoutParams p = mView.getLayoutParams();
		p.height = (int) height;
		p.width = (int) width;
		mView.requestLayout();
	}
}

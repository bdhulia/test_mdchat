package com.mobile.health.one.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.res.Resources;

import com.mobile.health.one.R;

/**
 * @author Denis Migol
 *
 */
public class AlertDialogUtil {
	private AlertDialogUtil() {
	}

	public static AlertDialog.Builder infoBuilder(final Context context) {
		return new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_info);
	}

	public static AlertDialog infoDialog(final Context context, final String title, final String message,
			final OnClickListener okListener) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(android.R.drawable.ic_dialog_info);
		builder.setCancelable(false);
		builder.setTitle(title);
		builder.setMessage(message);
		if (okListener != null) {
			builder.setPositiveButton(android.R.string.ok, okListener);
		}
		return builder.create();
	}

	public static AlertDialog infoDialog(final Context context, final String title, final String message) {
		return infoDialog(context, title, message, new OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				try {
					dialog.dismiss();
				} catch (final Exception e) {
				}
			}
		});
	}

	public static AlertDialog.Builder alertBuilder(final Context context) {
		return new AlertDialog.Builder(context).setIcon(android.R.drawable.ic_dialog_alert);
	}

	public static AlertDialog alertDialog(final Context context, final String title, final String message,
			final OnClickListener okListener, final OnClickListener cancelListener) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setIcon(R.drawable.ic_launcher);
		builder.setCancelable(false);
		builder.setTitle(title);
		builder.setMessage(message);
		if (okListener != null) {
			builder.setPositiveButton(android.R.string.ok, okListener);
		}
		if (cancelListener != null) {
			builder.setNegativeButton(android.R.string.cancel, cancelListener);
		}
		return builder.create();
	}

	public static AlertDialog alertDialog(final Context context, final int messageRes,
			final OnClickListener okListener, final OnClickListener cancelListener) {
		final Resources res = context.getResources();
		return alertDialog(context, res.getString(android.R.string.dialog_alert_title), res.getString(messageRes), okListener, cancelListener);
	}

	public static AlertDialog alertDialog(final Context context, final String title, final String message,
			final OnClickListener okListener) {
		return alertDialog(context, title, message, okListener, null);
	}

	public static AlertDialog alertDialog(final Context context, final String title, final String message) {
		return alertDialog(context, title, message, new OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				try {
					dialog.dismiss();
				} catch (final Exception e) {
				}
			}
		});
	}

	public static AlertDialog alertDialog(final Context context, final int titleResId, final int messageResId) {
		final Resources res = context.getResources();
		return alertDialog(context, res.getString(titleResId), res.getString(messageResId), new OnClickListener() {

			@Override
			public void onClick(final DialogInterface dialog, final int which) {
				try {
					dialog.dismiss();
				} catch (final Exception e) {
				}
			}
		});
	}

	public static AlertDialog errorDialog(final Context context, final String message) {
		return alertDialog(context, context.getResources().getString(android.R.string.dialog_alert_title), message);
	}

	public static AlertDialog errorDialog(final Context context, final int messageId) {
		final Resources res = context.getResources();
		return alertDialog(context, res.getString(android.R.string.dialog_alert_title), res.getString(messageId));
	}

	public static AlertDialog errorDialog(final Context context, final String message, final OnClickListener okListener) {
		return alertDialog(context, context.getResources().getString(android.R.string.dialog_alert_title), message, okListener);
	}
}

package com.mobile.health.one.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

import com.mobile.health.one.R;


/**
 * Common set of utility functions for launching apps.
 */
public class LauncherUtils {

	private static Notification getNotificationWithView(final Context context, final String msg,
			final String title, final Intent intent) {
		return getNotificationWithView(context, msg, title, intent, 0);
	}

	private static Notification getNotificationWithView(final Context context, final String msg,
			String title, final Intent intent, int icon) {
		if (icon == 0)
			icon = R.drawable.ic_launcher;
		if (title == null)
			title = context.getString(R.string.app_name);

		final Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

		final NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setTicker(msg);
		builder.setContentText(msg);
		builder.setContentTitle(title);
		builder.setSmallIcon(icon);
		builder.setSound(soundUri);
		builder.setAutoCancel(true);
		//builder.setNumber(10); // TODO: DEBUG!!
		builder.setContentIntent(PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT));

		return builder.build();
	}

	public static void generateNotification(final Context context, final String msg,
			final String title, final Intent intent) {

		final SettingsManager manager = new SettingsManager(context);
		int notificatonID = manager.getNotificationId(); // allow
														 // multiple
														 // notifications
		final NotificationManager nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		nm.notify(notificatonID, getNotificationWithView(context, msg, title, intent));

		manager.setNotificationId(++notificatonID % 32);
	}

	public static void generateNotification(final Context context, final String msg,
			final Intent intent) {
		generateNotification(context, msg, null, intent);
	}

	public static void playNotificationSound(final Context context) {
		final Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		if (uri != null) {
			final Ringtone rt = RingtoneManager.getRingtone(context, uri);
			if (rt != null) {
				rt.setStreamType(AudioManager.STREAM_NOTIFICATION);
				rt.play();
			}
		}
	}
}
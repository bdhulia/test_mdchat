package com.mobile.health.one.util;

public class Settings {
	public static final boolean RELEASE_LIVE = true;
	public final static String URI_BASE = "https://" + (RELEASE_LIVE ? "www" : "demo") + ".mdchat.com";
	//public final static String URI_BASE = "https://www.mdchat.com";
	public final static String SENDER_ID = "4424953717";
	public static final long INACTIVITY_TIME = 60 * 60 * 1000; // 3 min delay
	public static final long FILE_SIZE_LIMIT = 2 * 1024 * 1024; // 2 MB
}

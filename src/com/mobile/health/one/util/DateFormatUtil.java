package com.mobile.health.one.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateFormatUtil {

	public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd kk:mm:ss z";
	public static final String DEFAULT_DATE_FORMAT2 = "yyyy-MM-dd hh:mm a z";
	private static DateFormat mTimeFormatter;
	private static DateFormat mDateFormatter;

	public static String getFormattedDate(final String date) throws ParseException {
		return getFormattedDate(getFormattedDateEpoch(date));
	}

	public static String getFormattedDate(final String date, final String format) throws ParseException {
		return getFormattedDate(getFormattedDateEpoch(date, format));
	}

	public static Date getFormattedDateEpoch(final String date, final String format) {
		Date dateTime;
		try {
			dateTime = new SimpleDateFormat(format).parse(date);
			return dateTime;
		} catch (final ParseException e) {
			e.printStackTrace();
			return new Date();
		}
	}

	// TODO shitcode, should do somethimg with server devs :)
	public static Date getFormattedDateEpoch(final String date) {
		Date dateTime;
		try {
			dateTime = new SimpleDateFormat(DEFAULT_DATE_FORMAT).parse(date);
			return dateTime;
		} catch (final ParseException e) {
			try {
				dateTime = new SimpleDateFormat(DEFAULT_DATE_FORMAT2).parse(date);
				return dateTime;
			} catch (final ParseException e1) {
				e1.printStackTrace();
			}
			return new Date();
		}
	}

	public static String getFormattedDate(final Date dateTime) {
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateTime);
		final Calendar today = Calendar.getInstance();
		final Calendar yesterday = Calendar.getInstance();
		yesterday.add(Calendar.DATE, -1);
		if (mTimeFormatter == null)
			mTimeFormatter = new SimpleDateFormat("hh:mm a");

		if (mDateFormatter == null)
			mDateFormatter = new SimpleDateFormat("MM/dd/yyyy h:mm a");

		if (calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR)
				&& calendar.get(Calendar.DAY_OF_YEAR) == today
						.get(Calendar.DAY_OF_YEAR)) {
			return "Today " + mTimeFormatter.format(dateTime);
		} else if (calendar.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR)
				&& calendar.get(Calendar.DAY_OF_YEAR) == yesterday
						.get(Calendar.DAY_OF_YEAR)) {
			return "Yesterday " + mTimeFormatter.format(dateTime);
		} else {
			return mDateFormatter.format(dateTime);
			/*return android.text.format.DateFormat.format("MM/dd/yyyy h:mm a",
					dateTime).toString();*/
		}
	}
}
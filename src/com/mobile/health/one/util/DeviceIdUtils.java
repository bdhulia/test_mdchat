package com.mobile.health.one.util;

import android.content.Context;
import android.provider.Settings;

import java.util.UUID;

/**
 * Created by Igor on 10.01.14.
 */
public class DeviceIdUtils {
    protected static String mId;

    /**
     * Returns a unique ID for the current android device. As with all UUIDs,
     * this unique ID is "very highly likely" to be unique across all Android
     * devices. Much more so than ANDROID_ID is.
     *
     * In some rare circumstances, this ID may change. In particular, if the
     * device is factory reset a new device ID may be generated. In addition, if
     * a user upgrades their phone from certain buggy implementations of Android
     * 2.2 to a newer, non-buggy version of Android, the device ID may change.
     * Or, if a user uninstalls your app on a device that has neither a proper
     * Android ID nor a Device ID, this ID may change on reinstallation.
     *
     * Note that if the code falls back on using TelephonyManager.getDeviceId(),
     * the resulting ID will NOT change after a factory reset. Something to be
     * aware of.
     *
     * Works around a bug in Android 2.2 for many devices when using ANDROID_ID
     * directly.
     *
     * @see "http://code.google.com/p/android/issues/detail?id=10603"
     *
     * @return a ID that may be used to uniquely identify your device for most
     *         purposes.
     */
    public static String getDeviceId(Context context) {
        if (mId == null) {
            final String androidId = Settings.Secure.getString(
                    context.getContentResolver(), Settings.Secure.ANDROID_ID);
            // Use the Android ID unless it's broken, in which case
            // fallback on deviceId,
            // unless it's not available, then fallback on a random
            // number which we store to a prefs file
            if (!"9774d56d682e549c".equals(androidId)) {
                mId = UUID.randomUUID().toString();
            }

        }

        return mId;
    }
}

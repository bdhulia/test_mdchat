package com.mobile.health.one.util;

import android.text.TextUtils;

public class ContentItemUtils {
	private ContentItemUtils() {
		// static class
	}
	/**
	 * Gets the full content item name with table prefix
	 * for ex "foo.bar"; for querying more than one table
	 * 
	 * @param contentItem
	 * @return content item path + content item name
	 * 
	 * TODO: do something to omit first param with path
	 */

	public static String f(final String path, final String contentItem) {
		if (TextUtils.isEmpty(path)) {
			return contentItem;
		} else {
			return path + "." + contentItem;
		}
	}

}

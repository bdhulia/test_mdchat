package com.mobile.health.one.util;

import android.text.TextUtils;

/**
 * @author Denis Migol
 */
public class ValidationHelper {
	public static boolean validateText(final String text) {
		return !TextUtils.isEmpty(text);
	}

	public static boolean validatePhoneNumber(final String number) {
		return number.matches("\\+?\\d{1,15}");
	}
}

package com.mobile.health.one.util;

import android.database.Cursor;

public interface CursorSaver {
	public Cursor saveToCursor();
}

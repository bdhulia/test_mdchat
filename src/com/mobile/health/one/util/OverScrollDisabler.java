package com.mobile.health.one.util;

import android.view.View;

public class OverScrollDisabler {

	public static void disableOverScroll(final View view)
    {
        view.setOverScrollMode(View.OVER_SCROLL_NEVER);
    }
}

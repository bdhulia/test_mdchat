package com.mobile.health.one.util;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

public class TextCounterListener implements TextWatcher {

	private final TextView counterView;
	private int maxCount;
	private boolean inverse = false;

	public TextCounterListener(final TextView counterView) {
		if (counterView == null)
			throw new IllegalArgumentException("Null could not be passed to TextCounterListener");
		this.counterView = counterView;
	}

	public TextCounterListener(final TextView counterView, final int maxCount) {
		this(counterView);
		this.maxCount = maxCount;
		inverse = true;
		setTextValue(maxCount);
	}

	private void setTextValue(final int count) {
		int finalCount;
		if (inverse) {
			finalCount = maxCount - count;
		} else {
			finalCount = count;
		}
		counterView.setText(Integer.toString(finalCount));
	}

	@Override
	public void beforeTextChanged(final CharSequence s, final int start, final int count,
			final int after) {
		// nop
	}

	@Override
	public void onTextChanged(final CharSequence s, final int start, final int before, final int count) {
		setTextValue(s.length());
	}

	@Override
	public void afterTextChanged(final Editable s) {
		// nop
	}

}
